<?php

include_once 'models/confucio.php';
include_once 'SED.php';

class Pre_inscripcionModel extends Model{

  public function __construct()
  {
    parent::__construct();
  }
  

  public function insert($datos){
    $oferta=$datos['id_oferta_academica'];
    $sql_1 = $this->db->connect()->query("SELECT seccion.id_seccion as seccion,cupos FROM oferta_academica,seccion WHERE oferta_academica.id_seccion=seccion.id_seccion and id_oferta_academica='$oferta' ");
    $nombre=$sql_1->fetch();
    $cupos=$nombre['cupos'];
    $seccion=$nombre['seccion'];
    try{

      $pdo=$this->db->connect();
      $pdo->beginTransaction();

      $persona=$pdo->prepare('INSERT INTO persona(primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, identificacion, correo, telefono, genero, id_tipo_documento_identidad) VALUES (:primer_nombre, :segundo_nombre, :primer_apellido, :segundo_apellido, :identificacion, :correo, :telefono, :genero, :id_tipo_documento_identidad)');
      $persona->execute(['primer_nombre'=>$datos['primer_nombre'], 'segundo_nombre'=>$datos['segundo_nombre'], 'primer_apellido'=>$datos['primer_apellido'], 'segundo_apellido'=>$datos['segundo_apellido'], 'identificacion'=>$datos['identificacion'],'correo'=>$datos['correo'], 'telefono'=>$datos['telefono'], 'genero'=>$datos['genero'], 'id_tipo_documento_identidad'=>$datos['id_tipo_documento_identidad']]);

      $persona_nacimiento=$pdo->prepare('INSERT INTO persona_nacimiento(fecha_nacimiento, id_pais) VALUES (:fecha_nacimiento, :id_pais)');
      $persona_nacimiento->execute(['fecha_nacimiento'=>$datos['fecha_nacimiento'], 'id_pais'=>$datos['pais']]);

      $direccion=$pdo->prepare('INSERT INTO direccion_domicilio(direccion, telefono_habitacion, id_estado, id_etnia) VALUES (:direccion, :telefono_habitacion, :id_estado, :id_etnia)');
      $direccion->execute(['direccion'=>$datos['direccion'], 'telefono_habitacion'=>$datos['telefono_habitacion'], 'id_estado'=>$datos['estado'], 'id_etnia'=>$datos['etnia']]);

      $dato_laboral=$pdo->prepare('INSERT INTO dato_laboral(empleo, telefono_institucion, correo_institucion, direccion_institucion, inst_publica, inst_privada, organismo, inst_inv, tipo_institucion, trabajador_ubv, id_cargo, id_estado, id_profesion) VALUES (:empleo, :telefono_institucion, :correo_institucion, :direccion_institucion, :inst_publica, :inst_privada, :organismo, :inst_inv, :tipo_institucion, :trabajador_ubv, :id_cargo, :id_estado, :id_profesion)');
      $dato_laboral->execute(['empleo'=>$datos['empleo'], 'telefono_institucion'=>$datos['telefono_institucion'], 'correo_institucion'=>$datos['correo_institucion'], 'direccion_institucion'=>$datos['direccion_institucion'], 'inst_publica'=>$datos['inst_publica'], 'inst_privada'=>$datos['inst_privada'], 'organismo'=>$datos['organismo'], 'inst_inv'=>$datos['inst_inv'], 'tipo_institucion'=>$datos['tipo_institucion'], 'trabajador_ubv'=>$datos['trabajador_ubv'], 'id_cargo'=>$datos['id_cargo'], 'id_estado'=>$datos['state'], 'id_profesion'=>$datos['id_profesion']]);

      $interes=$pdo->prepare('INSERT INTO datos_interes(discapacidad, grupo_sanguineo, enfermedad, descripcion_enfermedad, tratamiento, idioma, idiomas_descripcion, estudiante_ubv, mision_sucre, id_tipo_discapacidad, institucion_nivel) VALUES (:discapacidad, :grupo_sanguineo, :enfermedad, :descripcion_enfermedad, :tratamiento, :idioma, :idiomas_descripcion, :estudiante_ubv, :mision_sucre, :id_tipo_discapacidad, :institucion_nivel)');  
      $interes->execute(['discapacidad'=>$datos['discapacidad'], 'grupo_sanguineo'=>$datos['grupo_sanguineo'], 'enfermedad'=>$datos['enfermedad'], 'descripcion_enfermedad'=>$datos['descripcion_enfermedad'], 'tratamiento'=>$datos['tratamiento'], 'idioma'=>$datos['idioma'], 'idiomas_descripcion'=>$datos['descripcion'], 'estudiante_ubv'=>$datos['estudiante_ubv'], 'mision_sucre'=>$datos['mision_sucre'], 'id_tipo_discapacidad'=>$datos['id_tipo_discapacidad'], 'institucion_nivel'=>$datos['institucion_nivel']]);

      $aspirante=$pdo->prepare('INSERT INTO aspirante(estatus, id_persona_tipo, id_persona, id_direccion, id_nacimiento, id_datos_interes, id_dato_laboral, id_nivel_academico, id_civil, id_oferta_academica) VALUES (:estatus, :id_persona_tipo, (SELECT id_persona FROM persona ORDER BY id_persona DESC LIMIT 1), (SELECT id_direccion FROM direccion_domicilio ORDER BY id_direccion DESC LIMIT 1), (SELECT id_nacimiento FROM persona_nacimiento ORDER BY id_nacimiento DESC LIMIT 1), (SELECT id_datos_interes FROM datos_interes ORDER BY id_datos_interes DESC LIMIT 1), (SELECT id_dato_laboral FROM dato_laboral ORDER BY id_dato_laboral DESC LIMIT 1), :id_nivel_academico, :id_civil, :id_oferta_academica)');
      $aspirante->execute(['estatus'=>'0', 'id_persona_tipo'=>'2', 'id_nivel_academico'=>$datos['id_nivel_academico'], 'id_civil'=>$datos['id_civil'], 'id_oferta_academica'=>$datos['id_oferta_academica']]);
      
      $resultados= $cupos - 1;
      
      $query=$pdo->query("UPDATE seccion SET cupos='$resultados' WHERE id_seccion='$seccion' ");

      

      $pdo->commit();
      return true;

      

    }catch(PDOException $e ){

      $pdo->rollBack();
      return false;

    }


  }


  public function insertInscripcion($datos){

    try{

      $pdo=$this->db->connect();

      $pdo->beginTransaction();

      $inscripcion=$pdo->prepare("INSERT INTO inscripcion(estatus, id_aspirante, id_oferta_academica, id_persona_tipo, id_deposito) 
        VALUES(
        :estatus, 
        :id_aspirante,
        :id_oferta_academica,
        :id_persona_tipo,
        :id_deposito)");
      $inscripcion->execute([
        'estatus'=>'1',
        'id_aspirante'=>$datos['id_aspirante'],
        'id_oferta_academica'=>$datos['id_oferta_academica'],
        'id_persona_tipo'=>1,
        'id_deposito'=>$datos['id_deposito']
      ]);

      $query=$pdo->prepare("UPDATE aspirante SET estatus=:estatus WHERE id_aspirante=:id_aspirante");
      $query->execute(['estatus'=>'1', 'id_aspirante'=>$datos['id_aspirante']]);

      $crypt= new SED();
      $clave=$crypt->encryption("icubv$".$datos['identificacion']);
      
      $usuario=$pdo->prepare('INSERT INTO usuario(usuario, clave, fecha, estatus, id_perfil, id_persona) VALUES (:usuario, :clave, :fecha, :estatus, :id_perfil, :id_persona)');
      $usuario->execute(['usuario'=>'icubv'.$datos['identificacion'], 'clave'=>$clave, 'fecha'=>date("Y-m-d"), 'estatus'=>'Activo', 'id_perfil'=>'4', 'id_persona'=>$datos['id_persona']]);

      $pdo->commit();

      return true;

    }catch(PDOException $e ){

      $pdo->rollBack();
      return false;

    }

  }


  public function insertDeposito($datos){

    try{

      $pdo=$this->db->connect();
      $pdo->beginTransaction();


      $deposito=$pdo->prepare('INSERT INTO deposito(numero_vouche, exonerado, modalidad_exo, monto, fecha_deposito, observacion, numero_vouche_50, monto_50, fecha_deposito_50, observacion_50, id_aspirante) VALUES (:numero_vouche, :exonerado, :modalidad_exo, :monto, :fecha_deposito, :observacion, :numero_vouche_50, :monto_50, :fecha_deposito_50, :observacion_50, :id_aspirante)');
      $deposito->execute(['numero_vouche'=>$datos['numero_vouche'], 'exonerado'=>$datos['exonerado'], 'modalidad_exo'=>$datos['modalidad_exo'], 'monto'=>$datos['monto'], 'fecha_deposito'=>$datos['fecha_deposito'], 'observacion'=>$datos['observacion'], 'numero_vouche_50'=>$datos['numero_vouche_50'], 'monto_50'=>$datos['monto_50'], 'fecha_deposito_50'=>$datos['fecha_deposito_50'], 'observacion_50'=>$datos['observacion_50'], 'id_aspirante'=>$datos['id_aspirante']]);

      $pdo->commit();
      return true;

    }catch(PDOException $e){

      $pdo->rollBack();
      return false;

    }

  }


///////////////////////////////// CONSULTAS ///////////////////////////////////////////


public function ExistenteInscrito($id_aspirante){
  try{


      $sql = $this->db->connect()->prepare("SELECT id_aspirante FROM inscripcion WHERE inscripcion.id_aspirante=:id_aspirante");
      $sql->execute(['id_aspirante' =>$id_aspirante]);
      $nombre=$sql->fetch();
      
      if($id_aspirante==$nombre['id_aspirante'] ){
          
          return $nombre['id_aspirante'];
      } 

      return false;
  } catch(PDOException $e){
      return false;
  }
}



  public function Existente($identificacion){
    try{


      $sql=$this->db->connect()->prepare("SELECT identificacion FROM persona WHERE identificacion=:identificacion");
      $sql->execute(['identificacion'=>$identificacion]);
      $nombre=$sql->fetch();
      
      if($identificacion==$nombre['identificacion'] ){
        
        return $nombre['identificacion'];
      } 

      return false;
    } catch(PDOException $e){
      return false;
    }
  }



  public function getCategoria($datos){

    $oferta=$datos['id_oferta_academica'];
    $sql_1 = $this->db->connect()->query("SELECT seccion.id_seccion as seccion,cupos FROM oferta_academica,seccion WHERE oferta_academica.id_seccion=seccion.id_seccion and id_oferta_academica='$oferta' ");
    $nombre=$sql_1->fetch();
    $cupos=$nombre['cupos'];
    $seccion=$nombre['seccion'];
    try{

      $query=$this->db->connect()->prepare("SELECT persona.identificacion, actividad.categoria
        FROM aspirante, persona, oferta_academica, seccion, actividad 
        WHERE aspirante.id_persona=persona.id_persona
        AND aspirante.id_oferta_academica=oferta_academica.id_oferta_academica 
        AND oferta_academica.id_seccion=seccion.id_seccion
        AND seccion.id_actividad=actividad.id_actividad
        AND identificacion=:identificacion
        AND actividad.categoria=:categoria");
      $query->execute([
        'identificacion'=> $datos['identificacion'], 
        'categoria'=>$datos['categoria']
      ]);
      $var=$query->fetch(PDO::FETCH_ASSOC);

      //validar coincidencia de datos de entrada con los de bbdd
      if($var['identificacion']==$datos['identificacion'] && $var['categoria'] == $datos['categoria']){

        return true;

      }else{

        $query=$this->db->connect()->prepare("SELECT aspirante.id_persona, persona.identificacion, aspirante.id_direccion, aspirante.id_nacimiento, aspirante.id_datos_interes, aspirante.id_dato_laboral, aspirante.id_nivel_academico, aspirante.id_civil,
          aspirante.id_persona_tipo, aspirante.estatus
          FROM aspirante, persona, direccion_domicilio, dato_laboral, 
          datos_interes, persona_nacimiento, nivel_academico, estado_civil, oferta_academica
          WHERE aspirante.id_persona=persona.id_persona 
          AND aspirante.id_nacimiento=persona_nacimiento.id_nacimiento
          AND aspirante.id_direccion=direccion_domicilio.id_direccion
          AND aspirante.id_datos_interes=datos_interes.id_datos_interes 
          AND aspirante.id_oferta_academica=oferta_academica.id_oferta_academica  
          AND estado_civil.id_civil=aspirante.id_civil
          AND aspirante.id_nivel_academico=nivel_academico.id_nivel_academico
          AND persona.id_persona=persona.id_persona
          AND persona.identificacion=:identificacion");
        $query->execute(['identificacion'=>$datos['identificacion']]);

        $var=$query->fetch(PDO::FETCH_ASSOC);
        

        if($var['identificacion']==''){
          ?>
          <script>
            alert('Este dato no se encuentra registrado. Debe realizar su Pre-Inscripcion');
            location.href='<?php echo constant ('URL')."main";?>';
          </script>
          <?php 
          return true;
          exit();  
        } 

        $aspirante=$this->db->connect()->prepare('INSERT INTO aspirante(estatus, id_persona_tipo, id_persona, id_direccion, id_nacimiento, id_datos_interes, id_dato_laboral, id_nivel_academico, id_civil, id_oferta_academica)
          VALUES (:estatus, :id_persona_tipo, :id_persona, :id_direccion, :id_nacimiento, :id_datos_interes, :id_dato_laboral, :id_nivel_academico, :id_civil, :id_oferta_academica)');
        $aspirante->execute(['estatus'=>'0',
         'id_persona_tipo'=>2, 
         'id_persona'=>$var['id_persona'],
         'id_direccion'=>$var['id_direccion'],
         'id_nacimiento'=>$var['id_nacimiento'], 
         'id_datos_interes'=>$var['id_datos_interes'], 
         'id_dato_laboral'=>$var['id_dato_laboral'], 
         'id_nivel_academico'=>$var['id_nivel_academico'], 
         'id_civil'=>$var['id_civil'], 
         'id_oferta_academica'=>$datos['id_oferta_academica']]);

        $resultados= $cupos - 1;
        
        $query=$this->db->connect()->query("UPDATE seccion SET cupos='$resultados' WHERE id_seccion='$seccion' ");

        
        return false;
      }

    } catch(PDOException $e){
      return false;
    }

  }





  public function get(){

    $items=[];

    try{

      $query=$this->db->connect()->query("SELECT primer_nombre, primer_apellido, identificacion, persona_tipo.descripcion AS persona_tipo, 
        aspirante.id_aspirante, aspirante.id_persona, aspirante.id_persona_tipo, aspirante.estatus AS aspirante,
        persona_nacimiento.id_nacimiento, direccion_domicilio.id_direccion, dato_laboral.id_dato_laboral, datos_interes.id_datos_interes,
        persona.id_persona
        FROM aspirante, persona, persona_tipo, persona_nacimiento, direccion_domicilio, dato_laboral, datos_interes 
        WHERE aspirante.id_persona=persona.id_persona 
        AND aspirante.id_persona_tipo=persona_tipo.id_persona_tipo 
        AND aspirante.id_aspirante=aspirante.id_aspirante 
        AND aspirante.id_nacimiento=persona_nacimiento.id_nacimiento
        AND aspirante.id_datos_interes=datos_interes.id_datos_interes 
        AND aspirante.id_direccion=direccion_domicilio.id_direccion
        AND aspirante.id_dato_laboral=dato_laboral.id_dato_laboral  ");
      while($row=$query->fetch()){

        $item=new Confucio();
        $item->id_aspirante=$row['id_aspirante'];
        $item->id_persona=$row['id_persona'];
        $item->id_persona_tipo=$row['id_persona_tipo'];
        $item->primer_nombre=$row['primer_nombre'];
        $item->primer_apellido=$row['primer_apellido'];
        $item->identificacion=$row['identificacion'];
        $item->persona_tipo=$row['persona_tipo'];
        $item->aspirante=$row['aspirante'];

        array_push($items,$item);
      }

      return $items;

    }catch(PDOException $e){

      return false;
    }

  }



  public function getCV($id_oferta_academica){

    try{
      $query=$this->db->connect()->prepare("SELECT id_oferta_academica, actividad.categoria 
        FROM oferta_academica, seccion, actividad 
        WHERE oferta_academica.id_seccion=seccion.id_seccion
        AND seccion.id_actividad=actividad.id_actividad
        AND id_oferta_academica=:id_oferta_academica");
      $query->execute(['id_oferta_academica'=>$id_oferta_academica]);
      $item=new Confucio();

      
      while($row=$query->fetch()){

        $item->id_oferta_academica=$row['id_oferta_academica'];
        $item->categoria=$row['categoria'];
      }
      
      return $item;
      
    }catch(PDOException $e){
      return false;
    }
    
  }


  public function getbyAspirante($id_aspirante){

    try{
      $query=$this->db->connect()->prepare("SELECT aspirante.id_aspirante, aspirante.id_persona AS persona, primer_nombre, segundo_nombre, primer_apellido, 
        segundo_apellido, identificacion, correo, telefono, genero, tipo_documento_identidad.descripcion AS tipo_documento,
        fecha_nacimiento, pais.descripcion AS pais, direccion, telefono_habitacion, etnia.descripcion AS etnia, estado_civil.descripcion as civil,
        empleo, telefono_institucion, correo_institucion, direccion_institucion, tipo_institucion, inst_publica, inst_privada, organismo, inst_inv, cargo.descripcion as cargo, trabajador_ubv,
        discapacidad, grupo_sanguineo, enfermedad, descripcion_enfermedad, tratamiento, idioma, idiomas_descripcion, tipo_discapacidad.descripcion AS tipo, nivel_academico.descripcion as nivel_aca,
        estado.descripcion AS estado, estudiante_ubv, mision_sucre, institucion_nivel, aspirante.id_oferta_academica, seccion.descripcion as oferta, modalidad.descripcion AS modalidad, turno.descripcion AS turno, hora_inicio, 
        hora_fin, aspirante.estatus, deposito.id_deposito, deposito.exonerado, modalidad_exo, numero_vouche, monto, fecha_deposito, deposito.observacion, numero_vouche_50, monto_50, fecha_deposito_50, deposito.observacion_50 as observacion_50,
        profesion.descripcion AS profesion
        FROM aspirante, persona, tipo_documento_identidad, persona_nacimiento, direccion_domicilio, etnia, dato_laboral, datos_interes, tipo_discapacidad, pais, estado, seccion, oferta_academica, modalidad, turno, horario, deposito, cargo, estado_civil, nivel_academico, profesion
        WHERE aspirante.id_persona=persona.id_persona 
        AND persona.id_tipo_documento_identidad=tipo_documento_identidad.id_tipo_documento_identidad 
        AND aspirante.id_nacimiento=persona_nacimiento.id_nacimiento
        AND persona_nacimiento.id_pais=pais.id_pais
        AND direccion_domicilio.id_etnia=etnia.id_etnia 
        AND direccion_domicilio.id_estado=estado.id_estado
        AND aspirante.id_direccion=direccion_domicilio.id_direccion
        AND aspirante.id_dato_laboral=dato_laboral.id_dato_laboral 
        AND datos_interes.id_tipo_discapacidad=tipo_discapacidad.id_tipo_discapacidad 
        AND aspirante.id_datos_interes=datos_interes.id_datos_interes 
        AND aspirante.id_oferta_academica=oferta_academica.id_oferta_academica  
        AND oferta_academica.id_seccion=seccion.id_seccion
        AND seccion.id_modalidad=modalidad.id_modalidad
        AND horario.id_turno=turno.id_turno
        AND horario.id_seccion=seccion.id_seccion
        AND estado_civil.id_civil=aspirante.id_civil
        AND cargo.id_cargo=dato_laboral.id_cargo
        AND deposito.id_aspirante=aspirante.id_aspirante
        AND deposito.id_aspirante=aspirante.id_aspirante
        AND profesion.id_profesion=dato_laboral.id_profesion
        AND aspirante.id_nivel_academico=nivel_academico.id_nivel_academico
        AND aspirante.id_aspirante=:id_aspirante");
      $query->execute(['id_aspirante'=>$id_aspirante]);
      $item=new Confucio();

      

      while($row=$query->fetch()){

        $item->id_aspirante=$row['id_aspirante'];
        $item->persona=$row['persona'];
        $item->pais=$row['pais'];
        $item->estado=$row['estado'];
        $item->primer_nombre=$row['primer_nombre'];
        $item->segundo_nombre=$row['segundo_nombre'];
        $item->primer_apellido=$row['primer_apellido'];
        $item->segundo_apellido=$row['segundo_apellido'];
        $item->identificacion=$row['identificacion'];
        $item->correo=$row['correo'];
        $item->telefono=$row['telefono'];
        $item->genero=$row['genero'];
        $item->tipo_documento=$row['tipo_documento'];
        $item->fecha_nacimiento=$row['fecha_nacimiento'];
        $item->direccion=$row['direccion'];
        $item->civil=$row['civil'];
        
        $item->telefono_habitacion=$row['telefono_habitacion'];
        $item->etnia=$row['etnia'];
        $item->empleo=$row['empleo'];
        $item->telefono_institucion=$row['telefono_institucion'];
        $item->correo_institucion=$row['correo_institucion'];
        $item->direccion_institucion=$row['direccion_institucion'];
        $item->inst_publica=$row['inst_publica'];
        $item->inst_privada=$row['inst_privada'];
        $item->organismo=$row['organismo'];
        $item->inst_inv=$row['inst_inv'];
        $item->cargo=$row['cargo'];
        $item->tipo_institucion=$row['tipo_institucion'];
        $item->trabajador_ubv=$row['trabajador_ubv'];
        $item->discapacidad=$row['discapacidad'];
        $item->grupo_sanguineo=$row['grupo_sanguineo'];
        $item->enfermedad=$row['enfermedad'];
        $item->descripcion_enfermedad=$row['descripcion_enfermedad'];
        $item->tratamiento=$row['tratamiento'];
        $item->idioma=$row['idioma'];
        $item->idiomas_descripcion=$row['idiomas_descripcion'];
        $item->tipo=$row['tipo'];
        $item->estudiante_ubv=$row['estudiante_ubv'];
        $item->mision_sucre=$row['mision_sucre'];
        $item->institucion_nivel=$row['institucion_nivel'];
        $item->nivel_aca=$row['nivel_aca'];

        $item->id_oferta_academica=$row['id_oferta_academica'];

        $item->oferta=$row['oferta'];
        $item->modalidad=$row['modalidad'];
        $item->turno=$row['turno'];
        $item->hora_inicio=$row['hora_inicio'];
        $item->hora_fin=$row['hora_fin'];
        $item->estatus=$row['estatus'];
        $item->profesion=$row['profesion'];

        $item->id_deposito=$row['id_deposito'];
        $item->exonerado=$row['exonerado'];
        $item->modalidad_exo=$row['modalidad_exo'];
        $item->numero_vouche=$row['numero_vouche'];
        $item->monto=$row['monto'];
        $item->fecha_deposito=$row['fecha_deposito'];
        $item->observacion=$row['observacion'];
        $item->numero_vouche_50=$row['numero_vouche_50'];
        $item->monto_50=$row['monto_50'];
        $item->fecha_deposito_50=$row['fecha_deposito_50'];
        $item->observacion_50=$row['observacion_50'];

      }
      
      return $item;
      
    }catch(PDOException $e){
      return false;
    }
    
  }


  public function getbyAspiranteDetalle($id_aspirante){

    try{
      $query=$this->db->connect()->prepare("SELECT aspirante.id_aspirante, aspirante.id_persona AS persona, primer_nombre, segundo_nombre, primer_apellido, 
        segundo_apellido, identificacion, correo, telefono, genero, tipo_documento_identidad.descripcion AS tipo_documento,
        fecha_nacimiento, pais.descripcion AS pais, direccion, telefono_habitacion, etnia.descripcion AS etnia, estado_civil.descripcion as civil,
        empleo, telefono_institucion, correo_institucion, direccion_institucion, tipo_institucion, inst_publica, inst_privada, organismo, inst_inv, cargo.descripcion as cargo, trabajador_ubv,
        discapacidad, grupo_sanguineo, enfermedad, descripcion_enfermedad, tratamiento, idioma, idiomas_descripcion, tipo_discapacidad.descripcion AS tipo, nivel_academico.descripcion as nivel_aca,
        estado.descripcion AS estado, estudiante_ubv, mision_sucre, institucion_nivel, aspirante.id_oferta_academica, seccion.descripcion as oferta, modalidad.descripcion AS modalidad, turno.descripcion AS turno, hora_inicio, 
        hora_fin, aspirante.estatus, seccion.id_seccion
        FROM aspirante, persona, tipo_documento_identidad, persona_nacimiento, direccion_domicilio, etnia, dato_laboral, datos_interes, tipo_discapacidad, pais, estado, seccion, oferta_academica, modalidad, turno, horario, cargo, estado_civil, nivel_academico
        WHERE aspirante.id_persona=persona.id_persona 
        AND persona.id_tipo_documento_identidad=tipo_documento_identidad.id_tipo_documento_identidad 
        AND aspirante.id_nacimiento=persona_nacimiento.id_nacimiento
        AND persona_nacimiento.id_pais=pais.id_pais
        AND direccion_domicilio.id_etnia=etnia.id_etnia 
        AND direccion_domicilio.id_estado=estado.id_estado
        AND aspirante.id_direccion=direccion_domicilio.id_direccion
        AND aspirante.id_dato_laboral=dato_laboral.id_dato_laboral 
        AND datos_interes.id_tipo_discapacidad=tipo_discapacidad.id_tipo_discapacidad 
        AND aspirante.id_datos_interes=datos_interes.id_datos_interes 
        AND aspirante.id_oferta_academica=oferta_academica.id_oferta_academica  
        AND oferta_academica.id_seccion=seccion.id_seccion
        AND seccion.id_modalidad=modalidad.id_modalidad
        AND horario.id_turno=turno.id_turno
        AND horario.id_seccion=seccion.id_seccion
        AND estado_civil.id_civil=aspirante.id_civil
        AND cargo.id_cargo=dato_laboral.id_cargo
        AND aspirante.id_nivel_academico=nivel_academico.id_nivel_academico
        AND aspirante.id_aspirante=:id_aspirante");
      $query->execute(['id_aspirante'=>$id_aspirante]);
      $item=new Confucio();

      

      while($row=$query->fetch()){

        $item->id_aspirante=$row['id_aspirante'];
        $item->persona=$row['persona'];
        $item->pais=$row['pais'];
        $item->estado=$row['estado'];
        $item->primer_nombre=$row['primer_nombre'];
        $item->segundo_nombre=$row['segundo_nombre'];
        $item->primer_apellido=$row['primer_apellido'];
        $item->segundo_apellido=$row['segundo_apellido'];
        $item->identificacion=$row['identificacion'];
        $item->correo=$row['correo'];
        $item->telefono=$row['telefono'];
        $item->genero=$row['genero'];
        $item->tipo_documento=$row['tipo_documento'];
        $item->fecha_nacimiento=$row['fecha_nacimiento'];
        $item->direccion=$row['direccion'];
        $item->civil=$row['civil'];
        
        $item->telefono_habitacion=$row['telefono_habitacion'];
        $item->etnia=$row['etnia'];
        $item->empleo=$row['empleo'];
        $item->telefono_institucion=$row['telefono_institucion'];
        $item->correo_institucion=$row['correo_institucion'];
        $item->direccion_institucion=$row['direccion_institucion'];
        $item->inst_publica=$row['inst_publica'];
        $item->inst_privada=$row['inst_privada'];
        $item->organismo=$row['organismo'];
        $item->inst_inv=$row['inst_inv'];
        $item->cargo=$row['cargo'];
        $item->tipo_institucion=$row['tipo_institucion'];
        $item->trabajador_ubv=$row['trabajador_ubv'];
        $item->discapacidad=$row['discapacidad'];
        $item->grupo_sanguineo=$row['grupo_sanguineo'];
        $item->enfermedad=$row['enfermedad'];
        $item->descripcion_enfermedad=$row['descripcion_enfermedad'];
        $item->tratamiento=$row['tratamiento'];
        $item->idioma=$row['idioma'];
        $item->idiomas_descripcion=$row['idiomas_descripcion'];
        $item->tipo=$row['tipo'];
        $item->estudiante_ubv=$row['estudiante_ubv'];
        $item->mision_sucre=$row['mision_sucre'];
        $item->institucion_nivel=$row['institucion_nivel'];
        $item->nivel_aca=$row['nivel_aca'];

        $item->id_oferta_academica=$row['id_oferta_academica'];

        $item->id_seccion=$row['id_seccion'];
        $item->oferta=$row['oferta'];
        $item->modalidad=$row['modalidad'];
        $item->turno=$row['turno'];
        $item->hora_inicio=$row['hora_inicio'];
        $item->hora_fin=$row['hora_fin'];
        $item->estatus=$row['estatus'];

      }
      
      return $item;
      
    }catch(PDOException $e){
      return false;
    }
    
  }



  public function getbyEstudianteDetalle($id_inscripcion){

    try{
      $query=$this->db->connect()->prepare("SELECT inscripcion.id_inscripcion, aspirante.id_aspirante, aspirante.id_persona AS persona, primer_nombre, segundo_nombre, primer_apellido, 
        segundo_apellido, identificacion, correo, telefono, genero, tipo_documento_identidad.descripcion AS tipo_documento, seccion.id_seccion,
        fecha_nacimiento, pais.descripcion AS pais, direccion, telefono_habitacion, etnia.descripcion AS etnia, estado_civil.descripcion as civil,
        empleo, telefono_institucion, correo_institucion, direccion_institucion, tipo_institucion, inst_publica, inst_privada, organismo, inst_inv, cargo.descripcion as cargo, trabajador_ubv,
        discapacidad, grupo_sanguineo, enfermedad, descripcion_enfermedad, tratamiento, idioma, idiomas_descripcion, tipo_discapacidad.descripcion AS tipo, nivel_academico.descripcion as nivel_aca,
        estado.descripcion AS estado, estudiante_ubv, mision_sucre, institucion_nivel, aspirante.id_oferta_academica, seccion.descripcion as oferta, modalidad.descripcion AS modalidad, turno.descripcion AS turno, hora_inicio, 
        hora_fin, aspirante.estatus, deposito.id_deposito, deposito.exonerado, modalidad_exo, numero_vouche, monto, fecha_deposito, deposito.observacion, numero_vouche_50, monto_50, fecha_deposito_50, deposito.observacion_50 as observacion_50 
        FROM inscripcion, aspirante, persona, tipo_documento_identidad, persona_nacimiento, direccion_domicilio, etnia, dato_laboral, datos_interes, tipo_discapacidad, pais, estado, seccion, oferta_academica, modalidad, turno, horario, deposito, cargo, estado_civil, nivel_academico
        WHERE aspirante.id_persona=persona.id_persona 
        AND persona.id_tipo_documento_identidad=tipo_documento_identidad.id_tipo_documento_identidad 
        AND aspirante.id_nacimiento=persona_nacimiento.id_nacimiento
        AND persona_nacimiento.id_pais=pais.id_pais
        AND direccion_domicilio.id_etnia=etnia.id_etnia 
        AND direccion_domicilio.id_estado=estado.id_estado
        AND aspirante.id_direccion=direccion_domicilio.id_direccion
        AND aspirante.id_dato_laboral=dato_laboral.id_dato_laboral 
        AND datos_interes.id_tipo_discapacidad=tipo_discapacidad.id_tipo_discapacidad 
        AND aspirante.id_datos_interes=datos_interes.id_datos_interes 
        AND aspirante.id_oferta_academica=oferta_academica.id_oferta_academica  
        AND inscripcion.id_oferta_academica=oferta_academica.id_oferta_academica 
        AND oferta_academica.id_seccion=seccion.id_seccion
        AND seccion.id_modalidad=modalidad.id_modalidad
        AND horario.id_turno=turno.id_turno
        AND horario.id_seccion=seccion.id_seccion
        AND estado_civil.id_civil=aspirante.id_civil
        AND cargo.id_cargo=dato_laboral.id_cargo
        AND deposito.id_aspirante=aspirante.id_aspirante
        AND aspirante.id_nivel_academico=nivel_academico.id_nivel_academico
        AND inscripcion.id_aspirante=aspirante.id_aspirante
        AND inscripcion.id_inscripcion=:id_inscripcion
        AND inscripcion.estatus='1'");
      $query->execute(['id_inscripcion'=>$id_inscripcion]);
      $item=new Confucio();

      
      while($row=$query->fetch()){

        $item->id_inscripcion=$row['id_inscripcion'];
        $item->persona=$row['persona'];
        $item->pais=$row['pais'];
        $item->estado=$row['estado'];
        $item->primer_nombre=$row['primer_nombre'];
        $item->segundo_nombre=$row['segundo_nombre'];
        $item->primer_apellido=$row['primer_apellido'];
        $item->segundo_apellido=$row['segundo_apellido'];
        $item->identificacion=$row['identificacion'];
        $item->correo=$row['correo'];
        $item->telefono=$row['telefono'];
        $item->genero=$row['genero'];
        $item->tipo_documento=$row['tipo_documento'];
        $item->fecha_nacimiento=$row['fecha_nacimiento'];
        $item->direccion=$row['direccion'];
        $item->civil=$row['civil'];
        
        $item->telefono_habitacion=$row['telefono_habitacion'];
        $item->etnia=$row['etnia'];
        $item->empleo=$row['empleo'];
        $item->telefono_institucion=$row['telefono_institucion'];
        $item->correo_institucion=$row['correo_institucion'];
        $item->direccion_institucion=$row['direccion_institucion'];
        $item->inst_publica=$row['inst_publica'];
        $item->inst_privada=$row['inst_privada'];
        $item->organismo=$row['organismo'];
        $item->inst_inv=$row['inst_inv'];
        $item->cargo=$row['cargo'];
        $item->tipo_institucion=$row['tipo_institucion'];
        $item->trabajador_ubv=$row['trabajador_ubv'];
        $item->discapacidad=$row['discapacidad'];
        $item->grupo_sanguineo=$row['grupo_sanguineo'];
        $item->enfermedad=$row['enfermedad'];
        $item->descripcion_enfermedad=$row['descripcion_enfermedad'];
        $item->tratamiento=$row['tratamiento'];
        $item->idioma=$row['idioma'];
        $item->idiomas_descripcion=$row['idiomas_descripcion'];
        $item->tipo=$row['tipo'];
        $item->estudiante_ubv=$row['estudiante_ubv'];
        $item->mision_sucre=$row['mision_sucre'];
        $item->institucion_nivel=$row['institucion_nivel'];
        $item->nivel_aca=$row['nivel_aca'];

        $item->id_oferta_academica=$row['id_oferta_academica'];
        $item->id_seccion=$row['id_seccion'];
        $item->oferta=$row['oferta'];
        $item->modalidad=$row['modalidad'];
        $item->turno=$row['turno'];
        $item->hora_inicio=$row['hora_inicio'];
        $item->hora_fin=$row['hora_fin'];
        $item->estatus=$row['estatus'];

        $item->id_aspirante=$row['id_aspirante'];
        $item->id_deposito=$row['id_deposito'];
        $item->exonerado=$row['exonerado'];
        $item->modalidad_exo=$row['modalidad_exo'];
        $item->numero_vouche=$row['numero_vouche'];
        $item->monto=$row['monto'];
        $item->fecha_deposito=$row['fecha_deposito'];
        $item->observacion=$row['observacion'];
        $item->numero_vouche_50=$row['numero_vouche_50'];
        $item->monto_50=$row['monto_50'];
        $item->fecha_deposito_50=$row['fecha_deposito_50'];
        $item->observacion_50=$row['observacion_50'];

      }
      
      return $item;
      
    }catch(PDOException $e){
      return false;
    }
    
  }




  public function getConstanciadeEstudio($id_inscripcion){

    try{
      $query=$this->db->connect()->prepare("SELECT inscripcion.id_inscripcion, aspirante.id_aspirante, aspirante.id_persona AS persona, primer_nombre, segundo_nombre, primer_apellido, 
        segundo_apellido, identificacion, correo, telefono, genero, tipo_documento_identidad.descripcion AS tipo_documento,
        aspirante.id_oferta_academica, actividad.descripcion as oferta, modalidad.descripcion AS modalidad, turno.descripcion AS turno, hora_inicio, 
        hora_fin, inscripcion.estatus, nivel.descripcion as nivel, periodo.descripcion as periodo, current_date as fecha
        FROM inscripcion, aspirante, persona, tipo_documento_identidad, seccion, actividad, oferta_academica, modalidad, turno, horario, nivel, periodo
        WHERE aspirante.id_persona=persona.id_persona 
        AND persona.id_tipo_documento_identidad=tipo_documento_identidad.id_tipo_documento_identidad 
        AND inscripcion.id_oferta_academica=oferta_academica.id_oferta_academica  
        AND oferta_academica.id_seccion=seccion.id_seccion
        AND seccion.id_modalidad=modalidad.id_modalidad
        AND seccion.id_nivel=nivel.id_nivel
        AND seccion.id_actividad=actividad.id_actividad
        AND horario.id_turno=turno.id_turno
        AND horario.id_seccion=seccion.id_seccion
        AND inscripcion.id_aspirante=aspirante.id_aspirante
        AND seccion.id_periodo=periodo.id_periodo
        AND inscripcion.id_inscripcion=:id_inscripcion
        AND inscripcion.estatus='1'");
      $query->execute(['id_inscripcion'=>$id_inscripcion]);
      $item=new Confucio();

      
      while($row=$query->fetch()){

        $item->id_aspirante=$row['id_aspirante'];
        $item->primer_nombre=$row['primer_nombre'];
        $item->segundo_nombre=$row['segundo_nombre'];
        $item->primer_apellido=$row['primer_apellido'];
        $item->segundo_apellido=$row['segundo_apellido'];
        $item->identificacion=$row['identificacion'];
        $item->correo=$row['correo'];
        $item->telefono=$row['telefono'];
        $item->genero=$row['genero'];
        $item->tipo_documento=$row['tipo_documento'];
        $item->fecha=$row['fecha'];
        
        $item->id_oferta_academica=$row['id_oferta_academica'];
        $item->oferta=$row['oferta'];
        $item->modalidad=$row['modalidad'];
        $item->turno=$row['turno'];
        $item->hora_inicio=$row['hora_inicio'];
        $item->hora_fin=$row['hora_fin'];
        $item->nivel=$row['nivel'];
        $item->periodo=$row['periodo'];
        $item->estatus=$row['estatus'];

      }
      
      return $item;
      
    }catch(PDOException $e){
      return false;
    }
    
  }

  public function getDiaConstancia($id_inscripcion){

    $items=[];
    try{
      $query=$this->db->connect()->query("SELECT inscripcion.id_inscripcion, dia.descripcion as dia
        FROM inscripcion, aspirante, persona, seccion, oferta_academica, horario,dia, horario_dia
        WHERE aspirante.id_persona=persona.id_persona 
        AND inscripcion.id_oferta_academica=oferta_academica.id_oferta_academica  
        AND oferta_academica.id_seccion=seccion.id_seccion
        AND horario.id_seccion=seccion.id_seccion
        AND horario_dia.id_dia=dia.id_dia
        AND horario_dia.id_horario=horario.id_horario
        AND inscripcion.id_aspirante=aspirante.id_aspirante
        AND inscripcion.id_inscripcion=:id_inscripcion
        AND inscripcion.estatus='1'");
      $query->execute(['id_inscripcion'=>$id_inscripcion]);

      while($row=$query->fetch()){

        $item=new Confucio();
        $item->id_inscripcion=$row['id_inscripcion'];
        $item->dia=$row['dia'];
        array_push($items,$item);
        
      }
      return $items;
      
    }catch(PDOException $e){
      return[];
    }
  }



  public function getDeposito(){

    $items=[];
    try{
      $query=$this->db->connect()->query("SELECT id_inscripcion, primer_nombre, primer_apellido, identificacion
        FROM inscripcion, persona, aspirante WHERE aspirante.id_persona=persona.id_persona AND aspirante.id_aspirante=inscripcion.id_aspirante AND inscripcion.id_inscripcion=inscripcion.id_inscripcion");
      while($row=$query->fetch()){

        $item=new Confucio();
        $item->id_inscripcion=$row['id_inscripcion'];
        $item->primer_nombre=$row['primer_nombre'];
        $item->primer_apellido=$row['primer_apellido'];
        $item->identificacion=$row['identificacion'];
        array_push($items,$item);
        
      }
      return $items;
      
    }catch(PDOException $e){
      return[];
    }
  }


  public function getInscripcion(){

    $items=[];

    try{

      $query=$this->db->connect()->query("SELECT persona.id_persona, id_inscripcion,primer_nombre, primer_apellido, identificacion, correo, persona_tipo.descripcion AS tipo, inscripcion.id_oferta_academica, seccion.descripcion as oferta
        FROM inscripcion, persona, persona_tipo, aspirante, seccion, oferta_academica WHERE aspirante.id_persona=persona.id_persona AND inscripcion.id_persona_tipo=persona_tipo.id_persona_tipo AND inscripcion.id_inscripcion=inscripcion.id_inscripcion
        AND aspirante.id_aspirante=inscripcion.id_aspirante AND inscripcion.id_oferta_academica=oferta_academica.id_oferta_academica AND oferta_academica.id_seccion=seccion.id_seccion AND inscripcion.estatus='1'");

      while($row=$query->fetch()){

        $item=new Confucio();
        $item->id_inscripcion=$row['id_inscripcion'];
        $item->id_persona=$row['id_persona'];
        $item->primer_nombre=$row['primer_nombre'];
        $item->primer_apellido=$row['primer_apellido'];
        $item->identificacion=$row['identificacion'];
        $item->tipo=$row['tipo'];
        $item->oferta=$row['oferta'];
        $item->correo=$row['correo'];

        array_push($items,$item);
      }


      return $items;

    }catch(PDOException $e){

      return false;
    }
  }

  public function getIdDeposito($id){

    try{
      $query=$this->db->connect()->prepare("SELECT id_aspirante
        FROM aspirante 
        WHERE id_aspirante=:id");

      $query->execute(['id'=>$id]);
      $item=new Confucio();

      while($row=$query->fetch()){
        $item->id_aspirante = $row['id_aspirante'];
      }
      return $item;
    }catch(PDOException $e){
     return false;
   }
 }


 public function getPaisbyEstado($id_pais){
  $items=[];
  try{
    $query=$this->db->connect()->prepare("SELECT * FROM estado WHERE id_pais = :id_pais");
    $query->execute(['id_pais'=>$id_pais]);
    while($row=$query->fetch()){
      $item=new Confucio();
      $item->id_estado=$row['id_estado'];
      $item->descripcion=$row['descripcion'];

      array_push($items,$item);
      
    }
    return $items;
    
  }catch(PDOException $e){
    return[];
  }
  
}


public function getEstadobyMunicipio($id_estado){
  $items=[];
  try{
    $query=$this->db->connect()->prepare("SELECT * FROM municipio WHERE id_estado = :id_estado");
    $query->execute(['id_estado'=>$id_estado]);
    while($row=$query->fetch()){
      $item=new Confucio();
      $item->id_municipio=$row['id_municipio'];
      $item->descripcion=$row['descripcion'];

      array_push($items,$item);
      
    }
    return $items;
    
  }catch(PDOException $e){
    return[];
  }
  
}


public function getMunicipiobyParroquia($id_municipio){
  $items=[];
  try{
    $query=$this->db->connect()->prepare("SELECT * FROM parroquia WHERE id_municipio = :id_municipio");
    $query->execute(['id_municipio'=>$id_municipio]);
    while($row=$query->fetch()){
      $item=new Confucio();
      $item->id_parroquia=$row['id_parroquia'];
      $item->descripcion=$row['descripcion'];

      array_push($items,$item);
      
    }
    return $items;
    
  }catch(PDOException $e){
    return[];
  }
  
}


public function getIdOferta($id){

  try{
    $query=$this->db->connect()->prepare("SELECT id_oferta_academica
      FROM oferta_academica 
      WHERE id_oferta_academica=:id");

    $query->execute(['id'=>$id]);
    $item=new Confucio();

    while($row=$query->fetch()){
      $item->id_oferta_academica = $row['id_oferta_academica'];
    }
    return $item;
  }catch(PDOException $e){
   return false;
 }
}



public function getCatalogo($valor){

  $items=[];
  try{
    $query=$this->db->connect()->query("SELECT * FROM ".$valor."");
    
    while($row=$query->fetch()){

      $item=new Confucio();
      $item->id=$row['id_'.$valor.''];
      $item->descripcion=$row['descripcion'];
      array_push($items,$item);
      
    }
    return $items;
    
  }catch(PDOException $e){
    return[];
  }
}


public function getEstadoCivil(){

  $items=[];
  try{
    $query=$this->db->connect()->query("SELECT * FROM estado_civil");

    while($row=$query->fetch()){

      $item=new Confucio();
      $item->id_civil=$row['id_civil'];
      $item->descripcion=$row['descripcion'];
      array_push($items,$item);
      
    }
    return $items;
    
  }catch(PDOException $e){
    return[];
  }
}



public function getOfertas(){
  $items = [];
  try{
    $query = $this->db->connect()->query("SELECT  oferta_academica.id_oferta_academica, seccion.descripcion AS seccion, nivel.descripcion AS nivel,
      modalidad.descripcion AS modalidad, hora_inicio, hora_fin, turno.descripcion AS turno, periodo.descripcion AS periodo,
      periodo.estatus, periodo.inicio_periodo, periodo.fin_periodo, actividad.descripcion AS actividad
      FROM oferta_academica, seccion, nivel, modalidad, horario, turno, activar_oferta, periodo, actividad
      WHERE activar_oferta.id_oferta_academica=oferta_academica.id_oferta_academica
      AND oferta_academica.id_seccion=seccion.id_seccion
      AND seccion.id_nivel=nivel.id_nivel
      AND seccion.id_modalidad=modalidad.id_modalidad
      AND horario.id_seccion=seccion.id_seccion
      AND horario.id_turno=turno.id_turno
      AND seccion.id_periodo=periodo.id_periodo
      AND seccion.id_actividad=actividad.id_actividad
      AND oferta_academica.estatus='1'
      AND periodo.estatus='Activo'
      ORDER BY seccion.descripcion");

    while($row = $query->fetch()){
      $item = new Confucio(); 
      $item->id_oferta_academica = $row['id_oferta_academica'];
      $item->seccion = $row['seccion'];
      $item->nivel = $row['nivel'];
      $item->modalidad  = $row['modalidad'];
      $item->turno = $row['turno'];
      $item->hora_inicio  = $row['hora_inicio'];
      $item->hora_fin = $row['hora_fin'];
      $item->periodo = $row['periodo'];
      $item->actividad = $row['actividad'];
      $item->inicio_periodo = $row['inicio_periodo'];
      $item->fin_periodo = $row['fin_periodo'];
      array_push($items, $item);
    }
    return $items;
  }catch(PDOException $e){
    return [];
  }
}


public function getbyIDEstudiante($id_inscripcion){

  try{
    $query=$this->db->connect()->prepare("SELECT inscripcion.id_inscripcion, aspirante.id_aspirante, aspirante.id_persona AS persona, primer_nombre, segundo_nombre, primer_apellido, 
    segundo_apellido, identificacion, correo, telefono, genero, tipo_documento_identidad.id_tipo_documento_identidad AS documento, tipo_documento_identidad.descripcion AS tipo_documento,
    fecha_nacimiento, persona_nacimiento.id_pais as pais, direccion, telefono_habitacion, etnia.id_etnia, etnia.descripcion AS etnia, estado_civil.id_civil, estado_civil.descripcion as civil,
    empleo, telefono_institucion, correo_institucion, direccion_institucion, tipo_institucion, inst_publica, inst_privada, organismo, inst_inv, cargo.descripcion as cargo, trabajador_ubv,
    discapacidad, grupo_sanguineo, enfermedad, descripcion_enfermedad, tratamiento, idioma, idiomas_descripcion, tipo_discapacidad.descripcion AS tipo, nivel_academico.descripcion as nivel_aca,
    direccion_domicilio.id_estado, estado.descripcion AS estado, estudiante_ubv, mision_sucre, institucion_nivel, aspirante.estatus, dato_laboral.id_profesion, dato_laboral.id_estado as state_laboral, cargo.id_cargo, aspirante.id_nivel_academico, tipo_discapacidad.id_tipo_discapacidad,
    persona_nacimiento.id_nacimiento, direccion_domicilio.id_direccion, dato_laboral.id_dato_laboral, datos_interes.id_datos_interes
      FROM inscripcion, aspirante, persona, tipo_documento_identidad, persona_nacimiento, direccion_domicilio, etnia, dato_laboral, datos_interes, tipo_discapacidad, pais, estado, cargo, estado_civil, nivel_academico, profesion
      WHERE aspirante.id_persona=persona.id_persona 
      AND persona.id_tipo_documento_identidad=tipo_documento_identidad.id_tipo_documento_identidad 
      AND aspirante.id_nacimiento=persona_nacimiento.id_nacimiento
      AND persona_nacimiento.id_pais=pais.id_pais
      AND direccion_domicilio.id_etnia=etnia.id_etnia 
      AND direccion_domicilio.id_estado=estado.id_estado
      AND aspirante.id_direccion=direccion_domicilio.id_direccion
      AND aspirante.id_dato_laboral=dato_laboral.id_dato_laboral 
      AND datos_interes.id_tipo_discapacidad=tipo_discapacidad.id_tipo_discapacidad 
      AND aspirante.id_datos_interes=datos_interes.id_datos_interes 
      AND estado_civil.id_civil=aspirante.id_civil
      AND cargo.id_cargo=dato_laboral.id_cargo
      AND profesion.id_profesion=dato_laboral.id_profesion
      AND aspirante.id_nivel_academico=nivel_academico.id_nivel_academico
      AND inscripcion.id_aspirante=aspirante.id_aspirante
      AND inscripcion.id_inscripcion=:id_inscripcion
      AND inscripcion.estatus='1'");
    $query->execute(['id_inscripcion'=>$id_inscripcion]);
    $item=new Confucio();

    
    while($row=$query->fetch()){

      $item->id_aspirante=$row['id_aspirante'];
      $item->persona=$row['persona'];
      $item->pais=$row['pais'];
      $item->id_estado=$row['id_estado'];
      $item->estado=$row['estado'];
      $item->primer_nombre=$row['primer_nombre'];
      $item->segundo_nombre=$row['segundo_nombre'];
      $item->primer_apellido=$row['primer_apellido'];
      $item->segundo_apellido=$row['segundo_apellido'];
      $item->identificacion=$row['identificacion'];
      $item->correo=$row['correo'];
      $item->telefono=$row['telefono'];
      $item->genero=$row['genero'];
      $item->documento=$row['documento'];
      $item->tipo_documento=$row['tipo_documento'];
      $item->fecha_nacimiento=$row['fecha_nacimiento'];
      $item->direccion=$row['direccion'];
      $item->id_civil=$row['id_civil'];
      $item->civil=$row['civil'];
      $item->id_profesion=$row['id_profesion'];
      $item->state_laboral=$row['state_laboral'];
      $item->id_tipo_discapacidad=$row['id_tipo_discapacidad'];
      
      $item->id_nivel_academico=$row['id_nivel_academico'];
      $item->telefono_habitacion=$row['telefono_habitacion'];
      $item->etnia=$row['etnia'];
      $item->id_etnia=$row['id_etnia'];
      $item->empleo=$row['empleo'];
      $item->telefono_institucion=$row['telefono_institucion'];
      $item->correo_institucion=$row['correo_institucion'];
      $item->direccion_institucion=$row['direccion_institucion'];
      $item->inst_publica=$row['inst_publica'];
      $item->inst_privada=$row['inst_privada'];
      $item->organismo=$row['organismo'];
      $item->inst_inv=$row['inst_inv'];
      $item->cargo=$row['cargo'];
      $item->id_cargo=$row['id_cargo'];
      $item->tipo_institucion=$row['tipo_institucion'];
      $item->trabajador_ubv=$row['trabajador_ubv'];
      $item->discapacidad=$row['discapacidad'];
      $item->grupo_sanguineo=$row['grupo_sanguineo'];
      $item->enfermedad=$row['enfermedad'];
      $item->descripcion_enfermedad=$row['descripcion_enfermedad'];
      $item->tratamiento=$row['tratamiento'];
      $item->idioma=$row['idioma'];
      $item->idiomas_descripcion=$row['idiomas_descripcion'];
      $item->tipo=$row['tipo'];
      $item->estudiante_ubv=$row['estudiante_ubv'];
      $item->mision_sucre=$row['mision_sucre'];
      $item->institucion_nivel=$row['institucion_nivel'];
      $item->nivel_aca=$row['nivel_aca'];

      $item->id_nacimiento=$row['id_nacimiento'];
      $item->id_direccion=$row['id_direccion'];
      $item->id_dato_laboral=$row['id_dato_laboral'];
      $item->id_datos_interes=$row['id_datos_interes'];

    }
    
    return $item;
    
  }catch(PDOException $e){
    return false;
  }
  
}


public function getbyIDAspirante($id_aspirante){

  try{
    $query=$this->db->connect()->prepare("SELECT aspirante.id_aspirante, aspirante.id_persona AS persona, primer_nombre, segundo_nombre, primer_apellido, 
    segundo_apellido, identificacion, correo, telefono, genero, tipo_documento_identidad.id_tipo_documento_identidad AS documento, tipo_documento_identidad.descripcion AS tipo_documento,
    fecha_nacimiento, persona_nacimiento.id_pais as pais, direccion, telefono_habitacion, etnia.id_etnia, etnia.descripcion AS etnia, estado_civil.id_civil, estado_civil.descripcion as civil,
    empleo, telefono_institucion, correo_institucion, direccion_institucion, tipo_institucion, inst_publica, inst_privada, organismo, inst_inv, cargo.descripcion as cargo, trabajador_ubv,
    discapacidad, grupo_sanguineo, enfermedad, descripcion_enfermedad, tratamiento, idioma, idiomas_descripcion, tipo_discapacidad.descripcion AS tipo, nivel_academico.descripcion as nivel_aca,
    direccion_domicilio.id_estado, estado.descripcion AS estado, estudiante_ubv, mision_sucre, institucion_nivel, aspirante.id_oferta_academica, seccion.descripcion as oferta, modalidad.descripcion AS modalidad, turno.descripcion AS turno, hora_inicio, 
    hora_fin, aspirante.estatus, seccion.id_seccion, dato_laboral.id_profesion, dato_laboral.id_estado as state_laboral, cargo.id_cargo, aspirante.id_nivel_academico, tipo_discapacidad.id_tipo_discapacidad,
    persona_nacimiento.id_nacimiento, direccion_domicilio.id_direccion, dato_laboral.id_dato_laboral, datos_interes.id_datos_interes
    FROM aspirante, persona, tipo_documento_identidad, persona_nacimiento, direccion_domicilio, etnia, dato_laboral, datos_interes, tipo_discapacidad, pais, estado, seccion, oferta_academica, modalidad, turno, horario, cargo, estado_civil, nivel_academico
    WHERE aspirante.id_persona=persona.id_persona 
    AND persona.id_tipo_documento_identidad=tipo_documento_identidad.id_tipo_documento_identidad 
    AND aspirante.id_nacimiento=persona_nacimiento.id_nacimiento
    AND persona_nacimiento.id_pais=pais.id_pais
    AND direccion_domicilio.id_etnia=etnia.id_etnia 
    AND direccion_domicilio.id_estado=estado.id_estado
    AND aspirante.id_direccion=direccion_domicilio.id_direccion
    AND aspirante.id_dato_laboral=dato_laboral.id_dato_laboral 
    AND datos_interes.id_tipo_discapacidad=tipo_discapacidad.id_tipo_discapacidad 
    AND aspirante.id_datos_interes=datos_interes.id_datos_interes 
    AND aspirante.id_oferta_academica=oferta_academica.id_oferta_academica  
    AND oferta_academica.id_seccion=seccion.id_seccion
    AND seccion.id_modalidad=modalidad.id_modalidad
    AND horario.id_turno=turno.id_turno
    AND horario.id_seccion=seccion.id_seccion
    AND estado_civil.id_civil=aspirante.id_civil
    AND cargo.id_cargo=dato_laboral.id_cargo
    AND aspirante.id_nivel_academico=nivel_academico.id_nivel_academico
    AND aspirante.id_aspirante=:id_aspirante");
    $query->execute(['id_aspirante'=>$id_aspirante]);
    $item=new Confucio();

    

    while($row=$query->fetch()){

      $item->id_aspirante=$row['id_aspirante'];
      $item->persona=$row['persona'];
      $item->pais=$row['pais'];
      $item->id_estado=$row['id_estado'];
      $item->estado=$row['estado'];
      $item->primer_nombre=$row['primer_nombre'];
      $item->segundo_nombre=$row['segundo_nombre'];
      $item->primer_apellido=$row['primer_apellido'];
      $item->segundo_apellido=$row['segundo_apellido'];
      $item->identificacion=$row['identificacion'];
      $item->correo=$row['correo'];
      $item->telefono=$row['telefono'];
      $item->genero=$row['genero'];
      $item->documento=$row['documento'];
      $item->tipo_documento=$row['tipo_documento'];
      $item->fecha_nacimiento=$row['fecha_nacimiento'];
      $item->direccion=$row['direccion'];
      $item->id_civil=$row['id_civil'];
      $item->civil=$row['civil'];
      $item->id_profesion=$row['id_profesion'];
      $item->state_laboral=$row['state_laboral'];
      $item->id_tipo_discapacidad=$row['id_tipo_discapacidad'];
      
      $item->id_nivel_academico=$row['id_nivel_academico'];
      $item->telefono_habitacion=$row['telefono_habitacion'];
      $item->etnia=$row['etnia'];
      $item->id_etnia=$row['id_etnia'];
      $item->empleo=$row['empleo'];
      $item->telefono_institucion=$row['telefono_institucion'];
      $item->correo_institucion=$row['correo_institucion'];
      $item->direccion_institucion=$row['direccion_institucion'];
      $item->inst_publica=$row['inst_publica'];
      $item->inst_privada=$row['inst_privada'];
      $item->organismo=$row['organismo'];
      $item->inst_inv=$row['inst_inv'];
      $item->cargo=$row['cargo'];
      $item->id_cargo=$row['id_cargo'];
      $item->tipo_institucion=$row['tipo_institucion'];
      $item->trabajador_ubv=$row['trabajador_ubv'];
      $item->discapacidad=$row['discapacidad'];
      $item->grupo_sanguineo=$row['grupo_sanguineo'];
      $item->enfermedad=$row['enfermedad'];
      $item->descripcion_enfermedad=$row['descripcion_enfermedad'];
      $item->tratamiento=$row['tratamiento'];
      $item->idioma=$row['idioma'];
      $item->idiomas_descripcion=$row['idiomas_descripcion'];
      $item->tipo=$row['tipo'];
      $item->estudiante_ubv=$row['estudiante_ubv'];
      $item->mision_sucre=$row['mision_sucre'];
      $item->institucion_nivel=$row['institucion_nivel'];
      $item->nivel_aca=$row['nivel_aca'];

      $item->id_nacimiento=$row['id_nacimiento'];
      $item->id_direccion=$row['id_direccion'];
      $item->id_dato_laboral=$row['id_dato_laboral'];
      $item->id_datos_interes=$row['id_datos_interes'];


    }
    
    return $item;
    
  }catch(PDOException $e){
    return false;
  }
  
}



public function Update($datos){

  try{

      $pdo=$this->db->connect();
      $pdo->beginTransaction();

      $persona=$pdo->prepare("UPDATE persona SET primer_nombre=:primer_nombre, 
      segundo_nombre=:segundo_nombre, primer_apellido=:primer_apellido, segundo_apellido=:segundo_apellido, 
      correo=:correo, telefono=:telefono,
      genero=:genero, id_tipo_documento_identidad=:id_tipo_documento_identidad 
       WHERE id_persona=:id_persona");
      $persona->execute(['primer_nombre'=>$datos['primer_nombre'], 
      'segundo_nombre'=>$datos['segundo_nombre'], 
      'primer_apellido'=>$datos['primer_apellido'], 
      'segundo_apellido'=>$datos['segundo_apellido'], 
      'correo'=>$datos['correo'], 
      'telefono'=>$datos['telefono'], 
      'genero'=>$datos['genero'], 
      'id_tipo_documento_identidad'=>$datos['id_tipo_documento_identidad'], 
      'id_persona'=>$datos['id_persona']]);

      $nacimiento=$pdo->prepare("UPDATE persona_nacimiento SET fecha_nacimiento=:fecha_nacimiento, 
      id_pais=:id_pais WHERE id_nacimiento=:id_nacimiento");
      $nacimiento->execute(['fecha_nacimiento'=>$datos['fecha_nacimiento'], 
      'id_pais'=>$datos['pais'], 
      'id_nacimiento'=>$datos['id_nacimiento']]);

      $direccion=$pdo->prepare("UPDATE direccion_domicilio SET direccion=:direccion, 
      telefono_habitacion=:telefono_habitacion, 
      id_estado=:id_estado, 
      id_etnia=:id_etnia 
       WHERE id_direccion=:id_direccion");
      $direccion->execute(['direccion'=>$datos['direccion'], 
      'telefono_habitacion'=>$datos['telefono_habitacion'], 
      'id_estado'=>$datos['estado'], 
      'id_etnia'=>$datos['etnia'], 
      'id_direccion'=>$datos['id_direccion']]);

      $laboral=$pdo->prepare("UPDATE dato_laboral SET empleo=:empleo, 
      telefono_institucion=:telefono_institucion, 
      correo_institucion=:correo_institucion, 
      direccion_institucion=:direccion_institucion,
      inst_publica=:inst_publica,
      inst_privada=:inst_privada,
      organismo=:organismo,
      inst_inv=:inst_inv,
      tipo_institucion=:tipo_institucion,
      trabajador_ubv=:trabajador_ubv,
      id_cargo=:id_cargo,
      id_estado=:id_estado,
      id_profesion=:id_profesion
       WHERE id_dato_laboral=:id_dato_laboral");
      $laboral->execute(['empleo'=>$datos['empleo'], 
      'telefono_institucion'=>$datos['telefono_institucion'], 
      'correo_institucion'=>$datos['correo_institucion'], 
      'direccion_institucion'=>$datos['direccion_institucion'], 
      'inst_publica'=>$datos['inst_publica'],
      'inst_privada'=>$datos['inst_privada'],
      'organismo'=>$datos['organismo'],
      'inst_inv'=>$datos['inst_inv'],
      'tipo_institucion'=>$datos['tipo_institucion'],
      'trabajador_ubv'=>$datos['trabajador_ubv'],
      'id_cargo'=>$datos['id_cargo'],
      'id_estado'=>$datos['state'],
      'id_profesion'=>$datos['id_profesion'], 
      'id_dato_laboral'=>$datos['id_dato_laboral']]);

      $interes=$pdo->prepare("UPDATE datos_interes SET discapacidad=:discapacidad, 
      grupo_sanguineo=:grupo_sanguineo, 
      enfermedad=:enfermedad, 
      descripcion_enfermedad=:descripcion_enfermedad,
      tratamiento=:tratamiento,
      idioma=:idioma,
      idiomas_descripcion=:idiomas_descripcion,
      estudiante_ubv=:estudiante_ubv,
      mision_sucre=:mision_sucre,
      id_tipo_discapacidad=:id_tipo_discapacidad,
      institucion_nivel=:institucion_nivel
       WHERE id_datos_interes=:id_datos_interes");
      $interes->execute(['discapacidad'=>$datos['discapacidad'], 
      'grupo_sanguineo'=>$datos['grupo_sanguineo'], 
      'enfermedad'=>$datos['enfermedad'], 
      'descripcion_enfermedad'=>$datos['descripcion_enfermedad'], 
      'tratamiento'=>$datos['tratamiento'],
      'idioma'=>$datos['idioma'],
      'idiomas_descripcion'=>$datos['descripcion'],
      'estudiante_ubv'=>$datos['estudiante_ubv'],
      'mision_sucre'=>$datos['mision_sucre'],
      'id_tipo_discapacidad'=>$datos['id_tipo_discapacidad'],
      'institucion_nivel'=>$datos['institucion_nivel'],
      'id_datos_interes'=>$datos['id_datos_interes']]);

      $aspirante=$pdo->prepare("UPDATE aspirante SET id_civil=:id_civil, 
      id_nivel_academico=:id_nivel_academico
       WHERE id_aspirante=:id_aspirante");
      $aspirante->execute([
      'id_nivel_academico'=>$datos['id_nivel_academico'],
      'id_civil'=>$datos['id_civil'],
      'id_aspirante'=>$datos['id_aspirante']]);

      $pdo->commit();
      return true;

  }catch(PDOException $e){

      $pdo->rollBack();
      return false;
  }
}


public function getOfertasDisponibles(){
  $items = [];
  try{
    $query = $this->db->connect()->query("SELECT oferta_academica.id_oferta_academica, seccion.descripcion AS seccion, cupos
      FROM oferta_academica, seccion, periodo
      WHERE oferta_academica.id_seccion=seccion.id_seccion
      AND seccion.id_periodo=periodo.id_periodo
      AND periodo.estatus='Activo'
      AND seccion.cupos != 0
      ORDER BY seccion.descripcion");

    while($row = $query->fetch()){
      $item = new Confucio(); 
      $item->id_oferta_academica = $row['id_oferta_academica'];
      $item->seccion = $row['seccion'];
      array_push($items, $item);
    }
    return $items;
  }catch(PDOException $e){
    return [];
  }
}

public function updateSolicitud($item){

  $oferta=$item['id_oferta_academica'];
  $sql_1 = $this->db->connect()->query("SELECT seccion.id_seccion AS seccion, cupos FROM oferta_academica,seccion 
    WHERE oferta_academica.id_seccion=seccion.id_seccion AND id_oferta_academica='$oferta' ");

  $nombre_1=$sql_1->fetch();
  $cupos_1=$nombre_1['cupos'];
  $seccion_1=$nombre_1['seccion'];

  $seccion=$item['id_seccion'];
  $sql_2 = $this->db->connect()->query("SELECT seccion.id_seccion AS seccion, cupos FROM seccion 
    WHERE id_seccion='$seccion' ");

  $nombre_2=$sql_2->fetch();
  $cupos_2=$nombre_2['cupos'];
  $seccion_2=$nombre_2['seccion'];

  try{

    $pdo=$this->db->connect();
    $pdo->beginTransaction();

    $query=$pdo->prepare("UPDATE aspirante SET id_oferta_academica= :id_oferta_academica 
      WHERE id_aspirante=:id_aspirante");

    $query->execute([
      'id_aspirante'=>$item['id_aspirante'],
      'id_oferta_academica'=>$item['id_oferta_academica']
    ]);

    $resta_cupos= $cupos_1 - 1;    
    $query_resta=$pdo->query("UPDATE seccion SET cupos='$resta_cupos' WHERE id_seccion='$seccion_1' ");

    $suma_cupos= $cupos_2 + 1;    
    $query_suma=$pdo->query("UPDATE seccion SET cupos='$suma_cupos' WHERE id_seccion='$seccion_2' ");

    $pdo->commit();
    return true;

  }catch(PDOException $e){

    $pdo->rollBack();
    return false;
  }
}


public function updateInscrip($item){

  $oferta=$item['id_oferta_academica'];
  $sql_1 = $this->db->connect()->query("SELECT seccion.id_seccion AS seccion, cupos FROM oferta_academica,seccion 
    WHERE oferta_academica.id_seccion=seccion.id_seccion AND id_oferta_academica='$oferta' ");

  $nombre_1=$sql_1->fetch();
  $cupos_1=$nombre_1['cupos'];
  $seccion_1=$nombre_1['seccion'];

  $seccion=$item['id_seccion'];
  $sql_2 = $this->db->connect()->query("SELECT seccion.id_seccion AS seccion, cupos FROM seccion 
    WHERE id_seccion='$seccion' ");

  $nombre_2=$sql_2->fetch();
  $cupos_2=$nombre_2['cupos'];
  $seccion_2=$nombre_2['seccion'];

  try{

    $pdo=$this->db->connect();
    $pdo->beginTransaction();
    $query=$pdo->prepare("UPDATE inscripcion SET id_oferta_academica= :id_oferta_academica 
      WHERE id_inscripcion=:id_inscripcion");

    $query->execute([
      'id_inscripcion'=>$item['id_inscripcion'],
      'id_oferta_academica'=>$item['id_oferta_academica']
    ]);

    $query=$pdo->prepare("UPDATE aspirante SET id_oferta_academica= :id_oferta_academica 
      WHERE id_aspirante=:id_aspirante");

    $query->execute([
      'id_aspirante'=>$item['id_aspirante'],
      'id_oferta_academica'=>$item['id_oferta_academica']
    ]);

    $resta_cupos= $cupos_1 - 1;    
    $query_resta=$pdo->query("UPDATE seccion SET cupos='$resta_cupos' WHERE id_seccion='$seccion_1' ");

    $suma_cupos= $cupos_2 + 1;    
    $query_suma=$pdo->query("UPDATE seccion SET cupos='$suma_cupos' WHERE id_seccion='$seccion_2' ");

    $pdo->commit();
    return true;

  }catch(PDOException $e){

    $pdo->rollBack();
    return false;
  }
}



public function delete($id_aspirante){

  try{

    $persona=$this->db->connect()->prepare("DELETE FROM aspirante WHERE id_aspirante=:id_aspirante"); 
    $persona->execute(['id_aspirante'=>$id_aspirante]);
    //$aspirante=$this->db->connect()->prepare("DELETE FROM persona WHERE id_persona=:id_persona");
    //$aspirante->execute(['id_persona'=>$id_persona]); 

    return true;
    
  }catch(PDOException $e){
    return false;
    
  }

}


}

?>