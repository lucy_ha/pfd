<?php

include_once 'SED.php';

class LoginModel extends Model{

    public function __construct()
    {
       parent::__construct();
    }

    public function getStatus($datos){
        try{

            $crypt= new SED();
            $clave=$crypt->encryption($datos['clave']);
           
          
            $query=$this->db->connect()->prepare("SELECT 
                        usuario, 
                        clave,
                        estatus
                        FROM 
                        usuario
                        WHERE usuario=:usuario 
                        AND estatus=:estatus
                        AND clave=:clave;");
            $query->execute([
                'usuario'=> $datos['usuario'], 
                'estatus'=> 'Inactivo', 
                'clave' =>$clave
                ]);
            $var=$query->fetch(PDO::FETCH_ASSOC);

            //validar coincidencia de datos de entrada con los de bbdd
            if($var['usuario']==$datos['usuario'] && $var['clave'] == $clave && $var['estatus'] == 'Inactivo'){

                return true;

            }

        } catch(PDOException $e){
                return false;
        }
    }
  
      
        public function getLogin($datos){
            try{
    
                $crypt= new SED();
                $clave=$crypt->encryption($datos['clave']);

                $query=$this->db->connect()->prepare("SELECT 
                            usuario, 
                            primer_nombre, 
                            primer_apellido,
                            usuario.id_perfil, 
                            perfil.descripcion as perfil,
                            clave,
                            estatus,
                            persona.id_persona
                            FROM 
                            usuario, persona, 
                            perfil
                            WHERE usuario.id_persona=persona.id_persona 
                            AND usuario.id_perfil=perfil.id_perfil 
                            AND usuario=:usuario 
                            AND estatus=:estatus
                            AND clave=:clave");
                $query->execute([
                    'usuario'=> $datos['usuario'], 
                    'estatus'=> 'Activo', 
                    'clave' =>$clave
                    ]);
                $var=$query->fetch(PDO::FETCH_ASSOC);
    
                //validar coincidencia de datos de entrada con los de bbdd
                if($var['usuario']==$datos['usuario'] && $var['clave'] == $clave && $var['estatus'] == 'Activo'){
    
                    //variables de sesion
                     session_start();
                     $_SESSION['id_persona']=$var['id_persona'];
                     $_SESSION['primer_nombre']=$var['primer_nombre'];
                     $_SESSION['primer_apellido']=$var['primer_apellido'];
                     $_SESSION['perfil']=$var['perfil'];
                     $_SESSION['usuario']=$var['usuario'];
                     $_SESSION['id_perfil']=$var['id_perfil'];
    
                    //obtener roles de usuario
                    $roles_user=$this->db->connect()->prepare("SELECT usuario_rol.id_usuario, 
                        usuario_rol.id_rol, 
                        rol.descripcion AS rol 
                        FROM rol,
                        usuario_rol,
                        usuario, 
                        persona
                        WHERE usuario_rol.id_rol=rol.id_rol
                        AND usuario_rol.id_usuario=usuario.id_usuario 
                        AND usuario.id_persona=persona.id_persona
                        AND usuario.id_persona=:id_persona");
                        $roles_user->execute(['id_persona'=>$var['id_persona']]);
                        $_SESSION['Agregar']=false;
                        $_SESSION['Consultar']=false;
                        $_SESSION['Editar']=false;
                        $_SESSION['Eliminar']=false;
    
                    while($row=$roles_user->fetch(PDO::FETCH_ASSOC)){
                         switch($row['rol']){
                            case 'Agregar': 
                                $_SESSION['Agregar']=true;
                            
                            break;
                            case 'Consultar': 
                                $_SESSION['Consultar']=true;
                            break;
                            case 'Editar': 
                                $_SESSION['Editar']=true;
    
                            break;
                            case 'Eliminar': 
                                $_SESSION['Eliminar']=true;
                            break;
                        }
                    }
    
                    return true;
                }else {
                    return false;
                }//end if validar coincidencia
    
            } catch(PDOException $e){
                    return false;
            }
        }

    }








?>