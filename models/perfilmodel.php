<?php

require_once 'models/confucio.php';

class PerfilModel extends Model{

    function __construt(){

    parent::__construct();

    }

    public function getbyID_user($id){

        try{

        $query=$this->db->connect()->prepare("SELECT usuario.id_persona, usuario.id_usuario, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, identificacion, 
        correo, telefono, genero, persona.id_tipo_documento_identidad, tipo_documento_identidad.descripcion, usuario, clave, estatus, 
        usuario.id_perfil, perfil.descripcion AS perfil, usuario.id_persona FROM persona, usuario, perfil, tipo_documento_identidad WHERE usuario.id_persona=persona.id_persona AND usuario.id_perfil=perfil.id_perfil AND persona.id_tipo_documento_identidad=tipo_documento_identidad.id_tipo_documento_identidad AND usuario.id_persona=:id_persona");
        $query->execute(['id_persona'=>$id]);
        $item=new Confucio();

        while($row=$query->fetch()){
            
            $item->id_usuario=$row['id_usuario'];
            $item->usuario=$row['usuario'];
            $item->clave=$row['clave'];
            $item->estatus=$row['estatus'];
            $item->id_perfil=$row['id_perfil'];
            $item->perfil=$row['perfil'];

            $item->id_persona=$row['id_persona'];
            $item->identificacion=$row['identificacion'];
            $item->primer_nombre=$row['primer_nombre'];
            $item->segundo_nombre=$row['segundo_nombre'];

            $item->primer_apellido=$row['primer_apellido'];
            $item->segundo_apellido=$row['segundo_apellido'];
            $item->correo=$row['correo'];
            $item->telefono=$row['telefono'];
            $item->genero=$row['genero'];
            $item->id_tipo_documento_identidad=$row['id_tipo_documento_identidad'];
            $item->descripcion=$row['descripcion'];
        }

          return $item;

        }catch(PDOException $e){

           return false;
        }

    }


    public function getbyID_roles($id_persona){

        $items=[];
        $query = $this->db->connect()->prepare("SELECT usuario_rol.id_usuario, 
            usuario_rol.id_rol, rol.descripcion AS rol 
            FROM rol,usuario_rol,usuario, persona
            WHERE usuario_rol.id_rol=rol.id_rol
            AND usuario_rol.id_usuario=usuario.id_usuario 
            AND usuario.id_persona=persona.id_persona
            AND usuario.id_persona=:id_persona");
        try{
            $query ->execute(['id_persona'=>$id_persona]);

            while($row = $query->fetch()){
                $item = new Confucio();
                $item->id_usuario = $row['id_usuario'];  
                $item->id_rol = $row['id_rol'];
                $item->rol = $row['rol'];
                //  var_dump($row['usuario']);
                array_push($items,$item);
            }


            return $items;
        }catch(PDOException  $e){
                return null;
        }
    }

    public function getRolbyID($id_persona, $id_rol){

        try{
            $query=$this->db->connect()->prepare("SELECT usuario_rol.id_usuario, 
                usuario_rol.id_rol, rol.descripcion AS rol 
                FROM rol,usuario_rol,usuario, persona
                WHERE usuario_rol.id_rol=rol.id_rol
                AND usuario_rol.id_usuario=usuario.id_usuario 
                AND usuario.id_persona=persona.id_persona
                AND usuario.id_persona=:id_persona 
                AND usuario_rol.id_rol=:id_rol");
            $query->execute([
                'id_persona'=>$id_persona,
                'id_rol'=>$id_rol
            ]);
      
            $item=new Confucio();
            while($row=$query->fetch()){
                $item->id_usuario = $row['id_usuario'];
                $item->id_rol = $row['id_rol'];
                $item->rol = $row['rol'];
            }
              
            return $item;
                
        } catch(PDOException $e){
                return false;
        }
      
    }
    

    public function getPerfil(){
        $items=[];
        try{
          $query=$this->db->connect()->prepare("SELECT * FROM perfil WHERE id_perfil != :id_perfil;");
          $query->execute(['id_perfil'=>3]);
          while($row=$query->fetch()){
            $item=new Confucio();
            $item->id_perfil=$row['id_perfil'];
            $item->descripcion=$row['descripcion'];
            array_push($items,$item);
          }
          return $items;
          
        }catch(PDOException $e){
          return[];
        }
        
    }


    public function getCatalogo($valor){

        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT * FROM ".$valor."");
      
            while($row=$query->fetch()){

                $item=new Confucio();
                $item->id=$row['id_'.$valor.''];
                $item->descripcion=$row['descripcion'];
                array_push($items,$item);
            
            }
            return $items;
            
        }catch(PDOException $e){
            return[];
        }
    }



}


?>