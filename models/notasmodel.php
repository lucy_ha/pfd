<?php 
require_once 'models/confucio.php';
require_once 'models/perfil.php';
class NotasModel extends Model
{
	
  public function __construct(){
    parent::__construct();
}

////////////////////////////////////////////CONSULTAS///////////////////////////////////////////////
public function getSecciones_Docente($id){

    $items=[];
    $query=$this->db->connect()->prepare("SELECT seccion.id_seccion, seccion.descripcion AS seccion, 
        actividad.descripcion AS actividad, nivel.descripcion AS nivel, actividad.categoria
        FROM persona, docente, seccion, actividad, nivel, periodo
        WHERE seccion.id_actividad = actividad.id_actividad
        AND seccion.id_nivel = nivel.id_nivel
        AND seccion.id_docente = docente.id_docente
        AND docente.id_persona = persona.id_persona
        AND seccion.id_periodo = periodo.id_periodo
        AND periodo.estatus = 'Activo'
        AND persona.id_persona = :id");

    try{
        $query->execute(['id'=>$id]);
        while($row = $query->fetch()){
            $item=new Confucio();
            $item->id_seccion  = $row['id_seccion'];
            $item->seccion = $row['seccion'];
            $item->actividad    = $row['actividad'];
            $item->nivel    = $row['nivel'];
            $item->categoria    = $row['categoria'];
            array_push($items,$item);
        }

        return $items;
    }catch(PDOException  $e){
      return null;
  }
}

public function getSecciones(){
    $items = [];
    try{
        $query = $this->db->connect()->query("SELECT seccion.id_seccion, seccion.descripcion, actividad.descripcion AS actividad, categoria, nivel.descripcion AS nivel, periodo.descripcion AS periodo, persona.primer_nombre, persona.primer_apellido
            FROM seccion, actividad, nivel, periodo, persona, docente
            WHERE seccion.id_actividad = actividad.id_actividad
            AND seccion.id_nivel = nivel.id_nivel
            AND seccion.id_periodo = periodo.id_periodo
            AND seccion.id_docente = docente.id_docente
            AND docente.id_persona = persona.id_persona
            AND docente.id_docente!=1
            AND periodo.estatus = 'Activo' ");

        while($row = $query->fetch()){
            $item = new Confucio(); 
            $item->id_seccion  = $row['id_seccion'];
            $item->descripcion = $row['descripcion'];
            $item->actividad   = $row['actividad'];
            $item->nivel    = $row['nivel'];
            $item->periodo  = $row['periodo'];
            $item->categoria  = $row['categoria'];
            $item->primer_nombre    = $row['primer_nombre'];
            $item->primer_apellido  = $row['primer_apellido'];

            array_push($items, $item);
        }
        return $items;
    }catch(PDOException $e){
        return [];
    }
}

public function getEstudiantes($id){
    $items=[];
    $query=$this->db->connect()->prepare("SELECT inscripcion.id_inscripcion, persona.id_persona, seccion.id_seccion, nivel.id_nivel,
    primer_nombre,segundo_nombre, primer_apellido,segundo_apellido, identificacion
        FROM persona, aspirante, seccion, inscripcion, oferta_academica,actividad, nivel
        WHERE aspirante.id_persona=persona.id_persona 
        AND inscripcion.id_oferta_academica=oferta_academica.id_oferta_academica
        AND aspirante.id_aspirante=inscripcion.id_aspirante 
        AND seccion.id_nivel = nivel.id_nivel
        AND seccion.id_seccion=oferta_academica.id_seccion
        AND actividad.id_actividad=seccion.id_actividad
        AND seccion.id_seccion=:id");
    
    try{
        $query->execute(['id'=>$id]);
        while($row = $query->fetch()){
            $item=new Confucio();
            $item->id_inscripcion   = $row['id_inscripcion'];
            $item->id_persona       = $row['id_persona'];
            $item->id_nivel         = $row['id_nivel'];
            $item->id_seccion       = $row['id_seccion'];
            $item->primer_nombre    = $row['primer_nombre'];
            $item->segundo_nombre   = $row['segundo_nombre'];
            $item->primer_apellido  = $row['primer_apellido'];
            $item->segundo_apellido = $row['segundo_apellido'];
            $item->identificacion   = $row['identificacion'];
            array_push($items,$item);
        }
        return $items;
    }catch(PDOException  $e){
      return null;
  }
}

public function getNotas($id){
    $items=[];
    $query=$this->db->connect()->prepare("SELECT nota_actividad_history.nota AS nota, nota_actividad_history.descripcion AS descripcion, id_inscripcion,
    identificacion, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido
       FROM nota_actividad_history, persona, seccion
       WHERE nota_actividad_history.id_persona=persona.id_persona
       AND nota_actividad_history.id_seccion=seccion.id_seccion
       AND seccion.id_seccion=:id");

    try{
        $query->execute(['id'=>$id]);
        while($row = $query->fetch()){
            $item=new Confucio();
            $item->id_inscripcion = $row['id_inscripcion'];
            $item->nota  = $row['nota'];
            $item->descripcion  = $row['descripcion'];
            $item->primer_nombre  = $row['primer_nombre'];
            $item->segundo_nombre  = $row['segundo_nombre'];
            $item->primer_apellido = $row['primer_apellido'];
            $item->segundo_apellido = $row['segundo_apellido'];
            $item->identificacion    = $row['identificacion'];
            array_push($items,$item);
        }
        return $items;
    }catch(PDOException  $e){
      return null;
  }
}

public function getNotasIdioma($id){
    $items=[];
    $query=$this->db->connect()->prepare("SELECT nota_actividad_idioma_history.nota AS nota, nota_actividad_idioma_history.descripcion AS descripcion, id_inscripcion,
    identificacion, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido
       FROM nota_actividad_idioma_history, persona, seccion
       WHERE nota_actividad_idioma_history.id_persona=persona.id_persona
       AND nota_actividad_idioma_history.id_seccion=seccion.id_seccion
       AND seccion.id_seccion=:id ");

    try{
        $query->execute(['id'=>$id]);
        while($row = $query->fetch()){
            $item=new Confucio();
            $item->id_inscripcion = $row['id_inscripcion'];
            $item->nota  = $row['nota'];
            $item->descripcion  = $row['descripcion'];
            $item->primer_nombre  = $row['primer_nombre'];
            $item->segundo_nombre  = $row['segundo_nombre'];
            $item->primer_apellido = $row['primer_apellido'];
            $item->segundo_apellido = $row['segundo_apellido'];
            $item->identificacion    = $row['identificacion'];
            array_push($items,$item);
        }
        return $items;
    }catch(PDOException  $e){
      return null;
  }
}

public function getProsecucion($id){

    try{
        $query=$this->db->connect()->prepare("SELECT nota_actividad_idioma.id_nota_actividad_idioma, nota_actividad_idioma.nota, nota_actividad_idioma.descripcion, nota_actividad_idioma.estatus
            FROM persona, aspirante, seccion, inscripcion, oferta_academica, nota_actividad_idioma, actividad
            WHERE aspirante.id_persona=persona.id_persona 
            AND inscripcion.id_oferta_academica=oferta_academica.id_oferta_academica
            AND aspirante.id_aspirante=inscripcion.id_aspirante 
            AND seccion.id_seccion=oferta_academica.id_seccion
            AND inscripcion.id_inscripcion=nota_actividad_idioma.id_inscripcion
            AND actividad.id_actividad=seccion.id_actividad
            AND seccion.id_seccion=:id_seccion");
        $query->execute(['id'=>$id]);
        $item=new Confucio();
        
        while($row=$query->fetch()){
            $item->id_nota_actividad  = $row['id_nota_actividad'];
            $item->nota  = $row['nota'];
            $item->descripcion  = $row['descripcion'];
            $item->id_inscripcion  = $row['id_inscripcion'];
            $item->estatus = $row['estatus'];
        }      
        return $item;
    }catch(PDOException $e){
        return false;
    }
    
}


////////////////////////////////////////////CARGAR-NOTAS////////////////////////////////////////////////
public function insert($datos){
    try{
        $pdo=$this->db->connect();
        $pdo->beginTransaction();
        
        $inscripciones=$datos['id_inscripcion'];
        $personas=$datos['id_persona'];
        $nivel=$datos['id_nivel'];
        $seccion=$datos['id_seccion'];
        $notas=$datos['nota'];

        $query1=$pdo->prepare('INSERT INTO nota_actividad (nota, descripcion, id_inscripcion, id_persona, id_nivel, id_seccion) 
            VALUES(:nota, :descripcion, :id_inscripcion, :id_persona, :id_nivel, :id_seccion)');

        for ($i=0;$i<count($notas) && $i<count($inscripciones) && $i<count($personas) && $i<count($nivel) && $i<count($seccion);$i++)    
        { 
            if ($notas[$i] >= 60) {
                $query1->execute([
                    'nota'=>$notas[$i], 
                    'descripcion'=>'Aprobado',
                    'id_inscripcion'=>$inscripciones[$i],
                    'id_nivel'=>$nivel[$i],
                    'id_seccion'=>$seccion[$i],
                    'id_persona'=>$personas[$i]
                ]);
            } else {
                $query1->execute([
                    'nota'=>$notas[$i], 
                    'descripcion'=>'Reprobado',
                    'id_inscripcion'=>$inscripciones[$i],
                    'id_nivel'=>$nivel[$i],
                    'id_seccion'=>$seccion[$i],
                    'id_persona'=>$personas[$i]
                ]); 
            }
        }        
        $pdo->commit();
        return true;

    }catch(PDOException $e){
        $pdo->rollBack();
        return false;
    }
}

public function insertPro($datos){
    try{

        $pdo=$this->db->connect();
        $pdo->beginTransaction();
        
        
        $inscripciones=$datos['id_inscripcion'];
        $personas=$datos['id_persona'];
        $nivel=$datos['id_nivel'];
        $seccion=$datos['id_seccion'];
        $notas=$datos['nota'];

        $query2=$pdo->prepare('INSERT INTO nota_actividad_idioma (nota, descripcion, id_inscripcion, id_persona, estatus, id_nivel, id_seccion) 
           VALUES(:nota, :descripcion, :id_inscripcion, :id_persona, :estatus, :id_nivel, :id_seccion)');

        for ($l=0;$l<count($notas) && $l<count($inscripciones) && $l<count($nivel) && $l<count($personas) && $l<count($seccion);$l++)    
        {       
            if ($notas[$l] >= 60) {
                $query2->execute([
                    'nota'=>$notas[$l], 
                    'descripcion'=>'Aprobado',
                    'id_inscripcion'=>$inscripciones[$l],
                    'id_nivel'=>$nivel[$l],
                    'id_persona'=>$personas[$l],
                    'id_seccion'=>$seccion[$l],
                    'estatus'=>'1'
                ]);
            } else {
                $query2->execute([
                    'nota'=>$notas[$l], 
                    'descripcion'=>'Reprobado', 
                    'id_inscripcion'=>$inscripciones[$l],
                    'id_nivel'=>$nivel[$l],
                    'id_persona'=>$personas[$l],
                    'id_seccion'=>$seccion[$l],
                    'estatus'=>'0'
                ]);
            } 
        } 
     // $query3=$pdo->prepare("UPDATE nota_actividad_idioma SET estatus=:estatus WHERE descripcion=('Aprobado')");
     //$query3->execute([
      //  'estatus'=>1
   ///]);
        $pdo->commit();
        return true;
    }catch(PDOException $e){
        $pdo->rollBack();
        return false;
    }
}

///////////////////////////////////////////////UPDATE////////////////////////////////////////////////
public function update($datos){
    try{

        $pdo=$this->db->connect();
        $pdo->beginTransaction();

        $notas=$datos['nota'];
        $nota_actividad=$datos['id_inscripcion'];

        $query=$pdo->prepare('UPDATE nota_actividad_history 
        SET nota = :nota, descripcion = :descripcion  
        WHERE id_inscripcion = :id_inscripcion');

        for ($i=0;$i<count($notas) && $i<count($nota_actividad);$i++)    
        {       
            if ($notas[$i] >= 60) {
                $query->execute([
                    'nota'=>$notas[$i],
                    'descripcion'=>'Aprobado',
                    'id_inscripcion'=>$nota_actividad[$i]
                ]);
            } else {
                $query->execute([
                    'nota'=>$notas[$i],
                    'descripcion'=>'Reprobado',
                    'id_inscripcion'=>$nota_actividad[$i]
                ]); 
            } 
        } 
        $pdo->commit();
        return true;
    }catch(PDOException $e){
        $pdo->rollBack();
        return false;
    }
}

public function updateIdioma($datos){
    try{

        $pdo=$this->db->connect();
        $pdo->beginTransaction();

        $notas=$datos['nota'];
        $nota_actividad=$datos['id_inscripcion'];

        $query=$pdo->prepare("UPDATE nota_actividad_idioma_history
        SET nota = :nota, descripcion = :descripcion 
        WHERE id_inscripcion = :id_inscripcion");

        for ($i=0;$i<count($notas) && $i<count($nota_actividad);$i++)    
        {       
            if ($notas[$i] >= 60) {
                $query->execute([
                    'nota'=>$notas[$i],
                    'descripcion'=>'Aprobado',
                    'id_inscripcion'=>$nota_actividad[$i]
                ]);
            } else {
                $query->execute([
                    'nota'=>$notas[$i],
                    'descripcion'=>'Reprobado',
                    'id_inscripcion'=>$nota_actividad[$i]
                ]); 
            } 
        } 
        $pdo->commit();
        return true;
    }catch(PDOException $e){
        $pdo->rollBack();
        return false;
    }
}


////////////VALIDACIONES////////////
public function getExisteNota($datos){
    try{
        $inscripciones=$datos['id_inscripcion'];
        $query=$this->db->connect()->prepare("SELECT 
            id_inscripcion
            FROM 
            nota_actividad
            WHERE nota_actividad.id_inscripcion=:id_inscripcion;");

        for ($i=0;$i<count($inscripciones);$i++)    
        {                
            $query->execute([
                'id_inscripcion'=> $inscripciones[$i]
            ]);
            $var=$query->fetch(PDO::FETCH_ASSOC);
            if($var['id_inscripcion']==$inscripciones[$i]){

                return true;
            }
        }

    } catch(PDOException $e){
        return false;
    }
}

public function getExisteNotaIdioma($datos){
    try{

        $inscripciones=$datos['id_inscripcion'];
        $query=$this->db->connect()->prepare("SELECT 
            id_inscripcion
            FROM 
            nota_actividad_idioma
            WHERE nota_actividad_idioma.id_inscripcion=:id_inscripcion;");

        for ($i=0;$i<count($inscripciones);$i++)    
        {                
            $query->execute([
                'id_inscripcion'=> $inscripciones[$i]
            ]);
            $var=$query->fetch(PDO::FETCH_ASSOC);
            if($var['id_inscripcion']==$inscripciones[$i]){

                return true;
            }
        }
    } catch(PDOException $e){
        return false;
    }
}

public function getFechas(){
    $items = [];
    try{
        $query = $this->db->connect()->query("SELECT id_periodo, inicio_carga_notas, fin_carga_notas FROM periodo WHERE estatus='Activo' ");

        while($row = $query->fetch()){
            $item = new Confucio();
            $item->id_periodo         = $row['id_periodo'];
            $item->inicio_carga_notas = $row['inicio_carga_notas'];
            $item->fin_carga_notas    = $row['fin_carga_notas'];
            array_push($items, $item);
        }
        return $items;
    }catch(PDOException $e){
        return [];
    }
}

public function getbyID_roles($id_persona){

    $items=[];
    $query = $this->db->connect()->prepare("SELECT usuario_rol.id_usuario, usuario_rol.id_rol, rol.descripcion AS rol
    FROM usuario_rol, rol, usuario, persona
    WHERE usuario_rol.id_rol=rol.id_rol
    AND usuario_rol.id_usuario=usuario.id_usuario 
    AND usuario.id_persona=persona.id_persona
    AND persona.id_persona=:id_persona
    AND rol.id_rol=3");
    try{
        $query ->execute(['id_persona'=>$id_persona]);

        while($row = $query->fetch()){
            $item = new Confucio();
            $item->id_usuario = $row['id_usuario'];  
            $item->id_rol = $row['id_rol'];
            $item->rol = $row['rol'];
            array_push($items,$item);
        }

        return $items;
    }catch(PDOException  $e){
            return null;
    }
}


}
?>