<?php 
require_once 'models/confucio.php';
require_once 'models/perfil.php';
class Notas_historialModel extends Model
{
  public function __construct(){
    parent::__construct();
}

public function getSeccionesHistorial(){
    $items = [];
    try{
        $query = $this->db->connect()->query("SELECT seccion.id_seccion, seccion.descripcion, actividad.descripcion AS actividad, categoria, nivel.descripcion AS nivel, periodo.descripcion AS periodo, persona.primer_nombre, persona.primer_apellido
            FROM seccion, actividad, nivel, periodo, persona, docente
            WHERE seccion.id_actividad = actividad.id_actividad
            AND seccion.id_nivel = nivel.id_nivel
            AND seccion.id_periodo = periodo.id_periodo
            AND seccion.id_docente = docente.id_docente
            AND docente.id_persona = persona.id_persona
            AND periodo.estatus='Inactivo'");

        while($row = $query->fetch()){
            $item = new Confucio(); 
            $item->id_seccion  = $row['id_seccion'];
            $item->descripcion = $row['descripcion'];
            $item->actividad   = $row['actividad'];
            $item->nivel    = $row['nivel'];
            $item->periodo  = $row['periodo'];
            $item->categoria  = $row['categoria'];
            $item->primer_nombre    = $row['primer_nombre'];
            $item->primer_apellido  = $row['primer_apellido'];
            array_push($items, $item);
        }
        return $items;
    }catch(PDOException $e){
        return [];
    }
}

public function getDocenteHistorial($id){

    $items=[];
    $query=$this->db->connect()->prepare("SELECT seccion.id_seccion, seccion.descripcion AS seccion, 
    actividad.descripcion AS actividad, nivel.descripcion AS nivel, actividad.categoria, periodo.descripcion AS periodo
    FROM persona, docente, seccion, actividad, nivel, periodo
    WHERE seccion.id_actividad = actividad.id_actividad
    AND seccion.id_nivel = nivel.id_nivel
    AND seccion.id_docente = docente.id_docente
    AND docente.id_persona = persona.id_persona
    AND seccion.id_periodo = periodo.id_periodo
    AND persona.id_persona = :id");

    try{
        $query->execute(['id'=>$id]);
        while($row = $query->fetch()){
            $item=new Confucio();
            $item->id_seccion  = $row['id_seccion'];
            $item->seccion = $row['seccion'];
            $item->actividad    = $row['actividad'];
            $item->nivel    = $row['nivel'];
            $item->categoria    = $row['categoria'];
            $item->periodo    = $row['periodo'];
            array_push($items,$item);
        }

        return $items;
    }catch(PDOException  $e){
      return null;
  }
}

public function NotasHistorial_Otro($id){
    $items=[];
    $query=$this->db->connect()->prepare("SELECT nota_actividad_history.nota AS nota, nota_actividad_history.descripcion AS descripcion,
    identificacion, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido
       FROM nota_actividad_history, persona, seccion
       WHERE nota_actividad_history.id_persona=persona.id_persona
       AND nota_actividad_history.id_seccion=seccion.id_seccion
       AND seccion.id_seccion= :id");

    try{
        $query->execute(['id'=>$id]);
        while($row = $query->fetch()){
            $item=new Confucio();
            $item->nota  = $row['nota'];
            $item->descripcion  = $row['descripcion'];
            $item->identificacion  = $row['identificacion'];
            $item->primer_nombre  = $row['primer_nombre'];
            $item->segundo_nombre  = $row['segundo_nombre'];
            $item->primer_apellido  = $row['primer_apellido'];
            $item->segundo_apellido  = $row['segundo_apellido'];
            array_push($items,$item);
        }
        return $items;
    }catch(PDOException  $e){
      return null;
  }
}

public function NotasHistorial_Idioma($id){
    $items=[];
    $query=$this->db->connect()->prepare("SELECT nota_actividad_idioma_history.nota AS nota, nota_actividad_idioma_history.descripcion AS descripcion, id_inscripcion,
    identificacion, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido
       FROM nota_actividad_idioma_history, persona, seccion
       WHERE nota_actividad_idioma_history.id_persona=persona.id_persona
       AND nota_actividad_idioma_history.id_seccion=seccion.id_seccion
       AND seccion.id_seccion=:id");

    try{
        $query->execute(['id'=>$id]);
        while($row = $query->fetch()){
            $item=new Confucio();
            $item->nota  = $row['nota'];
            $item->descripcion  = $row['descripcion'];
            $item->identificacion  = $row['identificacion'];
            $item->primer_nombre  = $row['primer_nombre'];
            $item->segundo_nombre  = $row['segundo_nombre'];
            $item->primer_apellido  = $row['primer_apellido'];
            $item->segundo_apellido  = $row['segundo_apellido'];
            array_push($items,$item);
        }
        return $items;
    }catch(PDOException  $e){
      return null;
  }
}

public function getEgresados(){
    $items = [];
    try{
        $query = $this->db->connect()->query("SELECT nota, nota_actividad_idioma_history.descripcion, persona.id_persona, 
        identificacion, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido
        FROM public.nota_actividad_idioma_history, nivel, persona, seccion
        WHERE nota_actividad_idioma_history.id_nivel=nivel.id_nivel
        AND nota_actividad_idioma_history.id_persona=persona.id_persona
        AND nota_actividad_idioma_history.id_seccion=seccion.id_seccion
        AND nota_actividad_idioma_history.id_nivel=5
        AND nota_actividad_idioma_history.descripcion='Aprobado'");

        while($row = $query->fetch()){
            $item = new Confucio();
            $item->primer_nombre    = $row['primer_nombre'];
            $item->segundo_nombre   = $row['segundo_nombre'];
            $item->primer_apellido  = $row['primer_apellido'];
            $item->segundo_apellido = $row['segundo_apellido'];
            $item->identificacion   = $row['identificacion'];
            $item->nota             = $row['nota'];
            $item->descripcion      = $row['descripcion'];
            array_push($items, $item);
        }
        return $items;
    }catch(PDOException $e){
        return [];
    }
}



}
?>