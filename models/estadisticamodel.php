<?php

include_once 'models/confucio.php';

class EstadisticaModel extends Model{

   public function __construct()
    {
        parent::__construct();
    }
    
   

       
    public function lista_estadistica(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT  * FROM consulta ORDER BY id_consulta ");
            while($row=$query->fetch()){
                $item=new Confucio();
                
                $item->id_consulta=$row['id_consulta'];
                $item->descripcion=$row['descripcion'];
                array_push($items,$item);
            }
            return $items;
        }catch(PDOException $e){
            return [];
        }
    }
    public function consultas($estadistica){
        $items=[];
        if($estadistica==0){
            $query=$this->db->connect()->query("SELECT  * from consulta");
            while($row=$query->fetch()){
                $item=new Confucio();
                $item->nombre_consulta=$row['descripcion'];
                $item->id_consulta=$row['id_consulta'];
                $item->descripcion=$row['descripcion'];
                $item->tipo='0';
                array_push($items,$item);
            }
        }
        if($estadistica==1){
            $query=$this->db->connect()->query("SELECT (SELECT descripcion from consulta where id_consulta='$estadistica'), count(*) as cant,nivel_academico.descripcion as descripcion_nivel_academico,(SELECT COUNT(*) from aspirante where aspirante.estatus='1')as total from aspirante,nivel_academico where aspirante.id_nivel_academico=nivel_academico.id_nivel_academico and aspirante.estatus='1' group by nivel_academico.descripcion");  
            while($row=$query->fetch()){
                $item=new Confucio();
                 $item->nombre_consulta=$row['descripcion'];
                $item->cant=$row['cant'];
                $item->descripcion_nivel_academico=$row['descripcion_nivel_academico'];
                $item->total=$row['total'];
                $item->tipo='1';
                array_push($items,$item);
            }
        }
        if($estadistica==2){
            $query=$this->db->connect()->query("SELECT (SELECT descripcion from consulta where id_consulta='$estadistica'), count(*) as cant,datos_interes.estudiante_ubv as estudiante_ubv,(select count(*)from aspirante where aspirante.estatus='1') as total from aspirante,datos_interes where aspirante.id_datos_interes=datos_interes.id_datos_interes  and datos_interes.estudiante_ubv='SI' and aspirante.estatus='1' GROUP BY datos_interes.estudiante_ubv");  
            while($row=$query->fetch()){
                $item=new Confucio();
                 $item->nombre_consulta=$row['descripcion'];
                $item->cant=$row['cant'];
                $item->estudiante_ubv=$row['estudiante_ubv'];
                $item->total=$row['total'];
                $item->tipo='2';
                array_push($items,$item);
            }
        }
        if($estadistica==3){
            $query=$this->db->connect()->query("SELECT (SELECT descripcion from consulta where id_consulta='$estadistica'), count(*) as cant,cargo.descripcion as cargo_ubv,(select count(*)from aspirante where aspirante.estatus='1') as total,(select count(*)from aspirante,datos_interes where datos_interes.id_datos_interes=aspirante.id_datos_interes and aspirante.estatus='1' and datos_interes.mision_sucre='SI' group by datos_interes.mision_sucre) as mision_sucre from aspirante,dato_laboral,cargo where cargo.id_cargo=dato_laboral.id_cargo and aspirante.estatus='1' and dato_laboral.id_dato_laboral=aspirante.id_dato_laboral and dato_laboral.empleo='SI'and  dato_laboral.trabajador_ubv='SI'group by cargo.descripcion");  
            while($row=$query->fetch()){
                $item=new Confucio();
                 $item->nombre_consulta=$row['descripcion'];
                $item->cant=$row['cant'];
                $item->mision_sucre=$row['mision_sucre'];
                $item->cargo_ubv=$row['cargo_ubv'];
                $item->total=$row['total'];
                $item->tipo='3';
                array_push($items,$item);
            }
        }
        if($estadistica==4){
            $query=$this->db->connect()->query("SELECT (SELECT descripcion from consulta where id_consulta='$estadistica'), COUNT(*) as cantidad_persona_telefono,(SELECT COUNT(*)FROM aspirante,persona,direccion_domicilio where aspirante.id_persona=persona.id_persona and aspirante.id_direccion=direccion_domicilio.id_direccion and telefono_habitacion!='' and aspirante.estatus='1') as cantidad_persona_telefono_local FROM aspirante,persona where aspirante.id_persona=persona.id_persona and persona.telefono!='' and aspirante.estatus='1' ");  
               while($row=$query->fetch()){
                   $item=new Confucio();
                    $item->nombre_consulta=$row['descripcion'];
                   $item->cantidad_persona_telefono=$row['cantidad_persona_telefono'];
                   $item->cantidad_persona_telefono_local=$row['cantidad_persona_telefono_local'];
                   $item->total=$row['cantidad_persona_telefono'];
                   $item->tipo='4';
                   array_push($items,$item);
               }
           }
           if($estadistica==5){
            $query=$this->db->connect()->query("SELECT (SELECT descripcion from consulta where id_consulta='$estadistica'), COUNT(*)as pertenecientes_etnia , (SELECT COUNT(*) from aspirante where aspirante.estatus='1' ) as total from aspirante,direccion_domicilio where  direccion_domicilio.id_etnia!='89' and aspirante.id_direccion=direccion_domicilio.id_direccion and aspirante.estatus='1'");  
               while($row=$query->fetch()){
                   $item=new Confucio();
                    $item->nombre_consulta=$row['descripcion'];
                   $item->pertenecientes_etnia=$row['pertenecientes_etnia'];
                   $item->total=$row['total'];
                   $item->tipo='5';
                   array_push($items,$item);
               }
           }
           if($estadistica==6){
            $query=$this->db->connect()->query("SELECT (SELECT descripcion from consulta where id_consulta='$estadistica'), count(*)as cant,persona.genero as genero,(SELECT count(*)as total from aspirante,persona where aspirante.id_persona=persona.id_persona and aspirante.estatus='1' ) as total from aspirante,persona where aspirante.id_persona=persona.id_persona  and aspirante.estatus='1' GROUP BY persona.genero");  
               while($row=$query->fetch()){
                   $item=new Confucio();
                    $item->nombre_consulta=$row['descripcion'];
                   $item->cant=$row['cant'];
                   $item->genero=$row['genero'];
                   $item->total=$row['total'];
                   $item->tipo='6';
                   array_push($items,$item);
               }
           }
           if($estadistica==7){
            $query=$this->db->connect()->query("SELECT (SELECT descripcion from consulta where id_consulta='$estadistica'), count(*)as estudiantes_refieron_correo,(SELECT count(*)as Total from aspirante,persona where aspirante.id_persona=persona.id_persona and aspirante.estatus='1') as total from aspirante,persona where aspirante.id_persona=persona.id_persona and persona.correo!='' and aspirante.estatus='1'");  
               while($row=$query->fetch()){
                   $item=new Confucio();
                    $item->nombre_consulta=$row['descripcion'];
                   $item->estudiantes_refieron_correo=$row['estudiantes_refieron_correo'];
                   $item->total=$row['total'];
                   $item->tipo='7';
                   array_push($items,$item);
               }
           }
           if($estadistica==8){
            $query=$this->db->connect()->query("SELECT (SELECT descripcion from consulta where id_consulta='$estadistica'), count(*)as cant,tipo_documento_identidad.descripcion as nacionalidad,(SELECT count(*)as total from aspirante,persona where aspirante.id_persona=persona.id_persona and aspirante.estatus='1') as total from aspirante,persona,tipo_documento_identidad where aspirante.id_persona=persona.id_persona and persona.id_tipo_documento_identidad=tipo_documento_identidad.id_tipo_documento_identidad and aspirante.estatus='1' GROUP BY tipo_documento_identidad.descripcion");  
               while($row=$query->fetch()){
                   $item=new Confucio();
                    $item->nombre_consulta=$row['descripcion'];
                   $item->nacionalidad=$row['nacionalidad'];
                   $item->cant=$row['cant'];
                   $item->total=$row['total'];
                   $item->tipo='8';
                   array_push($items,$item);
               }
           }
           if($estadistica==9){
            $query=$this->db->connect()->query("SELECT (SELECT descripcion from consulta where id_consulta='$estadistica'), count(*) as cant, estado.descripcion as direccion,(SELECT count(*) from aspirante,direccion_domicilio  where  direccion_domicilio.id_direccion=aspirante.id_direccion and aspirante.estatus='1') as total  from aspirante,direccion_domicilio,estado  where  direccion_domicilio.id_direccion=aspirante.id_direccion and direccion_domicilio.id_estado=estado.id_estado and aspirante.estatus='1' GROUP BY estado.id_estado ORDER BY estado.id_estado");  
               while($row=$query->fetch()){
                   $item=new Confucio();
                    $item->nombre_consulta=$row['descripcion'];
                   $item->direccion=$row['direccion'];
                   $item->cant=$row['cant'];
                   $item->total=$row['total'];
                   $item->tipo='9';
                   array_push($items,$item);
               }
           }
           if($estadistica==10){
            $query=$this->db->connect()->query("SELECT (SELECT descripcion from consulta where id_consulta='$estadistica'), count(*) as cant, estado_civil.descripcion as estado_civil,(SELECT count(*) from aspirante where  aspirante.estatus='1') as total  from aspirante, estado_civil   where estado_civil.id_civil=aspirante.id_civil and aspirante.estatus='1' GROUP BY estado_civil.descripcion");  
               while($row=$query->fetch()){
                   $item=new Confucio();
                    $item->nombre_consulta=$row['descripcion'];
                   $item->estado_civil=$row['estado_civil'];
                   $item->cant=$row['cant'];
                   $item->total=$row['total'];
                   $item->tipo='10';
                   array_push($items,$item);
               }
           }
           if($estadistica==11){
            $query=$this->db->connect()->query("SELECT (SELECT descripcion from consulta where id_consulta='$estadistica'), count(*) as cant,datos_interes.discapacidad  as discapacidad,(select count(*)from aspirante where aspirante.estatus='1') as total from aspirante,datos_interes where aspirante.id_datos_interes=datos_interes.id_datos_interes and datos_interes.discapacidad='SI' and aspirante.estatus='1' group by datos_interes.discapacidad");  
               while($row=$query->fetch()){
                   $item=new Confucio();
                    $item->nombre_consulta=$row['descripcion'];
                   $item->discapacidad=$row['discapacidad'];
                   $item->cant=$row['cant'];
                   $item->total=$row['total'];
                   $item->tipo='11';
                   array_push($items,$item);
               }
           }
           if($estadistica==12){
            $query=$this->db->connect()->query("SELECT (SELECT descripcion from consulta where id_consulta='$estadistica'), count(*) as cant,tipo_discapacidad.descripcion as discapacidad,(select count(*)from aspirante where aspirante.estatus='1') as total from aspirante,datos_interes,tipo_discapacidad where aspirante.id_datos_interes=datos_interes.id_datos_interes and datos_interes.id_tipo_discapacidad=tipo_discapacidad.id_tipo_discapacidad  and datos_interes.discapacidad='SI' and aspirante.estatus='1' GROUP BY tipo_discapacidad.descripcion");  
               while($row=$query->fetch()){
                   $item=new Confucio();
                    $item->nombre_consulta=$row['descripcion'];
                   $item->discapacidad=$row['discapacidad'];
                   $item->cant=$row['cant'];
                   $item->total=$row['total'];
                   $item->tipo='12';
                   array_push($items,$item);
               }
           }
           if($estadistica==13){
            $query=$this->db->connect()->query("SELECT (SELECT descripcion from consulta where id_consulta='$estadistica'), count(*) as cant,datos_interes.enfermedad  as enfermedad,(select count(*)from aspirante where aspirante.estatus='1') as total from aspirante,datos_interes where aspirante.id_datos_interes=datos_interes.id_datos_interes and datos_interes.enfermedad='SI' and aspirante.estatus='1' group by datos_interes.enfermedad");  
               while($row=$query->fetch()){
                   $item=new Confucio();
                    $item->nombre_consulta=$row['descripcion'];
                   $item->enfermedad=$row['enfermedad'];
                   $item->cant=$row['cant'];
                   $item->total=$row['total'];
                   $item->tipo='13';
                   array_push($items,$item);
               }
           }
           if($estadistica==14){
            $query=$this->db->connect()->query("SELECT (SELECT descripcion from consulta where id_consulta='$estadistica'), count(*) as mision_sucre,(select count(*)from aspirante where aspirante.estatus='1') as total from aspirante,datos_interes where datos_interes.id_datos_interes=aspirante.id_datos_interes and aspirante.estatus='1' and datos_interes.mision_sucre='SI' group by datos_interes.mision_sucre");  
            while($row=$query->fetch()){
                $item=new Confucio();
                 $item->nombre_consulta=$row['descripcion'];
                $item->mision_sucre=$row['mision_sucre'];
                $item->total=$row['total'];
                $item->tipo='14';
                array_push($items,$item);
            }
        }
           if($estadistica==15){
            $query=$this->db->connect()->query("SELECT (SELECT descripcion from consulta where id_consulta='$estadistica'), count(*) as cant,datos_interes.descripcion_enfermedad as enfermedad,(select count(*)from aspirante where aspirante.estatus='1') as total from aspirante,datos_interes where aspirante.id_datos_interes=datos_interes.id_datos_interes  and datos_interes.enfermedad='SI' and aspirante.estatus='1' GROUP BY datos_interes.descripcion_enfermedad");  
               while($row=$query->fetch()){
                   $item=new Confucio();
                    $item->nombre_consulta=$row['descripcion'];
                   $item->enfermedad=$row['enfermedad'];
                   $item->cant=$row['cant'];
                   $item->total=$row['total'];
                   $item->tipo='15';
                   array_push($items,$item);
               }
           }
           if($estadistica==16){
            $query=$this->db->connect()->query("SELECT (SELECT descripcion from consulta where id_consulta='$estadistica'), count(*) as cant,(select count(*)from aspirante where aspirante.estatus='1') as total from aspirante,dato_laboral where aspirante.estatus='1' and dato_laboral.id_dato_laboral=aspirante.id_dato_laboral and dato_laboral.empleo='SI'");  
               while($row=$query->fetch()){
                   $item=new Confucio();
                    $item->nombre_consulta=$row['descripcion'];
             
                   $item->cant=$row['cant'];
                   $item->total=$row['total'];
                   $item->tipo='16';
                   array_push($items,$item);
               }
           }
           if($estadistica==17){
            $query=$this->db->connect()->query("SELECT (SELECT descripcion from consulta where id_consulta='$estadistica'), count(*) as cant,dato_laboral.tipo_institucion as tipo_institucion,(select count(*)from aspirante where aspirante.estatus='1') as total from aspirante,dato_laboral where aspirante.estatus='1' and dato_laboral.id_dato_laboral=aspirante.id_dato_laboral group by dato_laboral.tipo_institucion");  
               while($row=$query->fetch()){
                   $item=new Confucio();
                    $item->nombre_consulta=$row['descripcion'];
                   $item->tipo_institucion=$row['tipo_institucion'];
                   $item->cant=$row['cant'];
                   $item->total=$row['total'];
                   $item->tipo='17';
                   array_push($items,$item);
               }
           }
           if($estadistica==18){
            $query=$this->db->connect()->query("SELECT (SELECT descripcion from consulta where id_consulta='$estadistica'), count(*) as docentes from persona,docente where persona.id_persona=docente.id_persona");  
               while($row=$query->fetch()){
                   $item=new Confucio();
                    $item->nombre_consulta=$row['descripcion'];
                   $item->docentes=$row['docentes'];
                   $item->tipo='18';
                   array_push($items,$item);
               }
           }
           if($estadistica==19){
            $query=$this->db->connect()->query("SELECT (SELECT descripcion from consulta where id_consulta='$estadistica'), COUNT(*) as cant,seccion.descripcion as cursos,(select count(*)from aspirante where aspirante.estatus='1') as total from aspirante,oferta_academica,seccion where aspirante.id_oferta_academica=oferta_academica.id_oferta_academica and oferta_academica.id_seccion=seccion.id_seccion and aspirante.estatus='1' GROUP BY seccion.descripcion ORDER BY seccion.descripcion ASC");  
               while($row=$query->fetch()){
                   $item=new Confucio();
                    $item->nombre_consulta=$row['descripcion'];
                   $item->cant=$row['cant'];
                   $item->cursos=$row['cursos'];
                   $item->total=$row['total'];
                   $item->tipo='19';
                   array_push($items,$item);
               }
           }
          
           if($estadistica==20){
            $query=$this->db->connect()->query("SELECT (SELECT descripcion from consulta where id_consulta='$estadistica'), COUNT(*) as cant,seccion.descripcion as cursos, periodo.descripcion as periodo ,(select count(*)from aspirante where aspirante.estatus='1') as total from aspirante,oferta_academica,seccion,periodo where periodo.id_periodo=seccion.id_periodo and aspirante.id_oferta_academica=oferta_academica.id_oferta_academica and oferta_academica.id_seccion=seccion.id_seccion and aspirante.estatus='1' group by seccion.descripcion ,periodo.descripcion");  
               while($row=$query->fetch()){
                   $item=new Confucio();
                    $item->nombre_consulta=$row['descripcion'];
                   $item->cant=$row['cant'];
                   $item->cursos=$row['cursos'];
                   $item->periodo=$row['periodo'];
                   $item->total=$row['total'];
                   $item->tipo='20';
                   array_push($items,$item);
               }
           }
           if($estadistica==21){
            $query=$this->db->connect()->query("SELECT (SELECT descripcion from consulta where id_consulta='$estadistica'), COUNT(*) as cant,seccion.descripcion as cursos,nivel.descripcion as nivel ,periodo.descripcion as periodo ,(select count(*)from aspirante where aspirante.estatus='1') as total, (SELECT count(*) from aspirante,oferta_academica,seccion,periodo,nivel,nota_actividad_idioma   where seccion.id_nivel=nivel.id_nivel and periodo.id_periodo=seccion.id_periodo and aspirante.id_oferta_academica=oferta_academica.id_oferta_academica and oferta_academica.id_seccion=seccion.id_seccion and aspirante.estatus='1' and aspirante.id_persona=nota_actividad_idioma.id_persona and nota_actividad_idioma.nota >= '60') as total_aprobado from aspirante,oferta_academica,seccion,periodo,nivel,nota_actividad_idioma   where seccion.id_nivel=nivel.id_nivel and periodo.id_periodo=seccion.id_periodo and aspirante.id_oferta_academica=oferta_academica.id_oferta_academica and oferta_academica.id_seccion=seccion.id_seccion and aspirante.estatus='1' and aspirante.id_persona=nota_actividad_idioma.id_persona and nota_actividad_idioma.nota >= '60' group by seccion.descripcion ,periodo.descripcion,nivel.descripcion");  
               while($row=$query->fetch()){
                   $item=new Confucio();
                    $item->nombre_consulta=$row['descripcion'];
                   $item->cant=$row['cant'];
                   $item->cursos=$row['cursos'];
                   $item->nivel=$row['nivel'];
                   $item->periodo=$row['periodo'];
                   $item->total_aprobado=$row['total_aprobado'];
                   $item->total=$row['total'];
                   $item->tipo='21';
                   array_push($items,$item);
               }
           }
           if($estadistica==22){
            $query=$this->db->connect()->query("SELECT (SELECT descripcion from consulta where id_consulta='$estadistica'), COUNT(*) as cant,seccion.descripcion as cursos,nivel.descripcion as nivel ,periodo.descripcion as periodo ,(select count(*)from aspirante where aspirante.estatus='1') as total, (SELECT count(*) from aspirante,oferta_academica,seccion,periodo,nivel,nota_actividad   where seccion.id_nivel=nivel.id_nivel and periodo.id_periodo=seccion.id_periodo and aspirante.id_oferta_academica=oferta_academica.id_oferta_academica and oferta_academica.id_seccion=seccion.id_seccion and aspirante.estatus='1' and aspirante.id_persona=nota_actividad.id_persona and nota_actividad.nota >= '60') as total_aprobado from aspirante,oferta_academica,seccion,periodo,nivel,nota_actividad   where seccion.id_nivel=nivel.id_nivel and periodo.id_periodo=seccion.id_periodo and aspirante.id_oferta_academica=oferta_academica.id_oferta_academica and oferta_academica.id_seccion=seccion.id_seccion and aspirante.estatus='1' and aspirante.id_persona=nota_actividad.id_persona and nota_actividad.nota >= '60' group by seccion.descripcion ,periodo.descripcion,nivel.descripcion");  
               while($row=$query->fetch()){
                   $item=new Confucio();
                    $item->nombre_consulta=$row['descripcion'];
                   $item->cant=$row['cant'];
                   $item->cursos=$row['cursos'];
                   $item->nivel=$row['nivel'];
                   $item->periodo=$row['periodo'];
                   $item->total_aprobado=$row['total_aprobado'];
                   $item->total=$row['total'];
                   $item->tipo='22';
                   array_push($items,$item);
               }
           }
           if($estadistica==23){
            $query=$this->db->connect()->query("SELECT (SELECT descripcion from consulta where id_consulta='$estadistica'), count(*) as cant,exonerado,modalidad_exo,(select count(*)from aspirante where aspirante.estatus='1') as total from aspirante,deposito where aspirante.estatus='1' and deposito.id_aspirante=aspirante.id_aspirante group by exonerado,modalidad_exo");  
            while($row=$query->fetch()){
                $item=new Confucio();
                 $item->nombre_consulta=$row['descripcion'];
                $item->cant=$row['cant'];
                $item->exonerado=$row['exonerado'];
                $item->modalidad_exo=$row['modalidad_exo'];
                $item->total=$row['total'];
                $item->tipo='23';
                array_push($items,$item);
            }
        }
        if($estadistica==24){
            $query=$this->db->connect()->query("SELECT (SELECT descripcion from consulta where id_consulta='$estadistica'), count(*) as cant ,idioma,(select count(*)from aspirante where aspirante.estatus='1') as total from aspirante,datos_interes where aspirante.estatus='1' and datos_interes.id_datos_interes=aspirante.id_datos_interes group by idioma");  
            while($row=$query->fetch()){
                $item=new Confucio();
                 $item->nombre_consulta=$row['descripcion'];
                $item->cant=$row['cant'];
                $item->idioma=$row['idioma'];
                $item->modalidad_exo=$row['modalidad_exo'];
                $item->total=$row['total'];
                $item->tipo='24';
                array_push($items,$item);
            }
        }
        if($estadistica==25){
            $query=$this->db->connect()->query("SELECT (SELECT descripcion from consulta where id_consulta='$estadistica'), COUNT(*) as cant,nivel.descripcion as nivel ,periodo.descripcion as periodo ,(select count(*)from aspirante where aspirante.estatus='1') as total from aspirante,oferta_academica,seccion,periodo,nivel where seccion.id_nivel=nivel.id_nivel and periodo.id_periodo=seccion.id_periodo and aspirante.id_oferta_academica=oferta_academica.id_oferta_academica and oferta_academica.id_seccion=seccion.id_seccion and aspirante.estatus='1' group by periodo.descripcion,nivel.descripcion ORDER BY nivel.descripcion");  
            while($row=$query->fetch()){
                $item=new Confucio();
                 $item->nombre_consulta=$row['descripcion'];
                $item->cant=$row['cant'];
                $item->periodo=$row['periodo'];
                $item->nivel=$row['nivel'];
                $item->total=$row['total'];
                $item->tipo='25';
                array_push($items,$item);
            }
        }
        if($estadistica==26){
            $query=$this->db->connect()->query("SELECT (SELECT descripcion from consulta where id_consulta='$estadistica'), COUNT(*) as cant,seccion.descripcion as cursos,(select count(*)from aspirante where aspirante.estatus='0') as total from aspirante,oferta_academica,seccion where aspirante.id_oferta_academica=oferta_academica.id_oferta_academica and oferta_academica.id_seccion=seccion.id_seccion and aspirante.estatus='0' GROUP BY seccion.descripcion ORDER BY seccion.descripcion ASC");  
            while($row=$query->fetch()){
                $item=new Confucio();
                 $item->nombre_consulta=$row['descripcion'];
                $item->cant=$row['cant'];
                $item->cursos=$row['cursos'];
                $item->total=$row['total'];
                $item->tipo='26';
                array_push($items,$item);
            }
        }
        try{
                      
            
            return $items;
        }catch(PDOException $e){
            return [];
        }
    }


}

?>