<?php
include_once 'models/confucio.php';
class ComboModel extends Model{
    public function __construct(){
    parent::__construct();
    }
  
        //////////////////////////////////////////////////////////////////////
          //  GET PARA COMBO DEPENDIENTE DE PAIS

          public function getByPaisEstado($id_pais){
            $items=[];
           try{
          $query=$this->db->connect()->prepare("SELECT * FROM estado WHERE id_pais =:id_pais");
          $query->execute(['id_pais'=>$id_pais]);
          while($row=$query->fetch()){
          $item=new Confucio();
          $item->id_estado=$row['id_estado'];
          $item->descripcion=$row['descripcion'];
          array_push($items,$item);
          }
          return $items;
          }catch(PDOException $e){
          return[];
          }
          
          }
          
          public function getByEstadoMunicipio($id_estado){
            $items=[];
           try{
          $query=$this->db->connect()->prepare("SELECT * FROM municipio WHERE id_estado =:id_estado");
          $query->execute(['id_estado'=>$id_estado]);
          while($row=$query->fetch()){
          $item=new Confucio();
          $item->id_municipio=$row['id_municipio'];
          $item->descripcion=$row['descripcion'];
          array_push($items,$item);    
          }
          return $items;
          }catch(PDOException $e){
          return[];
          }
          
          }


          public function getByMunicipioParroquia($id_municipio){
            $items=[];
           try{
          $query=$this->db->connect()->prepare("SELECT * FROM parroquia WHERE id_municipio  =:id_municipio");
          $query->execute(['id_municipio'=>$id_municipio]);
          while($row=$query->fetch()){
          $item=new Confucio();
          $item->id_parroquia=$row['id_parroquia'];
          $item->descripcion=$row['descripcion'];
          array_push($items,$item);   
          }
          return $items;  
          }catch(PDOException $e){
          return[];
          }
          
          }
      
     

          ///////////////////////////////////////////////////////////////////
                //validar este get si funciona correctamente 
                public function getCatalogoEstatus($valor){
                    $items=[];
                try{
                $query=$this->db->connect()->query("SELECT * FROM  \"$valor\" WHERE estatus='1' ");

                while($row=$query->fetch()){
                $item=new Confucio();
                $item->id=$row['id_'.$valor.''];
                $item->descripcion=$row['descripcion'];

                array_push($items,$item);
                
                }
                return $items;
                
                }catch(PDOException $e){
                return[];
                }
                
                }  
////////////////////////////////////////////////////////////////////////


    }

    ?>