<?php 
require_once 'models/confucio.php';
class ActividadModel extends Model
{
	
  public function __construct(){
    parent::__construct();
}

///////////////////////////////////////////CONSULTAS////////////////////////////////////////////////
///OBTERNER LOS DATOS DE LA BD
public function get(){
    $items = [];
    try{
        $query = $this->db->connect()->query('SELECT * FROM actividad ORDER BY id_actividad');

        while($row = $query->fetch()){
            
            $item = new Confucio();
            $item->id_actividad = $row['id_actividad'];
            $item->descripcion  = $row['descripcion'];
            $item->categoria  = $row['categoria'];

            array_push($items, $item);
        }
        return $items;
    }catch(PDOException $e){
        return [];
    }
}

///OBTERNER LOS DATOS DE LA BD POR ID
public function getbyID($id){

    try{
        $query=$this->db->connect()->prepare("SELECT * FROM actividad WHERE id_actividad = :id");
        $query->execute(['id'=>$id]);
        $item=new Confucio();

        while($row=$query->fetch()){
            $item->id_actividad = $row['id_actividad'];
            $item->descripcion  = $row['descripcion'];
            $item->categoria  = $row['categoria'];
        }
        return $item;
    }catch(PDOException $e){
       return false;
   }
}

///////////////////////////////////////INSERT///////////////////////////////////////////////////////////////
public function insert($datos){
    try{

        $pdo=$this->db->connect();
        $pdo->beginTransaction();

        $query_insert=$pdo->prepare('INSERT INTO actividad (descripcion, categoria) VALUES (:descripcion, :categoria)');

        $query_insert->execute([
            'descripcion' => $datos['descripcion'],
            'categoria' => $datos['categoria']
        ]);

        $pdo->commit();
        return true;

    }catch(PDOException $e){
        $pdo->rollBack();
        return false;
    }
}

////////////////////////////////////////UPDATE////////////////////////////////////////////////
public function update($item){

    try{

        $pdo=$this->db->connect();
        $pdo->beginTransaction();
        $query=$pdo->prepare("UPDATE actividad SET descripcion = :descripcion, categoria = :categoria  
            WHERE id_actividad = :id_actividad");

        $query->execute([
            'id_actividad'=>$item['id_actividad'],
            'descripcion'=>$item['descripcion'],
            'categoria'=>$item['categoria']
        ]);

        $pdo->commit();
        return true;

    }catch(PDOException $e){

        $pdo->rollBack();
        return false;
    }
}


/////////////////////////////////////////////////DELETE///////////////////////////////////////////////////
public function delete($id){
    $query = $this->db->connect()->prepare('DELETE FROM actividad WHERE id_actividad = :id_actividad');
    try{
        $query->execute([
            'id_actividad' => $id
        ]);
        return true;
    }catch(PDOException $e){
        return false;
    }
}


}

?>