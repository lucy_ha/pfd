<?php 
require_once 'models/confucio.php';

class PeriodoModel extends Model{	
  public function __construct(){
    parent::__construct();
}

///////////////////////////////////////////CONSULTAS/////////////////////////////////////////
public function get(){
    $items = [];
    try{
        $query = $this->db->connect()->query("SELECT*FROM periodo");

        while($row = $query->fetch()){

            $item = new Confucio();
            $item->id_periodo = $row['id_periodo'];
            $item->descripcion  = $row['descripcion'];
            $item->inicio_periodo  = $row['inicio_periodo'];
            $item->fin_periodo  = $row['fin_periodo'];
            $item->inicio_carga_notas  = $row['inicio_carga_notas'];
            $item->fin_carga_notas  = $row['fin_carga_notas'];
            $item->estatus  = $row['estatus'];
            array_push($items, $item);
        }
        return $items;
    }catch(PDOException $e){
        return [];
    }
}

///OBTERNER LOS DATOS DE LA BD POR ID
public function getbyID($id){

    try{
        $query=$this->db->connect()->prepare("SELECT*FROM periodo WHERE id_periodo = :id");
        $query->execute(['id'=>$id]);
        $item=new Confucio();

        while($row=$query->fetch()){
            $item->id_periodo = $row['id_periodo'];
            $item->descripcion  = $row['descripcion'];
            $item->inicio_periodo  = $row['inicio_periodo'];
            $item->fin_periodo  = $row['fin_periodo'];
            $item->inicio_carga_notas  = $row['inicio_carga_notas'];
            $item->fin_carga_notas  = $row['fin_carga_notas'];
            $item->estatus  = $row['estatus'];
        }
        return $item;
    }catch(PDOException $e){
       return false;
   }
}

///////////////////////////////////////INSERT///////////////////////////////////////////////////////////////
public function insert($datos){
    try{

        $pdo=$this->db->connect();
        $pdo->beginTransaction();

        $query_insert=$pdo->prepare('INSERT INTO periodo (descripcion, inicio_periodo, fin_periodo, inicio_carga_notas, fin_carga_notas, estatus) 
            VALUES (:descripcion, :inicio_periodo, :fin_periodo, :inicio_carga_notas, :fin_carga_notas, :estatus)');

        $query_insert->execute([
            'descripcion' => $datos['descripcion'],
            'inicio_periodo' => $datos['inicio_periodo'],
            'fin_periodo' => $datos['fin_periodo'],
            'inicio_carga_notas' => $datos['inicio_carga_notas'],
            'fin_carga_notas' => $datos['fin_carga_notas'],
            'estatus' => $datos['estatus']
        ]);

        $pdo->commit();
        return true;

    }catch(PDOException $e){
        $pdo->rollBack();
        return false;
    }
}

////////////////////////////////////////UPDATE////////////////////////////////////////////////
public function update($item){

    try{

        $pdo=$this->db->connect();
        $pdo->beginTransaction();
        $query=$pdo->prepare("UPDATE periodo
            SET descripcion = :descripcion, inicio_periodo = :inicio_periodo, fin_periodo = :fin_periodo, inicio_carga_notas = :inicio_carga_notas, fin_carga_notas = :fin_carga_notas, estatus = :estatus
            WHERE id_periodo = :id_periodo");

        $query->execute([
            'id_periodo'=>$item['id_periodo'],
            'descripcion'=>$item['descripcion'],
            'inicio_periodo'=>$item['inicio_periodo'],
            'fin_periodo'=>$item['fin_periodo'],
            'inicio_carga_notas'=>$item['inicio_carga_notas'],
            'fin_carga_notas'=>$item['fin_carga_notas'],
            'estatus'=>$item['estatus']
        ]);

        $pdo->commit();
        return true;

    }catch(PDOException $e){
        $pdo->rollBack();
        return false;
    }
}



}

?>