<?php 
require_once 'models/confucio.php';
class NivelModel extends Model
{
	
  public function __construct(){
    parent::__construct();
}
///////////////////////////////////////////CONSULTAS////////////////////////////////////////////////
public function get(){
    $items = [];
    try{
        $query = $this->db->connect()->query("SELECT * FROM nivel ORDER BY descripcion");

        while($row = $query->fetch()){
            $item = new Confucio();
            $item->id_nivel = $row['id_nivel'];
            $item->descripcion  = $row['descripcion'];

            array_push($items, $item);
        }
        return $items;
    }catch(PDOException $e){
        return [];
    }
}
///
public function getbyID($id){

    try{
        $query=$this->db->connect()->prepare("SELECT * FROM nivel WHERE id_nivel = :id");
        $query->execute(['id'=>$id]);
        $item=new Confucio();

        while($row=$query->fetch()){
            $item->id_nivel = $row['id_nivel'];
            $item->descripcion  = $row['descripcion'];
        }
        return $item;
    }catch(PDOException $e){
     return false;
 }
}

////////////////////////////////////////////INSERT///////////////////////////////////////////////////////////
public function insert($datos){
    try{

        $pdo=$this->db->connect();
        $pdo->beginTransaction();

        $query_insert=$pdo->prepare('INSERT INTO nivel (descripcion) VALUES (:descripcion)');

        $query_insert->execute([
            'descripcion' => $datos['descripcion']
        ]);

        $pdo->commit();
        return true;

    }catch(PDOException $e){
        $pdo->rollBack();
        return false;
    }
}

////////////////////////////////////////UPDATE////////////////////////////////////////////////
public function update($item){

    try{

        $pdo=$this->db->connect();
        $pdo->beginTransaction();
        $query=$pdo->prepare("UPDATE nivel SET descripcion = :descripcion WHERE id_nivel = :id_nivel");

        $query->execute([
            'descripcion'=>$item['descripcion'], 
            'id_nivel'=>$item['id_nivel']
        ]);

        $pdo->commit();
        return true;

    }catch(PDOException $e){

        $pdo->rollBack();
        return false;
    }
}

/////////////////////////////////////////////////DELETE///////////////////////////////////////////////////
public function delete($id){
    $query = $this->db->connect()->prepare("DELETE FROM nivel WHERE id_nivel = :id_nivel");
    try{
        $query->execute([
            'id_nivel' => $id
        ]);
        return true;
    }catch(PDOException $e){
        return false;
    }
}



}

?>