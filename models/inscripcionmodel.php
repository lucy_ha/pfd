<?php

require_once 'models/confucio.php';

class InscripcionModel extends Model{


function __construct()
{
    parent::__construct();
    
}

/////avance de nivel en nota de Idiomas 
public function update($datos){

//var_dump($datos['id_oferta_academica']);
//var_dump($datos['id_aspirante']);

    try{

        $pdo=$this->db->connect();
  
        $pdo->beginTransaction();


          $reingreso=$pdo->prepare("UPDATE aspirante SET estatus=:estatus, id_oferta_academica=:id_oferta_academica WHERE id_aspirante=id_aspirante AND id_persona=:id_persona");
          $reingreso->execute(['estatus'=>0, 'id_oferta_academica'=>$datos['id_oferta_academica'], 'id_persona'=>$datos['id_persona']]);

         $reingreso=$pdo->prepare("DELETE FROM nota_actividad_idioma WHERE id_inscripcion=:id_inscripcion");
         $reingreso->execute(['id_inscripcion'=>$datos['id_inscripcion']]);

          $reingreso=$pdo->prepare("DELETE FROM inscripcion WHERE id_inscripcion=:id_inscripcion");
          $reingreso->execute(['id_inscripcion'=>$datos['id_inscripcion']]);

          $reingreso=$pdo->prepare("DELETE FROM usuario WHERE id_persona=:id_persona");
          $reingreso->execute(['id_persona'=>$datos['id_persona']]);
    

        $pdo->commit();
  
        return true;
  
    }catch(PDOException $e ){
  
        $pdo->rollBack();
        return false;
  
    }

}

/// almacenar registro de nota del curso sin prosecución
// Nota: Para realizar esta función, debemos hacer un disparador que inserte la nota registrada y que quede como un historico y limpiar la tabla donde fue registrada esa nota por primera vez la cual es: nota_actividad. Esto con la intención de que hayas registros de notas nuevos y los viejos queden como historico. /////
public function delete($id_nota_actividad){
  $query=$this->db->connect()->prepare('DELETE FROM nota_actividad WHERE id_nota_actividad = :id_nota_actividad');
  try{
      $query->execute([
          'id_nota_actividad' => $id_nota_actividad
      ]);
      return true;
  }catch(PDOException $e){
      return false;
  }
}


////////////////////////////////////// CONSULTAS //////////////////////////////////////////////////////

public function getCursoIdioma($id_persona){

    try{
        $query=$this->db->connect()->prepare("SELECT seccion.id_seccion, persona.id_persona, nivel.id_nivel, nota_actividad_idioma.id_nota_actividad_idioma, seccion.descripcion as seccion , nivel.descripcion as nivel,
        nota_actividad_idioma_history.nota, nota_actividad_idioma_history.descripcion AS estatus_escrito, actividad.categoria
                FROM persona, seccion, nota_actividad_idioma_history, nota_actividad_idioma, nivel, inscripcion, aspirante, oferta_academica, actividad
                WHERE seccion.id_nivel=nivel.id_nivel
                AND nota_actividad_idioma_history.id_nivel=nivel.id_nivel
                AND nota_actividad_idioma_history.id_inscripcion=inscripcion.id_inscripcion
				AND nota_actividad_idioma.id_nivel=nivel.id_nivel
                AND nota_actividad_idioma.id_inscripcion=inscripcion.id_inscripcion
                AND aspirante.id_persona=persona.id_persona 
                AND inscripcion.id_oferta_academica=oferta_academica.id_oferta_academica
                AND aspirante.id_aspirante=inscripcion.id_aspirante 
                AND seccion.id_seccion=oferta_academica.id_seccion
                AND seccion.id_actividad=actividad.id_actividad
                AND actividad.categoria='Idioma'
                AND persona.id_persona=:id_persona");
        $query->execute(['id_persona'=>$id_persona]);
        $item=new Confucio();
  
        while($row=$query->fetch()){
            $item->id_nota_actividad_idioma=$row['id_nota_actividad_idioma'];
            $item->id_aspirante=$row['id_aspirante'];
            $item->id_seccion=$row['id_seccion'];
            $item->id_persona=$row['id_persona'];
            $item->id_nivel=$row['id_nivel'];
            $item->seccion=$row['seccion'];
            $item->nivel=$row['nivel'];
            $item->nota=$row['nota'];
            $item->categoria=$row['categoria'];
            $item->estatus_escrito=$row['estatus_escrito'];
        }
        
        return $item;
      
    }catch(PDOException $e){
        return false;
    }
    
  }



  public function getCurso($id_persona){

    try{
        $query=$this->db->connect()->prepare("SELECT seccion.id_seccion, persona.id_persona, nivel.id_nivel, nota_actividad.id_nota_actividad, seccion.descripcion as seccion , nivel.descripcion as nivel,
        nota_actividad.nota, nota_actividad.descripcion AS estatus_escrito, actividad.categoria
                FROM persona, seccion, nota_actividad, nivel, inscripcion, aspirante, oferta_academica, actividad
                WHERE seccion.id_nivel=nivel.id_nivel
                AND nota_actividad.id_inscripcion=inscripcion.id_inscripcion
                AND aspirante.id_persona=persona.id_persona 
                AND inscripcion.id_oferta_academica=oferta_academica.id_oferta_academica
                AND aspirante.id_aspirante=inscripcion.id_aspirante 
                AND persona.id_persona=nota_actividad.id_persona 
                AND seccion.id_seccion=oferta_academica.id_seccion
                AND seccion.id_actividad=actividad.id_actividad
                AND actividad.categoria='Otro'
                AND persona.id_persona=:id_persona");
        $query->execute(['id_persona'=>$id_persona]);
        $item=new Confucio();
  
        while($row=$query->fetch()){
            $item->id_nota_actividad=$row['id_nota_actividad'];
            $item->id_aspirante=$row['id_aspirante'];
            $item->id_seccion=$row['id_seccion'];
            $item->id_persona=$row['id_persona'];
            $item->seccion=$row['seccion'];
            $item->nivel=$row['nivel'];
            $item->nota=$row['nota'];
            $item->estatus_escrito=$row['estatus_escrito'];
            $item->categoria=$row['categoria'];
        }
        
        return $item;
      
    }catch(PDOException $e){
        return false;
    }
    
  }


public function getbyEstudiante($id_persona){

    try{
        $query=$this->db->connect()->prepare("SELECT aspirante.id_aspirante, aspirante.id_persona AS persona, primer_nombre, segundo_nombre, primer_apellido, 
        segundo_apellido, identificacion, correo, telefono, genero, tipo_documento_identidad.descripcion AS tipo_documento,
        fecha_nacimiento, pais.descripcion AS pais, direccion, telefono_habitacion, etnia.descripcion AS etnia, estado_civil.descripcion as civil,
        empleo, telefono_institucion, correo_institucion, direccion_institucion, tipo_institucion, inst_publica, inst_privada, organismo, inst_inv, cargo.descripcion as cargo, trabajador_ubv,
        discapacidad, grupo_sanguineo, enfermedad, descripcion_enfermedad, tratamiento, idioma, idiomas_descripcion, tipo_discapacidad.descripcion AS tipo, nivel_academico.descripcion as nivel_aca,
         estado.descripcion AS estado, estudiante_ubv, mision_sucre, institucion_nivel, aspirante.id_oferta_academica, seccion.descripcion as oferta, modalidad.descripcion AS modalidad, turno.descripcion AS turno, hora_inicio, 
         hora_fin, aspirante.estatus, deposito.id_deposito, deposito.exonerado, modalidad_exo, numero_vouche, monto, fecha_deposito, deposito.observacion, numero_vouche_50, monto_50, fecha_deposito_50, deposito.observacion_50 as observacion_50 
        FROM aspirante, persona, tipo_documento_identidad, persona_nacimiento, direccion_domicilio, etnia, dato_laboral, datos_interes, tipo_discapacidad, pais, estado, seccion, oferta_academica, modalidad, turno, horario, deposito, cargo, estado_civil, nivel_academico
        WHERE aspirante.id_persona=persona.id_persona 
        AND persona.id_tipo_documento_identidad=tipo_documento_identidad.id_tipo_documento_identidad 
        AND aspirante.id_nacimiento=persona_nacimiento.id_nacimiento
        AND persona_nacimiento.id_pais=pais.id_pais
        AND direccion_domicilio.id_etnia=etnia.id_etnia 
        AND direccion_domicilio.id_estado=estado.id_estado
        AND aspirante.id_direccion=direccion_domicilio.id_direccion
        AND aspirante.id_dato_laboral=dato_laboral.id_dato_laboral 
        AND datos_interes.id_tipo_discapacidad=tipo_discapacidad.id_tipo_discapacidad 
        AND aspirante.id_datos_interes=datos_interes.id_datos_interes 
        AND aspirante.id_oferta_academica=oferta_academica.id_oferta_academica  
        AND oferta_academica.id_seccion=seccion.id_seccion
        AND seccion.id_modalidad=modalidad.id_modalidad
        AND horario.id_turno=turno.id_turno
        AND horario.id_seccion=seccion.id_seccion
        AND estado_civil.id_civil=aspirante.id_civil
        AND cargo.id_cargo=dato_laboral.id_cargo
        AND deposito.id_aspirante=aspirante.id_aspirante
        AND aspirante.id_nivel_academico=nivel_academico.id_nivel_academico
        AND persona.id_persona=:id_persona");
        $query->execute(['id_persona'=>$id_persona]);
        $item=new Confucio();
  
        
        while($row=$query->fetch()){
  
            $item->id_aspirante=$row['id_aspirante'];
            $item->persona=$row['persona'];
            $item->pais=$row['pais'];
            $item->estado=$row['estado'];
            $item->primer_nombre=$row['primer_nombre'];
            $item->segundo_nombre=$row['segundo_nombre'];
            $item->primer_apellido=$row['primer_apellido'];
            $item->segundo_apellido=$row['segundo_apellido'];
            $item->identificacion=$row['identificacion'];
            $item->correo=$row['correo'];
            $item->telefono=$row['telefono'];
            $item->genero=$row['genero'];
            $item->tipo_documento=$row['tipo_documento'];
            $item->fecha_nacimiento=$row['fecha_nacimiento'];
            $item->direccion=$row['direccion'];
            $item->civil=$row['civil'];
   
            $item->telefono_habitacion=$row['telefono_habitacion'];
            $item->etnia=$row['etnia'];
            $item->empleo=$row['empleo'];
            $item->telefono_institucion=$row['telefono_institucion'];
            $item->correo_institucion=$row['correo_institucion'];
            $item->direccion_institucion=$row['direccion_institucion'];
            $item->inst_publica=$row['inst_publica'];
            $item->inst_privada=$row['inst_privada'];
            $item->organismo=$row['organismo'];
            $item->inst_inv=$row['inst_inv'];
            $item->cargo=$row['cargo'];
            $item->tipo_institucion=$row['tipo_institucion'];
            $item->trabajador_ubv=$row['trabajador_ubv'];
            $item->discapacidad=$row['discapacidad'];
            $item->grupo_sanguineo=$row['grupo_sanguineo'];
            $item->enfermedad=$row['enfermedad'];
            $item->descripcion_enfermedad=$row['descripcion_enfermedad'];
            $item->tratamiento=$row['tratamiento'];
            $item->idioma=$row['idioma'];
            $item->idiomas_descripcion=$row['idiomas_descripcion'];
            $item->tipo=$row['tipo'];
            $item->estudiante_ubv=$row['estudiante_ubv'];
            $item->mision_sucre=$row['mision_sucre'];
            $item->institucion_nivel=$row['institucion_nivel'];
            $item->nivel_aca=$row['nivel_aca'];
  
            $item->id_oferta_academica=$row['id_oferta_academica'];
            $item->oferta=$row['oferta'];
            $item->modalidad=$row['modalidad'];
            $item->turno=$row['turno'];
            $item->hora_inicio=$row['hora_inicio'];
            $item->hora_fin=$row['hora_fin'];
            $item->estatus=$row['estatus'];
  
            $item->id_deposito=$row['id_deposito'];
            $item->exonerado=$row['exonerado'];
            $item->modalidad_exo=$row['modalidad_exo'];
            $item->numero_vouche=$row['numero_vouche'];
            $item->monto=$row['monto'];
            $item->fecha_deposito=$row['fecha_deposito'];
            $item->observacion=$row['observacion'];
            $item->numero_vouche_50=$row['numero_vouche_50'];
            $item->monto_50=$row['monto_50'];
            $item->fecha_deposito_50=$row['fecha_deposito_50'];
            $item->observacion_50=$row['observacion_50'];
  
        }
        
        return $item;
      
    }catch(PDOException $e){
        return false;
    }
    
  }


  public function getOfertas(){
    $items = [];
    try{
        $query = $this->db->connect()->query("SELECT  oferta_academica.id_oferta_academica, seccion.descripcion AS seccion, nivel.descripcion AS nivel,
        modalidad.descripcion AS modalidad, hora_inicio, hora_fin, turno.descripcion AS turno, periodo.descripcion AS periodo,
        periodo.estatus, periodo.inicio_periodo, periodo.fin_periodo, actividad.descripcion AS actividad
        FROM oferta_academica, seccion, nivel, modalidad, horario, turno, activar_oferta, periodo, actividad
        WHERE activar_oferta.id_oferta_academica=oferta_academica.id_oferta_academica
        AND oferta_academica.id_seccion=seccion.id_seccion
        AND seccion.id_nivel=nivel.id_nivel
        AND seccion.id_modalidad=modalidad.id_modalidad
        AND horario.id_seccion=seccion.id_seccion
        AND horario.id_turno=turno.id_turno
        AND seccion.id_periodo=periodo.id_periodo
		AND seccion.id_actividad=actividad.id_actividad
        AND oferta_academica.estatus='1'
        AND periodo.estatus='Activo'");

        while($row = $query->fetch()){
            $item = new Confucio(); 
            $item->id_oferta_academica = $row['id_oferta_academica'];
            $item->seccion = $row['seccion'];
            $item->nivel = $row['nivel'];
            $item->modalidad  = $row['modalidad'];
            $item->turno = $row['turno'];
            $item->hora_inicio  = $row['hora_inicio'];
            $item->hora_fin = $row['hora_fin'];
            $item->periodo = $row['periodo'];
            $item->actividad = $row['actividad'];
            $item->inicio_periodo = $row['inicio_periodo'];
            $item->fin_periodo = $row['fin_periodo'];
            array_push($items, $item);
        }
        return $items;
    }catch(PDOException $e){
        return [];
    }
}



public function getCursosRealizados($id_persona){

    $items = [];
    try{
        $query = $this->db->connect()->prepare("SELECT seccion.descripcion AS seccion, nivel.descripcion AS nivel, actividad.descripcion AS actividad, periodo.descripcion AS periodo,
        nota_actividad_idioma_history.nota as nota, nota_actividad_idioma_history.descripcion as estatus_escrito, persona.id_persona
        FROM seccion, nivel, actividad, periodo, nota_actividad_idioma_history, persona
        WHERE seccion.id_nivel=nivel.id_nivel
        AND seccion.id_actividad=actividad.id_actividad
        AND seccion.id_periodo=periodo.id_periodo
        AND nota_actividad_idioma_history.id_seccion=seccion.id_seccion
        AND nota_actividad_idioma_history.id_persona=persona.id_persona
        AND persona.id_persona=:id_persona");

       $query->execute(['id_persona'=>$id_persona]);
       
        while($row = $query->fetch()){
            $item=new Confucio();
            $item->id_persona = $row['id_persona'];
            $item->seccion = $row['seccion'];
            $item->nivel = $row['nivel'];
            $item->actividad = $row['actividad'];
            $item->periodo = $row['periodo'];
            $item->nota = $row['nota'];
            $item->estatus_escrito = $row['estatus_escrito'];

            array_push($items, $item);
        }
        return $items;
    }catch(PDOException $e){
        return [];
    }
}


public function getCursosRealizadosOtro($id_persona){

    $items = [];
    try{
        $query = $this->db->connect()->prepare("SELECT seccion.descripcion AS seccion, nivel.descripcion AS nivel, actividad.descripcion AS actividad, periodo.descripcion AS periodo,
        nota_actividad_history.nota as nota, nota_actividad_history.descripcion as estatus_escrito, persona.id_persona
        FROM seccion, nivel, actividad, periodo, nota_actividad_history, persona
        WHERE seccion.id_nivel=nivel.id_nivel
        AND seccion.id_actividad=actividad.id_actividad
        AND seccion.id_periodo=periodo.id_periodo
        AND nota_actividad_history.id_seccion=seccion.id_seccion
        AND nota_actividad_history.id_persona=persona.id_persona
        AND persona.id_persona=:id_persona");

       $query->execute(['id_persona'=>$id_persona]);
       
        while($row = $query->fetch()){
            $item=new Confucio();
            $item->id_persona = $row['id_persona'];
            $item->seccion = $row['seccion'];
            $item->nivel = $row['nivel'];
            $item->actividad = $row['actividad'];
            $item->periodo = $row['periodo'];
            $item->nota = $row['nota'];
            $item->estatus_escrito = $row['estatus_escrito'];

            array_push($items, $item);
        }
        return $items;
    }catch(PDOException $e){
        return [];
    }
}





public function getReingreso($id_persona){

    try{
        $query = $this->db->connect()->prepare("SELECT aspirante.id_aspirante, inscripcion.id_inscripcion, nota_actividad_idioma.id_nota_actividad_idioma, persona.id_persona,
        aspirante.id_oferta_academica
        FROM aspirante, inscripcion, nota_actividad_idioma, persona, oferta_academica
        WHERE aspirante.id_aspirante=inscripcion.id_aspirante
        AND aspirante.id_persona=persona.id_persona 
        AND inscripcion.id_inscripcion=nota_actividad_idioma.id_inscripcion
        AND aspirante.id_oferta_academica=oferta_academica.id_oferta_academica
        AND persona.id_persona=:id_persona");

        $query->execute(['id_persona'=>$id_persona]);
        $item=new Confucio();

        while($row = $query->fetch()){
            $item->id_aspirante = $row['id_aspirante'];
            $item->id_inscripcion = $row['id_inscripcion'];
            $item->id_nota_actividad_idioma = $row['id_nota_actividad_idioma'];
            $item->id_oferta_academica=$row['id_oferta_academica'];
            $item->id_persona=$row['id_persona'];
        }
        return $item;
    }catch(PDOException $e){
        return false;
    }
}


public function getConstanciadeEstudio($id_persona){

    try{
        $query=$this->db->connect()->prepare("SELECT inscripcion.id_inscripcion, aspirante.id_aspirante, aspirante.id_persona AS persona, primer_nombre, segundo_nombre, primer_apellido, 
        segundo_apellido, identificacion, correo, telefono, genero, tipo_documento_identidad.descripcion AS tipo_documento,
        aspirante.id_oferta_academica, actividad.descripcion as oferta, modalidad.descripcion AS modalidad, turno.descripcion AS turno, hora_inicio, 
        hora_fin, inscripcion.estatus, nivel.descripcion as nivel, periodo.descripcion as periodo, current_date as fecha
        FROM inscripcion, aspirante, persona, tipo_documento_identidad, seccion, actividad, oferta_academica, modalidad, turno, horario, nivel, periodo
        WHERE aspirante.id_persona=persona.id_persona 
        AND persona.id_tipo_documento_identidad=tipo_documento_identidad.id_tipo_documento_identidad 
        AND inscripcion.id_oferta_academica=oferta_academica.id_oferta_academica  
        AND oferta_academica.id_seccion=seccion.id_seccion
        AND seccion.id_modalidad=modalidad.id_modalidad
        AND seccion.id_nivel=nivel.id_nivel
        AND seccion.id_actividad=actividad.id_actividad
        AND horario.id_turno=turno.id_turno
        AND horario.id_seccion=seccion.id_seccion
        AND inscripcion.id_aspirante=aspirante.id_aspirante
        AND seccion.id_periodo=periodo.id_periodo
        AND persona.id_persona=:id_persona
        AND inscripcion.estatus='1'");
        $query->execute(['id_persona'=>$id_persona]);
        $item=new Confucio();
  
        
        while($row=$query->fetch()){
  
            $item->id_aspirante=$row['id_aspirante'];
            $item->persona=$row['persona'];
            $item->primer_nombre=$row['primer_nombre'];
            $item->segundo_nombre=$row['segundo_nombre'];
            $item->primer_apellido=$row['primer_apellido'];
            $item->segundo_apellido=$row['segundo_apellido'];
            $item->identificacion=$row['identificacion'];
            $item->correo=$row['correo'];
            $item->telefono=$row['telefono'];
            $item->genero=$row['genero'];
            $item->tipo_documento=$row['tipo_documento'];
            $item->fecha=$row['fecha'];
   
            $item->id_oferta_academica=$row['id_oferta_academica'];
            $item->oferta=$row['oferta'];
            $item->modalidad=$row['modalidad'];
            $item->turno=$row['turno'];
            $item->hora_inicio=$row['hora_inicio'];
            $item->hora_fin=$row['hora_fin'];
            $item->nivel=$row['nivel'];
            $item->periodo=$row['periodo'];
            $item->estatus=$row['estatus'];
  
        }
        
        return $item;
      
    }catch(PDOException $e){
        return false;
    }
    
  }


  public function getCertificadoIdioma($id_persona){

    try{
        $query=$this->db->connect()->prepare("SELECT persona.id_persona, persona.primer_nombre, persona.primer_apellido, persona.segundo_nombre, persona.segundo_apellido, nivel.id_nivel, nivel.descripcion as nivel,
        nota_actividad_idioma_history.nota, nota_actividad_idioma_history.descripcion AS estatus_escrito, actividad.categoria, periodo.descripcion as periodo, current_date as fecha 
                FROM persona, seccion, nota_actividad_idioma_history, nota_actividad_idioma, nivel, inscripcion, aspirante, oferta_academica, actividad, periodo
                WHERE seccion.id_nivel=nivel.id_nivel
                AND nota_actividad_idioma_history.id_nivel=nivel.id_nivel
                AND nota_actividad_idioma_history.id_inscripcion=inscripcion.id_inscripcion
	        AND nota_actividad_idioma.id_nivel=nivel.id_nivel
                AND nota_actividad_idioma.id_inscripcion=inscripcion.id_inscripcion
                AND aspirante.id_persona=persona.id_persona 
                AND inscripcion.id_oferta_academica=oferta_academica.id_oferta_academica
                AND aspirante.id_aspirante=inscripcion.id_aspirante 
                AND seccion.id_seccion=oferta_academica.id_seccion
                AND seccion.id_actividad=actividad.id_actividad
                AND seccion.id_periodo=periodo.id_periodo
                AND actividad.categoria='Idioma'
                AND persona.id_persona=:id_persona");
        $query->execute(['id_persona'=>$id_persona]);
        $item=new Confucio();
  
        while($row=$query->fetch()){
            $item->id_nota_actividad_idioma=$row['id_nota_actividad_idioma'];
            $item->id_persona=$row['id_persona'];
            $item->id_nivel=$row['id_nivel'];
            $item->nivel=$row['nivel'];
            $item->nota=$row['nota'];
            $item->categoria=$row['categoria'];
            $item->primer_nombre=$row['primer_nombre'];
            $item->primer_apellido=$row['primer_apellido'];
            $item->segundo_nombre=$row['segundo_nombre'];
            $item->segundo_apellido=$row['segundo_apellido'];
            $item->periodo=$row['periodo'];
            $item->fecha=$row['fecha'];
            $item->estatus_escrito=$row['estatus_escrito'];
        }
        
        return $item;
      
    }catch(PDOException $e){
        return false;
    }
    
  }


  public function getCertificadoActividad($id_persona){

    try{
        $query=$this->db->connect()->prepare("SELECT  persona.id_persona, nivel.id_nivel, nota_actividad.id_nota_actividad, nivel.descripcion as nivel, 
        persona.primer_nombre, persona.primer_apellido, persona.segundo_nombre, persona.segundo_apellido,
                nota_actividad.nota, nota_actividad.descripcion AS estatus_escrito, actividad.categoria, periodo.descripcion as periodo, current_date as fecha
                        FROM persona, seccion, nota_actividad, nivel, inscripcion, aspirante, oferta_academica, actividad, periodo
                        WHERE seccion.id_nivel=nivel.id_nivel
                        AND nota_actividad.id_inscripcion=inscripcion.id_inscripcion
                        AND aspirante.id_persona=persona.id_persona 
                        AND inscripcion.id_oferta_academica=oferta_academica.id_oferta_academica
                        AND aspirante.id_aspirante=inscripcion.id_aspirante 
                        AND persona.id_persona=nota_actividad.id_persona 
                        AND seccion.id_seccion=oferta_academica.id_seccion
                        AND seccion.id_actividad=actividad.id_actividad
                        AND seccion.id_periodo=periodo.id_periodo
                        AND actividad.categoria='Otro'
                AND persona.id_persona=:id_persona");
        $query->execute(['id_persona'=>$id_persona]);
        $item=new Confucio();
  
        while($row=$query->fetch()){
            $item->id_nota_actividad_idioma=$row['id_nota_actividad_idioma'];
            $item->id_persona=$row['id_persona'];
            $item->id_nivel=$row['id_nivel'];
            $item->nivel=$row['nivel'];
            $item->nota=$row['nota'];
            $item->categoria=$row['categoria'];
            $item->primer_nombre=$row['primer_nombre'];
            $item->primer_apellido=$row['primer_apellido'];
            $item->segundo_nombre=$row['segundo_nombre'];
            $item->segundo_apellido=$row['segundo_apellido'];
            $item->periodo=$row['periodo'];
            $item->fecha=$row['fecha'];
            $item->estatus_escrito=$row['estatus_escrito'];
        }
        
        return $item;
      
    }catch(PDOException $e){
        return false;
    }
    
  }



}

?>