<?php

include_once 'models/confucio.php';
include_once 'models/perfil.php';
include_once 'SED.php';

class UsuariosModel extends Model{

     function __construct()
    {
        parent::__construct();
    }

    public function insert($datos){

        try{

            $pdo=$this->db->connect();

            $pdo->beginTransaction();


            $persona=$pdo->prepare('INSERT INTO persona(primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, identificacion, correo, telefono, genero, id_tipo_documento_identidad) VALUES (:primer_nombre, :segundo_nombre, :primer_apellido, :segundo_apellido, :identificacion, :correo, :telefono, :genero, :id_tipo_documento_identidad)');
            $persona->execute(['primer_nombre'=>$datos['primer_nombre'], 'segundo_nombre'=>$datos['segundo_nombre'], 'primer_apellido'=>$datos['primer_apellido'], 'segundo_apellido'=>$datos['segundo_apellido'], 'identificacion'=>$datos['identificacion'],'correo'=>$datos['correo'], 'telefono'=>$datos['telefono'], 'genero'=>$datos['genero'], 'id_tipo_documento_identidad'=>$datos['id_tipo_documento_identidad']]);
        
            $crypt= new SED();
            $clave=$crypt->encryption("vice$".$datos['clave']);

            $usuario=$pdo->prepare('INSERT INTO usuario(usuario, clave, fecha, estatus, id_perfil, id_persona) VALUES (:usuario, :clave, :fecha, :estatus, :id_perfil, (SELECT id_persona FROM persona ORDER BY id_persona DESC LIMIT 1))');
            $user="vice".$datos['identificacion'];
            $usuario->execute(['usuario'=>$user, 'clave'=>$clave, 'fecha'=>date("Y-m-d"), 'estatus'=>'Activo', 'id_perfil'=>$datos['perfil']]);

        
            $roles=$datos['rol'];
            
            $query=$pdo->prepare('INSERT INTO usuario_rol(id_usuario, id_rol) VALUES ((SELECT id_usuario FROM usuario ORDER BY id_usuario DESC LIMIT 1), :id_rol)');
           
            //Recorrer el arreglo de roles
            for ($i=0;$i<count($roles);$i++)    
            {       
                $query->execute(['id_rol'=>$roles[$i] ]);
            } 

            $pdo->commit();
            return true;


        }catch(PDOException $e){

        $pdo->rollBack();
        return false;

        }

    }


    public function update($datos){

        try{

            $pdo=$this->db->connect();

            $pdo->beginTransaction();


            $crypt= new SED();
            $clave=$crypt->encryption($datos['clave']);

            $query=$pdo->prepare("UPDATE usuario SET usuario=:usuario, clave=:clave, estatus=:estatus, id_perfil=:id_perfil WHERE id_persona=:id_persona");

            $query->execute(['usuario'=>$datos['usuario'], 'clave'=>$clave, 'estatus'=>$datos['estatus'], 'id_perfil'=>$datos['perfil'], 'id_persona'=>$datos['id_persona']]);
            

            //Eliminamos los roles actules del usuario para agregarlos desde cero ya que si el usurio tiene uno y se pretende agregar otro con un update no bastara.
            $deleteRoles=$pdo->prepare('DELETE FROM usuario_rol WHERE id_usuario=(SELECT id_usuario  FROM usuario WHERE id_persona=:id_persona LIMIT 1)');
            //Recorrer el arreglo
            $deleteRoles->execute(['id_persona'=>$datos['id_persona']]);

                
            $roles=$datos['rol'];
            $query=$pdo->prepare('INSERT INTO usuario_rol(
                id_usuario, 
                id_rol) VALUES (
                    (SELECT id_usuario FROM usuario WHERE id_persona=:id_persona LIMIT 1),
                    :id_rol)');
            //Recorrer el arreglo de roles
            for ($i=0;$i<count($roles);$i++)    
            {       
                $query->execute([
                    'id_persona'=>$datos['id_persona'],
                    'id_rol'=>$roles[$i]
                    ]);
            } 

            $pdo->commit();

            return true;

        }catch(PDOException $e){

            $pdo->rollBack();
            return false;
        }
    }

    public function update_date($datos){

        try{

            $pdo=$this->db->connect();

            $pdo->beginTransaction();


            $query=$pdo->prepare("UPDATE persona SET primer_nombre=:primer_nombre, segundo_nombre=:segundo_nombre, primer_apellido=:primer_apellido, segundo_apellido=:segundo_apellido, correo=:correo, telefono=:telefono, genero=:genero, id_tipo_documento_identidad=:id_tipo_documento_identidad WHERE id_persona=:id_persona");

            $query->execute(['primer_nombre'=>$datos['primer_nombre'], 'segundo_nombre'=>$datos['segundo_nombre'], 'primer_apellido'=>$datos['primer_apellido'], 'segundo_apellido'=>$datos['segundo_apellido'], 'correo'=>$datos['correo'], 'telefono'=>$datos['telefono'], 'genero'=>$datos['genero'], 'id_tipo_documento_identidad'=>$datos['id_tipo_documento_identidad'], 'id_persona'=>$datos['id_persona']]);

            $pdo->commit();

            return true;

        }catch(PDOException $e){

            $pdo->rollBack();
            return false;
        }
    }

    ///////////////////////////////// CONSULTAS ///////////////////////////////////////////


    public function getUsuario(){

        $items=[];

        try{

      $query=$this->db->connect()->query("SELECT id_usuario, usuario.id_persona, usuario, primer_nombre, primer_apellido, usuario.id_perfil, perfil.descripcion, estatus FROM usuario, persona, perfil WHERE usuario.id_persona=persona.id_persona AND usuario.id_perfil=perfil.id_perfil AND persona.identificacion!='116611'");
      while($row=$query->fetch()){

        $item=new Confucio();
        $item->id_usuario=$row['id_usuario'];
        $item->id_persona=$row['id_persona'];
        $item->usuario=$row['usuario'];
        $item->primer_nombre=$row['primer_nombre'];
        $item->primer_apellido=$row['primer_apellido'];
        $item->id_perfil=$row['id_perfil'];
        $item->descripcion=$row['descripcion'];
        $item->estatus=$row['estatus'];
        array_push($items,$item);
      }

      return $items;

        }catch(PDOException $e){

            return false;
        }

    }

    public function getbyID_user($id){

        try{

        $query=$this->db->connect()->prepare("SELECT usuario.id_persona, usuario.id_usuario, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, identificacion, 
        correo, telefono, genero, persona.id_tipo_documento_identidad, tipo_documento_identidad.descripcion, usuario, clave, estatus, 
        usuario.id_perfil, perfil.descripcion AS perfil, usuario.id_persona FROM persona, usuario, perfil, tipo_documento_identidad WHERE usuario.id_persona=persona.id_persona AND usuario.id_perfil=perfil.id_perfil AND persona.id_tipo_documento_identidad=tipo_documento_identidad.id_tipo_documento_identidad AND usuario.id_persona=:id_persona");
        $query->execute(['id_persona'=>$id]);
        $item=new Confucio();

            //se encripta la contraseña
            $crypt= new SED();

        while($row=$query->fetch()){
            
            $item->id_usuario=$row['id_usuario'];
            $item->usuario=$row['usuario'];

            //se desencripta la contraseña
            $clave=$crypt->decryption($row['clave']);
            $item->clave=$clave;

            $item->estatus=$row['estatus'];
            $item->id_perfil=$row['id_perfil'];
            $item->perfil=$row['perfil'];

            $item->id_persona=$row['id_persona'];
            $item->identificacion=$row['identificacion'];
            $item->primer_nombre=$row['primer_nombre'];
            $item->segundo_nombre=$row['segundo_nombre'];

            $item->primer_apellido=$row['primer_apellido'];
            $item->segundo_apellido=$row['segundo_apellido'];
            $item->correo=$row['correo'];
            $item->telefono=$row['telefono'];
            $item->genero=$row['genero'];
            $item->id_tipo_documento_identidad=$row['id_tipo_documento_identidad'];
            $item->descripcion=$row['descripcion'];
        }

          return $item;

        }catch(PDOException $e){

           return false;
        }

    }


    public function getbyID_roles($id_persona){

        $items=[];
        $query = $this->db->connect()->prepare("SELECT usuario_rol.id_usuario, 
            usuario_rol.id_rol, rol.descripcion AS rol 
            FROM rol,usuario_rol,usuario, persona
            WHERE usuario_rol.id_rol=rol.id_rol
            AND usuario_rol.id_usuario=usuario.id_usuario 
            AND usuario.id_persona=persona.id_persona
            AND usuario.id_persona=:id_persona");
        try{
            $query ->execute(['id_persona'=>$id_persona]);

            while($row = $query->fetch()){
                $item = new Confucio();
                $item->id_usuario = $row['id_usuario'];  
                $item->id_rol = $row['id_rol'];
                $item->rol = $row['rol'];
                //  var_dump($row['usuario']);
                array_push($items,$item);
            }


            return $items;
        }catch(PDOException  $e){
                return null;
        }
    }

    public function getRolbyID($id_persona, $id_rol){

        try{
            $query=$this->db->connect()->prepare("SELECT usuario_rol.id_usuario, 
                usuario_rol.id_rol, rol.descripcion AS rol 
                FROM rol,usuario_rol,usuario, persona
                WHERE usuario_rol.id_rol=rol.id_rol
                AND usuario_rol.id_usuario=usuario.id_usuario 
                AND usuario.id_persona=persona.id_persona
                AND usuario.id_persona=:id_persona 
                AND usuario_rol.id_rol=:id_rol");
            $query->execute([
                'id_persona'=>$id_persona,
                'id_rol'=>$id_rol
            ]);
      
            $item=new Confucio();
            while($row=$query->fetch()){
                $item->id_usuario = $row['id_usuario'];
                $item->id_rol = $row['id_rol'];
                $item->rol = $row['rol'];
            }
              
            return $item;
                
        } catch(PDOException $e){
                return false;
        }
      
    }


    public function getPerfil(){

        $items=[];
        try{

            $query=$this->db->connect()->query("SELECT * FROM perfil");
            while($row=$query->fetch()){

                $item=new Perfil();
                $item->id_perfil=$row['id_perfil'];
                $item->descripcion=$row['descripcion'];
                array_push($items,$item);
                
            }
            return $items;
        }catch(PDOException $e){
            return [];
        }
    }

    public function getCatalogo($valor){

        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT * FROM ".$valor."");
      
            while($row=$query->fetch()){

                $item=new Confucio();
                $item->id=$row['id_'.$valor.''];
                $item->descripcion=$row['descripcion'];
                array_push($items,$item);
            
            }
            return $items;
            
        }catch(PDOException $e){
            return[];
        }
    }

}

?>