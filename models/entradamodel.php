<?php

include_once 'models/confucio.php';

class EntradaModel extends Model{

     function __construct()
    {
        parent::__construct();
    }

    public function insert($datos){
        $s=$_SESSION['id_persona'];
       

        try{

            $pdo=$this->db->connect();

            $pdo->beginTransaction();

            if (isset($datos['imagen'])){  

                copy($datos['ruta1'],$datos['destino1']);
        
            $persona=$pdo->prepare('INSERT INTO portal(captura,titulo,contenido,id_usuario) VALUES (:captura,:titulo,:contenido,:id_usuario)');
            $persona->execute([
              'titulo'=>$datos['titulo'], 
              'contenido'=>$datos['contenido'],
              'id_usuario'=>$s,    
              'captura'=>$datos['imagen']]);
            }
            $pdo->commit();
            return true;


        }catch(PDOException $e){

        $pdo->rollBack();
        return false;

        }

    }


    public function update($datos){

       
        try{
            
            $pdo=$this->db->connect();
            $pdo->beginTransaction();


            if($datos['ruta1']==''){
                $evento=$pdo->prepare("UPDATE portal 
                    SET titulo=:titulo, 
                    contenido=:contenido
                    WHERE id_portal=:id_portal;");
    
                $evento->execute([
                    'titulo'=>$datos['titulo'],
                    'contenido'=>$datos['contenido'],
                    'id_portal'=>$datos['id_portal']
                    ]);

                }else{
    
                    $query2=$pdo->prepare("SELECT captura FROM portal WHERE id_portal=:id_portal");
                    $query2->execute(['id_portal'=>$datos['id_portal']]);
                    $imagen=$query2->fetch();
    
                    $ruta="src/img/".$imagen['captura'];
                    unlink($ruta);

                  
                   if(copy($datos['ruta1'],$datos['destino1'])){
    
                    $evento=$pdo->prepare("UPDATE portal 
                    SET captura=:captura,
                    titulo=:titulo, 
                    contenido=:contenido
                    WHERE id_portal=:id_portal");
        
                    $evento->execute([
                        'titulo'=>$datos['titulo'],
                        'contenido'=>$datos['contenido'],
                        'captura'=>$datos['file1'],
                        'id_portal'=>$datos['id_portal']
                        ]);
                    }
                }


            $pdo->commit();
            return true;

        }catch(PDOException $e){

            $pdo->rollBack();
            return false;
        }
    }


 public function delete($id){

    $query2=$this->db->connect()->prepare("SELECT captura FROM portal WHERE id_portal=:id_portal");
    $query2->execute([
        'id_portal'=>$id,
        ]);
    $imagen=$query2->fetch();
    $imagen['captura'];

    $query = $this->db->connect()->prepare('DELETE FROM portal WHERE id_portal = :id_portal');
   
    $ruta1="src/img/".$imagen['captura'];
    unlink($ruta1);
   
    try{
        $query->execute([
            'id_portal' => $id
        ]);
        return true;
    }catch(PDOException $e){
        return false;
    }
}


public function UpdateRequisito($datos){

    try{

        $pdo=$this->db->connect();

        $pdo->beginTransaction();

    
        $requisito=$pdo->prepare("UPDATE requisito SET monto_uno=:monto_uno, monto_dos=:monto_dos, inicio_clase=:inicio_clase, inscripcion_inicio=:inscripcion_inicio, inscripcion_fin=:inscripcion_fin WHERE id_requisito=:id_requisito");
        $requisito->execute([
          'monto_uno'=>$datos['monto_uno'], 
          'monto_dos'=>$datos['monto_dos'],
          'inicio_clase'=>$datos['inicio_clase'],    
          'inscripcion_inicio'=>$datos['inscripcion_inicio'],
          'inscripcion_fin'=>$datos['inscripcion_fin'],
          'id_requisito'=>$datos['id_requisito']]);
        
        $pdo->commit();
        return true;


    }catch(PDOException $e){

    $pdo->rollBack();
    return false;

    }

}


public function UpdateHistoria($datos){

    try{

        $pdo=$this->db->connect();

        $pdo->beginTransaction();

    
        $requisito=$pdo->prepare("UPDATE portal_historia SET descripcion_historia=:descripcion_historia, descripcion_mision=:descripcion_mision, descripcion_vision=:descripcion_vision WHERE id_historia=:id_historia");
        $requisito->execute([
          'descripcion_historia'=>$datos['descripcion_historia'], 
          'descripcion_mision'=>$datos['descripcion_mision'],
          'descripcion_vision'=>$datos['descripcion_vision'],    
          'id_historia'=>$datos['id_historia']]);
        
        $pdo->commit();
        return true;


    }catch(PDOException $e){

    $pdo->rollBack();
    return false;

    }

}


public function UpdateRedes($datos){

    try{

        $pdo=$this->db->connect();

        $pdo->beginTransaction();

    
        $requisito=$pdo->prepare("UPDATE portal_redes SET link_facebook=:link_facebook, link_instagram=:link_instagram, link_twitter=:link_twitter WHERE id_redes=:id_redes");
        $requisito->execute([
          'link_facebook'=>$datos['link_facebook'], 
          'link_instagram'=>$datos['link_instagram'],
          'link_twitter'=>$datos['link_twitter'],    
          'id_redes'=>$datos['id_redes']]);
        
        $pdo->commit();
        return true;


    }catch(PDOException $e){

    $pdo->rollBack();
    return false;

    }

}

///// CONSULTA ///////////////////

public function getentrada(){

    $items=[];

    try{

  $query=$this->db->connect()->query("SELECT * from portal");
  while($row=$query->fetch()){
    $item=new Confucio();
    $item->id_portal=$row['id_portal'];
    $item->titulo=$row['titulo'];
    $item->captura=$row['captura'];
    $item->contenido=$row['contenido'];
    $item->id_usuario=$row['id_usuario'];
    array_push($items,$item);
  }

  return $items;

    }catch(PDOException $e){

        return false;
    }

}

public function getbyID($id_portal){

    try{
    
        $query=$this->db->connect()->prepare("SELECT * FROM portal WHERE id_portal=:id_portal");
        $query->execute(['id_portal'=>$id_portal]);
        $item=new Confucio();
    
    
        while($row=$query->fetch()){
                
            $item->id_portal=$row['id_portal'];
            $item->titulo=$row['titulo'];
            $item->captura=$row['captura'];
            $item->contenido=$row['contenido'];
        }
    
        return $item;
    
    }catch(PDOException $e){
    
    return false;
    
    }
    
    }


    public function get(){

        $items=[];
    
        try{
    
      $query=$this->db->connect()->query("SELECT * FROM requisito");
      while($row=$query->fetch()){
        $item=new Confucio();
        $item->id_requisito=$row['id_requisito'];
        $item->monto_uno=$row['monto_uno'];
        $item->monto_dos=$row['monto_dos'];
        $item->inicio_clase=$row['inicio_clase'];
        $item->inscripcion_inicio=$row['inscripcion_inicio'];
        $item->inscripcion_fin=$row['inscripcion_fin'];
        array_push($items,$item);
      }
    
      return $items;
    
        }catch(PDOException $e){
    
            return false;
        }
    
    }


    public function getRedes(){

        $items=[];
    
        try{
    
      $query=$this->db->connect()->query("SELECT * FROM portal_redes");
      while($row=$query->fetch()){
        $item=new Confucio();
        $item->link_facebook=$row['link_facebook'];
        $item->link_instagram=$row['link_instagram'];
        $item->link_twitter=$row['link_twitter'];
        $item->id_redes=$row['id_redes'];
        array_push($items,$item);
      }
    
      return $items;
    
        }catch(PDOException $e){
    
            return false;
        }
    
    }


    public function getHistoria(){

        $items=[];
    
        try{
    
      $query=$this->db->connect()->query("SELECT * FROM portal_historia");
      while($row=$query->fetch()){
        $item=new Confucio();
        $item->descripcion_historia=$row['descripcion_historia'];
        $item->descripcion_mision=$row['descripcion_mision'];
        $item->descripcion_vision=$row['descripcion_vision'];
        $item->id_historia=$row['id_historia'];
        array_push($items,$item);
      }
    
      return $items;
    
        }catch(PDOException $e){
    
            return false;
        }
    
    }


    public function getByIDRequisito($id_requisito){

        try{

            $query=$this->db->connect()->prepare("SELECT * FROM requisito WHERE id_requisito=:id_requisito");
            $query->execute(['id_requisito'=>$id_requisito]);
            $item=new Confucio();
        
        
            while($row=$query->fetch()){
                    
                $item->id_requisito=$row['id_requisito'];
                $item->monto_uno=$row['monto_uno'];
                $item->monto_dos=$row['monto_dos'];
                $item->inicio_clase=$row['inicio_clase'];
                $item->inscripcion_inicio=$row['inscripcion_inicio'];
                $item->inscripcion_fin=$row['inscripcion_fin'];
            }
        
            return $item;


        }catch(PDOException $e){

        return false;

        }

    }


    public function getByIDRedes($id_redes){

        try{

            $query=$this->db->connect()->prepare("SELECT * FROM portal_redes WHERE id_redes=:id_redes");
            $query->execute(['id_redes'=>$id_redes]);
            $item=new Confucio();
        
        
            while($row=$query->fetch()){
                    
                $item->link_facebook=$row['link_facebook'];
                $item->link_instagram=$row['link_instagram'];
                $item->link_twitter=$row['link_twitter'];
                $item->id_redes=$row['id_redes'];
            }
        
            return $item;


        }catch(PDOException $e){

        return false;

        }

    }


    public function getByIDHistoria($id_historia){

        try{

            $query=$this->db->connect()->prepare("SELECT * FROM portal_historia WHERE id_historia=:id_historia");
            $query->execute(['id_historia'=>$id_historia]);
            $item=new Confucio();
        
        
            while($row=$query->fetch()){
                    
                $item->descripcion_historia=$row['descripcion_historia'];
                $item->descripcion_mision=$row['descripcion_mision'];
                $item->descripcion_vision=$row['descripcion_vision'];
                $item->id_historia=$row['id_historia'];
            }
        
            return $item;


        }catch(PDOException $e){

        return false;

        }

    }


}

?>