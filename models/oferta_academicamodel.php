<?php 
require_once 'models/confucio.php';
class Oferta_academicaModel extends Model
{
	
  public function __construct(){
    parent::__construct();
  }
  
///////////////////////////////////////////OFERTA-ACADEMICA///////////////////////////////////////////////////
///Datos de ofertas academecas inactivas(En status por default, es decir, 0)
  public function getOfertas(){
    $items = [];
    try{
      $query = $this->db->connect()->query("SELECT id_oferta_academica, seccion.descripcion AS seccion, actividad.descripcion AS actividad, 
      nivel.descripcion AS nivel, turno.descripcion AS turno, periodo.estatus AS estatus, periodo.descripcion AS periodo, seccion.cupos
        FROM oferta_academica, seccion, actividad, nivel, turno, horario, periodo
        WHERE oferta_academica.id_seccion = seccion.id_seccion
        AND seccion.id_actividad = actividad.id_actividad
        AND seccion.id_nivel = nivel.id_nivel
        AND seccion.id_seccion = horario.id_seccion
        AND horario.id_turno = turno.id_turno
        AND seccion.id_periodo = periodo.id_periodo
        AND periodo.estatus = 'Activo'
        AND oferta_academica.estatus = '0'
        ORDER BY seccion.descripcion ASC");

      while($row = $query->fetch()){
        
        $item = new Confucio();
        $item->id_oferta_academica = $row['id_oferta_academica'];
        $item->seccion  = $row['seccion'];
        $item->actividad = $row['actividad'];
        $item->nivel  = $row['nivel'];
        $item->turno = $row['turno'];
        $item->periodo = $row['periodo'];
        $item->estatus = $row['estatus'];
        $item->cupos = $row['cupos'];
        array_push($items, $item);
      }
      return $items;
    }catch(PDOException $e){
      return [];
    }
  }
/////////ACTIVAR-OFERTA//////////////////////////
  public function activar($datos){

    try{

      $pdo=$this->db->connect();
      $pdo->beginTransaction();

      $query_activar=$pdo->prepare('INSERT INTO activar_oferta (fecha_inicio, fecha_fin, estatus, id_oferta_academica) 
        VALUES (:fecha_inicio, :fecha_fin, :estatus, :id_oferta_academica)');

      $query_activar->execute([
        'fecha_inicio'=>$datos['fecha_inicio'],
        'fecha_fin'=>$datos['fecha_fin'],
        'estatus'=>'1',
        'id_oferta_academica'=>$datos['id_oferta_academica']
      ]);

      $query_oferta=$pdo->prepare("UPDATE oferta_academica SET estatus = :estatus WHERE id_oferta_academica= :id_oferta_academica");

      $query_oferta->execute([
        'estatus'=>'1',
        'id_oferta_academica'=>$datos['id_oferta_academica']
      ]);

      $pdo->commit();
      return true;

    }catch(PDOException $e){
      $pdo->rollBack();
      return false;
    }
  }

//////////////////////DELETE-OFERTA/////////////
  public function delete($id){
    $query = $this->db->connect()->prepare('DELETE FROM oferta_academica WHERE id_oferta_academica = :id_oferta_academica');
    try{
      $query->execute([
        'id_oferta_academica' => $id
      ]);
      return true;
    }catch(PDOException $e){
      return false;
    }
  }

////////////////////////////////////////////////OFERTAS-ACTIVAS///////////////////////////////////////////////////
///Datos de ofertas academecas activas (status 1) junto con las fechas de activar_oferta
  public function getOfertasActivas(){
    $items = [];
    try{
      $query = $this->db->connect()->query("SELECT oferta_academica.id_oferta_academica, id_activar_oferta,fecha_inicio,fecha_fin, seccion.descripcion AS seccion, actividad.descripcion AS actividad, 
      nivel.descripcion AS nivel, turno.descripcion AS turno, periodo.estatus AS estatus, periodo.descripcion AS periodo
        FROM oferta_academica, seccion, actividad, nivel, turno, horario, activar_oferta, periodo
        WHERE oferta_academica.id_seccion = seccion.id_seccion
        AND seccion.id_actividad = actividad.id_actividad
        AND seccion.id_nivel = nivel.id_nivel
        AND seccion.id_seccion = horario.id_seccion
        AND horario.id_turno = turno.id_turno
        AND oferta_academica.id_oferta_academica = activar_oferta.id_oferta_academica
        AND seccion.id_periodo = periodo.id_periodo
        AND periodo.estatus = 'Activo'
        AND oferta_academica.estatus = '1'
        ORDER BY seccion.descripcion ASC");

      while($row = $query->fetch()){
        $item = new Confucio();
        $item->id_oferta_academica = $row['id_oferta_academica'];
        $item->id_activar_oferta = $row['id_activar_oferta'];
        $item->seccion  = $row['seccion'];
        $item->actividad = $row['actividad'];
        $item->nivel  = $row['nivel'];
        $item->turno = $row['turno'];
        $item->fecha_inicio  = $row['fecha_inicio'];
        $item->fecha_fin = $row['fecha_fin'];
        $item->periodo = $row['periodo'];
        $item->estatus = $row['estatus'];

        array_push($items, $item);
      }
      return $items;
    }catch(PDOException $e){
      return [];
    }
  }

///////////////UPDATE-FECHA/////////////////
///Editar los datos de activar_oferta
  public function update($item){

    try{
      $pdo=$this->db->connect();
      $pdo->beginTransaction();

      $query_update=$pdo->prepare("UPDATE activar_oferta 
        SET fecha_inicio = :fecha_inicio, fecha_fin = :fecha_fin
        WHERE id_activar_oferta = :id_activar_oferta");

      $query_update->execute([
        'id_activar_oferta'=>$item['id_activar_oferta'],
        'fecha_inicio'=>$item['fecha_inicio'], 
        'fecha_fin'=>$item['fecha_fin']
      ]);

      $pdo->commit();
      return true;

    }catch(PDOException $e){
      $pdo->rollBack();
      return false;
    }
  }

///////////////////////////////////////DELETE-ACTIVAR/UPDATE-OFERTA//////////////////////////////////////////////////
  public function desactivar($id){
    
    try{

      $query = $this->db->connect()->prepare('UPDATE oferta_academica SET estatus= :estatus WHERE id_oferta_academica= :id_oferta_academica');
      $query->execute([
        'id_oferta_academica'=> $id,
        'estatus' => '0'
      ]);

      $query_delete = $this->db->connect()->prepare('DELETE FROM activar_oferta WHERE id_oferta_academica = :id_oferta_academica');
      $query_delete->execute([
        'id_oferta_academica' => $id
      ]);
      return true;
    }catch(PDOException $e){
      return false;
    }
  }

///////////////////////////////////////////DETALLES////////////////////////////////////////////////
///Obtener los datos de activar_oferta para la vista de editar
  public function getActivar_Oferta($id){

    try{
      $query=$this->db->connect()->prepare("SELECT*FROM activar_oferta WHERE id_oferta_academica = :id");

      $query->execute(['id'=>$id]);
      $item=new Confucio();

      while($row=$query->fetch()){
        $item->id_activar_oferta = $row['id_activar_oferta'];
        $item->fecha_inicio = $row['fecha_inicio'];
        $item->fecha_fin = $row['fecha_fin'];
        $item->estatus = $row['estatus'];
      }
      return $item;
    }catch(PDOException $e){
     return false;
   }
 }

///Obetener id y datos de oferta_academica para poder activar la misma
 public function getDetalles_Seccion($id){

  try{
    $query=$this->db->connect()->prepare("SELECT id_oferta_academica, seccion.descripcion AS seccion, seccion.cupos, actividad.descripcion AS actividad, nivel.descripcion AS nivel, 
    modalidad.descripcion AS modalidad, periodo.descripcion AS periodo, docente.id_docente AS docente, primer_nombre, primer_apellido,
    horario.id_horario, hora_inicio, hora_fin, turno.descripcion AS turno, periodo.estatus AS estatus
    FROM oferta_academica, seccion, actividad, nivel, modalidad, docente, persona, periodo, horario, turno
    WHERE seccion.id_actividad = actividad.id_actividad
    AND seccion.id_nivel = nivel.id_nivel
    AND seccion.id_modalidad = modalidad.id_modalidad
    AND seccion.id_docente = docente.id_docente
    AND persona.id_persona = docente.id_persona
    AND seccion.id_periodo = periodo.id_periodo
    AND seccion.id_seccion = horario.id_seccion
    AND horario.id_turno = turno.id_turno
    AND oferta_academica.id_seccion = seccion.id_seccion
    AND id_oferta_academica = :id");

    $query->execute(['id'=>$id]);
    $item=new Confucio();

    while($row=$query->fetch()){
      $item->id_oferta_academica = $row['id_oferta_academica'];
      $item->seccion = $row['seccion'];
      $item->cupos = $row['cupos'];
      $item->actividad   = $row['actividad'];
      $item->nivel       = $row['nivel'];
      $item->modalidad   = $row['modalidad'];
      $item->docente   = $row['docente'];
      $item->primer_nombre   = $row['primer_nombre'];
      $item->primer_apellido = $row['primer_apellido'];
      $item->periodo     = $row['periodo'];
      $item->estatus     = $row['estatus'];
      $item->id_horario  = $row['id_horario'];
      $item->hora_inicio = $row['hora_inicio'];
      $item->hora_fin    = $row['hora_fin'];
      $item->turno       = $row['turno'];
    }
    return $item;
  }catch(PDOException $e){
   return false;
 }
}

public function getbyID_dias($id_oferta_academica){ 

  $items=[];
  $query = $this->db->connect()->prepare("SELECT oferta_academica.id_oferta_academica, horario_dia.id_dia, dia.descripcion AS dia
    FROM dia, horario_dia, horario, seccion, oferta_academica
    WHERE horario_dia.id_dia = dia.id_dia
    AND horario_dia.id_horario = horario.id_horario 
    AND seccion.id_seccion = horario.id_seccion 
    AND oferta_academica.id_seccion = seccion.id_seccion
    AND oferta_academica.id_oferta_academica = :id_oferta_academica");

  try{
    $query ->execute(['id_oferta_academica'=>$id_oferta_academica]);

    while($row = $query->fetch()){
      $item = new Confucio();
      $item->id_dia = $row['id_dia'];
      $item->dia = $row['dia'];
      array_push($items,$item);
    }

    return $items;
  }catch(PDOException  $e){
    return null;
  }
}

public function getDiabyID($id_oferta_academica, $id_dia){

  try{
    $query=$this->db->connect()->prepare("SELECT oferta_academica.id_oferta_academica, horario_dia.id_dia, dia.descripcion AS dia 
      FROM dia, horario_dia, horario, seccion,oferta_academica
      WHERE horario_dia.id_dia=dia.id_dia
      AND horario_dia.id_horario=horario.id_horario
      AND seccion.id_seccion=horario.id_seccion
      AND oferta_academica.id_seccion= seccion.id_seccion
      AND oferta_academica.id_oferta_academica= :id_oferta_academica
      AND horario_dia.id_dia= :id_dia");

    $query->execute([
      'id_oferta_academica'=>$id_oferta_academica,
      'id_dia'=>$id_dia
    ]);

    $item=new Confucio();
    while($row=$query->fetch()){
      $item->id_dia = $row['id_dia'];
      $item->dia = $row['dia'];
    }
    return $item;

  } catch(PDOException $e){
    return false;
  }

}


}

?>