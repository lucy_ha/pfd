<?php 
require_once 'models/confucio.php';
require_once 'models/perfil.php';
include_once 'SED.php';
class DocentesModel extends Model
{
	
  public function __construct(){
    parent::__construct();
}

///////////////////////////////////////INSERT///////////////////////////////////////////////////////////////
public function insert($datos){
    try{

        $pdo=$this->db->connect();
        $pdo->beginTransaction();

        $query_persona=$pdo->prepare('INSERT INTO persona(primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, identificacion, correo, telefono, genero, id_tipo_documento_identidad) 
            VALUES (:primer_nombre, :segundo_nombre, :primer_apellido, :segundo_apellido, :identificacion, :correo, :telefono, :genero, :id_tipo_documento_identidad)');

        $query_persona->execute([
            'primer_nombre'=>$datos['primer_nombre'], 
            'segundo_nombre'=>$datos['segundo_nombre'], 
            'primer_apellido'=>$datos['primer_apellido'], 
            'segundo_apellido'=>$datos['segundo_apellido'], 
            'identificacion'=>$datos['identificacion'],
            'correo'=>$datos['correo'], 
            'telefono'=>$datos['telefono'], 
            'genero'=>$datos['genero'], 
            'id_tipo_documento_identidad'=>$datos['id_tipo_documento_identidad']
        ]);

        $query_docente=$pdo->prepare('INSERT INTO docente (identificacion_visa, idioma, idiomas_descripcion, id_persona) 
            VALUES (:identificacion_visa, :idioma, :idiomas_descripcion, (SELECT id_persona FROM persona ORDER BY id_persona DESC LIMIT 1))');

        $query_docente->execute([
            'identificacion_visa' => $datos['identificacion_visa'],
            'idioma'=>$datos['idioma'], 
            'idiomas_descripcion'=>$datos['descripcion']
        ]);

        $query_usuario=$pdo->prepare('INSERT INTO usuario(usuario, clave, fecha, estatus, id_perfil, id_persona) 
            VALUES (:usuario, :clave, :fecha, :estatus, :id_perfil, (SELECT id_persona FROM persona ORDER BY id_persona DESC LIMIT 1))');
        
        $user="icubv".$datos['identificacion'];

        $crypt= new SED();
        $clave=$crypt->encryption("icubv$".$datos['identificacion']);

        $query_usuario->execute([
            'usuario'=>$user, 
            'clave'=>$clave, 
            'fecha'=>date("Y-m-d"), 
            'estatus'=>'Activo', 
            'id_perfil'=>3
        ]);

        $pdo->commit();
        return true;

    }catch(PDOException $e){
        $pdo->rollBack();
        return false;
    }
}

////////////////////////////////////////UPDATE////////////////////////////////////////////////
public function update($item){

    try{

        $pdo=$this->db->connect();
        $pdo->beginTransaction();

        $query_persona=$pdo->prepare("UPDATE persona 
            SET primer_nombre=:primer_nombre, segundo_nombre=:segundo_nombre,
            primer_apellido=:primer_apellido, segundo_apellido=:segundo_apellido, 
            id_tipo_documento_identidad=:id_tipo_documento_identidad, identificacion=:identificacion, 
            correo=:correo, telefono=:telefono, genero=:genero 
            WHERE id_persona=:id_persona");

        $query_persona->execute([
            'primer_nombre'=>$item['primer_nombre'], 
            'segundo_nombre'=>$item['segundo_nombre'], 
            'primer_apellido'=>$item['primer_apellido'], 
            'segundo_apellido'=>$item['segundo_apellido'],
            'id_tipo_documento_identidad'=>$item['id_tipo_documento_identidad'], 
            'identificacion'=>$item['identificacion'],
            'correo'=>$item['correo'], 
            'telefono'=>$item['telefono'],
            'genero'=>$item['genero'], 
            'id_persona'=>$item['id_persona']
        ]);

        $query_docente=$pdo->prepare("UPDATE docente 
            SET identificacion_visa=:identificacion_visa, idioma=:idioma, idiomas_descripcion=:idiomas_descripcion 
            WHERE id_persona=:id_persona");

        $query_docente->execute([
          'identificacion_visa'=>$item['identificacion_visa'],
          'idioma'=>$item['idioma'],
          'idiomas_descripcion'=>$item['idiomas_descripcion'],
          'id_persona'=>$item['id_persona']
      ]);

        $pdo->commit();
        return true;

    }catch(PDOException $e){
        $pdo->rollBack();
        return false;
    }
}

///////////////////////////////////////////CONSULTAS////////////////////////////////////////////////
public function get(){
    $items = [];
    try{
        $query = $this->db->connect()->query("SELECT docente.id_docente, docente.id_persona, primer_nombre, primer_apellido, correo, telefono, identificacion, identificacion_visa
            FROM persona, docente 
            WHERE docente.id_persona = persona.id_persona
            AND docente.id_docente!=1");
        
        while($row = $query->fetch()){
            $item = new Confucio();
            $item->id_persona = $row['id_persona'];
            $item->id_docente = $row['id_docente'];
            $item->primer_nombre  = $row['primer_nombre'];
            $item->primer_apellido = $row['primer_apellido'];
            $item->correo  = $row['correo'];
            $item->telefono = $row['telefono'];
            $item->identificacion  = $row['identificacion'];
            $item->identificacion_visa = $row['identificacion_visa'];

            array_push($items, $item);
        }
        return $items;
    }catch(PDOException $e){
        return [];
    }
}
///
public function getbyID($id){

    try{
        $query=$this->db->connect()->prepare("SELECT docente.id_docente, docente.id_persona, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, correo, telefono, id_tipo_documento_identidad, identificacion, identificacion_visa,idioma,idiomas_descripcion
            FROM persona, docente 
            WHERE docente.id_persona = persona.id_persona
            AND id_docente = :id");
        
        $query->execute(['id'=>$id]);
        $item = new Confucio();

        while($row=$query->fetch()){
            $item->id_persona = $row['id_persona'];
            $item->id_docente = $row['id_docente'];
            $item->primer_nombre  = $row['primer_nombre'];
            $item->segundo_nombre = $row['segundo_nombre'];
            $item->primer_apellido  = $row['primer_apellido'];
            $item->segundo_apellido = $row['segundo_apellido'];
            $item->genero  = $row['genero'];
            $item->correo  = $row['correo'];
            $item->telefono = $row['telefono'];
            $item->identificacion  = $row['identificacion'];
            $item->identificacion_visa = $row['identificacion_visa'];
            $item->id_tipo_documento_identidad=$row['id_tipo_documento_identidad'];
            $item->idioma  = $row['idioma'];
            $item->idiomas_descripcion = $row['idiomas_descripcion'];
        }
        return $item;
    }catch(PDOException $e){
     return false;
 }
}

///////////////////////////////////////////CATALOGO////////////////////////////////////////////////
public function getCatalogo($valor){

    $items=[];
    try{
        $query=$this->db->connect()->query("SELECT * FROM ".$valor."");
        
        while($row=$query->fetch()){
            $item=new Confucio();
            $item->id=$row['id_'.$valor.''];
            $item->descripcion=$row['descripcion'];
            array_push($items,$item);
        }
        return $items;
        
    }catch(PDOException $e){
        return[];
    }
}

////////////////////////////////////////////LISTAS///////////////////////////////////////////////
public function getSecciones_Docente($id){

    $items=[];
    $query=$this->db->connect()->prepare("SELECT seccion.id_seccion, seccion.descripcion AS seccion, actividad.descripcion AS actividad, nivel.descripcion AS nivel
        FROM persona, docente, seccion, actividad, nivel, periodo
        WHERE seccion.id_actividad = actividad.id_actividad
        AND seccion.id_nivel = nivel.id_nivel
        AND seccion.id_docente = docente.id_docente
        AND docente.id_persona = persona.id_persona
        AND seccion.id_periodo = periodo.id_periodo
        AND periodo.estatus = 'Activo'
        AND persona.id_persona = :id");
    
    try{
        $query->execute(['id'=>$id]);
        while($row = $query->fetch()){
            $item=new Confucio();
            $item->id_seccion  = $row['id_seccion'];
            $item->seccion = $row['seccion'];
            $item->actividad    = $row['actividad'];
            $item->nivel    = $row['nivel'];
            array_push($items,$item);
        }
        
        return $items;
    }catch(PDOException  $e){
      return null;
  }
}


}
?>