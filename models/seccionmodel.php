<?php 
require_once 'models/confucio.php';
class SeccionModel extends Model {
	
  public function __construct(){
    parent::__construct();
}

////////////////////////////////////////////INSERT///////////////////////////////////////////////////////////
public function insert($datos){
    try{

        $pdo=$this->db->connect();
        $pdo->beginTransaction();

        $query_seccion=$pdo->prepare('INSERT INTO seccion (descripcion, cupos, id_docente, id_periodo, id_actividad, id_nivel, id_modalidad) 
            VALUES(:descripcion, :cupos, :id_docente, :id_periodo, :id_actividad, :id_nivel, :id_modalidad)');

        $query_seccion->execute([
            'descripcion'=>$datos['descripcion'],
            'cupos'=>$datos['cupos'],  
            'id_docente'=>$datos['id_docente'], 
            'id_periodo'=>$datos['id_periodo'], 
            'id_actividad'=>$datos['id_actividad'],
            'id_nivel'=>$datos['id_nivel'],
            'id_modalidad'=>$datos['id_modalidad']
        ]);

        $query_oferta=$pdo->prepare('INSERT INTO oferta_academica (estatus, id_seccion) 
            VALUES(:estatus, (SELECT id_seccion FROM seccion ORDER BY id_seccion DESC LIMIT 1))');

        $query_oferta->execute([
            'estatus'=>'0'
        ]);

        $query_horario=$pdo->prepare('INSERT INTO horario (hora_inicio, hora_fin, id_turno, id_seccion) 
            VALUES(:hora_inicio, :hora_fin, :id_turno, (SELECT id_seccion FROM seccion ORDER BY id_seccion DESC LIMIT 1))');

        $query_horario->execute([
            'hora_inicio'=>$datos['hora_inicio'], 
            'hora_fin'=>$datos['hora_fin'], 
            'id_turno'=>$datos['id_turno']
        ]);

        $dias=$datos['dia'];
        $query_dias=$pdo->prepare('INSERT INTO horario_dia (id_horario, id_dia) 
            VALUES((SELECT id_horario FROM horario ORDER BY id_horario DESC LIMIT 1), :id_dia)');

        for ($i=0;$i<count($dias);$i++)    
        {       
            $query_dias->execute([
                'id_dia'=>$dias[$i] 
            ]);
        } 

        $pdo->commit();
        return true;

    }catch(PDOException $e){
        $pdo->rollBack();
        return false;
    }
}

////////////////////////////////////////UPDATE////////////////////////////////////////////////
public function update($item){

    try{

        $pdo=$this->db->connect();
        $pdo->beginTransaction();

        $update_seccion=$pdo->prepare("UPDATE seccion 
            SET descripcion = :descripcion, cupos = :cupos, id_docente = :id_docente, id_periodo = :id_periodo, id_actividad = :id_actividad, id_nivel = :id_nivel, id_modalidad = :id_modalidad
            WHERE id_seccion = :id_seccion");

        $update_seccion->execute([
            'id_seccion'=>$item['id_seccion'],
            'descripcion'=>$item['descripcion'], 
            'cupos'=>$item['cupos'], 
            'id_docente'=>$item['id_docente'], 
            'id_periodo'=>$item['id_periodo'], 
            'id_actividad'=>$item['id_actividad'],
            'id_nivel'=>$item['id_nivel'], 
            'id_modalidad'=>$item['id_modalidad']  
        ]);

        $update_horario=$pdo->prepare("UPDATE horario 
            SET hora_inicio = :hora_inicio, hora_fin = :hora_fin, id_turno = :id_turno 
            WHERE id_horario = :id_horario");

        $update_horario->execute([
            'id_horario'=>$item['id_horario'],
            'hora_inicio'=>$item['hora_inicio'], 
            'hora_fin'=>$item['hora_fin'], 
            'id_turno'=>$item['id_turno'] 
        ]);

        //Eliminamos los roles actules del usuario para agregarlos desde cero ya que si el usurio tiene uno y se pretende agregar otro con un update no bastara.
        $deleteDias=$pdo->prepare('DELETE FROM horario_dia WHERE id_horario = :id_horario');
        //Recorrer el arreglo
        $deleteDias->execute(['id_horario'=>$item['id_horario']]);

        $dias=$item['dia'];
        $query_dias=$pdo->prepare('INSERT INTO horario_dia( id_horario, id_dia) VALUES (:id_horario,:id_dia)');
        //Recorrer el arreglo de dias
        for ($i=0;$i<count($dias);$i++)    
        {       
            $query_dias->execute([
                'id_horario'=>$item['id_horario'],
                'id_dia'=>$dias[$i]
            ]);
        } 

        $pdo->commit();
        return true;

    }catch(PDOException $e){

        $pdo->rollBack();
        return false;
    }
}

/////////////////////////////////////////////////DELETE///////////////////////////////////////////////////
public function delete($id){

    try{

        $query_horario = $this->db->connect()->prepare("DELETE FROM horario WHERE id_seccion = :id_seccion");
        $query_horario->execute([
            'id_seccion' => $id
        ]);

        $query_seccion = $this->db->connect()->prepare("DELETE FROM seccion WHERE id_seccion = :id_seccion");
        $query_seccion->execute([
            'id_seccion' => $id
        ]);

        return true;
    }catch(PDOException $e){
        return false;
    }
}

///////////////////////////////////////////CONSULTAS////////////////////////////////////////////////
public function get(){
    $items = [];
    try{
        $query = $this->db->connect()->query("SELECT seccion.id_seccion, seccion.descripcion, actividad.descripcion AS actividad, nivel.descripcion AS nivel, seccion.cupos,
        modalidad.descripcion AS modalidad, docente.id_docente AS docente, persona.primer_nombre, persona.primer_apellido, periodo.descripcion AS periodo, periodo.estatus AS estatus,
        actividad.categoria, oferta_academica.id_oferta_academica
            FROM seccion, actividad, nivel, modalidad, docente, persona, periodo, oferta_academica
            WHERE seccion.id_actividad = actividad.id_actividad
            AND seccion.id_nivel = nivel.id_nivel
            AND seccion.id_modalidad = modalidad.id_modalidad
            AND seccion.id_docente = docente.id_docente
            AND seccion.id_periodo = periodo.id_periodo
            AND docente.id_persona = persona.id_persona
            AND seccion.id_seccion = oferta_academica.id_seccion
            ORDER BY seccion.descripcion");

        while($row = $query->fetch()){
            $item = new Confucio(); 
            $item->id_seccion      = $row['id_seccion'];
            $item->descripcion     = $row['descripcion'];
            $item->actividad       = $row['actividad'];
            $item->nivel           = $row['nivel'];
            $item->modalidad       = $row['modalidad'];
            $item->docente       = $row['docente'];
            $item->primer_nombre   = $row['primer_nombre'];
            $item->primer_apellido = $row['primer_apellido'];
            $item->periodo         = $row['periodo'];
            $item->estatus         = $row['estatus'];
    $item->id_oferta_academica  = $row['id_oferta_academica'];
            $item->cupos           = $row['cupos'];
            $item->categoria       = $row['categoria'];
            array_push($items, $item);
        }
        return $items;
    }catch(PDOException $e){
        return [];
    }
}
///
public function getbyID($id){

    try{
        $query=$this->db->connect()->prepare("SELECT seccion.id_seccion, seccion.descripcion, cupos, actividad.id_actividad AS actividad, nivel.id_nivel AS nivel, 
            modalidad.id_modalidad AS modalidad, docente.id_docente AS docente, periodo.id_periodo AS periodo,
            horario.id_horario, hora_inicio, hora_fin, turno.id_turno AS id_turno, turno.descripcion AS turno
            FROM seccion, actividad, nivel, modalidad, docente, periodo, horario, turno 
            WHERE seccion.id_actividad = actividad.id_actividad
            AND seccion.id_nivel = nivel.id_nivel
            AND seccion.id_modalidad = modalidad.id_modalidad
            AND seccion.id_docente = docente.id_docente
            AND seccion.id_periodo = periodo.id_periodo
            AND seccion.id_seccion = horario.id_seccion
            AND horario.id_turno = turno.id_turno
            AND seccion.id_seccion = :id");

        $query->execute(['id'=>$id]);
        $item=new Confucio();

        while($row=$query->fetch()){
            $item->id_seccion  = $row['id_seccion'];
            $item->descripcion = $row['descripcion'];
            $item->cupos       = $row['cupos'];
            $item->actividad   = $row['actividad'];
            $item->nivel       = $row['nivel'];
            $item->modalidad   = $row['modalidad'];
            $item->docente     = $row['docente'];
            $item->periodo     = $row['periodo'];
            $item->id_horario  = $row['id_horario'];
            $item->hora_inicio = $row['hora_inicio'];
            $item->hora_fin    = $row['hora_fin'];
            $item->id_turno    = $row['id_turno'];
            $item->turno       = $row['turno'];    
        }
        return $item;
    }catch(PDOException $e){
       return false;
   }
}
////////////////////////////////////////////HORARIO-SECCIÓN//////////////////////////////////////////////
public function getDetalles_Seccion($id){

    try{
        $query=$this->db->connect()->prepare("SELECT seccion.id_seccion, seccion.descripcion, seccion.cupos, actividad.descripcion AS actividad, nivel.descripcion AS nivel, 
            modalidad.descripcion AS modalidad, periodo.descripcion AS periodo, docente.id_docente AS docente, primer_nombre, primer_apellido,
            horario.id_horario, hora_inicio, hora_fin, turno.descripcion AS turno, periodo.estatus AS estatus
            FROM seccion, actividad, nivel, modalidad, docente, persona, periodo, horario, turno
            WHERE seccion.id_actividad = actividad.id_actividad
            AND seccion.id_nivel = nivel.id_nivel
            AND seccion.id_modalidad = modalidad.id_modalidad
            AND seccion.id_docente = docente.id_docente
            AND persona.id_persona = docente.id_persona
            AND seccion.id_periodo = periodo.id_periodo
            AND seccion.id_seccion = horario.id_seccion
            AND horario.id_turno = turno.id_turno
            AND seccion.id_seccion = :id");

        $query->execute(['id'=>$id]);
        $item=new Confucio();

        while($row=$query->fetch()){
            $item->id_seccion  = $row['id_seccion'];
            $item->descripcion = $row['descripcion'];
            $item->cupos = $row['cupos'];
            $item->actividad   = $row['actividad'];
            $item->nivel       = $row['nivel'];
            $item->modalidad   = $row['modalidad'];
            $item->docente       = $row['docente'];
            $item->primer_nombre   = $row['primer_nombre'];
            $item->primer_apellido = $row['primer_apellido'];
            $item->periodo     = $row['periodo'];
            $item->estatus     = $row['estatus'];
            $item->id_horario  = $row['id_horario'];
            $item->hora_inicio = $row['hora_inicio'];
            $item->hora_fin    = $row['hora_fin'];
            $item->turno       = $row['turno'];
        }
        return $item;
    }catch(PDOException $e){
       return false;
   }
}

//Obterner los dias del horario
public function getbyID_dias($id_seccion){ 

  $items=[];
  try{
    $query = $this->db->connect()->prepare("SELECT seccion.id_seccion, horario_dia.id_dia, dia.descripcion AS dia
        FROM dia, horario_dia, horario, seccion
        WHERE horario_dia.id_dia = dia.id_dia
        AND horario_dia.id_horario = horario.id_horario 
        AND seccion.id_seccion = horario.id_seccion 
        AND seccion.id_seccion = :id_seccion");
    $query ->execute(['id_seccion'=>$id_seccion]);

    while($row = $query->fetch()){
      $item = new Confucio();
      $item->id_dia = $row['id_dia'];
      $item->dia    = $row['dia'];
      array_push($items,$item);
  }

  return $items;
}catch(PDOException  $e){
    return null;
}
}

public function getDiabyID($id_seccion, $id_dia){

  try{
    $query=$this->db->connect()->prepare("SELECT seccion.id_seccion, horario_dia.id_dia, dia.descripcion AS dia 
        FROM dia, horario_dia, horario, seccion
        WHERE horario_dia.id_dia=dia.id_dia
        AND horario_dia.id_horario=horario.id_horario
        AND seccion.id_seccion=horario.id_seccion
        AND seccion.id_seccion= :id_seccion
        AND horario_dia.id_dia= :id_dia");

    $query->execute([
      'id_seccion'=>$id_seccion,
      'id_dia'=>$id_dia
  ]);

    $item=new Confucio();
    while($row=$query->fetch()){
      $item->id_dia = $row['id_dia'];
      $item->dia    = $row['dia'];
  }

  return $item;
} catch(PDOException $e){
    return false;
}

}
//////////////////////////////////////////////CATALOGO////////////////////////////////////////////////
public function getCatalogo($valor){

    $items=[];
    try{
        $query=$this->db->connect()->query("SELECT * FROM ".$valor."");
        
        while($row=$query->fetch()){
            $item=new Confucio();
            $item->id          = $row['id_'.$valor.''];
            $item->descripcion = $row['descripcion'];
            array_push($items,$item);
        }
        return $items;
        
    }catch(PDOException $e){
        return[];
    }
}

///
public function getDocentes(){
    $items = [];
    try{
        $query = $this->db->connect()->query("SELECT docente.id_docente, docente.id_persona, primer_nombre, primer_apellido
            FROM persona, docente 
            WHERE docente.id_persona = persona.id_persona
            AND docente.id_docente!=1");
        
        while($row = $query->fetch()){
            $item = new Confucio();
            $item->id_persona      = $row['id_persona'];
            $item->id_docente      = $row['id_docente'];
            $item->primer_nombre   = $row['primer_nombre'];
            $item->primer_apellido = $row['primer_apellido'];
            array_push($items, $item);
        }
        return $items;
    }catch(PDOException $e){
        return [];
    }
}

//
public function getEstudiantes($id){
    $items=[];
    $query=$this->db->connect()->prepare("SELECT inscripcion.id_inscripcion, primer_nombre,segundo_nombre, primer_apellido,segundo_apellido, identificacion, correo, telefono,
        fecha_nacimiento, direccion, pais.descripcion AS pais,
        profesion.descripcion AS profesion,  inst_publica, inst_privada, organismo, inst_inv,
        numero_vouche, deposito.observacion, numero_vouche_50, deposito.observacion_50 as observacion_50 
        FROM persona, aspirante, seccion, inscripcion, oferta_academica,persona_nacimiento, direccion_domicilio, pais, profesion, dato_laboral, deposito
        WHERE aspirante.id_persona=persona.id_persona 
        AND inscripcion.id_oferta_academica=oferta_academica.id_oferta_academica
        AND aspirante.id_aspirante=inscripcion.id_aspirante 
        AND seccion.id_seccion=oferta_academica.id_seccion
        AND aspirante.id_nacimiento=persona_nacimiento.id_nacimiento
        AND aspirante.id_direccion=direccion_domicilio.id_direccion
        AND persona_nacimiento.id_pais=pais.id_pais
        AND aspirante.id_dato_laboral=dato_laboral.id_dato_laboral 
        AND profesion.id_profesion=dato_laboral.id_profesion
        AND deposito.id_aspirante=aspirante.id_aspirante
        AND deposito.id_deposito=inscripcion.id_deposito
        AND seccion.id_seccion=:id");
    
    try{
        $query->execute(['id'=>$id]);
        while($row = $query->fetch()){
            $item=new Confucio();

            $item->primer_nombre=$row['primer_nombre'];
            $item->segundo_nombre=$row['segundo_nombre'];
            $item->primer_apellido=$row['primer_apellido'];
            $item->segundo_apellido=$row['segundo_apellido'];
            $item->identificacion=$row['identificacion'];
            $item->correo=$row['correo'];
            $item->telefono=$row['telefono'];

            $item->fecha_nacimiento=$row['fecha_nacimiento'];
            $item->direccion=$row['direccion'];
            $item->pais=$row['pais'];

            $item->inst_publica=$row['inst_publica'];
            $item->inst_privada=$row['inst_privada'];
            $item->organismo=$row['organismo'];
            $item->inst_inv=$row['inst_inv'];
            $item->profesion=$row['profesion'];

            $item->numero_vouche=$row['numero_vouche'];
            $item->observacion=$row['observacion'];
            $item->numero_vouche_50=$row['numero_vouche_50'];
            $item->observacion_50=$row['observacion_50'];
            array_push($items,$item);
        }
        
        return $items;
    }catch(PDOException  $e){
      return null;
  }

}

  //////////////////////////////////////////////// REGISTRAR ASPIRANTES EN SECCION////////////////////////////////////

  public function InsertAspirante($datos){
    $oferta=$datos['id_oferta_academica'];
    $sql_1 = $this->db->connect()->query("SELECT seccion.id_seccion as seccion,cupos FROM oferta_academica,seccion WHERE oferta_academica.id_seccion=seccion.id_seccion and id_oferta_academica='$oferta' ");
    $nombre=$sql_1->fetch();
    $cupos=$nombre['cupos'];
    $seccion=$nombre['seccion'];
    try{

      $pdo=$this->db->connect();
      $pdo->beginTransaction();

      $persona=$pdo->prepare('INSERT INTO persona(primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, identificacion, correo, telefono, genero, id_tipo_documento_identidad) VALUES (:primer_nombre, :segundo_nombre, :primer_apellido, :segundo_apellido, :identificacion, :correo, :telefono, :genero, :id_tipo_documento_identidad)');
      $persona->execute(['primer_nombre'=>$datos['primer_nombre'], 'segundo_nombre'=>$datos['segundo_nombre'], 'primer_apellido'=>$datos['primer_apellido'], 'segundo_apellido'=>$datos['segundo_apellido'], 'identificacion'=>$datos['identificacion'],'correo'=>$datos['correo'], 'telefono'=>$datos['telefono'], 'genero'=>$datos['genero'], 'id_tipo_documento_identidad'=>$datos['id_tipo_documento_identidad']]);

      $persona_nacimiento=$pdo->prepare('INSERT INTO persona_nacimiento(fecha_nacimiento, id_pais) VALUES (:fecha_nacimiento, :id_pais)');
      $persona_nacimiento->execute(['fecha_nacimiento'=>$datos['fecha_nacimiento'], 'id_pais'=>$datos['pais']]);

      $direccion=$pdo->prepare('INSERT INTO direccion_domicilio(direccion, telefono_habitacion, id_estado, id_etnia) VALUES (:direccion, :telefono_habitacion, :id_estado, :id_etnia)');
      $direccion->execute(['direccion'=>$datos['direccion'], 'telefono_habitacion'=>$datos['telefono_habitacion'], 'id_estado'=>$datos['estado'], 'id_etnia'=>$datos['etnia']]);

      $dato_laboral=$pdo->prepare('INSERT INTO dato_laboral(empleo, telefono_institucion, correo_institucion, direccion_institucion, inst_publica, inst_privada, organismo, inst_inv, tipo_institucion, trabajador_ubv, id_cargo, id_estado, id_profesion) VALUES (:empleo, :telefono_institucion, :correo_institucion, :direccion_institucion, :inst_publica, :inst_privada, :organismo, :inst_inv, :tipo_institucion, :trabajador_ubv, :id_cargo, :id_estado, :id_profesion)');
      $dato_laboral->execute(['empleo'=>$datos['empleo'], 'telefono_institucion'=>$datos['telefono_institucion'], 'correo_institucion'=>$datos['correo_institucion'], 'direccion_institucion'=>$datos['direccion_institucion'], 'inst_publica'=>$datos['inst_publica'], 'inst_privada'=>$datos['inst_privada'], 'organismo'=>$datos['organismo'], 'inst_inv'=>$datos['inst_inv'], 'tipo_institucion'=>$datos['tipo_institucion'], 'trabajador_ubv'=>$datos['trabajador_ubv'], 'id_cargo'=>$datos['id_cargo'], 'id_estado'=>$datos['state'], 'id_profesion'=>$datos['id_profesion']]);

      $interes=$pdo->prepare('INSERT INTO datos_interes(discapacidad, grupo_sanguineo, enfermedad, descripcion_enfermedad, tratamiento, idioma, idiomas_descripcion, estudiante_ubv, mision_sucre, id_tipo_discapacidad, institucion_nivel) VALUES (:discapacidad, :grupo_sanguineo, :enfermedad, :descripcion_enfermedad, :tratamiento, :idioma, :idiomas_descripcion, :estudiante_ubv, :mision_sucre, :id_tipo_discapacidad, :institucion_nivel)');  
      $interes->execute(['discapacidad'=>$datos['discapacidad'], 'grupo_sanguineo'=>$datos['grupo_sanguineo'], 'enfermedad'=>$datos['enfermedad'], 'descripcion_enfermedad'=>$datos['descripcion_enfermedad'], 'tratamiento'=>$datos['tratamiento'], 'idioma'=>$datos['idioma'], 'idiomas_descripcion'=>$datos['descripcion'], 'estudiante_ubv'=>$datos['estudiante_ubv'], 'mision_sucre'=>$datos['mision_sucre'], 'id_tipo_discapacidad'=>$datos['id_tipo_discapacidad'], 'institucion_nivel'=>$datos['institucion_nivel']]);

      $aspirante=$pdo->prepare('INSERT INTO aspirante(estatus, id_persona_tipo, id_persona, id_direccion, id_nacimiento, id_datos_interes, id_dato_laboral, id_nivel_academico, id_civil, id_oferta_academica) VALUES (:estatus, :id_persona_tipo, (SELECT id_persona FROM persona ORDER BY id_persona DESC LIMIT 1), (SELECT id_direccion FROM direccion_domicilio ORDER BY id_direccion DESC LIMIT 1), (SELECT id_nacimiento FROM persona_nacimiento ORDER BY id_nacimiento DESC LIMIT 1), (SELECT id_datos_interes FROM datos_interes ORDER BY id_datos_interes DESC LIMIT 1), (SELECT id_dato_laboral FROM dato_laboral ORDER BY id_dato_laboral DESC LIMIT 1), :id_nivel_academico, :id_civil, :id_oferta_academica)');
      $aspirante->execute(['estatus'=>'0', 'id_persona_tipo'=>'2', 'id_nivel_academico'=>$datos['id_nivel_academico'], 'id_civil'=>$datos['id_civil'], 'id_oferta_academica'=>$datos['id_oferta_academica']]);
      
      $resultados= $cupos - 1;
      
      $query=$pdo->query("UPDATE seccion SET cupos='$resultados' WHERE id_seccion='$seccion' ");

      

      $pdo->commit();
      return true;

      

    }catch(PDOException $e ){

      $pdo->rollBack();
      return false;

    }


  }

  public function getEstadoCivil(){
  
    $items=[];
    try{
      $query=$this->db->connect()->query("SELECT * FROM estado_civil");
  
      while($row=$query->fetch()){
  
        $item=new Confucio();
        $item->id_civil=$row['id_civil'];
        $item->descripcion=$row['descripcion'];
        array_push($items,$item);
        
      }
      return $items;
      
    }catch(PDOException $e){
      return[];
    }
  }

  
  public function getIdOferta($id){

    try{
      $query=$this->db->connect()->prepare("SELECT id_oferta_academica
        FROM oferta_academica 
        WHERE id_oferta_academica=:id");
  
      $query->execute(['id'=>$id]);
      $item=new Confucio();
  
      while($row=$query->fetch()){
        $item->id_oferta_academica = $row['id_oferta_academica'];
      }
      return $item;
    }catch(PDOException $e){
     return false;
   }
  }

  public function Existente($identificacion){
    try{


      $sql=$this->db->connect()->prepare("SELECT identificacion FROM persona WHERE identificacion=:identificacion");
      $sql->execute(['identificacion'=>$identificacion]);
      $nombre=$sql->fetch();
      
      if($identificacion==$nombre['identificacion'] ){
        
        return $nombre['identificacion'];
      } 

      return false;
    } catch(PDOException $e){
      return false;
    }
  }

//////////////////////////////// REGISTRAR ASPIRANTE QUE YA SE ENCUENTRAN INSCRITOS EN EL SISTEMA /////////////////////////////////////////////

public function getCV($id_oferta_academica){

    try{
      $query=$this->db->connect()->prepare("SELECT id_oferta_academica, actividad.categoria 
        FROM oferta_academica, seccion, actividad 
        WHERE oferta_academica.id_seccion=seccion.id_seccion
        AND seccion.id_actividad=actividad.id_actividad
        AND id_oferta_academica=:id_oferta_academica");
      $query->execute(['id_oferta_academica'=>$id_oferta_academica]);
      $item=new Confucio();

      
      while($row=$query->fetch()){

        $item->id_oferta_academica=$row['id_oferta_academica'];
        $item->categoria=$row['categoria'];
      }
      
      return $item;
      
    }catch(PDOException $e){
      return false;
    }
    
  }


  public function getCategoria($datos){

    $oferta=$datos['id_oferta_academica'];
    $sql_1 = $this->db->connect()->query("SELECT seccion.id_seccion as seccion,cupos FROM oferta_academica,seccion WHERE oferta_academica.id_seccion=seccion.id_seccion and id_oferta_academica='$oferta' ");
    $nombre=$sql_1->fetch();
    $cupos=$nombre['cupos'];
    $seccion=$nombre['seccion'];
    try{

        $query=$this->db->connect()->prepare("SELECT aspirante.id_persona, persona.identificacion, aspirante.id_direccion, aspirante.id_nacimiento, aspirante.id_datos_interes, aspirante.id_dato_laboral, aspirante.id_nivel_academico, aspirante.id_civil,
          aspirante.id_persona_tipo, aspirante.estatus
          FROM aspirante, persona, direccion_domicilio, dato_laboral, 
          datos_interes, persona_nacimiento, nivel_academico, estado_civil, oferta_academica
          WHERE aspirante.id_persona=persona.id_persona 
          AND aspirante.id_nacimiento=persona_nacimiento.id_nacimiento
          AND aspirante.id_direccion=direccion_domicilio.id_direccion
          AND aspirante.id_datos_interes=datos_interes.id_datos_interes 
          AND aspirante.id_oferta_academica=oferta_academica.id_oferta_academica  
          AND estado_civil.id_civil=aspirante.id_civil
          AND aspirante.id_nivel_academico=nivel_academico.id_nivel_academico
          AND persona.id_persona=persona.id_persona
          AND persona.identificacion=:identificacion");
        $query->execute(['identificacion'=>$datos['identificacion']]);

        $var=$query->fetch(PDO::FETCH_ASSOC);
        

        if($var['identificacion']==''){
          ?>
          <script>
            alert('Este dato no se encuentra registrado. Debe realizar su Pre-Inscripcion');
            location.href='<?php echo constant ('URL')."main";?>';
          </script>
          <?php 
          return true;
          exit();  
        } 

        $aspirante=$this->db->connect()->prepare('INSERT INTO aspirante(estatus, id_persona_tipo, id_persona, id_direccion, id_nacimiento, id_datos_interes, id_dato_laboral, id_nivel_academico, id_civil, id_oferta_academica)
          VALUES (:estatus, :id_persona_tipo, :id_persona, :id_direccion, :id_nacimiento, :id_datos_interes, :id_dato_laboral, :id_nivel_academico, :id_civil, :id_oferta_academica)');
        $aspirante->execute(['estatus'=>'0',
         'id_persona_tipo'=>2, 
         'id_persona'=>$var['id_persona'],
         'id_direccion'=>$var['id_direccion'],
         'id_nacimiento'=>$var['id_nacimiento'], 
         'id_datos_interes'=>$var['id_datos_interes'], 
         'id_dato_laboral'=>$var['id_dato_laboral'], 
         'id_nivel_academico'=>$var['id_nivel_academico'], 
         'id_civil'=>$var['id_civil'], 
         'id_oferta_academica'=>$datos['id_oferta_academica']]);

        $resultados= $cupos - 1;
        
        $query=$this->db->connect()->query("UPDATE seccion SET cupos='$resultados' WHERE id_seccion='$seccion' ");

        
        return true;
      

    } catch(PDOException $e){
      return false;
    }

  }


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  }

?>