<?php

include_once 'models/confucio.php';

class HomeModel extends Model{

   public function __construct()
    {
        parent::__construct();
    }
    
    public function getCant($tabla){
        try{
            $query=$this->db->connect()->query("SELECT count(*) as cantidad from ".$tabla.";");
            $item=new Confucio();

            while($row=$query->fetch()){
                $item->cantidad=$row['cantidad'];
            }

            return $item;
        }catch(PDOException $e){
            return false;
        }
    }

    public function getAspiranteCant(){
        try{
            $query=$this->db->connect()->query("SELECT count(*) as cantidad FROM aspirante WHERE aspirante.estatus='0' ");
            $item=new Confucio();

            while($row=$query->fetch()){
                $item->cantidad=$row['cantidad'];
            }

            return $item;
        }catch(PDOException $e){
            return false;
        }
    }

       
    public function getCursos(){
        try{
            $query=$this->db->connect()->query("SELECT count(id_oferta_academica) as oferta FROM activar_oferta WHERE estatus='1'");
            $item=new Confucio();

            while($row=$query->fetch()){
                $item->oferta=$row['oferta'];
            }

            return $item;
        }catch(PDOException $e){
            return false;
        }
    }


    public function getEstudianteAsig($id_persona){

          try{
              $query=$this->db->connect()->prepare("SELECT count(id_inscripcion) as estudiantes FROM inscripcion, oferta_academica, seccion, docente, persona
              WHERE inscripcion.estatus='1'
              AND inscripcion.id_oferta_academica=oferta_academica.id_oferta_academica
              AND oferta_academica.id_seccion=seccion.id_seccion
              AND seccion.id_docente=docente.id_docente 
              AND docente.id_persona = persona.id_persona	
              AND persona.id_persona=:id_persona");
  $query->execute(['id_persona'=>$id_persona]);
              $item=new Confucio();
  
              while($row=$query->fetch()){
                  $item->estudiantes=$row['estudiantes'];
              }
  
              return $item;
          }catch(PDOException $e){
              return false;
          }
      }


    public function getSeccionesA($id_persona){

      //var_dump($id_persona);
     //exit();

        try{
            $query=$this->db->connect()->prepare("SELECT count(id_seccion) as seccion FROM seccion, docente, persona, periodo
            WHERE persona.id_persona= :id_persona
            AND seccion.id_docente = docente.id_docente
            AND persona.id_persona = docente.id_persona
            AND seccion.id_periodo = periodo.id_periodo
            AND periodo.estatus = 'Activo'");
$query->execute(['id_persona'=>$id_persona]);
            $item=new Confucio();

            while($row=$query->fetch()){
                $item->seccion=$row['seccion'];
            }

            return $item;
        }catch(PDOException $e){
            return false;
        }
    }


    public function getCantDocent(){
        try{
            $query=$this->db->connect()->query("SELECT count(id_docente) as docentes FROM docente WHERE id_docente!=1");
            $item=new Confucio();

            while($row=$query->fetch()){
                $item->docentes=$row['docentes'];
            }

            return $item;
        }catch(PDOException $e){
            return false;
        }
    }


}

?>