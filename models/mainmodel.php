<?php

include_once 'models/confucio.php';
include_once 'models/ubv.php';

class MainModel extends Model{

 public function __construct()
 {
  parent::__construct();
}


public function getnoticia(){
  $items=[];
  try{
    $query=$this->db->connect()->query("SELECT * FROM  portal ORDER BY id_portal ASC");
    while($row=$query->fetch()){
      $item=new ubv();
      $item->id_portal=$row['id_portal'];
      $item->captura=$row['captura'];
      $item->titulo=$row['titulo'];
      $item->contenido=$row['contenido'];
      array_push($items,$item);
      
    }
    
    
    return $items;
    
  }catch(PDOException $e){
    return false;
  }
  
}

public function getRedes(){

  try{
    $query=$this->db->connect()->query("SELECT * FROM  portal_redes");
    
    while($row=$query->fetch()){
      $item=new ubv();
      $item->link_facebook=$row['link_facebook'];
      $item->link_twitter=$row['link_twitter'];
      $item->link_instagram=$row['link_instagram'];
    }
    
    return $item;    

  }catch(PDOException $e){
    return false;
  }
  
}


public function getHistoria(){
  try{
    $query=$this->db->connect()->query("SELECT * FROM  portal_historia");
    while($row=$query->fetch()){

      $item=new ubv();
      $item->descripcion_historia=$row['descripcion_historia'];
      $item->descripcion_mision=$row['descripcion_mision'];
      $item->descripcion_vision=$row['descripcion_vision'];
       //$item->descripcion_objetivo=$row['descripcion_objetivo'];
      
    }
    return $item;
    
  }catch(PDOException $e){
    return false;
  }
  
}

public function get(){
  $items = [];
  try{
    $query = $this->db->connect()->query("SELECT actividad.categoria as categoria,oferta_academica.id_oferta_academica, seccion.id_seccion AS id_seccion, seccion.descripcion AS seccion, seccion.cupos, actividad.descripcion AS actividad, nivel.descripcion AS nivel,
      modalidad.descripcion AS modalidad, hora_inicio, hora_fin, turno.descripcion AS turno, fecha_inicio, fecha_fin, periodo.descripcion AS periodo
      FROM oferta_academica, seccion, actividad, nivel, modalidad, horario, turno, activar_oferta, periodo
      WHERE activar_oferta.id_oferta_academica=oferta_academica.id_oferta_academica
      AND oferta_academica.id_seccion=seccion.id_seccion
      AND seccion.id_actividad=actividad.id_actividad
      AND seccion.id_nivel=nivel.id_nivel
      AND seccion.id_modalidad=modalidad.id_modalidad
      AND horario.id_seccion=seccion.id_seccion
      AND horario.id_turno=turno.id_turno
      AND periodo.estatus='Activo'
      AND seccion.id_periodo=periodo.id_periodo
      AND oferta_academica.estatus='1'
      ORDER BY seccion.descripcion");

    while($row = $query->fetch()){
      $item = new Confucio();
      $item->id_oferta_academica = $row['id_oferta_academica'];
      $item->id_seccion = $row['id_seccion'];
      $item->seccion = $row['seccion'];
      $item->cupos = $row['cupos'];
      $item->actividad  = $row['actividad'];
      $item->nivel = $row['nivel'];
      $item->modalidad  = $row['modalidad'];
      $item->turno = $row['turno'];
      $item->hora_inicio  = $row['hora_inicio'];
      $item->hora_fin = $row['hora_fin'];
      $item->fecha_inicio  = $row['fecha_inicio'];
      $item->fecha_fin = $row['fecha_fin'];
      $item->categoria = $row['categoria'];
      $item->periodo = $row['periodo'];
      array_push($items, $item);
    }
    return $items;
  }catch(PDOException $e){
    return [];
  }
}

public function getDetalles($id){

  try{
    $query=$this->db->connect()->prepare("SELECT seccion.id_seccion, oferta_academica.id_oferta_academica, seccion.descripcion, seccion.cupos, actividad.descripcion AS actividad, nivel.descripcion AS nivel, 
      modalidad.descripcion AS modalidad, periodo.descripcion AS periodo,
      hora_inicio, hora_fin, turno.descripcion AS turno
      FROM seccion, actividad, nivel, modalidad, periodo, horario, turno, oferta_academica
      WHERE seccion.id_actividad = actividad.id_actividad
      AND seccion.id_nivel = nivel.id_nivel
      AND seccion.id_modalidad = modalidad.id_modalidad
      AND seccion.id_periodo = periodo.id_periodo
      AND seccion.id_seccion = horario.id_seccion
      AND seccion.id_seccion = oferta_academica.id_seccion
      AND horario.id_turno = turno.id_turno
      AND seccion.id_seccion = :id");
    
    $query->execute(['id'=>$id]);
    $item=new Confucio();
    
    while($row=$query->fetch()){
      $item->id_seccion  = $row['id_seccion'];
      $item->id_oferta_academica  = $row['id_oferta_academica'];
      $item->descripcion = $row['descripcion'];
      $item->cupos = $row['cupos'];
      $item->actividad   = $row['actividad'];
      $item->nivel       = $row['nivel'];
      $item->modalidad   = $row['modalidad'];
      $item->periodo     = $row['periodo'];
      $item->hora_inicio = $row['hora_inicio'];
      $item->hora_fin    = $row['hora_fin'];
      $item->turno       = $row['turno'];
    }
    return $item;
  }catch(PDOException $e){
   return false;
 }
}

public function getbyID_dias($id_seccion){ 

  $items=[];
  $query = $this->db->connect()->prepare("SELECT seccion.id_seccion, horario_dia.id_dia, dia.descripcion AS dia
    FROM dia, horario_dia, horario, seccion
    WHERE horario_dia.id_dia = dia.id_dia
    AND horario_dia.id_horario = horario.id_horario 
    AND seccion.id_seccion = horario.id_seccion 
    AND seccion.id_seccion = :id_seccion");
  
  try{
    $query ->execute(['id_seccion'=>$id_seccion]);
    
    while($row = $query->fetch()){
      $item = new Confucio();
      $item->id_dia = $row['id_dia'];
      $item->dia = $row['dia'];
      array_push($items,$item);
    }
    
    
    return $items;
  }catch(PDOException  $e){
    return null;
  }
}

public function getDiabyID($id_seccion, $id_dia){
  
  try{
    $query=$this->db->connect()->prepare("SELECT seccion.id_seccion, horario_dia.id_dia, dia.descripcion AS dia 
      FROM dia, horario_dia, horario, seccion
      WHERE horario_dia.id_dia=dia.id_dia
      AND horario_dia.id_horario=horario.id_horario
      AND seccion.id_seccion=horario.id_seccion
      AND seccion.id_seccion= :id_seccion
      AND horario_dia.id_dia= :id_dia");
    
    $query->execute([
      'id_seccion'=>$id_seccion,
      'id_dia'=>$id_dia
    ]);
    
    $item=new Confucio();
    while($row=$query->fetch()){
      $item->id_dia = $row['id_dia'];
      $item->dia = $row['dia'];
    }
    
    return $item;
    
  } catch(PDOException $e){
    return false;
  }
  
}

    public function getCertificados(){
      
      $items=[];
      try{
        $query=$this->db->connect()->query("SELECT * FROM  tipo_certificado ORDER BY id_tipo_certificado ASC FETCH FIRST 10 ROWS ONLY");
        while($row=$query->fetch()){
          $item=new ubv();
          $item->id_tipo_certificado=$row['id_tipo_certificado'];
          $item->descripcion=$row['descripcion'];
          $item->codigo=$row['codigo'];
          array_push($items,$item);
        }

        return $items;
        
      }catch(PDOException $e){
        return false;
      }
      
    }


    public function getCertificadosDos(){
      
      $items=[];
      try{
        $query=$this->db->connect()->query("SELECT * FROM  tipo_certificado 
        WHERE id_tipo_certificado!=1 
        AND id_tipo_certificado!=2
        AND id_tipo_certificado!=3
        AND id_tipo_certificado!=4
        AND id_tipo_certificado!=5
        AND id_tipo_certificado!=6
        AND id_tipo_certificado!=7
        AND id_tipo_certificado!=8
        AND id_tipo_certificado!=9
        AND id_tipo_certificado!=10
        ORDER BY id_tipo_certificado ASC FETCH FIRST 10 ROWS ONLY");
        while($row=$query->fetch()){
          $item=new ubv();
          $item->id_tipo_certificado=$row['id_tipo_certificado'];
          $item->descripcion=$row['descripcion'];
          $item->codigo=$row['codigo'];
          array_push($items,$item);
        }

        return $items;
        
      }catch(PDOException $e){
        return false;
      }
      
    }


    public function getCertificadosTres(){
      
      $items=[];
      try{
        $query=$this->db->connect()->query("SELECT * FROM  tipo_certificado 
        WHERE id_tipo_certificado!=1 
        AND id_tipo_certificado!=2
        AND id_tipo_certificado!=3
        AND id_tipo_certificado!=4
        AND id_tipo_certificado!=5
        AND id_tipo_certificado!=6
        AND id_tipo_certificado!=7
        AND id_tipo_certificado!=8
        AND id_tipo_certificado!=9
        AND id_tipo_certificado!=10
        AND id_tipo_certificado!=11 
        AND id_tipo_certificado!=12
        AND id_tipo_certificado!=13
        AND id_tipo_certificado!=14
        AND id_tipo_certificado!=15
        AND id_tipo_certificado!=16
        AND id_tipo_certificado!=17
        AND id_tipo_certificado!=18
        AND id_tipo_certificado!=19
        AND id_tipo_certificado!=20
        ORDER BY id_tipo_certificado ASC FETCH FIRST 10 ROWS ONLY");
        while($row=$query->fetch()){
          $item=new ubv();
          $item->id_tipo_certificado=$row['id_tipo_certificado'];
          $item->descripcion=$row['descripcion'];
          $item->codigo=$row['codigo'];
          array_push($items,$item);
        }

        return $items;
        
      }catch(PDOException $e){
        return false;
      }
      
    }



    public function getCertificadosCuatro(){
      
      $items=[];
      try{
        $query=$this->db->connect()->query("SELECT * FROM  tipo_certificado 
        WHERE id_tipo_certificado!=11
        AND id_tipo_certificado!=12
        AND id_tipo_certificado!=13
        AND id_tipo_certificado!=14
        AND id_tipo_certificado!=15
        AND id_tipo_certificado!=16
        AND id_tipo_certificado!=17
        AND id_tipo_certificado!=18
        AND id_tipo_certificado!=19
        AND id_tipo_certificado!=20
        ORDER BY id_tipo_certificado DESC FETCH FIRST 11 ROWS ONLY");
        while($row=$query->fetch()){
          $item=new ubv();
          $item->id_tipo_certificado=$row['id_tipo_certificado'];
          $item->descripcion=$row['descripcion'];
          $item->codigo=$row['codigo'];
          array_push($items,$item);
        }

        return $items;
        
      }catch(PDOException $e){
        return false;
      }
      
    }



    public function getTipoCertificado($id_tipo_certificado){

      try{
        $query=$this->db->connect()->prepare("SELECT id_tipo_certificado FROM tipo_certificado WHERE tipo_certificado.id_tipo_certificado = :id_tipo_certificado");
        
        $query->execute(['id_tipo_certificado'=>$id_tipo_certificado]);
        $item=new ubv();
        
        while($row=$query->fetch()){
          $item->id_tipo_certificado = $row['id_tipo_certificado'];
        }
        return $item;
      }catch(PDOException $e){
      return false;
    }
    
    }


    public function getLeeCertificado($datos){
      try{

          $query=$this->db->connect()->prepare("SELECT dato_certificado.id_tipo_certificado, identificacion FROM dato_certificado WHERE identificacion=:identificacion AND id_tipo_certificado=:id_tipo_certificado");
          $query->execute([
              'identificacion'=> $datos['identificacion'],
              'id_tipo_certificado'=> $datos['id_tipo_certificado']
              ]);
          $var=$query->fetch(PDO::FETCH_ASSOC);

          //validar coincidencia de datos de entrada con los de bbdd
          if($var['identificacion']==$datos['identificacion'] && $var['id_tipo_certificado'] == $datos['id_tipo_certificado']){

              return true;
          }else {
              return false;
          }//end if validar coincidencia

      } catch(PDOException $e){
              return false;
      }
    }


    public function getDatosCertificados($identificacion, $tipo){
           
      $item=new ubv();
      
      $query=$this->db->connect()->prepare("SELECT identificacion, nombres, apellidos, current_date as fecha, 
                                            dato_certificado.id_tipo_certificado as tipo,
                                            tipo_certificado.descripcion,
                                            tipo_certificado.codigo FROM dato_certificado, tipo_certificado 
                                            WHERE dato_certificado.id_tipo_certificado=tipo_certificado.id_tipo_certificado AND identificacion=:identificacion AND dato_certificado.id_tipo_certificado=:tipo");
      try{
      
          $query->execute(['identificacion'=>$identificacion, 'tipo'=>$tipo]);
      
          while ($row=$query->fetch()){
            
              $item->identificacion=$row['identificacion'];
              $item->nombres=$row['nombres'];
              $item->apellidos=$row['apellidos'];
              $item->fecha=$row['fecha'];
              $item->tipo=$row['tipo'];
              $item->descripcion=$row['descripcion'];
              $item->codigo=$row['codigo'];
  
  
          }
          return $item;
          
      }catch(PDOException $e){
      
          return null;
      
      }
      
        
    }

}
?>