<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Confucio-Ver solicitud</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico' />
</head>

<body>

<?php require 'views/header.php'; ?>  
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-body">
            <div class="row">
           
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Aspirante </h4>
                    <div class="card-header-action">
                    <a href="<?php echo constant ('URL')?>pre_inscripcion/solicitud" class="btn btn-warning">Volver</a>
                  </div>
                  </div>
                  <div class="row">
                  <div class="card-body">
                    <ul class="nav nav-pills" id="myTab3" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="home-tab3" data-toggle="tab" href="#home3" role="tab"
                          aria-controls="home" aria-selected="true">Información Personal</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="profile-tab3" data-toggle="tab" href="#profile3" role="tab"
                          aria-controls="profile" aria-selected="false">Información Laboral</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="contact-tab3" data-toggle="tab" href="#contact3" role="tab"
                          aria-controls="contact" aria-selected="false">Información de Interes</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="oferta-tab3" data-toggle="tab" href="#oferta3" role="tab"
                          aria-controls="contact" aria-selected="false">Curso Pre-inscrito</a>
                      </li>
                    </ul>

   <div class="tab-content" id="myTabContent2">
   <div class="tab-pane fade show active" id="home3" role="tabpanel" aria-labelledby="home-tab3">
         <div class="row">
         <div class="col-lg-12 col-xl-6">
            <div class="table-responsive">
                <table class="table m-0">
                    <tbody>
                        <tr>
                            <th scope="row">Tipo de Documento de Identidad</th>
                            <td><?php echo $this->aspirante->tipo_documento;?></td>
                        </tr>
                        <tr>
                            <th scope="row">Nro. de Identificación</th>
                            <td><?php echo $this->aspirante->identificacion;?></td>
                        </tr>
                        <tr>
                            <th scope="row">Nombres</th>
                            <td><?php echo $this->aspirante->primer_nombre . " " . $this->aspirante->segundo_nombre;?></td>
                        </tr>
                        <tr>
                            <th scope="row">Apellidos</th>
                            <td><?php echo $this->aspirante->primer_apellido . " " . $this->aspirante->segundo_apellido;?></td>
                        </tr>
                        <tr>
                            <th scope="row">Género</th>
                            <td><?php echo $this->aspirante->genero;?></td>
                        </tr>
                        <tr>
                            <th scope="row">Estado Civil</th>
                            <td><?php echo $this->aspirante->civil;?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>


        <div class="col-lg-12 col-xl-6">
        <div class="table-responsive">
             <table class="table">
                <tbody>
                <tr>
                        <th scope="row">Fecha de Nacimiento</th>
                        <td><?php echo $this->aspirante->fecha_nacimiento; ?></td>
                    </tr>
                    <tr>
                        <th scope="row">Lugar de nacimiento (Pais / Estado)</th>
                        <td><?php echo $this->aspirante->pais. " / " . $this->aspirante->estado; ?></td>
                    </tr>
                    <tr>
                        <th scope="row">Correo Electrónico</th>
                        <td><?php echo $this->aspirante->correo; ?></td>
                    </tr>
                     <tr>
                        <th scope="row">Teléfono Celular</th>
                        <td><?php echo $this->aspirante->telefono; ?></td>
                    </tr>
                    <tr>
                        <th scope="row">Dirección</th>
                        <td><?php echo $this->aspirante->direccion; ?></td>
                    </tr>
                    <tr>
                        <th scope="row">Telefono de Habitación</th>
                        <td><?php echo $this->aspirante->telefono_habitacion; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div><!-- end of table col-lg-6 -->

    
         </div>
             </div>


   <div class="tab-pane fade" id="profile3" role="tabpanel" aria-labelledby="profile-tab3">
        <div class="row">
         <div class="col-lg-12 col-xl-6">
            <div class="table-responsive">
                <table class="table m-0">
                    <tbody>
                    <?php if($this->aspirante->empleo == 'SI'){?>
                        <tr>
                            <th scope="row">¿Trabaja?</th>
                            <td><?php echo $this->aspirante->empleo;?></td>
                        </tr>
                        <?php if($this->aspirante->tipo_institucion == 'Privada'){?>
                        <tr>
                            <th scope="row">Tipo Institución</th>
                            <td><?php echo $this->aspirante->tipo_institucion;?></td>
                        </tr>
                        <tr>
                            <th scope="row">Institución de Procedencia</th>
                            <td><?php echo $this->aspirante->inst_privada;?></td>
                        </tr>
                        <?php } ?>
                        <?php if($this->aspirante->tipo_institucion == 'Organismo'){?>
                        <tr>
                            <th scope="row">Tipo Institución</th>
                            <td><?php echo $this->aspirante->tipo_institucion;?></td>
                        </tr>
                        <tr>
                            <th scope="row">Institución de Procedencia</th>
                            <td><?php echo $this->aspirante->organismo;?></td>
                        </tr>
                        <?php } ?>
                        <?php if($this->aspirante->tipo_institucion == 'Investigacion'){?>
                        <tr>
                            <th scope="row">Tipo Institución</th>
                            <td><?php echo $this->aspirante->tipo_institucion;?></td>
                        </tr>
                        <tr>
                            <th scope="row">Institución de Procedencia</th>
                            <td><?php echo $this->aspirante->inst_inv;?></td>
                        </tr>
                        <?php } ?>
                        <tr>
                        <?php if($this->aspirante->trabajador_ubv == 'SI'){?>
                            <th scope="row">¿Trabajador UBV?</th>
                            <td><?php echo $this->aspirante->trabajador_ubv;?></td>
                        </tr>
                        <?php } ?>
                        <?php if($this->aspirante->trabajador_ubv == 'NO'){?>
                            <th scope="row">¿Trabajador UBV?</th>
                            <td><?php echo $this->aspirante->trabajador_ubv;?></td>
                        </tr>
                        <tr>
                            <th scope="row">Institución de Procedencia</th>
                            <td><?php echo $this->aspirante->inst_publica;?></td>
                        </tr>
                        <?php } ?>
                        <?php } else{?>
                    <tr>
                    <th scope="row">¿Trabaja?</th>    
                    <td><div class="badge badge-pill badge-danger"> No Trabaja</div></td>
                    </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-lg-12 col-xl-6">
            <div class="table-responsive">
                <table class="table m-0">
                    <tbody>
                    <?php if($this->aspirante->empleo == 'SI'){?>
                        <tr>
                            <th scope="row">Telefono de Oficina</th>
                            <td><?php echo $this->aspirante->telefono_institucion ;?></td>
                        </tr>
                        <tr>
                            <th scope="row">Correo Institucional</th>
                            <td><?php echo $this->aspirante->correo_institucion;?></td>
                        </tr>
                            <th scope="row">Dirección de la institución</th>
                            <td><?php echo $this->aspirante->direccion_institucion;?></td>
                        </tr>
                            <th scope="row">Cargo que Desempeña</th>
                            <td><?php echo $this->aspirante->cargo;?></td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
 </div>

  <div class="tab-pane fade" id="contact3" role="tabpanel" aria-labelledby="contact-tab3">
       <div class="row">
         <div class="col-lg-12 col-xl-6">
            <div class="table-responsive">
                <table class="table m-0">
                    <tbody>
                    <?php if($this->aspirante->discapacidad == 'si'){?>
                        <tr>
                        <th scope="row">¿Presenta Discapacidad?</th>
                            <td><?php echo $this->aspirante->discapacidad;?></td>
                        </tr>
                        <tr>
                            <th scope="row">Tipo de Discapacidad</th>
                            <td><?php echo $this->aspirante->tipo;?></td>
                        </tr>
                        <tr>
                            <th scope="row">Grupo Sanguineo</th>
                            <td><?php echo $this->aspirante->grupo_sanguineo;?></td>
                        </tr>
                        <?php } else{?>
                    <tr>
                    <th scope="row">¿Presenta Discapacidad?</th>    
                    <td><div class="badge badge-pill badge-danger mb-1 float-right">No Posee discapacidad</div></td>
                    </tr>
                       <?php }?>
                       <?php if($this->aspirante->enfermedad == 'si'){?>
                        <tr>
                            <th scope="row">¿Presenta alguna enfermedad?</th>
                            <td><?php echo $this->aspirante->enfermedad;?></td>
                        </tr>
                        <tr>
                            <th scope="row">Enfermedad que Presenta</th>
                            <td><?php echo $this->aspirante->descripcion_enfermedad;?></td>
                        </tr>
                        <?php } else{?>
                    <tr>
                    <th scope="row">¿Presenta alguna enfermedad?</th>    
                    <td><div class="badge badge-pill badge-danger mb-1 float-right">No Posee Enfermedad</div></td>
                    </tr>
                       <?php }?>
                       <?php if($this->aspirante->idioma == 'Si'){?>
                        <tr>
                            <th scope="row">¿Habla otro idioma?</th>
                            <td><?php echo $this->aspirante->idioma;?></td>
                        </tr>
                        <tr>
                            <th scope="row">Idiomas que Habla</th>
                            <td><?php echo $this->aspirante->idiomas_descripcion;?></td>
                        </tr>
                        <?php } else{?>
                    <tr>
                    <th scope="row">¿Habla otro idioma?</th>    
                    <td><div class="badge badge-pill badge-danger mb-1 float-right">No Habla otro Idioma</div></td>
                    </tr>
                       <?php }?>    
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-lg-12 col-xl-6">
            <div class="table-responsive">
                <table class="table m-0">
                    <tbody>
                    <?php if($this->aspirante->mision_sucre == 'SI'){?>
                        <tr>
                            <th scope="row">¿Pertenece a la Misión Sucre?</th>
                            <td><?php echo $this->aspirante->mision_sucre ;?></td>
                        </tr>
                        <?php } else{?>
                            <tr>
                    <th scope="row">¿Pertenece a la Misión Sucre?</th>    
                    <td><div class="badge badge-pill badge-danger mb-1 float-right">No pertenece a la Misión Sucre</div></td>
                    </tr>
                       <?php }?>    
                       <?php if($this->aspirante->estudiante_ubv == 'SI'){?>
                        <tr>
                            <th scope="row">¿Estudiante UBV?</th>
                            <td><?php echo $this->aspirante->estudiante_ubv ;?></td>
                        </tr>
                        <?php } else{?>
                    <tr>
                    <th scope="row">¿Estudiante UBV?</th>    
                    <td><?php echo $this->aspirante->estudiante_ubv ;?></td>
                    </tr>
                       <?php }?>   
                       <tr>
                            <th scope="row">Experiencia Académica</th>
                            <td><?php echo $this->aspirante->nivel_aca ;?></td>
                        </tr>
                        <tr>
                    <th scope="row">Institucion Academica</th>    
                    <td><?php echo $this->aspirante->institucion_nivel ;?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

   </div>
</div>

<div class="tab-pane fade" id="oferta3" role="tabpanel" aria-labelledby="oferta-tab3">
       <div class="row">
         <div class="col-lg-12 col-xl-4">
            <div class="table-responsive">
                <table class="table m-0">
                    <tbody>
                        <tr>
                        <th scope="row">Curso elegido:</th>
                            <td><?php echo $this->aspirante->oferta;?></td>
                        </tr>
                        <tr>
                            <th scope="row">Modalidad:</th>
                            <td><?php echo $this->aspirante->modalidad;?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-lg-12 col-xl-4">
            <div class="table-responsive">
                <table class="table m-0">
                    <tbody>
                        <tr>
                        <th scope="row">Turno:</th>
                            <td><?php echo $this->aspirante->turno;?></td>
                        </tr>
                        <tr>
                            <th scope="row">Horario:</th>
                            <td><?php echo date("g:i a", strtotime($this->aspirante->hora_inicio)).' - '.date("g:i a", strtotime($this->aspirante->hora_fin)); ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-lg-12 col-xl-4">
            <div class="table-responsive">
                <table class="table m-0">
                    <tbody>
                        <tr>
                            <td>
                               <a href="<?php echo constant('URL') . 'pre_inscripcion/editarSolicitud/' . $this->aspirante->id_aspirante; ?>" class="btn btn-outline-success" title="Editar solicitud"><i class="fas fa-edit"></i>  Editar solicitud</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

   </div>
</div>




   </div>
</div>
                   

</div> <!-- END TAB -->
</form>
                  </div>
                </div>
              </div>

     



            </div>
          </div>
        </div>
 <!--End Main Content-->
                 
                 </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div>
      <?php require 'views/footer.php'; ?>
  <!-- General JS Scripts -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <!-- JS Libraies -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-ui/jquery-ui.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/datatables.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno//assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>
</body>

</html>