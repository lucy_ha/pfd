<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Confucio-Deposito</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/css/select2.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant('URL'); ?>src/principal_uno/assets/bundles/bootstrap-daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">

  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico' />
</head>

<body>

<?php require 'views/header.php'; ?>  
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-body">

          
            <div class="row clearfix">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Registrar Deposito Bancario</h4>
                    <div class="card-header-action">
                    <a href="<?php echo constant ('URL')?>pre_inscripcion/solicitud" class="btn btn-warning">Volver</a>
                  </div>
                  </div>
                  <div class="card-body">
                  <form action="<?php echo constant ('URL');?>pre_inscripcion/FormRegistrarDeposito" id="wizard_with_validation"  class="needs-validation"  novalidate="" enctype="multipart/form-data" method="POST">
                  <input name="id_aspirante" type="hidden" class="form-control"value="<?php echo $this->aspirante->id_aspirante; ?>">
                      <h3>Datos de Pago</h3>
                      <fieldset>
                      <div class="row">
                  <div class="col-lg-4">
                        <label>¿Exonerado?<span style="color: red;">&nbsp;*</span></label>
                        <div class="form-group">
                          <select name="exonerado" class="form-control select_2 required" onChange="mostrar(this.value);">
                            <option value="">Seleccione</option>
                            <option value="SI">SI</option>
                            <option value="NO">NO</option>
                          </select>
                        </div>
                      </div>
 <div id=SI style="display: none;">
   <label>Modalidad de exoneración <span style="color: red;">&nbsp;*</span></label>
 <div class="form-group">
    <select name="modalidad_exo" class="form-control select_2" onChange="visualizar(this.value);">
      <option value="0%">Seleccione</option>
      <option value="100%">100%</option>
      <option value="50%">50%</option>
    </select>
  </div>
</div><!--end div para select boolean--> 
</div><!--end class row-->

   <div class="row" id=exonerado style="display: none;">

                      <div class="col-lg-6">
                        <div class="form-group">
                          <div class="form-line">
                            <label class="form-label">N° de Vouche<span style="color: red;">&nbsp;*</span></label>
                            <input type="text" name="numero_vouche" class="form-control">
                          </div>
                        </div>
                      </div>

                    <div class="col-lg-6">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Monto<span style="color: red;">&nbsp;*</span></label>
                            <input type="text" id="dinero" name="monto" class="form-control">
                          </div>
                        </div>
                    </div>

                    <script>
                                            $("#dinero").on({
                                            "focus": function (event) {
                                            $(event.target).select();
                                                                },
                                            "keyup": function (event) {
                                        $(event.target).val(function (index, value ) {
                                        return value.replace(/\D/g, "")
                                        .replace(/([0-9])([0-9]{2})$/, '$1,$2')
                                        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
                                            });
                                    }
                                            });
                                        
                                        </script>

                    <div class="col-lg-6">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label>Fecha de Deposito<span style="color: red;">&nbsp;*</span></label>
                            <div class="form-group">
                              <input name="fecha_deposito" class="form-control datepicker">
                            </div>
                          </div>
                        </div>
                      </div>

                    <div class="col-lg-6">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Banco del Deposito<span style="color: red;">&nbsp;*</span></label>
                            <input type="text" name="observacion" class="form-control">
                          </div>
                        </div>
                    </div>
             </div>
 




<div class="row" id=modalidad_exo style="display: none;">

<div class="col-lg-6">
                        <div class="form-group">
                          <div class="form-line">
                            <label class="form-label">N° de Vouche<span style="color: red;">&nbsp;*</span></label>
                            <input type="text" name="numero_vouche_50" class="form-control">
                          </div>
                        </div>
                      </div>

                    <div class="col-lg-6">
                    <label>Monto <span style="color: red;">&nbsp;*</span></label>
                      <div class="input-group">
                        <input type="text" id="cash" name="monto_50" class="form-control">
                      </div>
                    </div>

                    <script>
                                            $("#cash").on({
                                            "focus": function (event) {
                                            $(event.target).select();
                                                                },
                                            "keyup": function (event) {
                                        $(event.target).val(function (index, value ) {
                                        return value.replace(/\D/g, "")
                                        .replace(/([0-9])([0-9]{2})$/, '$1,$2')
                                        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
                                            });
                                    }
                                            });
                                        
                                        </script>

                    <div class="col-lg-6">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label>Fecha de Deposito<span style="color: red;">&nbsp;*</span></label>
                            <div class="form-group">
                              <input name="fecha_deposito_50" class="form-control datepicker">
                            </div>
                          </div>
                        </div>
                      </div>

                    <div class="col-lg-6">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Banco del Deposito <span style="color: red;">&nbsp;*</span></label>
                            <input type="text" name="observacion_50" class="form-control">
                          </div>
                        </div>
                    </div>

</div><!--end div para select boolean--> 

                      </fieldset>
                    </form>
                  </div>
                </div>
              </div>
            </div>
       
        </section>

      </div>

      <?php require 'views/footer.php'; ?>

        
  <!-- General JS Scripts -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-steps/jquery.steps.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/js/select2.full.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-validation/dist/jquery.validate.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/form-wizard.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>
  <script src="<?php echo constant('URL'); ?>src/principal_uno/assets/bundles/bootstrap-daterangepicker/daterangepicker.js"></script>

 

    <script>
       $(document).ready(function(){

    $(".select_2").select2({
                                placeholder: "Seleccione",
                                width: "100%",
                                dropdownAutoWidth: true
                            });

                          });
      </script>


<script type="text/javascript">
    function mostrar(id) {
      if (id == "SI") {
        $("#exonerado").hide();
      }
      if (id == "NO") {
        $("#exonerado").show();
      }
      if (id == "SI") {
        $("#SI").show();
      }
      if (id == "NO") {
        $("#SI").hide();
      }
    }
  </script>

<script type="text/javascript">
    function visualizar(id) {
      if (id == "100%") {
        $("#modalidad_exo").hide();
      }
      if (id == "50%") {
        $("#modalidad_exo").show();
      }
    }
  </script>  



</body>


</html>