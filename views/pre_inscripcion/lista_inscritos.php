<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Confucio-Inscritos</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico'/>
</head>

<body>

<?php require 'views/header.php'; ?>  
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-body">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Listado de Inscritos</h4>
                  </div>
                  <div class="card-body">
                  <?php echo $this->mensaje;?>
                    <div class="table-responsive">
                      <table class="table table-striped" id="table-1">
                        <thead>
                          <tr>
                            <th class="text-center">
                              #
                            </th>
                            <th>Nombre y Apellido</th>
                            <th>Cedula de Identidad</th>
                            <th>Correo Electronico</th>
                            <th>Curso Inscrito</th>
                            <th>Estatus</th>
                            <th>Acciones</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php 
                      foreach($this->inscripciones as $row){
                      $inscripcion=new Confucio();
                      $inscripcion=$row;?> 
                       <tr>
                          <td><?php echo $inscripcion->id_inscripcion; ?></td>
                          <td><?php echo $inscripcion->primer_nombre . " " . $inscripcion->primer_apellido; ?></td>
                          <td><?php echo $inscripcion->identificacion; ?></td>
                          <td><?php echo $inscripcion->correo; ?></td>
                          <td><?php echo $inscripcion->oferta; ?></td>
                          <td><?php echo $inscripcion->tipo; ?></td>
                          <td> 
                          <a href="<?php echo constant ('URL') . "pre_inscripcion/editarEstudiante/" . $inscripcion->id_inscripcion;?>" class="btn btn-icon btn-success" data-toggle="tooltip" data-placement="bottom" title="Editar Datos Registrados"><i class="far fa-edit"></i></a>
                          <a href="<?php echo constant ('URL') . "pre_inscripcion/VerInscripcionDetalle/" . $inscripcion->id_inscripcion ;?>" class="btn btn-icon btn-info" data-toggle="tooltip" data-placement="bottom" title="Ver Inscripción"><i class="far fa-eye"></i></a>
                          <a href="<?php echo constant ('URL') . "pre_inscripcion/comp_inscripcion/" . $inscripcion->id_inscripcion ;?>" class="btn btn-icon btn-danger" data-toggle="tooltip" data-placement="bottom" title="Comprobante de Inscripcion"><i class="fas fa-file-pdf"></i></a>
                          <a href="<?php echo constant ('URL') . "pre_inscripcion/cons_estudio/" . $inscripcion->id_inscripcion ;?>" class="btn btn-icon btn-danger" data-toggle="tooltip" data-placement="bottom" title="Constancia de Estudio"><i class="fas fa-file-pdf"></i></a>
                        </td>
                      </tr>
                  <?php }?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
 <!--End Main Content-->
                 
                 </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div>
      <?php require 'views/footer.php'; ?>
  <!-- General JS Scripts -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <!-- JS Libraies -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-ui/jquery-ui.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/datatables.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno//assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>
</body>

</html>