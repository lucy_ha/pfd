<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Confucio - Inscritos</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/css/select2.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico'/>
</head>

<body>

  <?php require 'views/header.php'; ?>  

  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <div class="section-body">
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="card-header">
                <h4>Editar Inscripción: <?php echo $this->estudiante->identificacion; ?></h4>
                <div class="card-header-action">
                  <a href="<?php echo constant ('URL')?>pre_inscripcion/lista_inscritos" class="btn btn-warning">Volver</a>
                </div>
              </div>
              <div class="card-body">
                <form action="<?php echo constant('URL'); ?>pre_inscripcion/updateInsc" id="wizard_with_validation"  novalidate="" method="POST">
                  <h3>Sección</h3>
                  <fieldset>
                    <div class="row">
                      <input type="hidden" name="id_inscripcion" value="<?php echo $this->estudiante->id_inscripcion; ?>">
                      <input type="hidden" name="id_seccion" value="<?php echo $this->estudiante->id_seccion; ?>">
                      <input type="hidden" name="id_aspirante" value="<?php echo $this->estudiante->id_aspirante; ?>">

                      <div class="col-lg-12 col-xl-4">
                        <div class="table-responsive">
                          <table class="table m-0">
                            <tbody>
                              <tr>
                                <th>Sección actual: </th>
                                <td><?php echo $this->estudiante->oferta;?></td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>

                      <div class="col-lg-4">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Secciones ofertadas<span style="color: red;">&nbsp;*</span></label>
                            <select name="id_oferta_academica" class="form-control select2 required">
                              <option value="">Seleccione</option> 
                              <?php 
                              foreach($this->secciones as $row){
                                $seccion=new Confucio();
                                $seccion=$row; ?> 
                                <option value="<?php echo $seccion->id_oferta_academica;?>"><?php echo $seccion->seccion;?></option>             
                              <?php } ?>
                            </select> 
                          </div> 
                        </div>
                      </div>

                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <?php require 'views/footer.php'; ?>

  <!-- General JS Scripts -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/js/select2.full.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-validation/dist/jquery.validate.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/advance-elements/select2-custom.js"></script>
  <!-- JS Libraies -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-steps/jquery.steps.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/form-wizard.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>
</body>


</html>