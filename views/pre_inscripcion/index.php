<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Plan Formación Docente-Aspirante</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/css/select2.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico' />
  
</head>

<!-- class="font-bg-b" -->

<body>
  <div class="loader"></div>
  <div id="app">
    <section class="section">
      <div class="container mt-5">
      <div class="row clearfix">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Registro para Aspirante</h4>
                    <div class="card-header-action">
                    <a href="<?php echo constant ('URL')?>main" class="btn btn-light">Volver</a>
                  </div>
                  </div>
                  <div class="card-body">
                    <form action="<?php echo constant('URL'); ?>pre_inscripcion/FormAspirante" id="wizard_with_validation"  class="needs-validation" method="POST">
                    <?php echo $this->mensaje;?>
                      <h3>Datos Personales</h3>
                      <fieldset>
                    <div class="row">
                    <input name="id_oferta_academica" type="hidden" class="form-control" value="<?php echo $this->oferta->id_oferta_academica; ?>">
                      <div class="col-lg-6">
                        <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Primer Nombre<span style="color: red;">&nbsp;*</span></span>
                        </div>
                        <input id="" name="primer_nombre" type="text" class=" form-control " placeholder="Ingrese su Primer Nombre" onkeypress="return soloLetras(event)">
                      </div>
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Primer Apellido<span style="color: red;">&nbsp;*</span></span>
                        </div>
                        <input id="" name="primer_apellido" type="text" class=" form-control " placeholder="Ingrese su Primer Apellido" onkeypress="return soloLetras(event)">
                      </div>
                 </div>

                  <div class="col-lg-6">
                    <div class="form-group form-float">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Segundo Nombre (Opcional)</span>
                        </div>
                        <input name="segundo_nombre" type="text" class="form-control " placeholder="Ingrese su Segundo Nombre" onkeypress="return soloLetras(event)">
                      </div>
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Segundo Apellido (Opcional)</span>
                        </div>
                        <input name="segundo_apellido" type="text" class="form-control " placeholder="Ingrese su Segundo Apellido" onkeypress="return soloLetras(event)">
                      </div>
                   </div>
                   
              </div>


                   <div class="row">
                   <div class="col-lg-6">
                    <br>
                   <label class="form-label">Tipo de Identidad<span style="color: red;">&nbsp;*</span></label>
                    <div class="form-group">
                      <select name="id_tipo_documento_identidad" class="form-control select_2" >
                      <option value="">Seleccione</option>
                      <?php 
                      foreach($this->tipos as $row){
                      $tipo=new Confucio();
                      $tipo=$row;?> 
                    <option value="<?php echo $tipo->id;?>"><?php echo $tipo->descripcion;?></option>             
                  <?php }?>
                      </select>
                    </div>
                      </div>

                      <div class="col-lg-6">
                      <br>
                        <div class="form-group">
                          <div class="form-line">
                            <label class="form-label">Documento de Identidad o Pasaporte<span style="color: red;">&nbsp;*</span></label>
                            <input type="text" name="identificacion" class="form-control"  minlength="7"  maxlength="9" onkeypress="return soloNumeros(event)" required>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                      <label class="form-label">Estado Civil<span style="color: red;">&nbsp;*</span></label>
                      <div class="form-group">
                        <select name="id_civil" class="form-control select_2" >
                          <option value="">Seleccione</option>
                          <?php
                          foreach ($this->civiles as $row) {
                            $civil = new Confucio();
                            $civil = $row; ?>
                            <option value="<?php echo $civil->id_civil; ?>"><?php echo $civil->descripcion; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>

                    <div class="col-lg-4">
                      <label class="form-label">Género<span style="color: red;">&nbsp;*</span></label>
                      <div class="form-group form-float">
                        <div class="form-line ">
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline1" name="genero" value="Femenino"
                            class="custom-control-input">
                            <label class="custom-control-label" for="customRadioInline1">Femenino</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline2" name="genero" value="Masculino"
                            class="custom-control-input">
                            <label class="custom-control-label" for="customRadioInline2">Masculino</label>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Correo Electronico<span style="color: red;">&nbsp;*</span></label>
                            <input type="email" name="correo" class="form-control" maxlength="80" required>
                          </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Telefono<span style="color: red;">&nbsp;*</span></label>
                            <input id="telefono" name="telefono" type="text" class=" form-control us_telephone" data-mask="(0999) 999-9999" placeholder="Ingrese su Nro. de Teléfono">
                          </div>
                        </div>
                    </div>
                      </fieldset>
                      
                      <h3>Datos Generales</h3>
                      <fieldset>
                      <div class="row">
                      <div class="col-lg-4">
                      <div class="form-group">
                        <div class="form-line">
                          <label>Fecha de Nacimiento<span style="color: red;">&nbsp;*</span></label>
                          <div class="form-group">
                            <input type="date" name="fecha_nacimiento" class="form-control" required>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <label>Lugar de Nacimiento<span style="color: red;">&nbsp;*</span></label>
                      <div class="form-row">
                        <div class="form-group col-md-6">
                          <select id="pais" name="pais" class="form-control select_2" >
                            <option value="">Seleccione</option>
                            <?php
                            foreach ($this->paises as $row) {
                              $pais = new Confucio();
                              $pais = $row; ?>
                              <option value="<?php echo $pais->id; ?>"><?php echo $pais->descripcion; ?></option>
                            <?php } ?>
                          </select>
                        </div>

                      <div class="form-group col-md-6">
                          <select id="estado"  class="form-control select_2">
                            <option value="">Seleccione</option>
                          </select>
                        </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-6">
                      <div class="form-group">
                        <div class="form-row">
                          <div class="form-group col-md-4">
                            <div class="form-line">
                              <label>Estado<span style="color: red;">&nbsp;*</span></label>
                            </div>
                            <select id="estado_1" name="estado" class="form-control select_2 " >
                              <option value="">Seleccione</option>
                              <?php
                              foreach ($this->estados as $row) {
                                $estado = new Confucio();
                                $estado = $row; ?>
                                <option value="<?php echo $estado->id; ?>"><?php echo $estado->descripcion; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="form-group col-md-4">
                            <div class="form-line">
                              <label>Municipio<span style="color: red;">&nbsp;*</span></label>
                            </div>
                            <select id="municipio" name="municipio" class="form-control select_2">
                              <option value="">Seleccione</option>

                            </select>
                          </div>
                          <div class="form-group col-md-4">
                            <div class="form-line">  
                              <label>Parroquia<span style="color: red;">&nbsp;*</span></label>
                            </div>
                            <select id="parroquia"  class="form-control select_2">
                              <option value="">Seleccione</option>

                            </select>
                          </div>

                          <label>Dirección de domicilio<span style="color: red;">&nbsp;*</span></label>
                          <div class="form-group col-md-12">
                            <textarea name="direccion" class="form-control" ></textarea>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-4">
                      <div class="form-group form-float">
                        <div class="form-line">
                          <label class="form-label">Teléfono de Habitación<span style="color: red;">&nbsp;*</span></label>
                          <input type="text" name="telefono_habitacion" class="form-control phone-number "  placeholder="Ejemplo: 212-xxx-xxx" >
                        </div>
                      </div>

                      <label>Etnia Indigena (Opcional)</label>
                      <div class="form-group">
                        <select name="etnia" class="form-control select_2">
                          <option value="89">Seleccione</option>
                          <?php
                          foreach ($this->etnia as $row) {
                            $etnias = new Confucio();
                            $etnias = $row; ?>
                            <option value="<?php echo $etnias->id; ?>"><?php echo $etnias->descripcion; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>


                    <script>
                                        $(function(){

                                    // Lista de paises
                                    $.post( '<?php echo constant ('URL');?>combo/getByPais' ).done( function(respuesta)
                                    {
                                        $( '#pais' ).html( respuesta );
                                    });


                                    // lista de estados	
                                    $('#pais').change(function()
                                    {
                                        var id_pais = $(this).val();
                                        
                                        // Lista de estados
                                        $.post( '<?php echo constant ('URL');?>combo/getByPaisEstado', { pais: id_pais} ).done( function( respuesta )
                                        {
                                            $( '#estado' ).html( respuesta );
                                        });
                                    });


                                    // lista de municipios	
                                    $('#estado_1').change(function()
                                    {
                                        var id_estado = $(this).val();
                                        
                                        // Lista de municipios
                                        $.post( '<?php echo constant ('URL');?>combo/getByEstadoMunicipio', { estado_1: id_estado} ).done( function( respuesta )
                                        {
                                            $( '#municipio' ).html( respuesta );
                                        });
                                    });


                                    // lista de parroquias	
                                    $('#municipio').change(function()
                                    {
                                        var id_municipio = $(this).val();
                                        
                                        // Lista de parroquias
                                        $.post( '<?php echo constant ('URL');?>combo/getByMunicipioParroquia', { parroquia: id_municipio} ).done( function( respuesta )
                                        {
                                            $( '#parroquia' ).html( respuesta );
                                        });
                                    });

                                    $('#estado_2').change(function()
                                    {
                                        var id_estado = $(this).val();
                                        
                                        // Lista de municipios
                                        $.post( '<?php echo constant ('URL');?>combo/getByEstadoMunicipio', { estado_1: id_estado} ).done( function( respuesta )
                                        {
                                            $( '#municipio_1' ).html( respuesta );
                                        });
                                    });

                                    })
                                    </script>  

                    </fieldset>

          <h3>Datos Laborales</h3>
              <fieldset>
                <div class="row">
                  <div class="form-group col-md-5">
                    <label>Institución de Educación Universitaria</label>
                    <select name="id_cargo" class="form-control select_2">
                      <option value="4">Seleccione</option>
                      <?php
                      foreach ($this->cargos as $row) {
                        $cargo = new Confucio();
                        $cargo = $row; ?>
                        <option value="<?php echo $cargo->id; ?>"><?php echo $cargo->descripcion; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div> 


                    <div class="row">
                      <div class="form-group col-md-5">
                        <label>Estado<span style="color: red;">&nbsp;*</span></label>
                        <select id="estado_2" name="state" class="form-control select_2">
                          <option value="25">Seleccione</option>
                          <?php
                          foreach ($this->estados as $row) {
                            $estado = new Confucio();
                            $estado = $row; ?>
                            <option value="<?php echo $estado->id; ?>"><?php echo $estado->descripcion; ?></option>
                          <?php } ?>
                        </select>
                      </div>

                      <div class="form-group col-md-5">
                        <label>Municipio<span style="color: red;">&nbsp;*</span></label>
                        <select id="municipio_1" name="municipio" class="form-control select_2">
                          <option value="">Seleccione</option>

                        </select>
                      </div>
                    </div>


                <div class="row">
                  <div class="form-group col-md-5">
                    <label>Cargo que Desempeña<span style="color: red;">&nbsp;*</span></label>
                    <select name="id_cargo" class="form-control select_2">
                      <option value="4">Seleccione</option>
                      <?php
                      foreach ($this->cargos as $row) {
                        $cargo = new Confucio();
                        $cargo = $row; ?>
                        <option value="<?php echo $cargo->id; ?>"><?php echo $cargo->descripcion; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="form-group col-md-5">
                    <label for="identificacion" class="block">Correo Institucional<span style="color: red;">&nbsp;*</span></label>
                    <input name="correo_institucion" type="text" class=" form-control form-control-capitalize" placeholder="Ingrese su Correo Institucional">
                  </div>
                </div> 

                <div class="row">
                 <div class="form-group col-md-5">
                  <label for="identificacion" class="block">Teléfono Oficina<span style="color: red;">&nbsp;*</span></label>
                  <input name="telefono_institucion" type="text" class=" form-control phone-number" placeholder="Ingrese el telefono de Oficina">
                </div>
                <div class="form-group col-md-5">
                    <label>Condición Laboral</label>
                    <select name="id_cargo" class="form-control select_2">
                      <option value="4">Seleccione</option>
                      <?php
                    foreach ($this->cargos as $row) {
                        $cargo = new Confucio();
                        $cargo = $row; ?>
                        <option value="<?php echo $cargo->id; ?>"><?php echo $cargo->descripcion; ?></option>
                      <?php } ?>
                    </select>
                  </div>
              </div>

              
                <div class="row">
                  <div class="form-group col-md-5">
                    <label>Escalafón<span style="color: red;">&nbsp;*</span></label>
                    <select name="id_cargo" class="form-control select_2">
                      <option value="4">Seleccione</option>
                      <?php
                      foreach ($this->cargos as $row) {
                        $cargo = new Confucio();
                        $cargo = $row; ?>
                        <option value="<?php echo $cargo->id; ?>"><?php echo $cargo->descripcion; ?></option>
                      <?php } ?>
                    </select>
                  </div>

                  <div class="form-group col-md-5">
                    <label>Dedicación<span style="color: red;">&nbsp;*</span></label>
                    <select name="id_cargo" class="form-control select_2">
                      <option value="4">Seleccione</option>
                      <?php
                    foreach ($this->cargos as $row) {
                        $cargo = new Confucio();
                        $cargo = $row; ?>
                        <option value="<?php echo $cargo->id; ?>"><?php echo $cargo->descripcion; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>  

               <div class="row">
                  <div class="form-group col-md-5">
                    <label>Ultimo Grado Académico<span style="color: red;">&nbsp;*</span></label>
                    <select name="id_cargo" class="form-control select_2">
                      <option value="4">Seleccione</option>
                      <?php
                      foreach ($this->cargos as $row) {
                        $cargo = new Confucio();
                        $cargo = $row; ?>
                        <option value="<?php echo $cargo->id; ?>"><?php echo $cargo->descripcion; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="col-lg-4">
                <label class="text-center"><b>Ingresar Captcha de Seguridad</b> <span style="color: red;">&nbsp;*</span></label>
                <div class="form-group" style="text-align: center;">
                  <img src="<?php echo constant ('URL');?>src/captcha/captcha.php" alt="Captcha Image">
                </div>  
    
                <div class="form-group">
                  <input type="text" name="captcha" placeholder="Ingrese los caracteres de la Imagen" class="form-control" >
                  <div class="invalid-feedback">
                    Este campo es requerido
                  </div>
                </div>
              </div>
            </div>
          </div> 
        </fieldset>

                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
       </section>
         

  <!-- General JS Scripts -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-steps/jquery.steps.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/js/select2.full.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-validation/dist/jquery.validate.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/form-wizard.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>

<script>
 $(document).ready(function(){

  $(".select_2").select2({
    placeholder: "Seleccione",
    width: "100%",
    dropdownAutoWidth: true
  });

});
</script>

<script>
  function soloLetras(e) {
    var key = e.keyCode || e.which,
    tecla = String.fromCharCode(key).toLowerCase(),
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz",
    especiales = [8, 37, 39, 46],
    tecla_especial = false;

    for (var i in especiales) {
      if (key == especiales[i]) {
        tecla_especial = true;
        break;
      }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
      return false;
    }
  }
</script>

<script>
function soloNumeros(e) {
     var key = e.keyCode || e.which,
    tecla = String.fromCharCode(key).toLowerCase(),
    letras = "12345678910",
     especiales = [tecla.charCode < 48 || tecla.charCode > 57],
    tecla_especial = false;

    for (var i in especiales) {
    if (key == especiales[i]) {
        tecla_especial = true;
        break;
    }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
     return false;
    }
 }
</script>


</body>
</html>