<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Confucio</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/bootstrap-social/bootstrap-social.css">

  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">

  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico'/>

</head>

<body class="font-bg">
  <div class="loader"></div>
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
            <div class="card card-success">
              <div class="card-header" >
                <h4>Pre-inscripción Confucio-UBV</h4>
              </div>
              <div class="card-body">
                <?php echo $this->mensaje; ?>
                <form action="<?php echo constant ('URL');?>pre_inscripcion/ValidarCategoria" method="POST" class="needs-validation" novalidate=""> 
                <div class="d-block">
                      <label for="usuario" class="control-label">Cedula de Identidad<span style="color: red;">&nbsp;*</span></</label>
                    </div>
                <div class="form-group">
                    <input  type="text" class="form-control" name="identificacion" tabindex="1" required autofocus>
                    <input type="hidden" class="form-control" name="categoria" value="<?php echo $this->categoria->categoria;?>" tabindex="1" required autofocus>
                    <input type="hidden" class="form-control" name="id_oferta_academica" value="<?php echo $this->categoria->id_oferta_academica;?>" tabindex="1" required autofocus>
                    <div class="invalid-feedback">
                      Por favor ingrese su Cedula de Identidad.
                    </div>
                  </div>
              
                  <div class="form-group" style="text-align: center;">
                  <img src="<?php echo constant ('URL');?>src/captcha/captcha.php" alt="Captcha Image">
                  </div>   
             
                  <div class="form-group">
                      <input type="text" name="captcha" placeholder="Ingrese los caracteres de la Imagen" class="form-control" required>
                      <div class="invalid-feedback">
                      Este campo es requerido
                    </div>
                    </div>

                  <div class="form-group">
                    <button type="submit" class="btn btn-success btn-lg btn-block" tabindex="4">
                     Pre-Inscribir Oferta
                    </button>
                  </div>
                </form>
              </div>
              <div class="mb-4 text-muted text-center">
                 <a href="<?php echo constant ('URL');?>main">Volver a la Pagina Principal</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  </div>

  <!-- General JS Scripts -->
  <script src="<?php echo constant ('URL'); ?>src/principal_uno/assets/js/app.min.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL'); ?>src/principal_uno/assets/js/scripts.js"></script>
  <script  src="<?php echo constant ('URL'); ?>src/principal_uno/fondo/script.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL'); ?>src/principal_uno/assets/js/custom.js"></script>
  <!-- partial -->

</body>
</html>