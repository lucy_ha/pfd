<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Confucio-Solicitud</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico' />
</head>

<body>

<?php require 'views/header.php'; ?>  

       <!-- Main Content -->
       <div class="main-content">
        <section class="section">
          <div class="section-body">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Solicitud para ingreso</h4>
                  </div>
                  <div class="card-body">
                  <?php echo $this->mensaje;?>  
                    <div class="table-responsive">
                      <table class="table table-striped" id="table-1">
                        <thead>
                        <tr>
                            <th class="text-center">
                              #
                            </th>
                            <th>Primer Nombre y Apellido</th>
                            <th>Cedula de Identidad</th>
                            <th>Estatus</th>
                            <th>Acciones</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php 
                      foreach($this->solicitudes as $row){
                        $solicitud=new Confucio();
                        $solicitud=$row;?> 
                          <?php if($solicitud->aspirante=='0'){ ?>
                       <tr>
                          <td><?php echo $solicitud->id_aspirante; ?></td>
                          <td><?php echo $solicitud->primer_nombre . " " . $solicitud->primer_apellido; ?></td>
                          <td><?php echo $solicitud->identificacion; ?></td>
                          <td><?php echo $solicitud->persona_tipo; ?></td>
                          <td>
                          <a href="<?php echo constant ('URL') . "pre_inscripcion/registrar_deposito/" . $solicitud->id_aspirante;?>" class="btn btn-outline-warning" data-toggle="tooltip" data-placement="bottom" title="Registrar Deposito"><i class="fas fa-address-card"></i></a>
                          <a href="<?php echo constant ('URL') . "pre_inscripcion/VerPreIngresoDetalle/" . $solicitud->id_aspirante;?>" class="btn btn-outline-info" data-toggle="tooltip" data-placement="bottom" title="Visualizar Aspirante"><i class="far fa-eye"></i></a>
                          <a href="<?php echo constant ('URL') . "pre_inscripcion/editarAspirante/" . $solicitud->id_aspirante;?>" class="btn btn-outline-success" data-toggle="tooltip" data-placement="bottom" title="Editar Datos Registrados"><i class="far fa-edit"></i></a>
                          <a href="<?php echo constant ('URL') . "pre_inscripcion/eliminar/" . $solicitud->id_aspirante;?>" class="btn btn-outline-danger" data-toggle="tooltip" data-placement="bottom" title="Remover Solicitud" onclick="return confirm('¿Desea eliminar esta solicitud?');"><i class="fas fa-trash"></i></a>
                          </td>
                      </tr>
                          <?php }?>
                  <?php }?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
 <!--End Main Content-->
                 
                 </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div>
      <?php require 'views/footer.php'; ?>
  <!-- General JS Scripts -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <!-- JS Libraies -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-ui/jquery-ui.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/datatables.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno//assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>
</body>

</html>
