<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Confucio-Actividad</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favicon.ico' />
</head>

<body>

  <?php require 'views/header.php'; ?>  
  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <div class="section-body">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4><?php echo $this->mensaje; ?></h4>
              </div>
              <div class="card-body">
                <?php echo $this->mensaje; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--End Main Content-->
    
  </div>
</div>
</div>
</div>
</div>
</section>

</div>
<?php require 'views/footer.php'; ?>
<!-- General JS Scripts -->
<script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
<!-- JS Libraies -->
<script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.js"></script>
<script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-ui/jquery-ui.min.js"></script>
<!-- Page Specific JS File -->
<script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/datatables.js"></script>
<!-- Template JS File -->
<script src="<?php echo constant ('URL');?>src/principal_uno//assets/js/scripts.js"></script>
<!-- Custom JS File -->
<script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>
</body>

</html>