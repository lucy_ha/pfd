<!DOCTYPE html>
<html lang="en">


<!-- form-wizard.html  21 Nov 2019 03:55:16 GMT -->
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Confucio - Carga de notas</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/pretty-checkbox/pretty-checkbox.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico'/>
</head>

<body>

  <?php require 'views/header.php'; ?>  
  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <div class="section-body">
        <div class="row">
          <div class="col-12">
            <form action="<?php echo constant('URL'); ?>notas/cargar_notas_idioma" id="wizard_with_validation"  novalidate="" method="POST">
              <div class="card">
                <div class="card-header">
                  <h4> Carga de notas</h4>
                  <div class="card-header-action">
                    <button type="submit" id="submit" value="submit" class="btn btn-primary" onclick="return confirm('¿Desea cargar las notas?');" ><i class="fa fa-check"></i>&nbsp;Cargar notas  &nbsp;</button>  
                  </div>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-striped" id="table-1">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Cédula</th>
                          <th>Nombres</th>
                          <th>Apellidos</th>
                          <th style="width: 12%;">Nota (%)</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                        foreach($this->estudiantes as $row){
                          $estudiante=new Confucio();
                          $estudiante=$row;?> 
                          <tr>
                            <td>
                              <input name="id_inscripcion[]" type="hidden" class="form-control" value="<?php echo $estudiante->id_inscripcion;?>">
                              <input name="id_nivel[]" type="hidden" class="form-control" value="<?php echo $estudiante->id_nivel;?>">
                              <input name="id_seccion[]" type="hidden" class="form-control" value="<?php echo $estudiante->id_seccion;?>">
                              <input name="id_persona[]" type="hidden" class="form-control" value="<?php echo $estudiante->id_persona;?>">
                              <?php echo $estudiante->id_inscripcion;?>
                            </td>
                            <td><?php echo $estudiante->identificacion;?></td>
                            <td><?php echo $estudiante->primer_nombre . " " . $estudiante->segundo_nombre;?></td>
                            <td><?php echo $estudiante->primer_apellido . " " . $estudiante->segundo_apellido;?></td> 
                            <td><input name="nota[]" type="number" class="form-control" min="0"></td>   
                            <!--<td><input name="estatus[]" type="hidden" <?php //echo $this->prosecucion->estatus;?>></td> -->
                          </tr>
                        <?php }?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!--End Main Content-->
  <?php require 'views/footer.php'; ?>
  
  <!-- General JS Scripts -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <!-- JS Libraies -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-ui/jquery-ui.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/datatables.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno//assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>
</body>

<!-- form-wizard.html  21 Nov 2019 03:55:20 GMT -->
</html>

