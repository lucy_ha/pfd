<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Confucio - Carga de notas</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/pretty-checkbox/pretty-checkbox.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico'/>
</head>

<body>

  <?php require 'views/header.php'; ?>  
  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <div class="section-body">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4>Editar notas</h4>
              </div>             
              <div class="card-body">
                <?php echo $this->mensaje; ?>
                <div class="table-responsive">
                  <?php if($_SESSION['Editar']==true){?>
                   <table class="table table-striped" id="table-1">
                    <thead>
                      <tr>
                        <th class="text-center">#</th>
                        <th>Seccion</th>
                        <th>Actividad</th>
                        <th>Nivel</th>
                        <th>Periodo Acad.</th>
                        <th>Docente</th>
                        <th>Editar Notas</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($this->secciones as $row) {
                        $seccion = new Confucio();
                        $seccion = $row;
                        ?>
                        <tr>
                          <td><?php echo $seccion->id_seccion; ?></td>
                          <td><?php echo $seccion->descripcion; ?></td>
                          <td><?php echo $seccion->actividad; ?></td>
                          <td><?php echo $seccion->nivel; ?></td>
                          <td><?php echo $seccion->periodo; ?></td>
                          <td><?php echo $seccion->primer_nombre ." ".  $seccion->primer_apellido; ?></td>
                          <?php if($seccion->categoria == 'Idioma'){ ?>
                            <td>
                              <a href="<?php echo constant('URL') . 'notas/lista_notas_idioma/' . $seccion->id_seccion; ?>" class="btn btn-primary" title="Editar Notas">
                                <i class="fas fa-edit"></i>
                              </a>
                            </td>      
                          <?php }?> 
                          <?php if($seccion->categoria == 'Otro'){ ?>
                            <td>
                              <a href="<?php echo constant('URL') . 'notas/lista_notas/' . $seccion->id_seccion; ?>" class="btn btn-primary" title="Editar Notas">
                                <i class="fas fa-edit"></i>
                              </a>
                            </td>
                          <?php } ?>
                        <?php  } ?>
                      </tr>
                    </tbody>
                  </table>
                <?php } 
                if($_SESSION['Editar']==false){?>
                  <div class="container">
                    <h4 class="text-center">No tiene los permisos para editar</h4>
                  </div>               
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<!--End Main Content-->
<?php require 'views/footer.php'; ?>

<!-- General JS Scripts -->
<script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
<!-- JS Libraies -->
<script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.js"></script>
<script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-ui/jquery-ui.min.js"></script>
<!-- Page Specific JS File -->
<script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/datatables.js"></script>
<!-- Template JS File -->
<script src="<?php echo constant ('URL');?>src/principal_uno//assets/js/scripts.js"></script>
<!-- Custom JS File -->
<script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>

</html>