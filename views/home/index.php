<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Plan Formación Docente</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/200.png' />
</head>

<body>

  <?php require 'views/header.php'; ?>    

  <!-- Main Content -->
  <div class="main-content">

    <?php if($_SESSION['id_perfil'] == 1 || $_SESSION['id_perfil'] == 2){?>

      <div class="alert alert-light alert-has-icon">
        <div class="alert-icon"><i class="far fa-user"></i></div>
        <div class="alert-body">
          <div class="alert-title">Bienvenido</div>
          <?php echo $_SESSION['primer_nombre'] . " " . $_SESSION['primer_apellido'];?> del Viceministerio MPPEU
        </div>
      </div>
      <br>
      <br>
      <!-- <div class="row">
       <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
          <div class="card-icon l-bg-green">
            <i class="fas fa-laptop"></i>
          </div>
          <div class="card-wrap">
            <div class="padding-20">
              <div class="text-right">
                <h3 class="font-light mb-0">
                  <i class="ti-arrow-up text-success"></i>
                  <?php echo $this->caspirante->cantidad; ?>
                </h3>
                <span class="text-muted">Aspirantes</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
          <div class="card-icon l-bg-cyan">
            <i class="fas fa-check-double"></i>
          </div>
          <div class="card-wrap">
            <div class="padding-20">
              <div class="text-right">
                <h3 class="font-light mb-0">
                  <i class="ti-arrow-up text-success"></i> <?php echo $this->cinscritos->cantidad?>
                </h3>
                <span class="text-muted">Estudiantes</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
          <div class="card-icon l-bg-green">
            <i class="fas fa-list"></i>
          </div>
          <div class="card-wrap">
            <div class="padding-20">
              <div class="text-right">
                <h3 class="font-light mb-0">
                  <i class="ti-arrow-up text-success"></i>
                  <?php echo $this->coferta->oferta?>
                </h3>
                <span class="text-muted">Cursos Activos</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
          <div class="card-icon l-bg-cyan">
            <i class="fas fa-chalkboard-teacher"></i>
          </div>
          <div class="card-wrap">
            <div class="padding-20">
              <div class="text-right">
                <h3 class="font-light mb-0">
                  <i class="ti-arrow-up text-success"></i>
                  <?php echo $this->cdocent->docentes?>
                </h3>
                <span class="text-muted">Docentes</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> -->

  <?php }?>


  <?php if($_SESSION['id_perfil'] == 3){?>

    <div class="alert alert-light alert-has-icon">
      <div class="alert-icon"><i class="far fa-user"></i></div>
      <div class="alert-body">
        <div class="alert-title">Bienvenido</div>
        <?php echo $_SESSION['primer_nombre'] . " " . $_SESSION['primer_apellido'];?> del Instituto Confucio UBV 
      </div>
    </div>
    <br>
           <div class="row">
              <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                  <div class="card-icon l-bg-cyan">
                    <i class="fas fa-users"></i>
                  </div>
                  <div class="card-wrap">
                    <div class="padding-20">
                      <div class="text-right">
                        <h3 class="font-light mb-0">
                          <i class="ti-arrow-up text-success"></i>
                          <?php echo $this->cestudiante->estudiantes?>
                        </h3>
                        <span class="text-muted">Estudiantes Asignados</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                  <div class="card-icon l-bg-green">
                    <i class="fas fa-list"></i>
                  </div>
                  <div class="card-wrap">
                    <div class="padding-20">
                      <div class="text-right">
                        <h3 class="font-light mb-0">
                          <i class="ti-arrow-up text-success"></i>
                          <?php echo $this->cseccion->seccion?>
                        </h3>
                        <span class="text-muted">Secciones Asignadas</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
           </div>

  <?php   }?>




  <?php if($_SESSION['id_perfil'] == 4){?>

    <div class="alert alert-light alert-has-icon">
      <div class="alert-icon"><i class="far fa-user"></i></div>
      <div class="alert-body">
        <div class="alert-title">Bienvenido</div>
        <?php echo $_SESSION['primer_nombre'] . " " . $_SESSION['primer_apellido'];?> del Instituto Confucio UBV 
      </div>
    </div>


    <br>

    <div class="row" style="text-align:center;">    
      <div class="col-xl-3 col-lg-6">
        <div class="card l-bg-green">
          <div class="card-statistic-3">
            <div class="card-icon card-icon-large"><i class="fa fa-award"></i></div>
            <div class="card-content">
              <h4 class="card-title">Certificados</h4>
              <p class="mb-0 text-sm">
                <span class="text-nowrap">Podrás visualizar los certificados<br>
                  obtenidos en los cursos que has<br> realizado.
                </span>
              </p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-lg-6">
        <div class="card l-bg-cyan">
          <div class="card-statistic-3">
            <div class="card-icon card-icon-large"><i class="fa fa-check-circle"></i></div>
            <div class="card-content">
              <h4 class="card-title">Ver tu Inscripción</h4>
              <p class="mb-0 text-sm">
                <span class="text-nowrap">Podrás visualizar tu datos <br> Registrados.
                </span>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php  }?>




</div><!--end Content--->

<?php require 'views/footer.php'; ?>
<!-- General JS Scripts -->
<script src="<?php echo constant ('URL'); ?>src/principal_uno/assets/js/app.min.js"></script>
<!-- JS Libraies -->
<script src="<?php echo constant ('URL'); ?>src/principal_uno/assets/bundles/apexcharts/apexcharts.min.js"></script>
<!-- Page Specific JS File -->
<script src="<?php echo constant ('URL'); ?>src/principal_uno/assets/js/page/index.js"></script>
<script src="<?php echo constant ('URL'); ?>src/principal_uno/assets/js/page/widget-data.js"></script>
<!-- Template JS File -->
<script src="<?php echo constant ('URL'); ?>src/principal_uno/assets/js/scripts.js"></script>
<!-- Custom JS File -->
<script src="<?php echo constant ('URL'); ?>src/principal_uno/assets/js/custom.js"></script>
</body>


</html>