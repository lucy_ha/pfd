
<div class="loader"></div>
<div id="app">
  <div class="main-wrapper main-wrapper-1">
    <div class="navbar-bg"></div>
    <nav class="navbar navbar-expand-lg main-navbar sticky">
      <div class="form-inline mr-auto">
        <ul class="navbar-nav mr-3">
          <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg
           collapse-btn"> <i data-feather="align-justify"></i></a></li>
           <li><a href="#" class="nav-link nav-link-lg fullscreen-btn">
            <i data-feather="maximize"></i>
          </a></li>
        </ul>
      </div>
      <ul class="navbar-nav navbar-right">
<li class="dropdown"><a href="#" data-toggle="dropdown"
  class="nav-link dropdown-toggle nav-link-lg nav-link-user"> <img alt="image" src="<?php echo constant ('URL');?>src/principal_dos/assets/img/logo_plan.png"
  class="user-img-radious-style"> <span class="d-sm-none d-lg-inline-block"></span></a>
  <div class="dropdown-menu dropdown-menu-right pullDown">
    <div class="dropdown-title"><?php echo $_SESSION['primer_nombre'] . " " . $_SESSION['primer_apellido'];?></div>
    <a href="<?php echo constant('URL') . "perfil/render/" . $_SESSION['id_persona'];?>" class="dropdown-item has-icon"> <i class="far fa-user"></i> Perfil</a>
    <div class="dropdown-divider"></div>
    <a onclick="return confirm('¿Estas Seguro que Deseas Cerrar Sesión?');" href="<?php echo constant('URL');?>login/logout" class="dropdown-item has-icon text-danger"> <i class="fas fa-sign-out-alt"></i>
      Cerrar Sesión
    </a>
  </div>
</li>
</ul>
</nav>

<div class="main-sidebar sidebar-style-2">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="#"> <img alt="image" src="<?php echo constant ('URL');?>src/principal_uno/assets/img/logo_plan.png" class="header-logo" /> <span>
      </a>
    </div>
    <ul class="sidebar-menu">
      <li class="dropdown active">
        <a href="<?php echo constant('URL');?>home" ><i data-feather="menu"></i><span>Inicio</span></a>
      </li>

      <?php if($_SESSION['id_perfil'] == 1 || $_SESSION['id_perfil'] == 2){?>
        <li class="menu-header">Gestión de Usuarios</li>
        <li class="dropdown">
          <a class="menu-toggle nav-link has-dropdown"><i data-feather="user"></i><span>Usuarios</span></a>
          <ul class="dropdown-menu">
            <li><a class="nav-link" href="<?php echo constant('URL');?>usuarios/registrar">Registrar Usuario</a></li>
            <li><a class="nav-link" href="<?php echo constant('URL');?>usuarios">Listado de Usuarios</a></li>
            <li><a class="nav-link" href="<?php echo constant('URL');?>usuarios/list_update">Editar Usuario</a></li>
          </ul>
        </li>
      <?php }?>

      <?php if($_SESSION['id_perfil'] == 1){?>
        <!-- <li class="menu-header">Ofertas Académicas</li> -->
        <!-- <li class="dropdown">
          <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="list"></i><span>Actividades Formativas</span></a>
          <ul class="dropdown-menu">
            <li>
              <a class="nav-link" href="<?php echo constant('URL') . 'actividad/' ?>">Lista de Actividades</a>
            </li>
            <li>
              <a class="nav-link" href="<?php echo constant('URL') . 'actividad/registrar' ?>">Registrar Actividad</a>
            </li>
          </ul>
        </li> -->
        <!-- <li class="dropdown">
          <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="users"></i><span>Fechas</span></a>
          <ul class="dropdown-menu">
            <li>
              <a class="nav-link" href="<?php echo constant('URL') . 'periodo/' ?>">Lista de periodos académicos</a>
            </li>
            <li>
              <a class="nav-link" href="<?php echo constant('URL') . 'periodo/registrar' ?>">Registrar Periodo Académico</a>
            </li>
          </ul>
        </li> -->
        <!-- <li class="dropdown">
          <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="users"></i><span>Docentes</span></a>
          <ul class="dropdown-menu">
            <li>
              <a class="nav-link" href="<?php echo constant('URL') . 'docentes/' ?>">Lista de Docentes</a>
            </li>
            <li>
              <a class="nav-link" href="<?php echo constant('URL') . 'docentes/registrar' ?>">Registrar Docente</a>
            </li>
          </ul>
        </li> -->
        <!-- <li class="dropdown">
          <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="layout"></i><span>Sección</span></a>
          <ul class="dropdown-menu">
            <li>
              <a class="nav-link" href="<?php echo constant('URL') . 'seccion/' ?>">Lista de secciones</a>
            </li>
            <li>
              <a class="nav-link" href="<?php echo constant('URL') . 'seccion/registrar' ?>">Registrar sección</a>
            </li>
          </ul>
        </li> -->
      <!-- <li class="dropdown">
          <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="monitor"></i><span>Activar Actividades</span></a>
          <ul class="dropdown-menu">
            <li>
              <a class="nav-link" href="<?php echo constant('URL') . 'oferta_academica/' ?>">Lista de Actividades inactivas</a>
            </li>
            <li>
              <a class="nav-link" href="<?php echo constant('URL') . 'oferta_academica/ofertas_activas' ?>">Lista de Actividades activas</a>
            </li>
          </ul>
        </li> -->
    <?php }?>

    <?php if($_SESSION['id_perfil'] == 2){?>
        <li class="menu-header">Gestión de Ofertas Académicas</li>
        <li class="dropdown">
          <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="list"></i><span>Carga de notas</span></a>
          <ul class="dropdown-menu">
            <li>
              <a class="nav-link" href="<?php echo constant('URL') . 'notas/secciones' ?>">Editar notas</a>
            </li>
          </ul>
        </li>
      </li>
    <?php }?>

    <?php if($_SESSION['id_perfil'] == 1 || $_SESSION['id_perfil'] == 2){?>
            <!-- <li class="menu-header">Registro</li>
            <li class="dropdown">
              <a class="menu-toggle nav-link has-dropdown"><i data-feather="check-circle"></i><span>Registro</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="<?php echo constant('URL');?>pre_inscripcion/solicitud">Solicitudes de Ingreso</a></li>
                <li><a class="nav-link" href="<?php echo constant('URL');?>pre_inscripcion/lista_inscritos">Listado de Inscritos</a></li>
              </ul>
            </li> -->
            <li class="menu-header">Portal Web</li>
            <li class="dropdown">
              <a class="menu-toggle nav-link has-dropdown"><i data-feather="info"></i><span>Noticias</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="<?php echo constant('URL');?>entrada">Panel de Noticias</a></li>
                <li><a class="nav-link" href="<?php echo constant('URL');?>entrada/lista_historia">Editar Reseña Historica</a></li>
                <li><a class="nav-link" href="<?php echo constant('URL');?>entrada/lista_redes">Editar Redes Sociales</a></li>
              </ul>
            </li>
    <?php }?>

    <?php if($_SESSION['id_perfil'] == 1){?>

      <!-- <li class="menu-header">Notas</li>
      <li class="dropdown">
          <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="list"></i><span>Carga de notas</span></a>
          <ul class="dropdown-menu">
          <li>
              <a class="nav-link" href="<?php echo constant('URL') . 'notas_historial/' ?>">Historico de notas</a>
            </li>
            <li>
              <a class="nav-link" href="<?php echo constant('URL') . 'notas/secciones' ?>">Editar notas</a>
            </li>
            <li>
              <a class="nav-link" href="<?php echo constant('URL') . 'notas_historial/lista_egresados' ?>">Lista de egresados</a>
            </li>
          </ul>
        </li>
      <li class="menu-header">Estadisticas</li>
      <li class="dropdown">
          <a href="<?php echo constant('URL') . 'estadistica' ?>" class="nav-link"><i data-feather="pie-chart"></i><span>Datos Estaditicos</span></a>
        </li>   -->
    <?php }?>

    <?php if($_SESSION['id_perfil'] == 4){?>
      <!-- <li class="menu-header">Estudiante</li>
      <li class="dropdown">
        <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="layout"></i><span>Panel de Acciones</span></a>
        <ul class="dropdown-menu">
          <li><a class="nav-link" href="<?php echo constant('URL') . "inscripcion/verInscripcion/" . $_SESSION['id_persona'];?>">Ver Inscripcion</a></li>
          <li><a class="nav-link" href="<?php echo constant('URL') . "inscripcion/reingreso/" . $_SESSION['id_persona'];?>">Ver Calificación</a></li>
          <li><a class="nav-link" href="<?php echo constant('URL') . "inscripcion/curso_realizado/" . $_SESSION['id_persona'];?>">Cursos Realizados</a></li>
        </ul>
      </li> -->
    <?php }?>

    <?php if($_SESSION['id_perfil'] == 3){?>
      <!-- <li class="menu-header">Docente</li>
      <li class="dropdown">
        <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="grid"></i><span>Docente</span></a>
        <ul class="dropdown-menu">
          <li><a class="nav-link" href="<?php echo constant('URL') . 'docentes/lista_secciones/' .$_SESSION['id_persona']; ?>">Actividad/Curso Asignados</a></li>
          <li><a class="nav-link" href="<?php echo constant('URL');?>notas/lista_secciones">Carga de notas</a></li>
          <li><a class="nav-link" href="<?php echo constant('URL') . 'notas_historial/historial' ?>">Historico de notas</a></li>
        </ul>
      </li> -->
    <?php }?>
  </ul>
</aside>
</div>

