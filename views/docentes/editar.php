<!DOCTYPE html>
<html lang="en">


<!-- form-wizard.html  21 Nov 2019 03:55:16 GMT -->
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Confucio - Docentes</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant('URL');?>src/principal_uno/assets/bundles/select2/dist/css/select2.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant('URL'); ?>src/principal_uno/assets/bundles/bootstrap-daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="<?php echo constant('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant('URL');?>src/principal_uno/assets/img/favi.ico'/>
</head>

<body>

  <?php require 'views/header.php'; ?>  
  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <div class="section-body">
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="card-header">
                <h4>Editar Docente</h4>
                <div class="card-header-action">
                  <a href="<?php echo constant ('URL')?>docentes" class="btn btn-warning">Volver</a>
                </div>
              </div>
              <div class="card-body">
                <form action="<?php echo constant ('URL');?>docentes/UpdateDocente" id="wizard_with_validation"  novalidate="" method="POST">
                  <h3>Datos Personales del Docente</h3>
                  <?php echo $this->mensaje; ?>
                  <input type="hidden" name="id_persona" value="<?php echo $this->docente->id_persona; ?>">
                  <fieldset>
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Primer Nombre<span style="color: red;">&nbsp;*</span></span>
                            </div>
                            <input id="" name="primer_nombre" type="text" value="<?php echo $this->docente->primer_nombre; ?>" class="required form-control " placeholder="Ingrese su Primer Nombre">
                          </div>
                        </div>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Primer Apellido<span style="color: red;">&nbsp;*</span></span>
                          </div>
                          <input id="" name="primer_apellido" type="text" value="<?php echo $this->docente->primer_apellido; ?>" class="required form-control " placeholder="Ingrese su Primer Apellido">
                        </div>
                      </div>

                      <div class="col-lg-6">
                        <div class="form-group form-float">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Segundo Nombre (Opcional)</span>
                            </div>
                            <input id="" name="segundo_nombre" type="text" class="form-control " value="<?php echo $this->docente->segundo_nombre; ?>" placeholder="Ingrese su Segundo Nombre">
                          </div>
                        </div>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Segundo Apellido (Opcional)</span>
                          </div>
                          <input id="" name="segundo_apellido" type="text" class="form-control" value="<?php echo $this->docente->segundo_apellido; ?>" placeholder="Ingrese su Segundo Apellido">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                     <div class="col-lg-6"><br>
                       <label class="form-label">Tipo de Identidad<span style="color: red;">&nbsp;*</span></label>
                       <div class="form-group">
                        <select name="id_tipo_documento_identidad" class="form-control select2" >
                          <option value="">Seleccione</option>
                          <?php 
                          foreach($this->tipos as $row){
                            $tipo=new Confucio();
                            $tipo=$row;?> 
                            <option value="<?php echo $tipo->id;?>" <?php if($tipo->id==$this->docente->id_tipo_documento_identidad){ echo "selected=selected"; }?>><?php echo $tipo->descripcion;?></option>             
                          <?php }?>
                        </select>
                      </div>
                    </div>

                    <div class="col-lg-6"><br>
                      <div class="form-group">
                        <div class="form-line">
                          <label class="form-label">Documento de Identidad o Pasaporte<span style="color: red;">&nbsp;*</span></label>
                          <input type="text" name="identificacion" class="form-control" value="<?php echo $this->docente->identificacion; ?>" required>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group form-float">
                        <div class="form-line">
                          <label class="form-label">Correo Electrónico<span style="color: red;">&nbsp;*</span></label>
                          <input type="email" name="correo" class="form-control" value="<?php echo $this->docente->correo; ?>" required>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group form-float">
                        <div class="form-line">
                          <label class="form-label">Teléfono<span style="color: red;">&nbsp;*</span></label>
                          <input id="telefono" name="telefono" type="text" class="required form-control us_telephone" data-mask="(0999) 999-9999" value="<?php echo $this->docente->telefono; ?>" placeholder="Ingrese su Nro. de Teléfono">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group form-float">
                        <div class="form-line">
                          <label class="form-label">Visa (Opcional)</label>
                          <input id="identificacion_visa" name="identificacion_visa" type="text" class="form-control" value="<?php echo $this->docente->identificacion_visa; ?>"  placeholder="Ingrese su Nro. de Visa">
                        </div>
                      </div>
                    </div>  

                    <div class="col-lg-6">
                      <div class="form-group form-float">
                        <div class="form-line">
                          <label class="form-label">Genero<span style="color: red;">&nbsp;*</span></label>
                          <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio1" name="genero" value="Femenino" class="custom-control-input" <?php if($this->docente->genero=="Femenino"){echo 'checked=""';}?>>
                            <label class="custom-control-label" for="customRadio1">Femenino</label>
                          </div>
                          <div class="custom-control custom-radio">
                           <input type="radio" id="customRadio4" name="genero" value="Masculino" class="custom-control-input" <?php if($this->docente->genero=="Masculino"){echo 'checked=""';}?>>
                           <label class="custom-control-label" for="customRadio4">Masculino</label>
                         </div>
                       </div>
                     </div>
                   </div>

                 </div>
               </fieldset>
               <h3>Datos de interés</h3>
               <fieldset>

                 <?php if ($this->docente->idioma=="Si") { ?>
                  <div class="row">
                    <div class="col-lg-4">
                      <label>¿Habla otro Idioma?</label>
                      <div class="form-group">
                        <div class="form-group">
                        <input name="idioma" type="hidden" class=" form-control form-control-capitalize" maxlength="20" value="<?php echo $this->docente->idioma; ?>">
                         <input name="idiomas_descripcion" type="text" class=" form-control form-control-capitalize" maxlength="20" value="<?php echo $this->docente->idiomas_descripcion; ?>">
                       </div>
                     </div>
                   </div>
                 </div><!--end class row-->
               <?php  }  

               if ($this->docente->idioma=="No") { ?>
                <div class="row">
                  <div class="col-lg-4">
                    <label>¿Habla otro Idioma?</label>
                    <div class="form-group">
                      <select name="idioma" class="form-control select_2" onChange="visualizar(this.value);">
                        <option value="">Seleccione</option>
                        <option value="Si" <?php if($this->docente->idioma=="Si"){echo 'selected=""';}?>>SI</option>
                        <option value="No" <?php if($this->docente->idioma=="No"){echo 'selected=""';}?>>NO</option>
                      </select>
                    </div>
                  </div>
                </div><!--end class row-->
                <div class="row" id=idioma style="display: none;">
                  <div class="col-lg-5">
                   <label>Indique los Idiomas</label>
                   <div class="form-group">
                     <input name="idiomas_descripcion" type="text" class=" form-control form-control-capitalize" maxlength="20" value="<?php echo $this->docente->idiomas_descripcion; ?>">
                   </div>
                 </div> 
               <?php  } ?>
               
             </fieldset>
           </form>
         </div>
       </div>
     </div>
   </div>
 </div>
</section>
</div>

<?php require 'views/footer.php'; ?>
<!-- General JS Scripts -->
<script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
<script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-steps/jquery.steps.min.js"></script>
<script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-validation/dist/jquery.validate.min.js"></script>
<!-- Page Specific JS File -->
<script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/form-wizard.js"></script>
<!-- Template JS File -->
<script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/scripts.js"></script>
<!-- Custom JS File -->
<script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>

<script>
 $(document).ready(function(){
  $(".select_2").select2({
    placeholder: "Seleccione",
    width: "100%",
    dropdownAutoWidth: true
  });
});
</script>
<script type="text/javascript">
  function visualizar(id) {
    if (id == "Si") {
      $("#idioma").show();
    }
    if (id == "No") {
      $("#idioma").hide();
    }
  }
</script> 

</body>
<!-- form-wizard.html  21 Nov 2019 03:55:20 GMT -->
</html>