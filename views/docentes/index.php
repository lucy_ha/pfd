<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Confucio - Docentes</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/pretty-checkbox/pretty-checkbox.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico'/>
</head>

<body>

  <?php require 'views/header.php'; ?>  
  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <div class="section-body">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4>Listado de Docentes</h4>
              </div>
              <div class="card-body">
                <?php echo $this->mensaje; ?>
                <div class="table-responsive">
                  <table class="table table-striped" id="table-1">
                    <thead>
                      <tr>
                        <th class="text-center">#</th>
                        <th>Identificación</th>
                        <th>Primer Nombre y Apellido</th>
                        <th>Visa</th>
                        <th>Correo</th>
                        <th>Teléfono</th>
                        <th>Secciones</th>
                        <th>Acciones</th>
                      </tr>
                    </thead>
                    <tbody>

                      <?php foreach ($this->docentes as $row) {
                        $docente = new Confucio();
                        $docente = $row;
                        ?>
                        
                        <tr>
                          <td><?php echo $docente->id_docente; ?></td>
                          <td><?php echo $docente->identificacion; ?></td>
                          <td><?php echo $docente->primer_nombre . " " . $docente->primer_apellido; ?> </td>
                          <td><?php echo $docente->identificacion_visa; ?></td>
                          <td><?php echo $docente->correo; ?></td>
                          <td><?php echo $docente->telefono; ?></td>
                          <td class="text-center">
                            <a href="<?php echo constant('URL') . 'docentes/lista_secciones/' . $docente->id_persona; ?>" class="btn btn-warning"title="Ver secciones asignadas">
                              <i class="fas fa-eye"></i>
                            </a>
                          </td>   
                          <td>
                          <?php if($_SESSION['Editar']==true){?>
                            <a href="<?php echo constant('URL') . 'docentes/editar/' . $docente->id_docente; ?>" class="btn btn-primary"title="Editar Datos">
                              <i class="fas fa-user-edit"></i>
                            </a>
                            <?php }?>
                          </td>
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!--End Main Content-->
  <?php require 'views/footer.php'; ?>
  
  <!-- General JS Scripts -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <!-- JS Libraies -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-ui/jquery-ui.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/datatables.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno//assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>
</body>

</html>