<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Plan FD - Actividad Formativa</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/css/select2.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico'/>
</head>

<body>

  <?php require 'views/header.php'; ?>  

  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <div class="section-body">
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="card-header">
                <h4>Editar Actividad Formativa</h4>
                <div class="card-header-action">
                  <a href="<?php echo constant ('URL')?>actividad" class="btn btn-warning">Volver</a>
                </div>
              </div>
              <div class="card-body">
                <form action="<?php echo constant('URL'); ?>actividad/UpdateActividad" id="wizard_with_validation"  novalidate="" method="POST">
                  <h3>Datos de la actividad</h3>
                  <fieldset>
                    <div class="row">
                      <input type="hidden" name="id_actividad" value="<?php echo $this->actividad->id_actividad; ?>">

                      <div class="col-lg-6">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Nombre de la actividad formativa<span style="color: red;">&nbsp;*</span></label>
                            <input type="text" class="form-control required" name="descripcion" value="<?php echo $this->actividad->descripcion; ?>">
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-6">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Categoría<span style="color: red;">&nbsp;*</span></label>
                            <select name="categoria" class="form-control select2">
                              <option>Seleccione</option> 
                              <option value="Idioma" <?php if("Idioma"==$this->actividad->categoria){ echo "selected=selected"; }?>>Idioma</option> 
                              <option value="Otro" <?php if("Otro"==$this->actividad->categoria){ echo "selected=selected"; }?>>Otro</option>
                              <option value="Otro" <?php if("HSK"==$this->actividad->categoria){ echo "selected=selected"; }?>>HSK</option>
                              <option value="Otro" <?php if("Conversacion"==$this->actividad->categoria){ echo "selected=selected"; }?>>Conversacion</option>
                            </select> 
                          </div> 
                        </div>
                      </div>

                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <?php require 'views/footer.php'; ?>

  <!-- General JS Scripts -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/js/select2.full.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-validation/dist/jquery.validate.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/advance-elements/select2-custom.js"></script>
  <!-- JS Libraies -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-steps/jquery.steps.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/form-wizard.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>
</body>


</html>