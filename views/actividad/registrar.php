<!DOCTYPE html>
<html lang="en">


<!-- form-wizard.html  21 Nov 2019 03:55:16 GMT -->
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Plan FD - Actividad Formativa</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/css/select2.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico'/>
</head>

<body>

  <?php require 'views/header.php'; ?>  
  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <div class="section-body">
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="card-header">
                <h4>Registrar Actividad Formativa</h4>
                <div class="card-header-action">
                  <a href="<?php echo constant ('URL')?>actividad" class="btn btn-warning">Volver</a>
                </div>
              </div>
              <div class="card-body">
                <form action="<?php echo constant('URL'); ?>actividad/RegisterActividad" id="wizard_with_validation"  novalidate="" method="POST">
                  <h3>Datos de la Actividad</h3>
                  <fieldset>
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Nombre de la actividad formativa<span style="color: red;">&nbsp;*</span></label>
                            <input type="text" class="form-control required" name="descripcion" >
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Categoría<span style="color: red;">&nbsp;*</span></label>
                            <select name="categoria" class="form-control select2">
                              <option>Seleccione</option> 
                              <option value="Idioma">Idioma</option> 
                              <option value="HSK">HSK</option> 
                              <option value="Conversacion">Conversación</option> 
                              <option value="Otro">Talleres</option> 
                              <option value="Otro">Conferencias</option> 
                              <option value="Otro">Ferias</option> 
                              <option value="Otro">Concursos</option> 
                              <option value="Otro">Eventos</option> 
                            </select> 
                          </div> 
                        </div>
                      </div>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <?php require 'views/footer.php'; ?>

  <!-- General JS Scripts -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-steps/jquery.steps.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/js/select2.full.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-validation/dist/jquery.validate.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/form-wizard.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>
</body>


<!-- form-wizard.html  21 Nov 2019 03:55:20 GMT -->
</html>