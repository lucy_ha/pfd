<!DOCTYPE html>
<html lang="en">


<!-- form-wizard.html  21 Nov 2019 03:55:16 GMT -->
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Confucio - Histórico de notas</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/pretty-checkbox/pretty-checkbox.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico'/>
</head>

<body>

  <?php require 'views/header.php'; ?>  
  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <div class="section-body">
        <div class="row">
          <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4>Histórico de notas</h4>
                  <!--<div class="card-header-action">
                  <a href="<?php //echo constant ('URL')?>notas_historial" class="btn btn-warning">Volver</a>
                </div> -->
                </div>
                <div class="card-body">
                  <?php echo $this->mensaje; ?>
                  <div class="table-responsive">        
                    <table class="table table-striped" id="table-1">
                      <thead>
                        <tr>
                          <th>Cédula</th>
                          <th>Nombres</th>
                          <th>Apellidos</th>
                          <th style="width: 12%;">Nota (%)</th>
                          <th>Estatus</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                        foreach($this->estudiantes as $row){
                          $estudiante=new Confucio();
                          $estudiante=$row;?> 
                          <tr>
                            <td><?php echo $estudiante->identificacion;?></td>
                            <td><?php echo $estudiante->primer_nombre . " " . $estudiante->segundo_nombre;?></td>
                            <td><?php echo $estudiante->primer_apellido . " " . $estudiante->segundo_apellido;?></td> 
                            <td><?php echo $estudiante->nota;?></td>
                            <?php if ($estudiante->descripcion == "Aprobado") { ?>
                              <td><div class="badge badge-pill badge-success">Aprobado</div></td>
                            <?php } ?>
                            <?php if ($estudiante->descripcion == "Reprobado") { ?>
                              <td><div class="badge badge-pill badge-danger">Reprobado</div></td>
                            <?php } ?>
                          </tr>
                        <?php }?>
                      </tbody>
                    </table>         
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!--End Main Content-->
  <?php require 'views/footer.php'; ?>

  <!-- General JS Scripts -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <!-- JS Libraies -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-ui/jquery-ui.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/datatables.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno//assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>
</body>

<!-- form-wizard.html  21 Nov 2019 03:55:20 GMT -->
</html>
