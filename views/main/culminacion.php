<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Plan Formación Docente</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant('URL'); ?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant('URL'); ?>src/principal_uno/assets/bundles/datatables/datatables.min.css">
  <link rel="stylesheet" href="<?php echo constant('URL'); ?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant('URL'); ?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant('URL'); ?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant('URL'); ?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/img/favicon.ico' />

</head>

<style>
.imagen5{
      width: 29%;
      height: 8%;
      margin: 1% 0 0 32%;
    }
</style>

<body>
  <div class="loader"></div>
  <div id="app">
    <section class="section">
      <div class="container mt-5">
      <img src="<?php echo constant ('URL');?>src/principal_dos/assets/img/logo_plan.png" class="imagen5" alt="">
        <div class="row">
          <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">
              <h4>Verificación</h4>
                <div class="card-header-action">
                  <a href="<?php echo constant ('URL')?>main" class="btn btn-success">Volver a la Pagina Principal</a>
                  
                </div>
              </div>
              <div class="card-body">
                <div class="row">
                <p style="margin-left: 17%;"> El Ministerio del Poder Popular para la Educación Universitaria por medio del Viceministerio acredita al Ciudadano(a): </p>
                </div>
                <div class="row">
                <p style="margin-left: 37%;"> <b>Nombres: <?php echo $this->culminacion->nombres;?></b></p>
              </div>
              <div class="row">
                <p style="margin-left: 37%;"><b>Apellidos: <?php echo $this->culminacion->apellidos;?></b></p>
              </div>
              <div class="row">
                <p style="margin-left: 37%;"><b> Cédula de Identidad: <?php echo $this->culminacion->identificacion;?></b></p>
              </div>
              <br>
              <div class="row">
                <p style="margin-left: 29%;"> Por Haber cumplido todos los requisitos exigidos por la ley. </p>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</div>
<div class="simple-footer">
     Copyright &copy; <script>document.write(new Date().getFullYear());</script> Ministerio del Poder Popular para la Educación Universitaria
 </div>



<!-- General JS Scripts -->
<script src="<?php echo constant('URL'); ?>src/principal_uno/assets/js/app.min.js"></script>
<!-- JS Libraies -->
<script src="<?php echo constant('URL'); ?>src/principal_uno/assets/bundles/datatables/datatables.min.js"></script>
<script src="<?php echo constant('URL'); ?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo constant('URL'); ?>src/principal_uno/assets/bundles/jquery-ui/jquery-ui.min.js"></script>
<!-- Page Specific JS File -->
<script src="<?php echo constant('URL'); ?>src/principal_uno/assets/js/page/datatables.js"></script>
<!-- Template JS File -->
<script src="<?php echo constant('URL'); ?>src/principal_uno//assets/js/scripts.js"></script>
<!-- Custom JS File -->
<script src="<?php echo constant('URL'); ?>src/principal_uno/assets/js/custom.js"></script>

</body>

</html>