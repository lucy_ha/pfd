<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Plan de Formación Docente</title>
  <meta content="" name="description">
  <meta content="" name="keywords">
  <!-- Favicons -->
  <link href="<?php echo constant ('URL');?>src/principal_dos/assets/img/200.png" rel="icon">
  <link href="<?php echo constant ('URL');?>src/principal_dos/assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <!-- Vendor CSS Files -->
  <link href="<?php echo constant ('URL');?>src/principal_dos/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo constant ('URL');?>src/principal_dos/assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="<?php echo constant ('URL');?>src/principal_dos/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?php echo constant ('URL');?>src/principal_dos/assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="<?php echo constant ('URL');?>src/principal_dos/assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="<?php echo constant ('URL');?>src/principal_dos/assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?php echo constant ('URL');?>src/principal_dos/assets/css/style.css" rel="stylesheet">


</head>

<body>
  <style>
    .imagen{
      width: 30%;
      margin-left:-2%;
      margin-right:4%;
      margin-top:-2%;
      background-clip: padding-box;
      /*border: 4px solid rgba(255,255,255,0.5);*/
      border-radius: 12px;
    }

    .imagen1{
      width: 9%;
      margin-left:-2%;
      margin-right:4%;
      margin-top:1%;
      background-clip: padding-box;
      /*border: 4px solid rgba(255,255,255,0.5);*/
      border-radius: 12px;
    }

    .imagen3{
      width: 20%;
      height: 100%;
      margin: 1% 0% 0 0;
    }

    .imagen4{
      width: 95%;
      margin: 0 2% 1% 2%;
    }

    .imagen5{
      width: 18%;
      height: 8%;
      margin: 1 % 0 0 1%;
    }
  </style>



  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top d-flex align-items-center header-transparent">
    <div class="container d-flex align-items-center">
      <img src="<?php echo constant ('URL');?>src/principal_dos/assets/img/logo_ministerio.png" class="imagen" alt="">
      
      <div class="logo mr-auto">

        <h1 class="text-light"><a href="<?php echo constant ('URL');?>main"><span> Plan de Formación Docente</span></a>
          <!-- <img src="<?php echo constant ('URL');?>src/principal_dos/assets/img/banderas.png" style="width: 17%; height: 12%; margin-left: 3%;" alt=""> -->
        </h1>

      </div>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="#">Inicio</a></li>
          <li><a href="#about">Acerca de Nosotros</a></li>
          <li><a href="#menu">Actividades Formativas</a></li> 
          <li class="book-a-table text-center" style="width:70%;font-size:8%;margin-top:-1%;margin-left:5%;"><a style="font-size:8px" href="<?php echo constant ('URL');?>login">Iniciar Sesión</a></li>
        </ul>
      </nav><!-- .nav-menu -->
      
    </div> 
    <img src="<?php echo constant ('URL');?>src/principal_dos/assets/img/bicentenario.png" class="imagen1" alt="">
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero">
    <div class="hero-container">
      <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">

        <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

        <div class="carousel-inner" role="listbox">

          <?php
          include_once 'models/ubv.php';
          
          foreach($this->noticia  as $row ){
            $nota= new Ubv();
            $nota=$row;  

            ?>              
            <!-- Slide 3 -->
            <div class="carousel-item <?php if($nota->titulo=='¡Bienvenidos y Bienvenidas!'){?> active<?php }  ?>">
              <div class="carousel-background"><img src="src/img/<?php echo $nota->captura;?>" style="width:100%;" alt=""></div>
              <div class="carousel-container">
                <div class="carousel-content">
                  <h2 class="animate__animated animate__fadeInDown"><?php echo $nota->titulo;?></h2>
                  <p class="animate__animated animate__fadeInUp"><?php echo $nota->contenido;?></p>
                </div>
              </div>
            </div>
            <?php
            
          }
          ?>
          
        </div>

        <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon icofont-simple-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>

        <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon icofont-simple-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>

      </div>
    </div>
  </section>
  <!-- End Hero -->


  <main id="main">


  <!-- ======= Specials Section ======= -->
<section id="about" class="specials">
  <div class="container">

    <div class="section-title">
      <h2>Acerca de <span>Nosotros</span></h2>
      <p>En esta sección usted podrá encontrar información importante acerca del Plan Formación Docente</p>
    </div>

    <div class="row" style='width:100%;margin-left:10%;'>
      <div class="col-lg-3">
        <ul class="nav nav-tabs flex-column">
          <li class="nav-item">
            <a class="nav-link active show" data-toggle="tab" href="#tab-1">Breve Historia</a>
          </li>
        </ul>
      </div>
      <div class="col-lg-9 mt-4 mt-lg-0">
        <div class="tab-content">
          <div class="tab-pane active show" id="tab-1">
            <div class="row">
              <div class="col-lg-8 details order-2 order-lg-1">
                <p style="text-align: center;" class="font-italic">Plan de Formación Docente del MPPEU</p>
                <p style="text-align: justify;"><?php echo $this->historia->descripcion_historia; ?></p>
              </div>
              <div class="col-lg-4 text-center order-1 order-lg-2">
                <img src="public/assets/img/specials-1.jpg" alt="" class="img-fluid">
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>

  </div>
</section><!-- End Specials Section -->

    <section id="menu" class="menu">
      <div class="container">

        <div class="section-title">
          <h2>Actividades <span>Formativas<span></h2>
        </div>

        <div class="row">
          <div class="col-lg-12 d-flex justify-content-center">
            <ul id="menu-flters">
             <li data-filter=".filter-salads">Certificados 1-10</li>
             <li data-filter=".filter-starters">Certificados 11-20</li>
             <li data-filter=".filter-postre">Certificados 21-30</li>
             <li data-filter=".filter-entris">Certificados 31-41</li>
           </ul>
         </div>
       </div>

       <div class="row menu-container">

        <?php $fecha_actual= date('Y-m-d');
        
        foreach ($this->ofertas as $row) {
          $oferta = new Confucio();
          $oferta = $row; 
          ?>
          
          <?php if ($fecha_actual >= $oferta->fecha_inicio && $fecha_actual <= $oferta->fecha_fin) {?>

            <?php if($oferta->categoria=='Idioma'){?>
              <div class="col-lg-6 menu-item filter-starters">
                <img src="<?php echo constant ('URL');?>src/principal_dos/assets/img/favicon.png" style="width:9%; margin-left:-10%; margin-bottom:-23%;" alt="" >
                <div class="menu-content">
                  <?php if($oferta->cupos<='0'){ ?>
                   <b><?php echo $oferta->seccion . ": " .$oferta->actividad; ?></b> <h6 style="margin-right:10%;color:red;" ><b>Cupos Agotados</b></h6>
                 <?php }else{?>
                  <b><?php echo $oferta->seccion . ": " . $oferta->actividad; ?></b><span><a href="#" data-toggle="modal" data-target="#basicModal">Requisitos</a><a class="link-oferta" href="<?php echo constant('URL') . 'main/detalles/' . $oferta->id_seccion; ?>"> Ver Oferta</a></span>
                <?php } ?>
              </div>

              <!-- basic modal -->
              <div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
              aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div style="font-size: 16px;" class="modal-title" id="exampleModalLabel"><b>Requisitos Instituto Confucio-UBV-Venezuela</b> </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body" style="font-size: 13px;" >
                  <img src="<?php echo constant ('URL');?>src/principal_dos/assets/img/requisitos.jpg" style="width:100%;" alt="" >
                 </div>
                 <div class="modal-footer bg-whitesmoke br">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal" style="font-size: 13px;">Close</button>
                </div>
              </div>
            </div>
          </div>
          <!--End Basic Modal-->
          <div class="menu-ingredients">
            <b>Modalidad:</b> <?php echo $oferta->modalidad;?>
          </div>
        </div>

      <?php }?>

  <?php } } ?>



      <?php
      include_once 'models/ubv.php';
          
      foreach($this->certificados  as $row ){
      $certificado= new Ubv();
      $certificado=$row; ?>  

      <div class="col-lg-6 menu-item filter-salads">
          <div class="menu-content">
          <?php echo $certificado->codigo;?> <?php echo $certificado->descripcion;?><a href="<?php echo constant('URL') . 'main/certificados/' . $certificado->id_tipo_certificado; ?>" target="_blank"><span>Entrar</span></a>
          </div>
            <div class="menu-ingredients">
            </div>
      </div>

     <?php }?>


     <?php
      include_once 'models/ubv.php';
          
      foreach($this->certificadosDos  as $row ){
      $certificado= new Ubv();
      $certificado=$row; ?>  

      <div class="col-lg-6 menu-item filter-starters">
          <div class="menu-content">
          <?php echo $certificado->codigo;?> <?php echo $certificado->descripcion;?><a href="<?php echo constant('URL') . 'main/certificados_dos/' . $certificado->id_tipo_certificado; ?>" target="_blank"><span>Entrar</span></a>
          </div>
            <div class="menu-ingredients">
            </div>
      </div>

     <?php }?>


     <?php
      include_once 'models/ubv.php';
          
      foreach($this->certificadosTres  as $row ){
      $certificado= new Ubv();
      $certificado=$row; ?>  

      <div class="col-lg-6 menu-item filter-postre">
          <div class="menu-content">
          <?php echo $certificado->codigo;?> <?php echo $certificado->descripcion;?><a href="<?php echo constant('URL') . 'main/certificados_tres/' . $certificado->id_tipo_certificado; ?>" target="_blank"><span>Entrar</span></a>
          </div>
            <div class="menu-ingredients">
            </div>
      </div>

     <?php }?>


     <?php
      include_once 'models/ubv.php';
          
      foreach($this->certificadosCuatro  as $row ){
      $certificado= new Ubv();
      $certificado=$row; ?>  

      <div class="col-lg-6 menu-item filter-entris">
          <div class="menu-content">
          <?php echo $certificado->codigo;?> <?php echo $certificado->descripcion;?><a href="<?php echo constant('URL') . 'main/certificados_cuatro/' . $certificado->id_tipo_certificado; ?>" target="_blank"><span>Entrar</span></a>
          </div>
            <div class="menu-ingredients">
            </div>
      </div>

     <?php }?>




  </div>
</div>



<hr style="width:50%;">
</section><!-- End Menu Section -->

</main><!-- End #main -->



<!-- ======= Footer ======= -->
<footer id="footer">
  <div class="container">
    <div class="social-links">
    <div>
        <img src="<?php echo constant ('URL');?>src/principal_dos/assets/img/logo_plan.png" class="imagen5" alt="">
        <img src="<?php echo constant ('URL');?>src/principal_dos/assets/img/logo-uni.png" class="imagen3" alt="">
      </div>
      <br>
      <a href="<?php echo $this->redes->link_twitter; ?>" class="twitter"><i class="bx bxl-twitter"></i></a>
      <a href="<?php echo $this->redes->link_facebook;?>" class="facebook"><i class="bx bxl-facebook"></i></a>
      <a href="<?php echo $this->redes->link_instagram; ?>" class="instagram"><i class="bx bxl-instagram"></i></a>
    </div>

    <div class="copyright">
      &copy; Copyright <strong><span>Ministerio del Poder Popular para la Educación Universitaria</span></strong>.
    </div>
  </div>
</footer><!-- End Footer -->
<div class="container">
</div class="container"><img src="<?php echo constant ('URL');?>src/principal_dos/assets/img/bannerubv.jpg" alt="" class="imagen4"></div>
</div>

<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

<!-- Vendor JS Files -->
<script src="<?php echo constant ('URL');?>src/principal_dos/assets/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo constant ('URL');?>src/principal_dos/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo constant ('URL');?>src/principal_dos/assets/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="<?php echo constant ('URL');?>src/principal_dos/assets/vendor/php-email-form/validate.js"></script>
<script src="<?php echo constant ('URL');?>src/principal_dos/assets/vendor/jquery-sticky/jquery.sticky.js"></script>
<script src="<?php echo constant ('URL');?>src/principal_dos/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="<?php echo constant ('URL');?>src/principal_dos/assets/vendor/venobox/venobox.min.js"></script>
<script src="<?php echo constant ('URL');?>src/principal_dos/assets/vendor/owl.carousel/owl.carousel.min.js"></script>

<!-- Template Main JS File -->
<script src="<?php echo constant ('URL');?>src/principal_dos/assets/js/main.js"></script>

</body>

</html>
