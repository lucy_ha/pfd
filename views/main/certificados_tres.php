<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>Plan Formacion Docente</title>
    <!-- General CSS Files -->
    <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
    <!-- Template CSS -->
    <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
    <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
    <!-- Custom style CSS -->
    <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
    <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/img/favicon.ico' />
</head>

<style>
.imagen5{
      width: 50%;
      height: 10%;
      margin: 1% 0 0 23%;
    }
</style>

<body>
        <section class="section">
            <div class="container mt-5">
                <div class="row">
                    <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                    <img src="<?php echo constant ('URL');?>src/principal_dos/assets/img/logo_plan.png" class="imagen5" alt="">
                        <div class="login-brand">
                            Certificados Plan Formación Docente
                        </div>
                        <div class="card card-primary">
                             <div class="row m-0">
                                    <div class="card-header text-center">
                                        <h4 style="margin-left: 22%;">Imprime tu certificado</h4>
                                    </div>
                                    <div class="card-body">
                                        <form action="<?php echo constant ('URL');?>main/validar_tres" method="POST"  target="_blank">
                                        <input name="id_tipo_certificado" type="hidden" class="form-control" value="<?php echo $this->tipo_certificados->id_tipo_certificado; ?>">
                                            <div class="form-group floating-addon">
                                                <label>Ingrese su Cédula de Identidad</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">
                                                            <i class="fas fa-address-card"></i>
                                                        </div>
                                                    </div>
                                                    <input type="text" name="identificacion" class="form-control" autofocus placeholder="Ingresar la cédula con ." required>
                                                    <button type="button" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="Nota: En caso de requerir alguna información referida a los certificados del Plan de Formación Docente, por favor escribir al correo saberesuniversitarios@gmail.com">&nbsp;<i class="fas fa-info-circle"></i></button>
                                                </div>
                                            </div>
                                            <div class="form-group" style="text-align: center;">
                                               <img src="<?php echo constant ('URL');?>src/captcha/captcha.php" alt="Captcha Image">
                                             </div>   
                                             
                                            <div class="form-group floating-addon">
                                                <label>Ingrese la imagen de Seguridad</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                    </div>
                                                    <input type="text" class="form-control" name="captcha"  placeholder="Ingresa el Captcha" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                               <button type="submit" class="btn btn-success btn-lg btn-block">
                                                Imprimir
                                               </button>
                                            </div>
                                        </form>
                                        <div class="mb-4 text-muted text-center">
                                      <a href="<?php echo constant ('URL');?>main">Volver a la Pagina Principal</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="simple-footer">
                            Copyright &copy; <script>document.write(new Date().getFullYear());</script> Ministerio del Poder Popular para la Educación Universitaria
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <!-- General JS Scripts -->
    <script src="<?php echo constant ('URL'); ?>src/principal_uno/js/app.min.js"></script>
    <!-- JS Libraies -->
    <script src="<?php echo constant ('URL'); ?>src/principal_uno/bundles/gmaps.js"></script>
    <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-ui/jquery-ui.min.js"></script>
    <!-- Page Specific JS File -->
    <script src="<?php echo constant ('URL'); ?>src/principal_uno/js/page/contact.js"></script>
    <!-- Template JS File -->
    <script src="<?php echo constant ('URL'); ?>src/principal_uno/js/scripts.js"></script>
    <!-- Custom JS File -->
    <script src="<?php echo constant ('URL'); ?>src/principal_uno/js/custom.js"></script>
</body>

</html>