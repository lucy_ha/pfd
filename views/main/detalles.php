<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Confucio - Oferta</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant('URL'); ?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant('URL'); ?>src/principal_uno/assets/bundles/datatables/datatables.min.css">
  <link rel="stylesheet" href="<?php echo constant('URL'); ?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant('URL'); ?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant('URL'); ?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant('URL'); ?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant('URL'); ?>src/principal_uno/assets/img/favi.ico' />

</head>

<body class="font-bg-b">
  <div class="loader"></div>
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12">
            <div class="card card-success">
              <div class="card-header">
                <h4><?php echo $this->seccion->descripcion .": ". $this->seccion->actividad; ?> </h4>
                <div class="card-header-action">
                  <a href="<?php echo constant ('URL')?>main" class="btn btn-info">Volver a la Pagina Principal</a>
                </div>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-lg-12 col-xl-4">
                    <div class="table-responsive">
                      <table class="table m-0">
                        <tbody>
                          <tr>
                            <th scope="row">Sección:</th>
                            <td><?php echo $this->seccion->descripcion; ?></td>
                          </tr>
                          <tr>
                            <th scope="row">Turno:</th>
                            <td><?php echo $this->seccion->turno; ?></td>
                          </tr>
                          <tr>
                            <th scope="row">Días:</th>
                            <td>
                              <?php   
                              $i=1;
                              while($i<=count($this->datos_h)){
                               echo $this->dia[$i]['dia'.$i];?>
                               <?php $i++;
                             } ?> 
                           </td>
                         </tr>
                       </tbody>
                     </table>
                   </div>
                 </div>
                 <div class="col-lg-12 col-xl-4">
                  <div class="table-responsive">
                    <table class="table m-0">
                      <tbody>
                        <tr>
                          <th scope="row">Actividad:</th>
                          <td><?php echo $this->seccion->actividad; ?></td>
                        </tr>
                        <tr>
                          <th scope="row">Modalidad:</th>
                          <td><?php echo $this->seccion->modalidad; ?></td>
                        </tr>
                        <tr>
                          <th scope="row">Periodo Acad.:</th>
                          <td><?php echo $this->seccion->periodo; ?></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="col-lg-12 col-xl-4">
                  <div class="table-responsive">
                    <table class="table m-0">
                      <tbody>
                        <tr>
                          <th scope="row">Nivel:</th>
                          <td><?php echo $this->seccion->nivel; ?></td>
                        </tr>
                        <tr>
                          <th scope="row">Horario:</th>
                          <td><?php echo date("g:i a", strtotime($this->seccion->hora_inicio)).' - '.date("g:i a", strtotime($this->seccion->hora_fin)); ?></td>
                        </tr>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              


            </div>

            <div class="card-footer text-center">
              <a href="<?php echo constant('URL') . 'pre_inscripcion/render/' . $this->seccion->id_oferta_academica; ?>"class="btn btn-success"><i class="fa fa-check"></i>&nbsp;Pre-inscribir&nbsp;</a>
              <a href="<?php echo constant('URL') . 'pre_inscripcion/validar/' . $this->seccion->id_oferta_academica; ?>"class="btn btn-warning"><i class="fa fa-check"></i>&nbsp;Ya Estoy Registrado&nbsp;</a>

            </div>


          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</div>


<!-- General JS Scripts -->
<script src="<?php echo constant('URL'); ?>src/principal_uno/assets/js/app.min.js"></script>
<!-- JS Libraies -->
<script src="<?php echo constant('URL'); ?>src/principal_uno/assets/bundles/datatables/datatables.min.js"></script>
<script src="<?php echo constant('URL'); ?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo constant('URL'); ?>src/principal_uno/assets/bundles/jquery-ui/jquery-ui.min.js"></script>
<!-- Page Specific JS File -->
<script src="<?php echo constant('URL'); ?>src/principal_uno/assets/js/page/datatables.js"></script>
<!-- Template JS File -->
<script src="<?php echo constant('URL'); ?>src/principal_uno//assets/js/scripts.js"></script>
<!-- Custom JS File -->
<script src="<?php echo constant('URL'); ?>src/principal_uno/assets/js/custom.js"></script>

</body>

</html>