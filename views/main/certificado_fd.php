<?php

include  'views/main/plantilla.php';
 require_once('src/phpqrcode/qrlib.php');

     
   $pdf = new PDF('L','mm','Letter','A10');
	//$pdf->AliasNbPages();
    $pdf->AddPage();

    $pdf->Ln(5);

    //UNEFM 007, 008 id= 8,40, 013 id=14, 

    if($this->siete->tipo == 7 ){
      $pdf->Image('src/reporte/imagen_1.png','','8','279.5','200','PNG');

      $pdf->SetFont('Courier','B',19);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->siete->identificacion).''));

      $pdf->SetFont('Courier','B',19);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->siete->nombres).' '.strtoupper($this->siete->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,131);
      $pdf->WriteHTML(utf8_decode('<p ALIGN="center">       '.strtoupper($this->siete->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(19,183);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->siete->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','97','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->siete->identificacion.$this->siete->tipo,"docente.png");
    }

    if($this->ocho->tipo == 8 ){
      $pdf->Image('src/reporte/imagen_1.png','','8','279.5','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->ocho->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->ocho->nombres).' '.strtoupper($this->ocho->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,131);
      $pdf->WriteHTML(utf8_decode('<p ALIGN="center">       '.strtoupper($this->ocho->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(19,183);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->ocho->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','97','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->ocho->identificacion.$this->ocho->tipo,"docente.png");

    }






    //CNTI 001,002,011 id=11,37 ,033 id=35,38, 034 id=36,39 ,

    if($this->uno->tipo == 1 ){
      $pdf->Image('src/reporte/imagen_3.png','','8','284','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->uno->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->uno->nombres).' '.strtoupper($this->uno->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,127);
      $pdf->WriteHTML(utf8_decode('<p ALIGN="center">       '.strtoupper($this->uno->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.3);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->uno->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','90','39','39','PNG');

      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->uno->identificacion.$this->uno->tipo,"docente.png");

    }

    if($this->dos->tipo == 2 ){
      $pdf->Image('src/reporte/imagen_3.png','','8','284','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->dos->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->dos->nombres).' '.strtoupper($this->dos->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,127);
      $pdf->WriteHTML(utf8_decode('<p ALIGN="center">       '.strtoupper($this->dos->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.3);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->dos->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','90','39','39','PNG');

      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->dos->identificacion.$this->dos->tipo,"docente.png");

    }


    //MPPEU 003 id= 3,13 , 005, 014, 031, 032


    if($this->tres->tipo == 3 ){
      $pdf->Image('src/reporte/imagen_8.png','','8','284','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->tres->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->tres->nombres).' '.strtoupper($this->tres->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,127);
      $pdf->WriteHTML(utf8_decode('<p ALIGN="center">       '.strtoupper($this->tres->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.3);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->tres->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','90','39','39','PNG');

      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->tres->identificacion.$this->tres->tipo,"docente.png");
    }



    if($this->cinco->tipo == 5 ){
        $pdf->Image('src/reporte/imagen_8.png','','8','279','200','PNG');

        $pdf->SetFont('Courier','B',20);
        $pdf->SetTextColor(240, 255, 240);
        $pdf->SetXY(104, 118);
        $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->cinco->identificacion).''));
  
        $pdf->SetFont('Courier','B',20);
        $pdf->SetTextColor(240, 255, 240);
        $pdf->SetXY(109,105);
        $pdf->WriteHTML(utf8_decode(''.strtoupper($this->cinco->nombres).' '.strtoupper($this->cinco->apellidos).''));
  
        $pdf->SetFont('Courier','B',14);
        $pdf->SetTextColor(240, 255, 240);
        $pdf->SetXY(24,131);
        $pdf->WriteHTML(utf8_decode('<p ALIGN="center">           '.strtoupper($this->cinco->descripcion).'</p>'));
  
        $pdf->SetFont('Arial','',16);
        $pdf->SetTextColor(224, 185, 93);
        $pdf->SetXY(20,183);
        $pdf->WriteHTML(utf8_decode(''.strtoupper($this->cinco->codigo).''));
  
        //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
        $pdf->Image('docente.png','10','96','39','39','PNG');

        QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->cinco->identificacion.$this->cinco->tipo,"docente.png");
    }



  


    //UCV 004 , 016 


    if($this->cuatro->tipo == 4 ){
      $pdf->Image('src/reporte/imagen_8.png','','8','279','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->cuatro->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->cuatro->nombres).' '.strtoupper($this->cuatro->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,131);
      $pdf->WriteHTML(utf8_decode('<p ALIGN="center">       '.strtoupper($this->cuatro->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.1);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->cuatro->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','94','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->cuatro->identificacion.$this->cuatro->tipo,"docente.png");

    }  

   


    //UCLA 006, 026


    if($this->seis->tipo == 6 ){
      $pdf->Image('src/reporte/imagen_8.png','','8','279.5','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->seis->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->seis->nombres).' '.strtoupper($this->seis->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,131);
      $pdf->WriteHTML(utf8_decode('<p ALIGN="center">       '.strtoupper($this->seis->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(19,183);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->seis->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','97','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->seis->identificacion.$this->seis->tipo,"docente.png");

    }  

   



    //UNETI 009 id= 9,28, 010, 012, 

    
    if($this->nueve->tipo == 9 ){
      
        $pdf->Image('src/reporte/imagen_6.png','','8','280  ','200','PNG');

        $pdf->SetFont('Courier','B',20);
        $pdf->SetTextColor(240, 255, 240);
        $pdf->SetXY(104, 118);
        $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->nueve->identificacion).''));
  
        $pdf->SetFont('Courier','B',20);
        $pdf->SetTextColor(240, 255, 240);
        $pdf->SetXY(109,105);
        $pdf->WriteHTML(utf8_decode(''.strtoupper($this->nueve->nombres).' '.strtoupper($this->nueve->apellidos).''));
  
        $pdf->SetFont('Courier','B', 16);
        $pdf->SetTextColor(240, 255, 240);
        $pdf->SetXY(24,127);
        $pdf->WriteHTML(utf8_decode('<p ALIGN="center">                     '.strtoupper($this->nueve->descripcion).'</p>'));
  
        $pdf->SetFont('Arial','',16);
        $pdf->SetTextColor(224, 185, 93);
        $pdf->SetXY(20.1,183.1);
        $pdf->WriteHTML(utf8_decode(''.strtoupper($this->nueve->codigo).''));
  
        //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
        $pdf->Image('docente.png','10','97','39','39','PNG');
        QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->nueve->identificacion.$this->nueve->tipo,"docente.png");
    }

  

    if($this->diez->tipo == 10 ){
      $pdf->Image('src/reporte/imagen_6.png','','8','279','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->diez->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->diez->nombres).' '.strtoupper($this->diez->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,131);
      $pdf->WriteHTML(utf8_decode('<p ALIGN="center">           '.strtoupper($this->diez->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.1);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->diez->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','96','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->diez->identificacion.$this->diez->tipo,"docente.png");

    }  

   

   

///////////////////////////FECHA EN LETRAS//////////////////////////////////////////7

$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
      
    //  $pdf->SetDrawColor(0,57,127);
      
           $pdf->Ln(15);

  
                $pdf->Ln();
                //Condición ternaria que cambia el valor de $letra
                ($letra == 'D') ? $letra = 'FD' : $letra = 'D';
                //Aumenta la siguiente posición de Y (recordar que X es fijo)
                //Se suma 7 porque cada celda tiene esa altura
                $ejeY = $ejeY + 7;
    

         	$pdf->Output();

?>