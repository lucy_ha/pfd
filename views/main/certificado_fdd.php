<?php

include  'views/main/plantilla.php';
 require_once('src/phpqrcode/qrlib.php');
 
     
   $pdf = new PDF('L','mm','Letter','A10');
	//$pdf->AliasNbPages();
    $pdf->AddPage();

    $pdf->Ln(5);

    //UNEFM 007, 008 id= 8,40, 013 id=14, 

    if($this->catorce->tipo == 14 ){
      $pdf->Image('src/reporte/imagen_1.png','','8','280','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->catorce->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->catorce->nombres).' '.strtoupper($this->catorce->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,127);
      $pdf->WriteHTML(utf8_decode('<p ALIGN="center">       '.strtoupper($this->catorce->descripcion).'</p>'));

      $pdf->SetFont('Arial','',15);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(19,183.1);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->catorce->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','93','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->catorce->identificacion.$this->catorce->tipo,"docente.png");

    }

    



    //CNTI 001,002,011 id=11,37 ,033 id=35,38, 034 id=36,39 ,

    

    if($this->once->tipo == 11 ){
        $pdf->Image('src/reporte/imagen_3.png','','8','279','200','PNG');

        $pdf->SetFont('Courier','B',20);
        $pdf->SetTextColor(240, 255, 240);
        $pdf->SetXY(104, 118);
        $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->once->identificacion).''));
  
        $pdf->SetFont('Courier','B',20);
        $pdf->SetTextColor(240, 255, 240);
        $pdf->SetXY(109,105);
        $pdf->WriteHTML(utf8_decode(''.strtoupper($this->once->nombres).' '.strtoupper($this->once->apellidos).''));
  
        $pdf->SetFont('Courier','B',14);
        $pdf->SetTextColor(240, 255, 240);
        $pdf->SetXY(24,131);
        $pdf->WriteHTML(utf8_decode('<p ALIGN="center">       '.strtoupper($this->once->descripcion).'</p>'));
  
        $pdf->SetFont('Arial','',16);
        $pdf->SetTextColor(224, 185, 93);
        $pdf->SetXY(21,183.1);
        $pdf->WriteHTML(utf8_decode(''.strtoupper($this->once->codigo).''));
  
        //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
        $pdf->Image('docente.png','10','93','39','39','PNG');

        QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->once->identificacion.$this->once->tipo,"docente.png");
    }

   




    //MPPEU 003 id= 3,13 , 005, 014, 031, 032


   

    if($this->trece->tipo == 13 ){
      $pdf->Image('src/reporte/imagen_8.png','','8','284','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->trece->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->trece->nombres).' '.strtoupper($this->trece->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,127);
      $pdf->WriteHTML(utf8_decode('<p ALIGN="center">       '.strtoupper($this->trece->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.3);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->trece->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','90','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->trece->identificacion.$this->trece->tipo,"docente.png");


    }

    

    if($this->quince->tipo == 15 ){
      $pdf->Image('src/reporte/imagen_8.png','','8','279','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->quince->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->quince->nombres).' '.strtoupper($this->quince->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,127);
      $pdf->WriteHTML(utf8_decode('<p ALIGN="center">       '.strtoupper($this->quince->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.1);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->quince->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','93','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->quince->identificacion.$this->quince->tipo,"docente.png");
    }

      

    if($this->diecisiete->tipo == 17 ){
      $pdf->Image('src/reporte/imagen_8.png','','8','284','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->diecisiete->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->diecisiete->nombres).' '.strtoupper($this->diecisiete->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,127);
      $pdf->WriteHTML(utf8_decode('<p align="justify">       '.strtoupper($this->diecisiete->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.3);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->diecisiete->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','90','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->diecisiete->identificacion.$this->diecisiete->tipo,"docente.png");


    }



    //UCLA 006, 026


   
   

    if($this->doce->tipo == 12 ){
      $pdf->Image('src/reporte/imagen_6.png','','8','279','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->doce->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->doce->nombres).' '.strtoupper($this->doce->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,132);
      $pdf->WriteHTML(utf8_decode('<p align="justify">                 '.strtoupper($this->doce->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.1);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->doce->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','96','39','39','PNG');

      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->doce->identificacion.$this->doce->tipo,"docente.png");

    }




    //UNELLEZ 015, 022, 023, 024, 025, 027, 030

    if($this->dieciseis->tipo == 16 ){
      $pdf->Image('src/reporte/imagen_5.png','','8','279','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->dieciseis->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->dieciseis->nombres).' '.strtoupper($this->dieciseis->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,131);
      $pdf->WriteHTML(utf8_decode('<p ALIGN="center">       '.strtoupper($this->dieciseis->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.3);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->dieciseis->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','94','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->dieciseis->identificacion.$this->dieciseis->tipo,"docente.png");

    }

   
    //IAEAG 017, 029

    if($this->dieciocho->tipo == 18 ){
      $pdf->Image('src/reporte/imagen_4.png','','8','280','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->dieciocho->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->dieciocho->nombres).' '.strtoupper($this->dieciocho->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,127);
      $pdf->WriteHTML(utf8_decode('<p ALIGN="center">       '.strtoupper($this->dieciocho->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(19,183);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->dieciocho->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','95','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->dieciocho->identificacion.$this->dieciocho->tipo,"docente.png");

    }  

  



    //UNA 018, 020

    if($this->diecinueve->tipo == 19 ){
      $pdf->Image('src/reporte/imagen_8.png','','8','279','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->diecinueve->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->diecinueve->nombres).' '.strtoupper($this->diecinueve->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,131);
      $pdf->WriteHTML(utf8_decode('<p ALIGN="center">             '.strtoupper($this->diecinueve->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.1);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->diecinueve->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','94','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->diecinueve->identificacion.$this->diecinueve->tipo,"docente.png");

    }  

   
    //UNERG 019 id= 20, 41

    if($this->veinte->tipo == 20 ){
      $pdf->Image('src/reporte/imagen_2.png','','8','279','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->veinte->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->veinte->nombres).' '.strtoupper($this->veinte->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,131);
      $pdf->WriteHTML(utf8_decode('<p ALIGN="center">                        '.strtoupper($this->veinte->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.4);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->veinte->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','96','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->veinte->identificacion.$this->veinte->tipo,"docente.png");
    }  


///////////////////////////FECHA EN LETRAS//////////////////////////////////////////7

$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
      
    //  $pdf->SetDrawColor(0,57,127);
      
           $pdf->Ln(15);

  
                $pdf->Ln();
                //Condición ternaria que cambia el valor de $letra
                ($letra == 'D') ? $letra = 'FD' : $letra = 'D';
                //Aumenta la siguiente posición de Y (recordar que X es fijo)
                //Se suma 7 porque cada celda tiene esa altura
                $ejeY = $ejeY + 7;
    

         	$pdf->Output();

?>