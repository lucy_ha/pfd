<?php

include  'views/main/plantilla.php';
 require_once('src/phpqrcode/qrlib.php');
 
     
   $pdf = new PDF('L','mm','Letter','A10');
	//$pdf->AliasNbPages();
    $pdf->AddPage();

    $pdf->Ln(5);

   
    if($this->cuarenta->tipo == 40 ){
      $pdf->Image('src/reporte/imagen_1.png','','8','284','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->cuarenta->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->cuarenta->nombres).' '.strtoupper($this->cuarenta->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,127);
      $pdf->WriteHTML(utf8_decode('<p ALIGN="center">       '.strtoupper($this->cuarenta->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.3);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->cuarenta->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','90','39','39','PNG');

      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->cuarenta->identificacion.$this->cuarenta->tipo,"docente.png");

    }



    
    if($this->t5->tipo == 35 ){
      $pdf->Image('src/reporte/imagen_3.png','','8','279','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->t5->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->t5->nombres).' '.strtoupper($this->t5->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,131);
      $pdf->WriteHTML(utf8_decode('<p ALIGN="center">       '.strtoupper($this->t5->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(21,183.1);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->t5->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','94','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->t5->identificacion.$this->t5->tipo,"docente.png");


    }

    if($this->t6->tipo == 36 ){
      $pdf->Image('src/reporte/imagen_3.png','','8','279','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->t6->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->t6->nombres).' '.strtoupper($this->t6->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,127);
      $pdf->WriteHTML(utf8_decode('<p ALIGN="center">       '.strtoupper($this->t6->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.1);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->t6->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','94','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->t6->identificacion.$this->t6->tipo,"docente.png");


    }  

    if($this->t7->tipo == 37 ){
      $pdf->Image('src/reporte/imagen_3.png','','8','279','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->t7->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->t7->nombres).' '.strtoupper($this->t7->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,131);
      $pdf->WriteHTML(utf8_decode('<p align="justify">       '.strtoupper($this->t7->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(21,183);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->t7->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','95','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->t7->identificacion.$this->t7->tipo,"docente.png");


    }

    if($this->t8->tipo == 38 ){
      $pdf->Image('src/reporte/imagen_3.png','','8','279','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->t8->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->t8->nombres).' '.strtoupper($this->t8->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,131);
      $pdf->WriteHTML(utf8_decode('<p ALIGN="center">       '.strtoupper($this->t8->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.1);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->t8->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','94','39','39','PNG');

      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->t8->identificacion.$this->t8->tipo,"docente.png");


    }

    if($this->t9->tipo == 39 ){
      $pdf->Image('src/reporte/imagen_3.png','','8','279','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->t9->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->t9->nombres).' '.strtoupper($this->t9->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,131);
      $pdf->WriteHTML(utf8_decode('<p ALIGN="center">       '.strtoupper($this->t9->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.1);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->t9->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','94','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->t9->identificacion.$this->t9->tipo,"docente.png");

    }

    if($this->t3->tipo == 33 ){
      $pdf->Image('src/reporte/imagen_8.png','','8','279','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->t3->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->t3->nombres).' '.strtoupper($this->t3->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,131);
      $pdf->WriteHTML(utf8_decode('<p ALIGN="center">               '.strtoupper($this->t3->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.1);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->t3->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','96','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->t3->identificacion.$this->t3->tipo,"docente.png");


    }  

    if($this->t4->tipo == 34 ){
      $pdf->Image('src/reporte/imagen_8.png','','8','279','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->t4->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->t4->nombres).' '.strtoupper($this->t4->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,131);
      $pdf->WriteHTML(utf8_decode('<p align="justify">       '.strtoupper($this->t4->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.1);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->t4->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','94','39','39','PNG');

      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->t4->identificacion.$this->t4->tipo,"docente.png");

    }




  

    if($this->t2->tipo == 32 ){
      $pdf->Image('src/reporte/imagen_5.png','','8','279','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->t2->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->t2->nombres).' '.strtoupper($this->t2->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,131);
      $pdf->WriteHTML(utf8_decode('<p align="justify">              '.strtoupper($this->t2->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.3);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->t2->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','96','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->t2->identificacion.$this->t2->tipo,"docente.png");

    }



   
    if($this->t1->tipo == 31 ){
      $pdf->Image('src/reporte/imagen_4.png','','8','279','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->t1->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->t1->nombres).' '.strtoupper($this->t1->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,131);
      $pdf->WriteHTML(utf8_decode('<p align="justify">                           '.strtoupper($this->t1->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(19,183.1);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->t1->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','96','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->t1->identificacion.$this->t1->tipo,"docente.png");

    }




    

    if($this->c1->tipo == 41 ){
      $pdf->Image('src/reporte/imagen_2.png','','8','279','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->c1->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->c1->nombres).' '.strtoupper($this->c1->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,131);
      $pdf->WriteHTML(utf8_decode('<p align="justify">                       '.strtoupper($this->c1->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.3);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->c1->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','96','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->c1->identificacion.$this->c1->tipo,"docente.png");

    }


///////////////////////////FECHA EN LETRAS//////////////////////////////////////////7

$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
      
    //  $pdf->SetDrawColor(0,57,127);
      
           $pdf->Ln(15);

  
                $pdf->Ln();
                //Condición ternaria que cambia el valor de $letra
                ($letra == 'D') ? $letra = 'FD' : $letra = 'D';
                //Aumenta la siguiente posición de Y (recordar que X es fijo)
                //Se suma 7 porque cada celda tiene esa altura
                $ejeY = $ejeY + 7;
    

         	$pdf->Output();

?>