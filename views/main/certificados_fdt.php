<?php

include  'views/main/plantilla.php';
 require_once('src/phpqrcode/qrlib.php');
     
   $pdf = new PDF('L','mm','Letter','A10');
	//$pdf->AliasNbPages();
    $pdf->AddPage();

    $pdf->Ln(5);


    if($this->v7->tipo == 27 ){
      $pdf->Image('src/reporte/imagen_8.png','','8','279','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->v7->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->v7->nombres).' '.strtoupper($this->v7->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,131);
      $pdf->WriteHTML(utf8_decode('<p align="justify">       '.strtoupper($this->v7->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->v7->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','94','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->v7->identificacion.$this->v7->tipo,"docente.png");


    }


    if($this->v8->tipo == 28 ){
      $pdf->Image('src/reporte/imagen_6.png','','8','279','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->v8->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->v8->nombres).' '.strtoupper($this->v8->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,131);
      $pdf->WriteHTML(utf8_decode('<p ALIGN="center">                           '.strtoupper($this->v8->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.1);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->v8->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','96','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->v8->identificacion.$this->v8->tipo,"docente.png");
    }


    if($this->v3->tipo == 23 ){
      $pdf->Image('src/reporte/imagen_5.png','','8','284','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->v3->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->v3->nombres).' '.strtoupper($this->v3->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,127);
      $pdf->WriteHTML(utf8_decode('<p ALIGN="center">       '.strtoupper($this->v3->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.3);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->v3->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','90','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->v3->identificacion.$this->v3->tipo,"docente.png");

    }

    if($this->v4->tipo == 24 ){
      $pdf->Image('src/reporte/imagen_5.png','','8','279','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->v4->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->v4->nombres).' '.strtoupper($this->v4->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,131);
      $pdf->WriteHTML(utf8_decode('<p ALIGN="center">       '.strtoupper($this->v4->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.3);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->v4->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','94','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->v4->identificacion.$this->v4->tipo,"docente.png");
    }

    if($this->v5->tipo == 25 ){
      $pdf->Image('src/reporte/imagen_5.png','','8','279','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->v5->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->v5->nombres).' '.strtoupper($this->v5->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,131);
      $pdf->WriteHTML(utf8_decode('<p ALIGN="center">                      '.strtoupper($this->v5->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.3);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->v5->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','96','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->v5->identificacion.$this->v5->tipo,"docente.png");

    }

    if($this->v6->tipo == 26 ){
      $pdf->Image('src/reporte/imagen_5.png','','8','279','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->v6->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->v6->nombres).' '.strtoupper($this->v6->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,131);
      $pdf->WriteHTML(utf8_decode('<p ALIGN="center">       '.strtoupper($this->v6->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.3);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->v6->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','96','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->v6->identificacion.$this->v6->tipo,"docente.png");
    }

    if($this->v9->tipo == 29 ){
      $pdf->Image('src/reporte/imagen_5.png','','8','279','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->v9->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->v9->nombres).' '.strtoupper($this->v9->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,131);
      $pdf->WriteHTML(utf8_decode('<p ALIGN="center">                       '.strtoupper($this->v9->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.4);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->v9->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','96','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->v9->identificacion.$this->v9->tipo,"docente.png");

    }  

    if($this->v1->tipo == 21 ){
      $pdf->Image('src/reporte/imagen_8.png','','8','279','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->v1->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->v1->nombres).' '.strtoupper($this->v1->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,132);
      $pdf->WriteHTML(utf8_decode('<p align="justify">       '.strtoupper($this->v1->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.1);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->v1->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','94','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->v1->identificacion.$this->v1->tipo,"docente.png");
    }


  //SUSCERTE 021

  if($this->v2->tipo == 22 ){
    $pdf->Image('src/reporte/imagen_7.png','','8','280','200','PNG');

    $pdf->SetFont('Courier','B',20);
    $pdf->SetTextColor(240, 255, 240);
    $pdf->SetXY(104, 118);
    $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->v2->identificacion).''));

    $pdf->SetFont('Courier','B',20);
    $pdf->SetTextColor(240, 255, 240);
    $pdf->SetXY(109,105);
    $pdf->WriteHTML(utf8_decode(''.strtoupper($this->v2->nombres).' '.strtoupper($this->v2->apellidos).''));

    $pdf->SetFont('Courier','B',14);
    $pdf->SetTextColor(240, 255, 240);
    $pdf->SetXY(24,127);
    $pdf->WriteHTML(utf8_decode('<p align="justify">       '.strtoupper($this->v2->descripcion).'</p>'));

    $pdf->SetFont('Arial','',16);
    $pdf->SetTextColor(224, 185, 93);
    $pdf->SetXY(20,183.3);
    $pdf->WriteHTML(utf8_decode(''.strtoupper($this->v2->codigo).''));

    //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
    $pdf->Image('docente.png','10','94','39','39','PNG');
    QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->v2->identificacion.$this->v2->tipo,"docente.png");
  }




    if($this->treinta->tipo == 30 ){
      $pdf->Image('src/reporte/imagen_8.png','','8','279','200','PNG');

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(104, 118);
      $pdf->WriteHTML(utf8_decode(' '.strtoupper($this->treinta->identificacion).''));

      $pdf->SetFont('Courier','B',20);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(109,105);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->treinta->nombres).' '.strtoupper($this->treinta->apellidos).''));

      $pdf->SetFont('Courier','B',14);
      $pdf->SetTextColor(240, 255, 240);
      $pdf->SetXY(24,127);
      $pdf->WriteHTML(utf8_decode('<p align="justify">       '.strtoupper($this->treinta->descripcion).'</p>'));

      $pdf->SetFont('Arial','',16);
      $pdf->SetTextColor(224, 185, 93);
      $pdf->SetXY(20,183.1);
      $pdf->WriteHTML(utf8_decode(''.strtoupper($this->treinta->codigo).''));

      //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
      $pdf->Image('docente.png','10','94','39','39','PNG');
      QrCode::png("http://certificadoplanformaciondocente.cnti.gob.ve/main/culminacion/".$this->treinta->identificacion.$this->treinta->tipo,"docente.png");
    }




    

   

///////////////////////////FECHA EN LETRAS//////////////////////////////////////////7

$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
      
    //  $pdf->SetDrawColor(0,57,127);
      
           $pdf->Ln(15);

  
                $pdf->Ln();
                //Condición ternaria que cambia el valor de $letra
                ($letra == 'D') ? $letra = 'FD' : $letra = 'D';
                //Aumenta la siguiente posición de Y (recordar que X es fijo)
                //Se suma 7 porque cada celda tiene esa altura
                $ejeY = $ejeY + 7;
    

         	$pdf->Output();

?>