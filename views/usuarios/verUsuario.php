<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>PFD-Usuarios</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/200.png'/>
</head>

<body>

<?php require 'views/header.php'; ?>  
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-body">
            <div class="row">
           
            <div class="col-lg-12 col-md-10 col-10 col-sm-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Usuario: &nbsp;<?php echo $this->datos->usuario; ?> </h4>
 
                  </div>
                  <div class="row">
                  <div class="card-body">
                    <ul class="nav nav-pills" id="myTab3" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="home-tab3" data-toggle="tab" href="#home3" role="tab"
                          aria-controls="home" aria-selected="true">Información Personal</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="profile-tab3" data-toggle="tab" href="#profile3" role="tab"
                          aria-controls="profile" aria-selected="false">Información de usuario</a>
                      </li>
                    </ul>

   <div class="tab-content" id="myTabContent2">
   <div class="tab-pane fade show active" id="home3" role="tabpanel" aria-labelledby="home-tab3">
         <div class="row">
         <div class="col-lg-12 col-xl-6">
            <div class="table-responsive">
                <table class="table m-0">
                    <tbody>
                        <tr>
                            <th scope="row">Tipo de Documento de Identidad</th>
                            <td><?php echo $this->datos->descripcion; ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Nro. de Identificación</th>
                            <td><?php echo $this->datos->identificacion; ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Nombres</th>
                            <td><?php echo $this->datos->primer_nombre; ?>&nbsp;<?php echo $this->datos->segundo_nombre; ?>&nbsp;</td>
                        </tr>
                        <tr>
                            <th scope="row">Apellidos</th>
                            <td><?php echo $this->datos->primer_apellido; ?>&nbsp;<?php echo $this->datos->segundo_apellido; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>


        <div class="col-lg-10 col-xl-4">
        <div class="table-responsive">
             <table class="table">
                <tbody>
                <tr>
                    <th scope="row">Género</th>
                    <td><?php echo $this->datos->genero; ?></td>
                </tr>
                    <tr>
                        <th scope="row">Correo Electrónico</th>
                        <td><?php echo $this->datos->correo; ?></td>
                    </tr>
                     <tr>
                        <th scope="row">Teléfono Celular</th>
                        <td><?php echo $this->datos->telefono; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div><!-- end of table col-lg-6 -->
         </div>
             </div>


   <div class="tab-pane fade" id="profile3" role="tabpanel" aria-labelledby="profile-tab3">
        <div class="row">
         <div class="col-lg-10 col-xl-4">
            <div class="table-responsive">
                <table class="table m-0">
                    <tbody>
                        <tr>
                            <th scope="row">Usuario</th>
                            <td><?php echo $this->datos->usuario;?></td>
                        </tr>
                        <tr>
                            <th scope="row">Perfil</th>
                            <td><?php echo $this->datos->perfil;?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-lg-10 col-xl-4">
            <div class="table-responsive">
                <table class="table m-0">
                    <tbody>
                    <tr>
                            <th scope="row">Roles de Usuario</th>
                            <td> 
                            <?php 
                            $i=1;
                            while($i<=count($this->datos_u)){
                        ?>                                                                                                            
                                <?php echo $this->rol[$i]['rol'.$i]; ?>                                                                   
                                <?php 
                                $i++;
                             }
                         ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Estatus</th>
                            <td><?php echo $this->datos->estatus;?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
 </div>
</div> <!-- END TAB -->

<div class="card-footer text-right">
<a href="<?php echo constant ('URL')?>usuarios" class="btn btn-warning">Volver</a>
</div>



            </div>
          </div>
        </div>
 <!--End Main Content-->
                 
                 </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div>
      <?php require 'views/footer.php'; ?>
  <!-- General JS Scripts -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <!-- JS Libraies -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-ui/jquery-ui.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/datatables.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno//assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>
</body>

</html>