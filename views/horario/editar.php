<!DOCTYPE html>
<html lang="en">


<!-- form-wizard.html  21 Nov 2019 03:55:16 GMT -->
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Confucio - Horario</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/css/select2.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico'/>
</head>

<body>

  <?php require 'views/header.php'; ?>  

  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <div class="section-body">
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="card-header">
                <h4>Editar Horario</h4>
                <div class="card-header-action">
                  <a href="<?php echo constant ('URL')?>seccion" class="btn btn-warning">Volver</a>
                </div>
              </div>
              <div class="card-body">
                <form action="<?php echo constant('URL'); ?>horario/UpdateHorario" id="wizard_with_validation"  novalidate="" method="POST">
                  <h3>Datos del horario</h3>
                  <fieldset>
                    <div class="row">
                      <input type="hidden" name="id_horario" value="<?php echo $this->horario->id_horario; ?>">

                      <div class="col-lg-6">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Turno*</label>
                            <select name="id_turno" class="form-control select2" >
                              <option value="">Seleccione</option>
                              <?php 
                              foreach($this->turnos as $row){
                                $turno=new Confucio();
                                $turno=$row;?> 
                                <option value="<?php echo $turno->id;?>" <?php if($turno->id==$this->horario->id_turno){ echo "selected=selected"; }?>><?php echo $turno->descripcion;?></option>              
                              <?php }?>
                            </select>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-6">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Días*</label>
                            <select id="dia" name="dia[]" class="form-control select2" multiple>
                             <?php                              
                             foreach($this->dias as $row){
                              $dia=new Confucio();
                              $dia=$row;?> 
                              <option value="<?php echo $dia->id;?>" <?php 
                              $i=1;
                              while($i<=count($this->datos_h)){

                                if($dia->id==$this->dia[$i]['id_dia'.$i]){ 
                                  echo "selected=selected"; 
                                }
                                $i++;
                              } ?>><?php echo $dia->descripcion;?></option>
                            <?php } ?> 
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group form-float">
                        <div class="form-line">
                          <label class="form-label">Hora Inicio*</label>
                          <input type="time" name="hora_inicio" class="form-control" value="<?php echo $this->horario->hora_inicio; ?>" required>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group form-float">
                        <div class="form-line">
                          <label class="form-label">Hora Fin*</label>
                          <input type="time" name="hora_fin" class="form-control" value="<?php echo $this->horario->hora_fin; ?>" required>
                        </div>
                      </div>
                    </div>

                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <?php require 'views/footer.php'; ?>

   <!-- General JS Scripts -->
   <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/js/select2.full.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-validation/dist/jquery.validate.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/advance-elements/select2-custom.js"></script>
  <!-- JS Libraies -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-steps/jquery.steps.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/form-wizard.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>
</body>


<!-- form-wizard.html  21 Nov 2019 03:55:20 GMT -->
</html>

