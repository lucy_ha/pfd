<!DOCTYPE html>
<html lang="en">


<!-- form-wizard.html  21 Nov 2019 03:55:16 GMT -->
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Plan FD - Periodo Académico</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/css/select2.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico'/>
</head>

<body>

  <?php require 'views/header.php'; ?>  
  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <div class="section-body">
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="card-header">
                <h4>Editar Periodo Académico</h4>
                <div class="card-header-action">
                  <a href="<?php echo constant ('URL')?>periodo" class="btn btn-warning">Volver</a>
                </div>
              </div>
              <div class="card-body">
                <form action="<?php echo constant('URL'); ?>periodo/UpdatePeriodo" id="wizard_with_validation"  novalidate="" method="POST">
                  <h3>Datos del Periodo Académico</h3>
                  <fieldset>
                    <div class="row">
                      <input type="hidden" name="id_periodo" value="<?php echo $this->periodo->id_periodo; ?>">
                      <div class="col-lg-4">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Fecha de inicio<span style="color: red;">&nbsp;*</span></label>
                            <input type="date" class="form-control required" name="inicio_periodo" value="<?php echo $this->periodo->inicio_periodo; ?>">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Fecha de culminación<span style="color: red;">&nbsp;*</span></label>
                            <input type="date" class="form-control required" name="fin_periodo" value="<?php echo $this->periodo->fin_periodo; ?>">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Inicio de carga de notas<span style="color: red;">&nbsp;*</span></label>
                            <input type="date" class="form-control required" name="inicio_carga_notas" value="<?php echo $this->periodo->inicio_carga_notas; ?>">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Fin de carga de notas<span style="color: red;">&nbsp;*</span></label>
                            <input type="date" class="form-control required" name="fin_carga_notas" value="<?php echo $this->periodo->fin_carga_notas; ?>">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Estatus<span style="color: red;">&nbsp;*</span></label>
                            <select name="estatus" class="form-control select2">
                              <option>Seleccione</option> 
                              <option value="Activo" <?php if("Activo"==$this->periodo->estatus){ echo "selected=selected"; }?>>Activo</option> 
                              <option value="Inactivo" <?php if("Inactivo"==$this->periodo->estatus){ echo "selected=selected"; }?>>Inactivo</option> 
                            </select> 
                          </div>
                        </div>
                      </div>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <?php require 'views/footer.php'; ?>

  <!-- General JS Scripts -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-steps/jquery.steps.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/js/select2.full.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-validation/dist/jquery.validate.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/form-wizard.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>
</body>


<!-- form-wizard.html  21 Nov 2019 03:55:20 GMT -->
</html>