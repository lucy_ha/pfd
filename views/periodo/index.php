<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Plan FD - Periodo Académico</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/pretty-checkbox/pretty-checkbox.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico'/>
</head>

<body>

  <?php require 'views/header.php'; ?>  
  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <div class="section-body">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4>Lista de Periodos Académicos</h4>
              </div>
              <div class="card-body">
                <?php echo $this->mensaje; ?>
                <div class="table-responsive">
                  <table class="table table-striped" id="table-1">
                    <thead>
                      <tr>
                        <th class="text-center">#</th>
                        <!-- <th>Periodo Acad.</th> -->
                        <th>Fecha inicio</th>
                        <th>Fecha fin</th>
                        <th>Inicio carga notas</th>
                        <th>Fin carga notas</th>
                        <th>Estatus</th>
                        <th>Acciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php  foreach ($this->periodos as $row) {
                        $periodo = new Confucio();
                        $periodo = $row;
                        ?>
                        <tr>
                          <td><?php echo $periodo->id_periodo; ?></td>
                          <!-- <td><?php echo $periodo->descripcion; ?></td> -->
                          <td><?php echo $periodo->inicio_periodo; ?></td>
                          <td><?php echo $periodo->fin_periodo; ?></td>
                          <td><?php echo $periodo->inicio_carga_notas; ?></td>
                          <td><?php echo $periodo->fin_carga_notas; ?></td>
                          <?php if ($periodo->estatus == "Activo") { ?>
                            <td><div class="badge badge-pill badge-success">Activo</div></td>
                          <?php } ?>
                          <?php if ($periodo->estatus == "Inactivo") { ?>
                            <td><div class="badge badge-pill badge-danger">Inactivo</div></td>
                          <?php } ?>
                          <td>
                          <?php if($_SESSION['Editar']==true){?>
                            <a href="<?php echo constant('URL') . 'periodo/editar/' . $periodo->id_periodo; ?>" class="btn btn-primary" title="Editar Periodo Académico">
                              <i class="fas fa-edit"></i>
                            </a>
                            <?php } ?>
                          </td>
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!--End Main Content-->
  <?php require 'views/footer.php'; ?>

  <!-- General JS Scripts -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <!-- JS Libraies -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-ui/jquery-ui.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/datatables.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno//assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>
</body>

</html>
