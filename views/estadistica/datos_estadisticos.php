<?php

require_once '././src/phpexcel/Classes/PHPExcel.php';

$objPHPExcel = new PHPExcel();

foreach($this->consul as $row){
    $consult=new Confucio();
    $consult=$row;
}

if($consult->tipo==1){
    $objPHPExcel->getActiveSheet()->mergeCells('A1:D1')->setCellValue('A1', $consult->nombre_consulta);
    $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFont()->setBold(true)->setSize(20);
    $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(60);
    $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setHorizontal('center');
    $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setVertical('center');

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(34);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(26);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);

    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A2', '#')
    ->setCellValue('B2', 'Nivel Académico')
    ->setCellValue('C2', 'Cantidad de estudiantes')
    ->setCellValue('D2', '%');

    $objPHPExcel->getActiveSheet()->getStyle('A2:D2')->getFont()->setBold(true)->setSize(12);
    $objPHPExcel->getActiveSheet()->getStyle('A2:D2')->getAlignment()->setHorizontal('center');
    $objPHPExcel->getActiveSheet()->getStyle('A2:D2')->getAlignment()->setVertical('center');
    $objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(25);

    $i = 3;
    $num=1;
    foreach($this->consul as $row){
        $consult=new Confucio();
        $consult=$row;
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("A$i", $num)
        ->setCellValue("B$i", $consult->descripcion_nivel_academico)
        ->setCellValue("C$i", $consult->cant);
        $porcentaje=$consult->cant / $consult->total * 100;
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("D$i", substr($porcentaje, 0, 4).'%');
        $i++;
        $num++;
    }

    $num= $num-1;
    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue("A$i", 'Total')
    ->setCellValue("B$i", $num)
    ->setCellValue("C$i", $consult->total)
    ->setCellValue("D$i", '100%');

    $objPHPExcel->getActiveSheet()->getStyle("A$i:D$i")->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle("A3:D$i")->getAlignment()->setHorizontal('center');
    $border_style= array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('argb' => '000000'),)));
    $objPHPExcel->getActiveSheet()->getStyle("A1:D$i")->applyFromArray($border_style);

    $objPHPExcel->getActiveSheet()->setTitle('Datos estadisticos');
    $objPHPExcel->setActiveSheetIndex(0);

}elseif($consult->tipo==9){
    $objPHPExcel->getActiveSheet()->mergeCells('A1:D1')->setCellValue('A1', $consult->nombre_consulta);
    $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFont()->setBold(true)->setSize(20);
    $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(60);
    $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setHorizontal('center');
    $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setVertical('center');

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(34);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(26);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);

    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A2', '#')
    ->setCellValue('B2', 'Estado')
    ->setCellValue('C2', 'Cantidad de estudiantes')
    ->setCellValue('D2', '%');

    $objPHPExcel->getActiveSheet()->getStyle('A2:D2')->getFont()->setBold(true)->setSize(12);
    $objPHPExcel->getActiveSheet()->getStyle('A2:D2')->getAlignment()->setHorizontal('center');
    $objPHPExcel->getActiveSheet()->getStyle('A2:D2')->getAlignment()->setVertical('center');
    $objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(25);

    $i = 3;
    $num=1;
    foreach($this->consul as $row){
        $consult=new Confucio();
        $consult=$row;
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("A$i", $num)
        ->setCellValue("B$i", $consult->direccion)
        ->setCellValue("C$i", $consult->cant);
        $porcentaje=$consult->cant / $consult->total * 100;
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("D$i", substr($porcentaje, 0, 4).'%');
        $i++;
        $num++;
    }

    $num= $num-1;
    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue("A$i", 'Total')
    ->setCellValue("B$i", $num)
    ->setCellValue("C$i", $consult->total)
    ->setCellValue("D$i", '100%');

    $objPHPExcel->getActiveSheet()->getStyle("A$i:D$i")->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle("A3:D$i")->getAlignment()->setHorizontal('center');
    $border_style= array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('argb' => '000000'),)));
    $objPHPExcel->getActiveSheet()->getStyle("A1:D$i")->applyFromArray($border_style);

    $objPHPExcel->getActiveSheet()->setTitle('Datos estadisticos');
    $objPHPExcel->setActiveSheetIndex(0);

}elseif($consult->tipo==19){
    $objPHPExcel->getActiveSheet()->mergeCells('A1:D1')->setCellValue('A1', $consult->nombre_consulta);
    $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFont()->setBold(true)->setSize(24);
    $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(60);
    $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setHorizontal('center');
    $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setVertical('center');

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(34);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(26);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);

    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A2', '#')
    ->setCellValue('B2', 'Sección')
    ->setCellValue('C2', 'Cantidad de estudiantes')
    ->setCellValue('D2', '%');

    $objPHPExcel->getActiveSheet()->getStyle('A2:D2')->getFont()->setBold(true)->setSize(12);
    $objPHPExcel->getActiveSheet()->getStyle('A2:D2')->getAlignment()->setHorizontal('center');
    $objPHPExcel->getActiveSheet()->getStyle('A2:D2')->getAlignment()->setVertical('center');
    $objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(25);

    $i = 3;
    $num=1;
    foreach($this->consul as $row){
        $consult=new Confucio();
        $consult=$row;
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("A$i", $num)
        ->setCellValue("B$i", $consult->cursos)
        ->setCellValue("C$i", $consult->cant);
        $porcentaje=$consult->cant / $consult->total * 100;
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("D$i", substr($porcentaje, 0, 4).'%');
        $i++;
        $num++;
    }

    $num= $num-1;
    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue("A$i", 'Total')
    ->setCellValue("B$i", $num)
    ->setCellValue("C$i", $consult->total)
    ->setCellValue("D$i", '100%');

    $objPHPExcel->getActiveSheet()->getStyle("A$i:D$i")->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle("A3:D$i")->getAlignment()->setHorizontal('center');
    $border_style= array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('argb' => '000000'),)));
    $objPHPExcel->getActiveSheet()->getStyle("A1:D$i")->applyFromArray($border_style);

    $objPHPExcel->getActiveSheet()->setTitle('Datos estadisticos');
    $objPHPExcel->setActiveSheetIndex(0);


}elseif($consult->tipo==20){

    $objPHPExcel->getActiveSheet()->mergeCells('A1:E1')->setCellValue('A1', $consult->nombre_consulta);
    $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFont()->setBold(true)->setSize(20);
    $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(60);
    $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getAlignment()->setHorizontal('center');
    $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getAlignment()->setVertical('center');

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(34);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(26);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);

    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A2', '#')
    ->setCellValue('B2', 'Periodo Acad.')
    ->setCellValue('C2', 'Curso')
    ->setCellValue('D2', 'Cantidad de estudiantes')
    ->setCellValue('E2', '%');

    $objPHPExcel->getActiveSheet()->getStyle('A2:E2')->getFont()->setBold(true)->setSize(12);
    $objPHPExcel->getActiveSheet()->getStyle('A2:E2')->getAlignment()->setHorizontal('center');
    $objPHPExcel->getActiveSheet()->getStyle('A2:E2')->getAlignment()->setVertical('center');
    $objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(25);

    $i = 3;
    $num=1;
    foreach($this->consul as $row){
        $consult=new Confucio();
        $consult=$row;
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("A$i", $num)
        ->setCellValue("B$i", $consult->periodo)
        ->setCellValue("C$i", $consult->cursos)
        ->setCellValue("D$i", $consult->cant);

        $porcentaje=$consult->cant / $consult->total * 100;
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("E$i", substr($porcentaje, 0, 4).'%');

        $i++;
        $num++;
    }

    $num= $num-1;
    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue("A$i", 'Total')
    ->setCellValue("B$i", '')
    ->setCellValue("C$i", $num)
    ->setCellValue("D$i", $consult->total)
    ->setCellValue("E$i", '100%');

    $objPHPExcel->getActiveSheet()->getStyle("A$i:E$i")->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle("A3:E$i")->getAlignment()->setHorizontal('center');
    $border_style= array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('argb' => '000000'),)));
    $objPHPExcel->getActiveSheet()->getStyle("A1:E$i")->applyFromArray($border_style);

    $objPHPExcel->getActiveSheet()->setTitle('Datos estadisticos');
    $objPHPExcel->setActiveSheetIndex(0);
}elseif($consult->tipo==21){

    $objPHPExcel->getActiveSheet()->mergeCells('A1:F1')->setCellValue('A1', $consult->nombre_consulta);
    $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true)->setSize(20);
    $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(60);
    $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getAlignment()->setHorizontal('center');
    $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getAlignment()->setVertical('center');

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(28);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(34);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(26);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);

    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A2', '#')
    ->setCellValue('B2', 'Periodo Acad.')
    ->setCellValue('C2', 'Idioma')
    ->setCellValue('D2', 'Nivel')
    ->setCellValue('E2', 'Cantidad de estudiantes')
    ->setCellValue('F2', '%');

    $objPHPExcel->getActiveSheet()->getStyle('A2:F2')->getFont()->setBold(true)->setSize(12);
    $objPHPExcel->getActiveSheet()->getStyle('A2:F2')->getAlignment()->setHorizontal('center');
    $objPHPExcel->getActiveSheet()->getStyle('A2:F2')->getAlignment()->setVertical('center');
    $objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(25);

    $i = 3;
    $num=1;
    foreach($this->consul as $row){
        $consult=new Confucio();
        $consult=$row;
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("A$i", $num)
        ->setCellValue("B$i", $consult->periodo)
        ->setCellValue("C$i", $consult->cursos)
        ->setCellValue("D$i", $consult->nivel)
        ->setCellValue("E$i", $consult->cant);
        $porcentaje=$consult->cant / $consult->total * 100;
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("F$i", substr($porcentaje, 0, 4).'%');
        $i++;
        $num++;
    }

    $num= $num-1;
    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue("A$i", 'Total')
    ->setCellValue("B$i", '')
    ->setCellValue("C$i", $num)
    ->setCellValue("D$i", $num)
    ->setCellValue("E$i", $consult->total)
    ->setCellValue("F$i", '100%');

    $objPHPExcel->getActiveSheet()->getStyle("A$i:F$i")->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle("A3:F$i")->getAlignment()->setHorizontal('center');
    $border_style= array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('argb' => '000000'),)));
    $objPHPExcel->getActiveSheet()->getStyle("A1:F$i")->applyFromArray($border_style);

    $objPHPExcel->getActiveSheet()->setTitle('Datos estadisticos');
    $objPHPExcel->setActiveSheetIndex(0);

}elseif($consult->tipo==22){
    $objPHPExcel->getActiveSheet()->mergeCells('A1:F1')->setCellValue('A1', $consult->nombre_consulta);
    $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true)->setSize(20);
    $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(60);
    $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getAlignment()->setHorizontal('center');
    $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getAlignment()->setVertical('center');

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(28);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(34);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(26);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);

    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A2', '#')
    ->setCellValue('B2', 'Periodo Acad.')
    ->setCellValue('C2', 'Idioma')
    ->setCellValue('D2', 'Nivel')
    ->setCellValue('E2', 'Cantidad de estudiantes')
    ->setCellValue('F2', '%');

    $objPHPExcel->getActiveSheet()->getStyle('A2:F2')->getFont()->setBold(true)->setSize(12);
    $objPHPExcel->getActiveSheet()->getStyle('A2:F2')->getAlignment()->setHorizontal('center');
    $objPHPExcel->getActiveSheet()->getStyle('A2:F2')->getAlignment()->setVertical('center');
    $objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(25);

    $i = 3;
    $num=1;
    foreach($this->consul as $row){
        $consult=new Confucio();
        $consult=$row;
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("A$i", $num)
        ->setCellValue("B$i", $consult->periodo)
        ->setCellValue("C$i", $consult->cursos)
        ->setCellValue("D$i", $consult->nivel)
        ->setCellValue("E$i", $consult->cant);
        $porcentaje=$consult->cant / $consult->total * 100;
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("F$i", substr($porcentaje, 0, 4).'%');
        $i++;
        $num++;
    }

    $num= $num-1;
    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue("A$i", 'Total')
    ->setCellValue("B$i", '')
    ->setCellValue("C$i", $num)
    ->setCellValue("D$i", $num)
    ->setCellValue("E$i", $consult->total)
    ->setCellValue("F$i", '100%');

    $objPHPExcel->getActiveSheet()->getStyle("A$i:F$i")->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle("A3:F$i")->getAlignment()->setHorizontal('center');
    $border_style= array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('argb' => '000000'),)));
    $objPHPExcel->getActiveSheet()->getStyle("A1:F$i")->applyFromArray($border_style);

    $objPHPExcel->getActiveSheet()->setTitle('Datos estadisticos');
    $objPHPExcel->setActiveSheetIndex(0);

}elseif($consult->tipo==23){
    
  $objPHPExcel->getActiveSheet()->mergeCells('A1:E1')->setCellValue('A1', $consult->nombre_consulta);
    $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFont()->setBold(true)->setSize(20);
    $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(60);
    $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getAlignment()->setHorizontal('center');
    $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getAlignment()->setVertical('center');

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(34);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(26);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);

    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A2', '#')
    ->setCellValue('B2', 'Exonerado')
    ->setCellValue('C2', 'Modalidad de exoneración')
    ->setCellValue('D2', 'Cantidad de estudiantes')
    ->setCellValue('E2', '%');

    $objPHPExcel->getActiveSheet()->getStyle('A2:E2')->getFont()->setBold(true)->setSize(12);
    $objPHPExcel->getActiveSheet()->getStyle('A2:E2')->getAlignment()->setHorizontal('center');
    $objPHPExcel->getActiveSheet()->getStyle('A2:E2')->getAlignment()->setVertical('center');
    $objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(25);

    $i=3;
    $num=1;
    
    foreach($this->consul as $row){
        $consult=new Confucio();
        $consult=$row;
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("A$i", $num)
        ->setCellValue("B$i", $consult->exonerado)
        ->setCellValue("C$i", $consult->modalidad_exo)
        ->setCellValue("D$i", $consult->cant);
        $porcentaje=$consult->cant / $consult->total * 100;
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("E$i", substr($porcentaje, 0, 4).'%');
        $i++;
        $num++;
    }

    $num= $num-1;
    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue("A$i", 'Total')
    ->setCellValue("B$i", $num)
    ->setCellValue("C$i", $num)
    ->setCellValue("D$i", $consult->total)
    ->setCellValue("E$i", '100%');

    $objPHPExcel->getActiveSheet()->getStyle("A$i:E$i")->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle("A3:E$i")->getAlignment()->setHorizontal('center');
    $border_style= array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('argb' => '000000'),)));
    $objPHPExcel->getActiveSheet()->getStyle("A1:E$i")->applyFromArray($border_style);

    $objPHPExcel->getActiveSheet()->setTitle('Datos estadisticos');
    $objPHPExcel->setActiveSheetIndex(0);

}
elseif($consult->tipo==25){
    $objPHPExcel->getActiveSheet()->mergeCells('A1:E1')->setCellValue('A1', $consult->nombre_consulta);
    $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFont()->setBold(true)->setSize(20);
    $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(60);
    $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getAlignment()->setHorizontal('center');
    $objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getAlignment()->setVertical('center');

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(34);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(26);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);

    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A2', '#')
    ->setCellValue('B2', 'Periodo Acad.')
    ->setCellValue('C2', 'Nivel')
    ->setCellValue('D2', 'Cantidad de estudiantes')
    ->setCellValue('E2', '%');

    $objPHPExcel->getActiveSheet()->getStyle('A2:E2')->getFont()->setBold(true)->setSize(12);
    $objPHPExcel->getActiveSheet()->getStyle('A2:E2')->getAlignment()->setHorizontal('center');
    $objPHPExcel->getActiveSheet()->getStyle('A2:E2')->getAlignment()->setVertical('center');
    $objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(25);

    $i = 3;
    $num=1;
    foreach($this->consul as $row){
        $consult=new Confucio();
        $consult=$row;
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("A$i", $num)
        ->setCellValue("B$i", $consult->periodo)
        ->setCellValue("C$i", $consult->nivel)
        ->setCellValue("D$i", $consult->cant);
        $porcentaje=$consult->cant / $consult->total * 100;
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("E$i", substr($porcentaje, 0, 4).'%');
        $i++;
        $num++;
    }

    $num= $num-1;
    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue("A$i", 'Total')
    ->setCellValue("B$i", '')
    ->setCellValue("C$i", $num)
    ->setCellValue("D$i", $consult->total)
    ->setCellValue("E$i", '100%');

    $objPHPExcel->getActiveSheet()->getStyle("A$i:E$i")->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle("A3:E$i")->getAlignment()->setHorizontal('center');
    $border_style= array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('argb' => '000000'),)));
    $objPHPExcel->getActiveSheet()->getStyle("A1:E$i")->applyFromArray($border_style);

    $objPHPExcel->getActiveSheet()->setTitle('Datos estadisticos');
    $objPHPExcel->setActiveSheetIndex(0);
}elseif($consult->tipo==26){
    $objPHPExcel->getActiveSheet()->mergeCells('A1:D1')->setCellValue('A1', $consult->nombre_consulta);
    $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFont()->setBold(true)->setSize(20);
    $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(60);
    $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setHorizontal('center');
    $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setVertical('center');

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(34);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(26);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);

    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A2', '#')
    ->setCellValue('B2', 'Nivel')
    ->setCellValue('C2', 'Cantidad de aspirantes')
    ->setCellValue('D2', '%');

    $objPHPExcel->getActiveSheet()->getStyle('A2:D2')->getFont()->setBold(true)->setSize(12);
    $objPHPExcel->getActiveSheet()->getStyle('A2:D2')->getAlignment()->setHorizontal('center');
    $objPHPExcel->getActiveSheet()->getStyle('A2:D2')->getAlignment()->setVertical('center');
    $objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(25);

    $i = 3;
    $num=1;
    foreach($this->consul as $row){
        $consult=new Confucio();
        $consult=$row;
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("A$i", $num)
        ->setCellValue("B$i", $consult->cursos)
        ->setCellValue("C$i", $consult->cant);
        $porcentaje=$consult->cant / $consult->total * 100;
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("D$i", substr($porcentaje, 0, 4).'%');
        $i++;
        $num++;
    }

    $num= $num-1;
    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue("A$i", 'Total')
    ->setCellValue("B$i", $num)
    ->setCellValue("C$i", $consult->total)
    ->setCellValue("D$i", '100%');

    $objPHPExcel->getActiveSheet()->getStyle("A$i:D$i")->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle("A3:D$i")->getAlignment()->setHorizontal('center');
    $border_style= array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('argb' => '000000'),)));
    $objPHPExcel->getActiveSheet()->getStyle("A1:D$i")->applyFromArray($border_style);

    $objPHPExcel->getActiveSheet()->setTitle('Datos estadisticos');
    $objPHPExcel->setActiveSheetIndex(0);
}


$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-Excel');
header('Content-Disposition: attachment;filename="Datos Estadisticos.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;