<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Confucio - Estadisticas</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/pretty-checkbox/pretty-checkbox.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico'/>

  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jqvmap/dist/jqvmap.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/flag-icon-css/css/flag-icon.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/css/select2.min.css">
</head>

<body>

  <?php require 'views/header.php'; ?>  
 

      <!-- Main Content -->

      
      <div class="main-content">
    
          <div class="section-body">

              <!----------------------------------------------------------->     
          <div class="col-xl-4 col-md-12 col-lg-4" style="margin-left:33%;">
                <div class="card l-bg-green" >
                  <div class="card-body">
                    <div class="text-white" >
                  
                        <form action="<?php echo constant ('URL');?>estadistica/consulta_estadistica"  method="POST">
                      
                        <select name="estadistica" class="form-control select_2" >
                      <option value="" style="margin-left:-30%;"  >SELECCIONE</option>
                     
                     <?php 
                      foreach($this->estadis as $row){
                      $esta=new Confucio();
                      $esta=$row;
                      ?> 
                      
                    <option value="<?php echo $esta->id_consulta;?>"><?php echo $esta->descripcion;?></option>             
                  <?php }?>

                      </select>
                      <br><br>
                  
              
                                <input  style="margin-left:30%;"  class="btn btn-success" title="Consultar" type="submit">
                                </form>
                         
                      
                     
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
            <!----------------------------------------------------------->
            <div class="row" style="width:155%;">
              <div class="col-xl-8 col-md-12 col-lg-8">
                <div class="card">
                  
                <?php 
                      foreach($this->consul as $row){
                      $consult=new Confucio();
                      $consult=$row;
                      }
                  
                      ?> 
                      
                  <div class="card-body">
                    <ul class="list-inline text-center">

                    <?php  if($consult->tipo==''){?>
                      <div><h3><?php echo $this->encabezado;?> <hr></div>
                   <?php } ?>
                        <?php if($consult->tipo==0){?>
                          <h3>Por Los momentos no tenemos registros para esta Estadistica <h3>
                    <?php }elseif($consult->tipo==1){?>
                      <div><h3><?php echo $consult->nombre_consulta;?>
                      <a class="btn btn-success" href="<?php echo constant('URL') . 'estadistica/reportes_estadistica/' . $consult->tipo; ?>"><i class="fas fa-arrow-down"></i> Exportar</a>
                      <hr></div>
                   <?php  }elseif($consult->tipo==2){?> 
                      <div><h3><?php echo $consult->nombre_consulta;?></h3><hr></div>
                      <?php  }elseif($consult->tipo==3){?>
                        <div><h3><?php echo $consult->nombre_consulta;?></h3><hr></div>
                        <?php  }elseif($consult->tipo==4){?> 
                          <div><h3><?php echo $consult->nombre_consulta;?></h3><hr></div>
                          <?php  }elseif($consult->tipo==5){?> 
                            <div><h3><?php echo $consult->nombre_consulta;?></h3><hr></div>
                            <?php  }elseif($consult->tipo==6){?> 
                              <div><h3><?php echo $consult->nombre_consulta;?></h3><hr></div>
                              <?php  }elseif($consult->tipo==7){?> 
                                <div><h3><?php echo $consult->nombre_consulta;?></h3><hr></div>
                                <?php  }elseif($consult->tipo==8){?> 
                                  <div><h3><?php echo $consult->nombre_consulta;?></h3><hr></div>
                                  <?php  }elseif($consult->tipo==9){?> 
                                    <div><h3><?php echo $consult->nombre_consulta;?></h3>
                                    <a class="btn btn-success" href="<?php echo constant('URL') . 'estadistica/reportes_estadistica/' . $consult->tipo; ?>"><i class="fas fa-arrow-down"></i> Exportar</a>
                                    <hr></div>
                                    <?php  }elseif($consult->tipo==10){?> 
                                      <div><h3><?php echo $consult->nombre_consulta;?></h3><hr></div>
                                      <?php  }elseif($consult->tipo==11){?> 
                                        <div><h3><?php echo $consult->nombre_consulta;?></h3><hr></div>
                                        <?php  }elseif($consult->tipo==12){?> 
                                          <div><h3><?php echo $consult->nombre_consulta;?></h3><hr></div>
                                          <?php  }elseif($consult->tipo==13){?> 
                                            <div><h3><?php echo $consult->nombre_consulta;?></h3><hr></div>
                                            <?php  }elseif($consult->tipo==14){?> 
                                              <div><h3><?php echo $consult->nombre_consulta;?></h3><hr></div>
                                              <?php  }elseif($consult->tipo==15){?> 
                                                <div><h3><?php echo $consult->nombre_consulta;?></h3><hr></div>
                                                <?php  }elseif($consult->tipo==16){?> 
                                                  <div><h3><?php echo $consult->nombre_consulta;?></h3><hr></div>
                                                  <?php  }elseif($consult->tipo==17){?> 
                                                    <div><h3><?php echo $consult->nombre_consulta;?></h3><hr></div>
                                                    <?php  }elseif($consult->tipo==18){?> 
                                                      <div><h3><?php echo $consult->nombre_consulta;?></h3><hr></div>
                                                      <?php  }elseif($consult->tipo==19){?> 
                                                        <div><h3><?php echo $consult->nombre_consulta;?></h3>
                                                        <a class="btn btn-success" href="<?php echo constant('URL') . 'estadistica/reportes_estadistica/' . $consult->tipo; ?>"><i class="fas fa-arrow-down"></i> Exportar</a>
                                                        <hr></div>
                                                        <?php  }elseif($consult->tipo==20){?> 
                                                          <div><h3><?php echo $consult->nombre_consulta;?></h3>
                                                          <a class="btn btn-success" href="<?php echo constant('URL') . 'estadistica/reportes_estadistica/' . $consult->tipo; ?>"><i class="fas fa-arrow-down"></i> Exportar</a>
                                                          <hr></div>
                                                          <?php  }elseif($consult->tipo==21){?> 
                                                            <div><h3><?php echo $consult->nombre_consulta;?></h3>
                                                            <a class="btn btn-success" href="<?php echo constant('URL') . 'estadistica/reportes_estadistica/' . $consult->tipo; ?>"><i class="fas fa-arrow-down"></i> Exportar</a>
                                                            <hr></div>
                                                            <?php  }elseif($consult->tipo==22){?> 
                                                              <div><h3><?php echo $consult->nombre_consulta;?></h3>
                                                              <a class="btn btn-success" href="<?php echo constant('URL') . 'estadistica/reportes_estadistica/' . $consult->tipo; ?>"><i class="fas fa-arrow-down"></i> Exportar</a>
                                                              <hr></div>
                                                              <?php  }elseif($consult->tipo==23){?> 
                                                                <div><h3><?php echo $consult->nombre_consulta;?></h3>
                                                                <a class="btn btn-success" href="<?php echo constant('URL') . 'estadistica/reportes_estadistica/' . $consult->tipo; ?>"><i class="fas fa-arrow-down"></i> Exportar</a>
                                                                <hr></div>
                                                                <?php  }elseif($consult->tipo==24){?> 
                                                                  <div><h3><?php echo $consult->nombre_consulta;?></h3><hr></div>
                                                                  <?php  }elseif($consult->tipo==25){?>  
                                                                    <div><h3><?php echo $consult->nombre_consulta;?></h3>
                                                                    <a class="btn btn-success" href="<?php echo constant('URL') . 'estadistica/reportes_estadistica/' . $consult->tipo; ?>"><i class="fas fa-arrow-down"></i> Exportar</a>
                                                                    <hr></div>
                                                                  <?php  }elseif($consult->tipo==26){?>  
                                                                    <div><h3><?php echo $consult->nombre_consulta;?></h3>
                                                                    <a class="btn btn-success" href="<?php echo constant('URL') . 'estadistica/reportes_estadistica/' . $consult->tipo; ?>"><i class="fas fa-arrow-down"></i> Exportar</a>
                                                                    <hr></div>
                                                                  <?php }?>
                   

                      <?php 
                      foreach($this->consul as $row){
                      $consult=new Confucio();
                      $consult=$row;
                     
                      ?> 
                       
                     <?php  if($consult->tipo==1){?>
                              
                      <li class="list-inline-item p-r-30"><i data-feather="arrow-up-circle" class="col-green"></i>
                        <h5 class="m-b-0"><?php echo $consult->cant;?></h5><h6>Porcentaje : <?php $porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>%</h6>
                        <p class="text-muted font-14 m-b-0"><?php echo $consult->descripcion_nivel_academico;?></p>
                      </li>
                      <?php }?>
 <!--------------------------------------------------------------------------------------------------->
                      <?php  if($consult->tipo==2){?>
                      <li class="list-inline-item p-r-30"><i data-feather="arrow-up-circle" class="col-green"></i>
                        <h5 class="m-b-0"><?php echo $consult->cant;?> Estudiantes</h5><h6>Porcentaje : <?php $porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>%</h6>
                        <p class="text-muted font-14 m-b-0"><b>Estudiantes UBV inscritos en IC</b></p>
                      </li>
                      <?php }?>
                      <?php  if($consult->tipo==3){?>
                        <li class="list-inline-item p-r-30"><i data-feather="users" class="col-green"></i>
                        <h5 class="m-b-0"><?php echo $consult->cant;?></h5><h6>Porcentaje : <?php $porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>%</h6>
                        <p class="text-muted font-14 m-b-0"><b><?php echo $consult->cargo_ubv;?></b></p>

                     

                      </li>
                      </li>
                      <?php }?>
                      
<!--------------------------------------------------------------------------------------------------->
                      <?php  if($consult->tipo==4){?>
                        <ul class="list-inline text-center">
                      <li class="list-inline-item p-r-30"><i data-feather="arrow-up-circle" class="col-blue"></i>
                        <h5 class="m-b-0"><p class="text-muted font-14 m-b-0"><b>Estudiantes que Refieren Telefono celular: <?php echo $consult->cantidad_persona_telefono;?> Estudiantes</b></p></h5><h6>Porcentaje : <?php $porcentaje=$consult->cantidad_persona_telefono / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>%</h6>
                        
                      </li>
                        </ul>
                        <ul class="list-inline text-center">
                      <li class="list-inline-item p-r-30"><i data-feather="arrow-up-circle" class="col-red"></i>
                        <h5 class="m-b-0"> <p class="text-muted font-14 m-b-0"><b>Estudiantes que Refieren Telefono Local(Habitación): <?php echo $consult->cantidad_persona_telefono_local;?> Estudiantes</b></p></h5><h6>Porcentaje : <?php $porcentaje=$consult->cantidad_persona_telefono_local / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>%</h6>
                        
                      </li>
                      </ul>
                      <?php }?>
<!--------------------------------------------------------------------------------------------------->
                      <?php  if($consult->tipo==5){?>
                        <ul class="list-inline text-center">
                      <li class="list-inline-item p-r-30"><i data-feather="users" class="col-blue"></i>
                        <h5 class="m-b-0"><p class="text-muted font-14 m-b-0"><b>Estudiantes Pertenecientes a Etnias: <?php echo $consult->pertenecientes_etnia;?> Estudiantes</b></p></h5><h6>Porcentaje : <?php $porcentaje=$consult->pertenecientes_etnia / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>%</h6>
                      </li>
                      <?php }?>
<!--------------------------------------------------------------------------------------------------->
                      <?php  if($consult->tipo==6){?>
                        <li class="list-inline-item p-r-30"><i data-feather="user" class="col-green"></i>
                        <h5 class="m-b-0"><?php echo $consult->cant;?></h5><h6>Porcentaje : <?php $porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>%</h6>
                        <p class="text-muted font-14 m-b-0"><?php echo $consult->genero;?></p>
                      </li>
                      <?php }?>
<!--------------------------------------------------------------------------------------------------->
                      <?php  if($consult->tipo==7){?>
                        <ul class="list-inline text-center">
                      <li class="list-inline-item p-r-30"><i data-feather="users" class="col-blue"></i>
                        <h5 class="m-b-0"><p class="text-muted font-14 m-b-0"><b>Estudiantes que refieron un Correo Electrónico: <?php echo $consult->estudiantes_refieron_correo;?> Estudiantes</b></p></h5><h6>Porcentaje : <?php $porcentaje=$consult->estudiantes_refieron_correo / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>%</h6>
                      </li>
                      <?php }?>
<!--------------------------------------------------------------------------------------------------->
                      <?php  if($consult->tipo==8){?>
                        <ul class="list-inline text-center">
                      <li class="list-inline-item p-r-30"><i data-feather="flag" class="col-blue"></i>
                        <h5 class="m-b-0"><p class="text-muted font-14 m-b-0"><b>Estudiantes <?php echo $consult->nacionalidad;?> : <?php echo $consult->cant;?> </b></p></h5><h6>Porcentaje : <?php $porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>%</h6>
                      </li>
                      <?php }?>
<!--------------------------------------------------------------------------------------------------->
                      <?php  if($consult->tipo==9){?>

                      
                      <div class="text-small float-right font-weight-bold text-muted"><br><?php $porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>% | CANTIDAD DE ESTUDIANTES: <?php echo $consult->cant;?></div>
                      <div class="font-weight-bold mb-1"><br><h6><?php echo $consult->direccion;?></h6></div>
                      <div class="progress" data-height="4" data-toggle="tooltip" title="<?php $porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>%">
                        <div class="progress-bar bg-success" data-width="<?php $porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>%"></div>
        
                    </div>
                    
                      <?php }?>
<!--------------------------------------------------------------------------------------------------->
<!--------------------------------------------------------------------------------------------------->
<?php  if($consult->tipo==10){?>

                      
<div class="text-small float-right font-weight-bold text-muted"><br><?php $porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>% | CANTIDAD DE ESTUDIANTES: <?php echo $consult->cant;?></div>
<div class="font-weight-bold mb-1"><br><h6><?php echo $consult->estado_civil;?></h6></div>
<div class="progress" data-height="4" data-toggle="tooltip" title="<?php $porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>%">
  <div class="progress-bar bg-success" data-width="<?php $porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>%"></div>

</div>

<?php }?>
<!--------------------------------------------------------------------------------------------------->
                      <?php  if($consult->tipo==11){?>
                        <ul class="list-inline text-center">
                      <li class="list-inline-item p-r-30"><i data-feather="users" class="col-blue"></i>
                        <h5 class="m-b-0"><p class="text-muted font-14 m-b-0"><b>Estudiantes que refieren presentar discapacidad <?php echo $consult->cant;?> Estudiantes</b></p></h5><h6>Porcentaje : <?php $porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>%</h6>
                      </li>
                      <?php }?>
<!--------------------------------------------------------------------------------------------------->
                         <?php  if($consult->tipo==12){?>
                      <div class="text-small float-right font-weight-bold text-muted"><br><?php $porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>% | CANTIDAD DE ESTUDIANTES: <?php echo $consult->cant;?></div>
                      <div class="font-weight-bold mb-1"><br><h6><?php echo $consult->discapacidad;?></h6></div>
                      <div class="progress" data-height="4" data-toggle="tooltip" title="<?php $porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>%">
                        <div class="progress-bar bg-success" data-width="<?php $porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>%"></div>
                    </div>
                    <?php }?>
<!----------------------------------de la lista de estadistica dada, 13 y 14 son iguales----------------------------------------------------------------->
<?php  if($consult->tipo==13){?>
                      <div class="text-small float-right font-weight-bold text-muted"><br><?php $porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>% | CANTIDAD DE ESTUDIANTES: <?php echo $consult->cant;?></div>
                      <div class="font-weight-bold mb-1"><br><h6><?php echo $consult->enfermedad;?></h6></div>
                      <div class="progress" data-height="4" data-toggle="tooltip" title="<?php $porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>%">
                        <div class="progress-bar bg-success" data-width="<?php $porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>%"></div>
                    </div>
                    <?php }?>
<!--------------------------------------------------------------------------------------------------->
                    <?php  if($consult->tipo==15){?>
                      <div class="text-small float-right font-weight-bold text-muted"><br><?php $porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>% | CANTIDAD DE ESTUDIANTES: <?php echo $consult->cant;?></div>
                      <div class="font-weight-bold mb-1"><br><h6><?php echo $consult->enfermedad;?></h6></div>
                      <div class="progress" data-height="4" data-toggle="tooltip" title="<?php $porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>%">
                        <div class="progress-bar bg-success" data-width="<?php $porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>%"></div>
                    </div>
                    <?php }?>
                                
                    <?php  if($consult->tipo==16){?>
                    <li class="list-inline-item p-r-30"><i data-feather="check-square" class="col-green"></i>
                      <h5 class="m-b-0"><?php echo $consult->cant;?></h5><h6>Porcentaje : <?php $porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>%</h6>
                      <p class="text-muted font-14 m-b-0"><b>Estudiantes que trabajan</b></p>
                    </li>
                    <?php }?>

                    <?php  if($consult->tipo==14){?>
                    <li class="list-inline-item p-r-30"><i data-feather="user" class="col-green"></i>
                      <h5 class="m-b-0"><?php echo $consult->mision_sucre;?></h5><h6>Porcentaje : <?php $porcentaje=$consult->mision_sucre / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>%</h6>
                      <p class="text-muted font-14 m-b-0"><b>Estudiantes inscritos en ic pertenecientes a Misión sucre</b></p>
                    </li>
                    <?php }?>

                    <?php  if($consult->tipo==17){?>
                    <li class="list-inline-item p-r-30"><i data-feather="user" class="col-green"></i>
                      <h5 class="m-b-0"><?php echo $consult->cant;?></h5><h6>Porcentaje : <?php $porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>%</h6>
                      <p class="text-muted font-14 m-b-0"><b><?php echo $consult->tipo_institucion;?></b></p>
                    </li>
                    <?php }?>

                    <?php  if($consult->tipo==18){?>
                    <li class="list-inline-item p-r-30"><i data-feather="user" class="col-green"></i>
                      <h5 class="m-b-0"><?php echo $consult->docentes;?></h5></h6>
                      <p class="text-muted font-14 m-b-0"><b>Docentes Inscritos en ic</b></p>
                    </li>
                    <?php }?>
       
 <!--<div class="text-small float-right font-weight-bold"><br>CANTIDAD DE ESTUDIANTES: <?php //echo $consult->cant;?> | <?php// $porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>% </div>
                      <div class="font-weight-bold mb-1"><br><h6><?php //echo $consult->cursos;?></h6></div>
                      <div class="progress" data-height="4" data-toggle="tooltip" title="<?php// $porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>%">
                        <div class="progress-bar bg-success" data-width="<?php //$porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>%"></div> 
                        </div>-->

            <?php  if($consult->tipo==23){?>

                <li class="list-inline-item p-r-30"><i data-feather="users" class="col-green"></i>
                  <h5 class="m-b-0"><?php echo $consult->cant;?></h5><h6>Porcentaje : <?php $porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>%</h6>
                  <p class="text-muted font-14 m-b-0"><b><?php echo $consult->exonerado;?>  /Exoneración : <?php echo $consult->modalidad_exo;?></b></p>
                </li>
                <?php }?>
                <?php  if($consult->tipo==24){?>

            <li class="list-inline-item p-r-30"><i data-feather="users" class="col-green"></i>
              <h5 class="m-b-0"><?php echo $consult->cant;?></h5><h6>Porcentaje : <?php $porcentaje=$consult->cant / $consult->total * 100;   echo substr($porcentaje, 0, 4);?>%</h6>
              <p class="text-muted font-14 m-b-0"><b><?php echo $consult->idioma;?> </b></p>
            </li>
            <?php }?>
                    

                    <?php }?>









                    <!--se coloco aparte, por la cantidad de datos mostrar-->

                    <?php  if($consult->tipo==19){?>

<table class="table table-striped" id="table-1">
  <thead>
    <tr>
      <th>Sección</th>
      <th>Cantidad de estudiantes</th>
    </tr>
  </thead>
  <tbody>
  <?php 
foreach($this->consul as $row){
$consult=new Confucio();
$consult=$row;
?>

 <tr>
    <td><?php echo $consult->cursos; ?></td>
    <td><?php echo $consult->cant; ?></td>
</tr>
<?php }?>
  </tbody>
</table>

<?php }?>

                    <?php  if($consult->tipo==20){?>

                      <table class="table table-striped" id="table-1">
                        <thead>
                          <tr>
                            
                            <th>Periodo</th>
                            <th>Cursos</th>
                            <th>Cantidad</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php 
                      foreach($this->consul as $row){
                      $consult=new Confucio();
                      $consult=$row;
                      ?>

                       <tr>
                          <td><?php echo $consult->periodo; ?></td>
                          <td><?php echo $consult->cursos; ?></td>
                          <td><?php echo $consult->cant; ?></td>
                        
                      </tr>
                  <?php }?>
                        </tbody>
                      </table>
 

                    <?php }?>
                      <!--se coloco aparte, por la cantidad de datos mostrar-->
                      <?php  if($consult->tipo==21){?>

<table class="table table-striped" id="table-1">
  <thead>
    <tr>
      
      <th>Periodo</th>
      <th>Idioma</th>
      <th>Nivel</th>
      <th>Cantidad</th>
    </tr>
  </thead>
  <tbody>
  <?php 
foreach($this->consul as $row){
$consult=new Confucio();
$consult=$row;
?>

 <tr>
    <td><?php echo $consult->periodo; ?></td>
    <td><?php echo $consult->cursos; ?></td>
    <td><?php echo $consult->nivel; ?></td>
    <td><?php echo $consult->cant; ?></td>
  
</tr>
<?php }?>
  </tbody>
</table>


<?php }?>
<?php  if($consult->tipo==22){?>

<table class="table table-striped" id="table-1">
  <thead>
    <tr>
      
      <th>Periodo</th>
      <th>Idioma</th>
      <th>Nivel</th>
      <th>Cantidad</th>
    </tr>
  </thead>
  <tbody>
  <?php 
foreach($this->consul as $row){
$consult=new Confucio();
$consult=$row;
?>

 <tr>
    <td><?php echo $consult->periodo; ?></td>
    <td><?php echo $consult->cursos; ?></td>
    <td><?php echo $consult->nivel; ?></td>
    <td><?php echo $consult->cant; ?></td>
  
</tr>
<?php }?>
  </tbody>
</table>


<?php }?>
<?php  if($consult->tipo==25){?>

<table class="table table-striped" id="table-1">
  <thead>
    <tr>
      
      <th>Periodo</th>
      <th>Nivel</th>
      <th>Cantidad de estudiantes</th>
    </tr>
  </thead>
  <tbody>
  <?php 
foreach($this->consul as $row){
$consult=new Confucio();
$consult=$row;
?>

 <tr>
    <td><?php echo $consult->periodo; ?></td>
    <td><?php echo $consult->nivel; ?></td>
    <td><?php echo $consult->cant; ?></td>
  
</tr>
<?php }?>
  </tbody>
</table>


<?php }?>

<?php  if($consult->tipo==26){?>

<table class="table table-striped" id="table-1">
  <thead>
    <tr>
      <th>Sección</th>
      <th>Cantidad de aspirantes</th>
    </tr>
  </thead>
  <tbody>
  <?php 
foreach($this->consul as $row){
$consult=new Confucio();
$consult=$row;
?>

 <tr>
    <td><?php echo $consult->cursos; ?></td>
    <td><?php echo $consult->cant; ?></td>
</tr>
<?php }?>
  </tbody>
</table>


<?php }?>





                      <hr>

                      <?php  if($consult->tipo==18 or $consult->tipo==0 or $consult->tipo=='null'){?>

                      <?php }elseif($consult->tipo==21 ){ ?>
                        <h5 class="m-b-0"><?php echo "Total de Estudiantes de Idioma Aprobados de IC : ".$consult->total_aprobado."";?></h5>
                        <?php }elseif($consult->tipo==22 ){ ?>
                        <h5 class="m-b-0"><?php echo "Total de Estudiantes de Otros Cursos Aprobados de IC : ".$consult->total_aprobado."";?></h5>
                        <?php }elseif($consult->tipo==26 ){ ?>
                        <h5 class="m-b-0"><?php echo "Total de Aspirantes : ".$consult->total."";?></h5>
                      <?php }else{ ?>
                        <h5 class="m-b-0"><?php echo "Total de Estudiantes de IC : ".$consult->total." = 100%";?></h5>
                        <?php }?>
                    </ul>
                   
                  </div>
                </div>
              </div>
                      
            

             
            

      
    
    </div>
  </div>


    <!--End Main Content-->
    <?php require 'views/footer.php'; ?>
  <!-- General JS Scripts -->

  
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/widget-chart.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>




  


  <!-- General JS Scripts -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <!-- JS Libraies -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-ui/jquery-ui.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/datatables.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno//assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/js/select2.full.min.js"></script>
  <script>
       $(document).ready(function(){

    $(".select_2").select2({
                                placeholder: "Seleccione",
                                width: "100%",
                                dropdownAutoWidth: true
                            });

                          });
      </script>

</body>

</html>
