<!DOCTYPE html>
<html lang="en">

<!-- form-wizard.html  21 Nov 2019 03:55:16 GMT -->
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Confucio - Oferta Académica</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/css/select2.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">

  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico'/>
</head>

<body>
  <?php require 'views/header.php'; ?>  

  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <div class="section-body">
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="card-header">
                <h4>Editar fecha Oferta Académica</h4>
                <div class="card-header-action">
                  <a href="<?php echo constant ('URL')?>oferta_academica/ofertas_activas" class="btn btn-warning">Volver</a>
                </div>
              </div>
              <div class="card-body">
                <form action="<?php echo constant('URL'); ?>oferta_academica/UpdateDate" id="wizard_with_validation"  novalidate="" method="POST">
                  <h3>Datos de la activación</h3>
                  <?php echo $this->mensaje; ?>
                  <fieldset>
                  <div class="row"><!--Datos de la oferta académica-->
                      <div class="col-lg-12 col-xl-4">
                        <div class="table-responsive">
                          <table class="table m-0">
                            <tbody>
                              <tr>
                                <th scope="row">Sección:</th>
                                <td><?php echo $this->detalles->seccion; ?></td>
                              </tr>
                              <tr>
                                <th scope="row">Periodo Acad.:</th>
                                <td><?php echo $this->detalles->periodo; ?></td>
                              </tr>
                              <tr>
                                <th scope="row">Turno:</th>
                                <td><?php echo $this->detalles->turno; ?></td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div class="col-lg-12 col-xl-4">
                        <div class="table-responsive">
                          <table class="table m-0">
                            <tbody>
                              <tr>
                                <th scope="row">Actividad:</th>
                                <td><?php echo $this->detalles->actividad; ?></td>
                              </tr>
                              <tr>
                                <th scope="row">Docente:</th>
                                <?php if ($this->detalles->docente == 1) { ?>
                                  <td>No Aplica</td>
                                  <?php }else {?>
                                <td><?php echo $this->detalles->primer_nombre . " " . $this->detalles->primer_apellido ; ?></td>
                                <?php } ?>
                              </tr>
                              
                              <tr>
                                <th scope="row">Horario:</th>
                                <td><?php echo date("g:i a", strtotime($this->detalles->hora_inicio)) . " - " . date("g:i a", strtotime($this->detalles->hora_fin)); ?></td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div class="col-lg-12 col-xl-4">
                        <div class="table-responsive">
                          <table class="table m-0">
                            <tbody>
                              <tr>
                                <th scope="row">Nivel:</th>
                                <td><?php echo $this->detalles->nivel; ?></td>
                              </tr>
                              <tr>
                                <th scope="row">Modalidad:</th>
                                <td><?php echo $this->detalles->modalidad; ?></td>
                              </tr>
                              <tr>
                                <th scope="row">Días:</th>
                                <td>
                                  <?php   
                                  $i=1;
                                  while($i<=count($this->datos_h)){
                                   echo $this->dia[$i]['dia'.$i];?>
                                   <?php $i++;
                                 } ?> 
                               </td>
                             </tr>
                           </tbody>
                         </table>
                       </div>
                     </div>
                   </div><!--Fin datos de la oferta académica-->
                    <div class="row">
                      <input type="hidden" name="id_activar_oferta" value="<?php echo $this->oferta->id_activar_oferta; ?>">
                      <div class="col-lg-6">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Fecha de inicio<span style="color: red;">&nbsp;*</span></label>
                            <input type="date" name="fecha_inicio" class="form-control" value="<?php echo $this->oferta->fecha_inicio; ?>" required>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Fecha final<span style="color: red;">&nbsp;*</span></label>
                            <input type="date" name="fecha_fin" class="form-control" value="<?php echo $this->oferta->fecha_fin; ?>" required>
                          </div>
                        </div>
                      </div>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>

    <?php require 'views/footer.php'; ?>

    <!-- General JS Scripts -->
    <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-validation/dist/jquery.validate.min.js"></script>
    <!-- JS Libraies -->
    <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-steps/jquery.steps.min.js"></script>
    <!-- Page Specific JS File -->
    <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/form-wizard.js"></script>
    <!-- Template JS File -->
    <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/scripts.js"></script>
    <!-- Custom JS File -->
    <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>
  </body>

  <!-- form-wizard.html  21 Nov 2019 03:55:20 GMT -->
  </html>

