<!DOCTYPE html>
<html lang="en">


<!-- form-wizard.html  21 Nov 2019 03:55:16 GMT -->
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Confucio-Usuario</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/css/select2.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">

  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico' />
</head>

<body>

<?php require 'views/header.php'; ?>  
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-body">

          
            <div class="row clearfix">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Editar Usuario</h4>
                    <div class="card-header-action">
                    <a href="<?php echo constant ('URL')?>usuarios/list_update" class="btn btn-primary">Volver</a>
                  </div>
                  </div>
                  <div class="card-body">
                    <form action="<?php echo constant ('URL');?>perfil/FormUpdate" id="wizard_with_validation"  novalidate="" method="POST">
                      <h3>Datos Usuario</h3>
                      <fieldset>
                      <div class="row">
                      <input type="hidden" name="id_persona" value="<?php echo $this->datos->id_persona; ?>">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <div class="form-line">
                            <label class="form-label">Nombre de Usuario*</label>
                            <input type="text" name="usuario" value="<?php echo $this->datos->usuario; ?>" class="form-control" required>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-6">
                      <label class="form-label">Perfil*</label>
                     <div class="form-group">
                      <select name="perfil" class="form-control select2" >
                      <option value="">Seleccione</option>
                      <?php 
                      foreach($this->perfiles as $row){
                      $perfil=new Perfil();
                      $perfil=$row;?> 
                    <option value="<?php echo $perfil->id_perfil;?>" <?php if($perfil->id_perfil==$this->datos->id_perfil){ echo "selected=selected"; }?>><?php echo $perfil->descripcion;?></option>             
                     <?php }?>
                      </select>
                    </div>
                    </div>

                    <div class="col-lg-6">
                      <label>Roles de Usuario</label>
                      <div class="form-group">
                       <select id="rol" name="rol[]" class="form-control select2" multiple>
                       <?php                              
                    foreach($this->roles as $row){
                        $rol=new Confucio();
                        $rol=$row;?> 
                    <option value="<?php echo $rol->id;?>" <?php 
                    $i=1;
                    while($i<=count($this->datos_u)){
                                                            
                        if($rol->id==$this->rol[$i]['id_rol'.$i]){ 
                            echo "selected='selected'"; 
                        }
                        $i++;
                    } ?>><?php echo $rol->descripcion;?></option>
                   <?php } ?> 
                      </select>
                        </div> 
                      </div>

                      <div class="col-lg-6">
                      <label for="estatus" class="block">Estatus de Usuario <span style="color: red;">*</span></label> 
                      <div class="form-group">
                    <select id="estatus" name="estatus" class="form-control select2">
                        <option value="">Seleccione</option>
                        <option value="Activo" <?php if("Activo"==$this->datos->estatus){ echo "selected=selected"; }?>>Activo</option>
                        <option value="Inactivo" <?php if("Inactivo"==$this->datos->estatus){ echo "selected=selected"; }?>>Inactivo</option>
                    </select>
                     </div> 
                   </div> 
                     

                      <div class="col-lg-6">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Clave*</label>
                            <input type="password" class="form-control" name="clave" value="<?php echo $this->datos->clave; ?>" id="password" required>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-6">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Confirmar Clave*</label>
                            <input type="password" class="form-control" name="confirm" value="<?php echo $this->datos->clave; ?>" required>
                          </div>
                        </div>
                      </div> 
                    


                      </div> <!--End CLASS ROW--->
                        
                    </fieldset>
                    </form>
                  </div>
                </div>
              </div>
            </div>
       
        </section>

      </div>

      <?php require 'views/footer.php'; ?>

        
  <!-- General JS Scripts -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/js/select2.full.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-validation/dist/jquery.validate.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/advance-elements/select2-custom.js"></script>
  <!-- JS Libraies -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-steps/jquery.steps.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/form-wizard.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>


</body>


</html>