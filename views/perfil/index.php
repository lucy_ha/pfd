<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Confucio- Mi Perfil</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/bootstrap-social/bootstrap-social.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/summernote/summernote-bs4.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico' />
</head>

<body>

<?php require 'views/header.php'; ?>    
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-body">
              <br>
              <br>
            <div class="row mt-sm-4">
              <div class="col-12 col-md-12 col-lg-4">
                <div class="card author-box">
                  <div class="card-body">
                    <div class="author-box-center">
                                           <div class="clearfix"></div>
                      <div class="author-box-name">
                        <b><?php echo $this->datos->primer_nombre; ?>&nbsp; <?php echo $this->datos->primer_apellido; ?></b>
                      </div>
                      <div class="author-box-job"><?php echo $this->datos->perfil; ?></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-12 col-md-12 col-lg-8">
                <div class="card">
                  <div class="padding-20">
                    <ul class="nav nav-tabs" id="myTab2" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="home-tab2" data-toggle="tab" href="#about" role="tab"
                          aria-selected="true">Información Personal</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="profile-tab2" data-toggle="tab" href="#settings" role="tab"
                          aria-selected="false">Información de usuario</a>
                      </li>
                    </ul>
                    <div class="tab-content tab-bordered" id="myTab3Content">
                      <div class="tab-pane fade show active" id="about" role="tabpanel" aria-labelledby="home-tab2">
                        <div class="row">
                        <div class="col-md-3 col-6 b-r">
                            <strong>Nombres completo</strong>
                            <br>
                            <p class="text-muted"><?php echo $this->datos->primer_nombre; ?>&nbsp;<?php echo $this->datos->segundo_nombre; ?>&nbsp;</p>
                          </div>
                          <div class="col-md-5 col-4 b-r">
                            <strong>Apellidos</strong>
                            <br>
                            <p class="text-muted"><?php echo $this->datos->primer_apellido; ?>&nbsp;<?php echo $this->datos->segundo_apellido; ?></p>
                          </div>
                          <div class="col-md-3 col-6">
                            <strong>Genero</strong>
                            <br>
                            <p class="text-muted"><?php echo $this->datos->genero; ?></p>
                          </div>
                        </div>
                        <div class="row">
                        <div class="col-md-3 col-6 b-r">
                            <strong>Tipo de Identidad</strong>
                            <br>
                            <p class="text-muted"><?php echo $this->datos->descripcion; ?></p></p>
                          </div>
                          <div class="col-md-3 col-6 b-r">
                            <strong>Cedula de Identidad</strong>
                            <br>
                            <p class="text-muted"><?php echo $this->datos->identificacion; ?></p>
                          </div>
                          <div class="col-md-4 col-6 b-r">
                            <strong>Correo</strong>
                            <br>
                            <p class="text-muted"><?php echo $this->datos->correo; ?></p>
                          </div>
                          <div class="col-md-8 col-6 b-r">
                            <strong>Telefono</strong>
                            <br>
                            <p class="text-muted"><?php echo $this->datos->telefono; ?></p>
                          </div>

                          <div class="col-md-4 col-6">
                            <br>
                            <?php if($_SESSION['Editar']==true){?>
                            <p><a href="<?php echo constant ('URL') . "perfil/update_date/" .  $this->datos->id_persona;?>" class="btn btn-outline-success"><i class="fas fa-user-edit"></i> Editar Datos Usuario</a></p>
                            <?php } ?>
                          </div>

                        </div>
                      </div>
                      <div class="tab-pane fade" id="settings" role="tabpanel" aria-labelledby="profile-tab2">
                      <div class="row">
                          <div class="col-md-4 col-6 b-r">
                            <strong>Usuario</strong>
                            <br>
                            <p class="text-muted"><?php echo $this->datos->usuario; ?></p>
                          </div>
                          <div class="col-md-4 col-6 b-r">
                            <strong>Perfil</strong>
                            <br>
                            <p class="text-muted"><?php echo $this->datos->perfil; ?></p>
                          </div>
                          <div class="col-md-4 col-6 b-r">
                          <br>
                          <?php if($_SESSION['Editar']==true){?>
                          <a href="<?php echo constant ('URL') . "usuarios/update/" . $this->datos->id_persona;?>" class="btn btn-outline-success"><i class="fas fa-user-cog"></i> Configurar Usuario</a>
                          <?php }?> 
                        </div>
                         
                        </div>
                        <div class="row">
                          <div class="col-md-4 col-6 b-r">
                            <strong>Roles de Usuario</strong>
                            <br>
                            <p class="text-muted">    <?php 
                            $i=1;
                            while($i<=count($this->datos_u)){
                        ?>                                                                                                            
                                <?php echo $this->rol[$i]['rol'.$i]; ?>                                                                   
                                <?php 
                                $i++;
                             }
                         ?>

                         </p>
                          </div>
                          <div class="col-md-4 col-6 b-r">
                            <strong>Estatus</strong>
                            <br>
                            <p class="text-muted"><?php echo $this->datos->estatus; ?></p>
                          </div>
                        </div>
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div> <!--END main-->

      <?php require 'views/footer.php'; ?>

  <!-- General JS Scripts -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <!-- JS Libraies -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/summernote/summernote-bs4.js"></script>
  <!-- Page Specific JS File -->
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>
</body>


</html>