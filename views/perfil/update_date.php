<!DOCTYPE html>
<html lang="en">


<!-- form-wizard.html  21 Nov 2019 03:55:16 GMT -->
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Confucio-Usuario</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/css/select2.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">

  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico' />
</head>

<body>

<?php require 'views/header.php'; ?>  
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-body">

          
            <div class="row clearfix">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Registrar Usuario</h4>
                    <div class="card-header-action">
                    <a href="<?php echo constant ('URL')?>usuarios/list_update" class="btn btn-primary"><i class="fas fa-less-than"></i>  Volver</a>
                  </div>
                  </div>
                  <div class="card-body">
                    <form action="<?php echo constant('URL'); ?>perfil/FormUpdate_Date" id="wizard_with_validation"  novalidate="" method="POST">
                      <h3>Datos Personales</h3>
                      <fieldset>
                      <input type="hidden" name="id_persona" value="<?php echo $this->datos->id_persona; ?>">
                        <div class="row">
                      <div class="col-lg-5">
                        <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Primer Nombre*</span>
                        </div>
                        <input id="" name="primer_nombre" type="text" value="<?php echo $this->datos->primer_nombre; ?>" class="required form-control " placeholder="Ingrese su Primer Nombre">
                      </div>
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Primer Apellido*</span>
                        </div>
                        <input id="" name="primer_apellido" type="text" value="<?php echo $this->datos->primer_apellido; ?>" class="required form-control " placeholder="Ingrese su Primer Apellido">
                      </div>
                      </div>

                  <div class="col-lg-6">
                    <div class="form-group form-float">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Segundo Nombre (Opcional)</span>
                        </div>
                        <input id="" name="segundo_nombre" type="text" class="form-control " value="<?php echo $this->datos->segundo_nombre; ?>" placeholder="Ingrese su Segundo Nombre">
                      </div>
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Segundo Apellido (Opcional)</span>
                        </div>
                        <input id="" name="segundo_apellido" type="text" class="form-control" value="<?php echo $this->datos->segundo_apellido; ?>" placeholder="Ingrese su Segundo Apellido">
                      </div>
                   </div>
                </div>

                <div class="row">
                   <div class="col-lg-5">
                   <br>
                   <label class="form-label">Tipo de Identidad*</label>
                    <div class="form-group">
                      <select name="id_tipo_documento_identidad" class="form-control select2" >
                      <option value="">Seleccione</option>
                      <?php 
                      foreach($this->tipos as $row){
                      $tipo=new Confucio();
                      $tipo=$row;?> 
                    <option value="<?php echo $tipo->id;?>" <?php if($tipo->id==$this->datos->id_tipo_documento_identidad){ echo "selected=selected"; }?>><?php echo $tipo->descripcion;?></option>             
                  <?php }?>
                      </select>
                    </div>
                      </div>

                      <div class="col-lg-5">
                        <br>
                        <div class="form-group">
                          <div class="form-line">
                            <label class="form-label">Documento de Identidad o Pasaporte*</label>
                            <input type="text" name="identificacion" value="<?php echo $this->datos->identificacion; ?>" class="form-control" required>
                          </div>
                        </div>
                      </div>
                </div>

                <div class="row">
                    <div class="col-lg-5">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Correo Electronico*</label>
                            <input type="email" name="correo" value="<?php echo $this->datos->correo; ?>" class="form-control" required>
                          </div>
                        </div>
                    </div>

                    <div class="col-lg-5">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Telefono*</label>
                            <input id="telefono" name="telefono" type="text" value="<?php echo $this->datos->telefono; ?>" class="required form-control us_telephone" data-mask="(0999) 999-9999" placeholder="Ingrese su Nro. de Teléfono">
                          </div>
                        </div>
                    </div>
                </div>


                <div class="col-lg-6">
                    <div class="form-group form-float">
                       <div class="form-line">
                        <label class="form-label">Genero*</label>
                      <div class="custom-control custom-radio custom-control-inline" >
                        <input type="radio" id="customRadio1" name="genero" value="Femenino" class="custom-control-input" <?php if($this->datos->genero=="Femenino"){echo 'checked=""';}?>>
                        <label class="custom-control-label" for="customRadio1">Femenino</label>
                    </div>
                       <div class="custom-control custom-radio custom-control-inline">
                         <input type="radio" id="customRadio4" name="genero" value="Masculino" class="custom-control-input" <?php if($this->datos->genero=="Masculino"){echo 'checked=""';}?>>
                         <label class="custom-control-label" for="customRadio4">Masculino</label>
                       </div>
                          </div>
                      </div>
                 </div>



                    </div>
                      </fieldset>
                    </form>
                  </div>
                </div>
              </div>
            </div>
       
        </section>

      </div>

      <?php require 'views/footer.php'; ?>

        
  <!-- General JS Scripts -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/js/select2.full.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-validation/dist/jquery.validate.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/advance-elements/select2-custom.js"></script>
  <!-- JS Libraies -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-steps/jquery.steps.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/form-wizard.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>


</body>


</html>