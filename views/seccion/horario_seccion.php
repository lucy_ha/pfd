<!DOCTYPE html>
<html lang="en">

<!-- form-wizard.html  21 Nov 2019 03:55:16 GMT -->
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Confucio - Sección</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico' />
</head>

<body>

  <?php require 'views/header.php'; ?>  

  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <div class="section-body">
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="card-header">
                <h4>Información de la sección: <?php echo $this->seccion->descripcion; ?> </h4>
                <div class="card-header-action">
                  <a href="<?php echo constant ('URL')?>seccion" class="btn btn-warning">Volver</a>
                </div>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-lg-12 col-xl-4">
                    <div class="table-responsive">
                      <table class="table m-0">
                        <tbody>
                          <tr>
                            <th scope="row">Actividad:</th>
                            <td><?php echo $this->seccion->actividad; ?></td>
                          </tr>
                          <tr>
                            <th scope="row">Docente:</th>
                            <?php if ($this->seccion->docente == 1) { ?>
                              <td>Por definir</td>
                            <?php }else {?>
                              <td><?php echo $this->seccion->primer_nombre.' '.$this->seccion->primer_apellido; ?></td>
                            <?php } ?>
                          </tr>
                          <tr>
                            <th scope="row">Turno:</th>
                            <td><?php echo $this->seccion->turno; ?></td>
                          </tr>
                          <tr>
                            <th scope="row">Estatus:</th>
                            <?php if ($this->seccion->estatus == "Activo") { ?>
                              <td><div class="badge badge-pill badge-success">Activo</div></td>
                            <?php } ?>
                            <?php if ($this->seccion->estatus == "Inactivo") { ?>
                              <td><div class="badge badge-pill badge-danger">Inactivo</div></td>
                            <?php } ?>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="col-lg-12 col-xl-4">
                    <div class="table-responsive">
                      <table class="table m-0">
                        <tbody>
                          <tr>
                            <th scope="row">Nivel:</th>
                            <td><?php echo $this->seccion->nivel; ?></td>
                          </tr>
                          <tr>
                            <th scope="row">Modalidad:</th>
                            <td><?php echo $this->seccion->modalidad; ?></td>
                          </tr>
                          <tr>
                            <th scope="row">Horario:</th>
                            <td><?php echo date("g:i a", strtotime($this->seccion->hora_inicio)).' - '.date("g:i a", strtotime($this->seccion->hora_fin)); ?></td>
                          </tr>
                          <tr>
                            <th scope="row">Estudiantes:</th>
                            <td><a href="<?php echo constant('URL') . 'seccion/lista_estudiantes/' . $this->seccion->id_seccion; ?>" class="btn btn-info" title="Ver estudiantes">
                              <i class="fas fa-users"> </i>
                            </a></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="col-lg-12 col-xl-4">
                    <div class="table-responsive">
                      <table class="table m-0">
                        <tbody>
                          <tr>
                            <th scope="row">Periodo Acad.:</th>
                            <td><?php echo $this->seccion->periodo; ?></td>
                          </tr>
                          <tr>
                            <th scope="row">Limite de cupos:</th>
                            <td><?php echo $this->seccion->cupos; ?></td>
                          </tr>
                          <tr>
                            <th scope="row">Días:</th>
                            <td>
                              <?php   
                              $i=1;
                              while($i<=count($this->datos_h)){
                               echo $this->dia[$i]['dia'.$i];?>
                               <?php $i++;
                             } ?> 
                           </td>
                         </tr>
                       </tbody>
                     </table>
                   </div>
                 </div>
               </div>
             </div>
             <div class="card-footer text-right">
              <a href="<?php echo constant('URL') . 'seccion/editar/' . $this->seccion->id_seccion; ?>" class="btn btn-primary" title="Editar">
                <i class="fas fa-edit"> Editar</i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <?php require 'views/footer.php'; ?>

  <!-- General JS Scripts -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-validation/dist/jquery.validate.min.js"></script>
  <!-- JS Libraies -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-steps/jquery.steps.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/form-wizard.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>
</body>


<!-- form-wizard.html  21 Nov 2019 03:55:20 GMT -->
</html>

