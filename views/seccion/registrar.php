<!DOCTYPE html>
<html lang="en">

<!-- form-wizard.html  21 Nov 2019 03:55:16 GMT -->
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Confucio - Sección</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/css/select2.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico'/>
</head>

<body>

  <?php require 'views/header.php'; ?>  
  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <div class="section-body">
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="card-header">
                <h4>Registrar Sección</h4>
                <div class="card-header-action">
                  <a href="<?php echo constant ('URL')?>seccion" class="btn btn-warning">Volver</a>
                </div>
              </div>
              <div class="card-body">
                <form action="<?php echo constant('URL'); ?>seccion/RegisterSeccion" id="wizard_with_validation"  novalidate="" method="POST">
                  <h3>Datos de la Sección</h3>
                  <fieldset>
                    <div class="row">

                      <div class="col-lg-4">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Nombre de la sección<span style="color: red;">&nbsp;*</span></label>
                            <input type="text" class="form-control required" name="descripcion" required placeholder="Ingrese el nombre de la sección">
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-4">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Actividad<span style="color: red;">&nbsp;*</span></label>
                            <select name="id_actividad" class="form-control select2 required">
                              <option value="">Seleccione</option> 
                              <?php 
                              foreach($this->actividades as $row){
                                $actividad=new Confucio();
                                $actividad=$row;?> 
                                <option value="<?php echo $actividad->id;?>"> <?php echo $actividad->descripcion;?> </option>             
                              <?php } ?>
                            </select> 
                          </div> 
                        </div>
                      </div>

                      <div class="col-lg-4">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Nivel<span style="color: red;">&nbsp;*</span></label>
                            <select name="id_nivel" class="form-control select2 required">
                              <option value="">Seleccione</option> 
                              <?php 
                              foreach($this->niveles as $row){
                                $nivel=new Confucio();
                                $nivel=$row;?> 
                                <option value="<?php echo $nivel->id;?>"> <?php echo $nivel->descripcion;?> </option>             
                              <?php } ?>
                            </select> 
                          </div> 
                        </div>
                      </div>

                      <div class="col-lg-4">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Periodo Académico<span style="color: red;">&nbsp;*</span></label>
                            <select name="id_periodo" class="form-control select2 required">
                              <option value="">Seleccione</option> 
                              <?php 
                              foreach($this->periodos as $row){
                                $periodo=new Confucio();
                                $periodo=$row;?> 
                                <option value="<?php echo $periodo->id;?>"> <?php echo $periodo->descripcion;?> </option>             
                              <?php } ?>
                            </select> 
                          </div> 
                        </div>
                      </div>

                      <div class="col-lg-4">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Docente<span style="color: red;">&nbsp;*</span></label>
                            <select name="id_docente" class="form-control select2 required">
                              <option value="">Seleccione</option> 
                              <option value="1">No Aplica</option> 
                              <?php 
                              foreach($this->docentes as $row){
                                $docente=new Confucio();
                                $docente=$row; ?> 
                                <option value="<?php echo $docente->id_docente;?>"><?php echo $docente->primer_nombre . " " . $docente->primer_apellido;?></option>             
                              <?php } ?>
                            </select> 
                          </div> 
                        </div>
                      </div>

                      <div class="col-lg-4">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Número de cupos<span style="color: red;">&nbsp;*</span></label>
                            <input type="number" class="form-control required" name="cupos" required placeholder="Ingrese el número de cupos" min="0">
                          </div>
                        </div>
                      </div>
                      
                      <div class="col-lg-4">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Turno<span style="color: red;">&nbsp;*</span></label>
                            <select name="id_turno" class="form-control select2" >
                              <option value="">Seleccione</option>
                              <?php 
                              foreach($this->turnos as $row){
                                $turno=new Confucio();
                                $turno=$row;?> 
                                <option value="<?php echo $turno->id;?>"><?php echo $turno->descripcion;?></option>             
                              <?php }?>
                            </select>
                          </div>
                        </div>
                      </div>
                      
                      <div class="col-lg-4">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Modalidad<span style="color: red;">&nbsp;*</span></label>
                            <select name="id_modalidad" class="form-control select2 required">
                              <option value="">Seleccione</option> 
                              <?php 
                              foreach($this->modalidades as $row){
                                $modalidad=new Confucio();
                                $modalidad=$row;?> 
                                <option value="<?php echo $modalidad->id;?>"> <?php echo $modalidad->descripcion;?> </option>             
                              <?php } ?>
                            </select> 
                          </div> 
                        </div>
                      </div>

                      <div class="col-lg-4">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Días<span style="color: red;">&nbsp;*</span></label>
                            <select name="dia[]" class="form-control select2" multiple>
                              <?php 
                              foreach($this->dias as $row){
                                $dia=new Confucio();
                                $dia=$row;?> 
                                <option value="<?php echo $dia->id;?>"><?php echo $dia->descripcion;?></option>             
                              <?php } ?>
                            </select> 
                          </div> 
                        </div>
                      </div>

                    
                  </fieldset>
                  <h3>Datos del Horario</h3>
                  <fieldset>
                    <div class="row">

                      <div class="col-lg-6">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Hora Inicio<span style="color: red;">&nbsp;*</span></label>
                            <input type="time" name="hora_inicio" class="form-control" required>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-6">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Hora Fin<span style="color: red;">&nbsp;*</span></label>
                            <input type="time" name="hora_fin" class="form-control" required>
                          </div>
                        </div>
                      </div>

                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <?php require 'views/footer.php'; ?>

  <!-- General JS Scripts -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-steps/jquery.steps.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/js/select2.full.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-validation/dist/jquery.validate.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/form-wizard.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>

  <script>
   $(document).ready(function(){

    $(".select_2").select2({
      placeholder: "Seleccione",
      width: "100%",
      dropdownAutoWidth: true
    });

  });
</script>


</body>
<!-- form-wizard.html  21 Nov 2019 03:55:20 GMT -->
</html>