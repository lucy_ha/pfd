<!DOCTYPE html>
<html lang="en">

<!-- form-wizard.html  21 Nov 2019 03:55:16 GMT -->
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Confucio - Sección</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/css/select2.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico'/>
</head>

<body>

  <?php require 'views/header.php'; ?>  
  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <div class="section-body">
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="card-header">
                <h4>Editar Sección</h4>
                <div class="card-header-action">
                  <a href="<?php echo constant ('URL')?>seccion" class="btn btn-warning">Volver</a>
                </div>
              </div>
              <div class="card-body">
                <form action="<?php echo constant('URL'); ?>seccion/UpdateSeccion" id="wizard_with_validation"  novalidate="" method="POST">
                  <h3>Datos de la sección</h3>
                  <fieldset>
                    <div class="row">
                     <input type="hidden" name="id_seccion" value="<?php echo $this->seccion->id_seccion; ?>">
                     <input type="hidden" name="id_horario" value="<?php echo $this->seccion->id_horario; ?>">

                     <div class="col-lg-4">
                      <div class="form-group form-float">
                        <div class="form-line">
                          <label class="form-label">Nombre de la sección<span style="color: red;">&nbsp;*</span></label>
                          <input type="text" class="form-control required" name="descripcion" value="<?php echo $this->seccion->descripcion; ?>" required placeholder="Ingrese el nombre de la sección">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-4">
                      <div class="form-group form-float">
                        <div class="form-line">
                          <label class="form-label">Actividad<span style="color: red;">&nbsp;*</span></label>
                          <select name="id_actividad" class="form-control select2 required">
                            <option value="">Seleccione</option> 
                            <?php 
                            foreach($this->actividades as $row){
                              $actividad=new Confucio();
                              $actividad=$row;?> 
                              <option value="<?php echo $actividad->id;?>" <?php if($actividad->id==$this->seccion->actividad){ echo "selected=selected"; }?>><?php echo $actividad->descripcion;?></option>               
                            <?php } ?>
                          </select> 
                        </div> 
                      </div>
                    </div>

                    <div class="col-lg-4">
                      <div class="form-group form-float">
                        <div class="form-line">
                          <label class="form-label">Nivel<span style="color: red;">&nbsp;*</span></label>
                          <select name="id_nivel" class="form-control select2">
                            <option value="">Seleccione</option> 
                            <?php 
                            foreach($this->niveles as $row){
                              $nivel=new Confucio();
                              $nivel=$row;?> 
                              <option value="<?php echo $nivel->id;?>" <?php if($nivel->id==$this->seccion->nivel){ echo "selected=selected"; }?>><?php echo $nivel->descripcion;?></option>             
                            <?php } ?>
                          </select> 
                        </div> 
                      </div>
                    </div>

                    <div class="col-lg-4">
                      <div class="form-group form-float">
                        <div class="form-line">
                          <label class="form-label">Periodo Académico<span style="color: red;">&nbsp;*</span></label>
                          <select name="id_periodo" class="form-control select2 required">
                            <option value="">Seleccione</option> 
                            <?php 
                            foreach($this->periodos as $row){
                              $periodo=new Confucio();
                              $periodo=$row;?> 
                              <option value="<?php echo $periodo->id;?>" <?php if($periodo->id==$this->seccion->periodo){ echo "selected=selected"; }?>><?php echo $periodo->descripcion;?></option>            
                            <?php } ?>
                          </select> 
                        </div> 
                      </div>
                    </div>

                    <div class="col-lg-4">
                      <div class="form-group form-float">
                        <div class="form-line">
                          <label class="form-label">Docente<span style="color: red;">&nbsp;*</span></label>
                          <select name="id_docente" class="form-control select2">
                            <option value="">Seleccione</option> 
                            <option value="1" <?php if($this->seccion->docente==1){ echo "selected=selected"; }?> >No Aplica</option> 
                            <?php 
                            foreach($this->docentes as $row){
                              $docente=new Confucio();
                              $docente=$row; ?> 
                              <option value="<?php echo $docente->id_docente;?>" <?php if($docente->id_docente==$this->seccion->docente){ echo "selected=selected"; }?>><?php echo $docente->primer_nombre . " " . $docente->primer_apellido;?></option>         
                            <?php } ?>
                          </select> 
                        </div> 
                      </div>
                    </div>

                    <div class="col-lg-4">
                      <div class="form-group form-float">
                        <div class="form-line">
                          <label class="form-label">Número de limite de cupos<span style="color: red;">&nbsp;*</span></label>
                          <input type="number" name="cupos" class="form-control" value="<?php echo $this->seccion->cupos; ?>" required placeholder="Ingrese el número de cupos" min="0">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-4">
                      <div class="form-group form-float">
                        <div class="form-line">
                          <label class="form-label">Modalidad<span style="color: red;">&nbsp;*</span></label>
                          <select name="id_modalidad" class="form-control select2">
                            <option value="">Seleccione</option> 
                            <?php 
                            foreach($this->modalidades as $row){
                              $modalidad=new Confucio();
                              $modalidad=$row;?> 
                              <option value="<?php echo $modalidad->id;?>" <?php if($modalidad->id==$this->seccion->modalidad){ echo "selected=selected"; }?>><?php echo $modalidad->descripcion;?></option>             
                            <?php } ?>
                          </select> 
                        </div> 
                      </div>
                    </div>

                    <div class="col-lg-4">
                      <div class="form-group form-float">
                        <div class="form-line">
                          <label class="form-label">Turno<span style="color: red;">&nbsp;*</span></label>
                          <select name="id_turno" class="form-control select2" >
                            <option value="">Seleccione</option>
                            <?php 
                            foreach($this->turnos as $row){
                              $turno=new Confucio();
                              $turno=$row;?> 
                              <option value="<?php echo $turno->id;?>" <?php if($turno->id==$this->seccion->id_turno){ echo "selected=selected"; }?>><?php echo $turno->descripcion;?></option>              
                            <?php }?>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-4">
                      <div class="form-group form-float">
                        <div class="form-line">
                          <label class="form-label">Días<span style="color: red;">&nbsp;*</span></label>
                          <select id="dia" name="dia[]" class="form-control select2" multiple>
                           <?php                              
                           foreach($this->dias as $row){
                            $dia=new Confucio();
                            $dia=$row;?> 
                            <option value="<?php echo $dia->id;?>" <?php 
                            $i=1;
                            while($i<=count($this->datos_h)){
                              if($dia->id==$this->dia[$i]['id_dia'.$i]){ 
                                echo "selected=selected"; 
                              }
                              $i++;
                            } ?>><?php echo $dia->descripcion;?></option>
                          <?php } ?> 
                        </select>
                      </div>
                    </div>
                  </div>

                  
                </fieldset>
                <h3>Datos del Horario</h3>
                <fieldset>
                  <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group form-float">
                        <div class="form-line">
                          <label class="form-label">Hora Inicio<span style="color: red;">&nbsp;*</span></label>
                          <input type="time" name="hora_inicio" class="form-control" value="<?php echo $this->seccion->hora_inicio; ?>" required>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group form-float">
                        <div class="form-line">
                          <label class="form-label">Hora Fin<span style="color: red;">&nbsp;*</span></label>
                          <input type="time" name="hora_fin" class="form-control" value="<?php echo $this->seccion->hora_fin; ?>" required>
                        </div>
                      </div>
                    </div>

                  </div>
                </fieldset>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<?php require 'views/footer.php'; ?>
<!-- General JS Scripts -->
<script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
<script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-steps/jquery.steps.min.js"></script>
<script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-validation/dist/jquery.validate.min.js"></script>
<!-- Page Specific JS File -->
<script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/form-wizard.js"></script>
<!-- Template JS File -->
<script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/scripts.js"></script>
<!-- Custom JS File -->
<script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>
</body>


<!-- form-wizard.html  21 Nov 2019 03:55:20 GMT -->
</html>