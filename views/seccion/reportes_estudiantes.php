<?php

require_once '././src/phpexcel/Classes/PHPExcel.php';

$objPHPExcel = new PHPExcel();

//Datos de la sección
$j=1;
while($j<=count($this->datos_h)){

    if ($this->seccion->docente == 1) {
        $objPHPExcel->getActiveSheet()->mergeCells('A2:M2')->setCellValue('A2', 
            'INSCRIPCIONES '.$this->seccion->periodo.
            ' ICUBV - HORARIO: '.strtoupper($this->dia[$j]['dia'.$j]).
            ' '.date("g:i A", strtotime($this->seccion->hora_inicio)).'-'.date("g:i A", strtotime($this->seccion->hora_fin)).
            ' - PROFESOR: POR DEFINIR');
    }else{
        $objPHPExcel->getActiveSheet()->mergeCells('A2:M2')->setCellValue('A2', 
            'INSCRIPCIONES '.$this->seccion->periodo.
            ' ICUBV - HORARIO: '.strtoupper($this->dia[$j]['dia'.$j]).
            ' '.date("g:i A", strtotime($this->seccion->hora_inicio)).'-'.date("g:i A", strtotime($this->seccion->hora_fin)).
            ' - PROFESOR: '.strtoupper($this->seccion->primer_nombre).' '.strtoupper($this->seccion->primer_apellido));
    }
    $j++;
}

//Estilo de los datos de la seccion
$objPHPExcel->getActiveSheet()->getStyle('A2:M2')->getFont()->setBold(true)->setSize(16);
$objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(80);//Altura
$objPHPExcel->getActiveSheet()->getStyle('A2:M2')->getAlignment()->setHorizontal('center');//Alineación
$objPHPExcel->getActiveSheet()->getStyle('A2:M2')->getAlignment()->setVertical('center');//Alineación
$objPHPExcel->getActiveSheet()->getStyle('A2:M2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('70AD47');

//Logo confucio
$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('Confucio');
$objDrawing->setDescription('Confucio');
$objDrawing->setPath('././src/principal_dos/assets/img/instituto.png');
$objDrawing->setCoordinates('C2');
$objDrawing->setOffsetX(5); 
$objDrawing->setOffsetY(5);                                        
$objDrawing->setHeight(85);
$objDrawing->setWidth(85);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

//Logo UBV
$objDrawing_1 = new PHPExcel_Worksheet_Drawing();
$objDrawing_1->setName('Confucio');
$objDrawing_1->setDescription('Confucio');
$objDrawing_1->setPath('././src/principal_dos/assets/img/logo-ubv.png');
$objDrawing_1->setCoordinates('D2'); 
$objDrawing_1->setOffsetX(5); 
$objDrawing_1->setOffsetY(5);                                       
$objDrawing_1->setHeight(95);
$objDrawing_1->setWidth(95);
$objDrawing_1->setWorksheet($objPHPExcel->getActiveSheet());

//Estilo columnas de datos
$objPHPExcel->setActiveSheetIndex(0)
->setCellValue('A3', '#')
->setCellValue('B3', 'Nacionalidad')
->setCellValue('C3', 'Cédula')
->setCellValue('D3', 'Nombres')
->setCellValue('E3', 'Apellidos')
->setCellValue('F3', 'Teléfono')
->setCellValue('G3', 'Correo electrónico')
->setCellValue('H3', 'Institución de prosedencia')
->setCellValue('I3', 'Profesión u Oficio')
->setCellValue('J3', 'Zonda donde vive')
->setCellValue('K3', 'Fecha de nacimiento')
->setCellValue('L3', 'Nro. de deposito')
->setCellValue('M3', 'Banco');

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(60);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(35);

$objPHPExcel->getActiveSheet()->getStyle('A3:M3')->getFont()->setBold(true)->setSize(12);
$objPHPExcel->getActiveSheet()->getStyle('A3:M3')->getAlignment()->setHorizontal('center');
$objPHPExcel->getActiveSheet()->getStyle('A3:M3')->getAlignment()->setVertical('center');
$objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(25);
$objPHPExcel->getActiveSheet()->getStyle('A3:M3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('666666');             

//Datos de los estudiantes 
$i = 4;
$num=1;
foreach($this->estudiantes as $row){
    $estudiante=new Confucio();
    $estudiante=$row;

    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue("A$i", $num)
    ->setCellValue("B$i", $estudiante->pais)
    ->setCellValue("C$i", $estudiante->identificacion)
    ->setCellValue("D$i", $estudiante->primer_nombre. " ".$estudiante->segundo_nombre)
    ->setCellValue("E$i", $estudiante->primer_apellido." ".$estudiante->segundo_apellido)
    ->setCellValue("F$i", $estudiante->telefono)
    ->setCellValue("G$i", $estudiante->correo);
    if ($estudiante->inst_publica) {
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue("H$i", $estudiante->inst_publica);
    }
    if ($estudiante->inst_privada) {
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue("H$i", $estudiante->inst_publica);
    }
    if ($estudiante->organismo) {
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue("H$i", $estudiante->inst_publica);
    }
    if ($estudiante->inst_inv) {
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue("H$i", $estudiante->inst_publica);
    }
    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue("I$i", $estudiante->profesion)
    ->setCellValue("J$i", $estudiante->direccion)
    ->setCellValue("K$i", $estudiante->fecha_nacimiento);
    if ($estudiante->numero_vouche) {
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue("L$i", $estudiante->numero_vouche);
    }
    if ($estudiante->numero_vouche_50) {
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue("L$i", $estudiante->numero_vouche_50);
    }
    if ($estudiante->observacion) {
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue("M$i", $estudiante->observacion);
    }
    if ($estudiante->observacion_50) {
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue("M$i", $estudiante->observacion_50);
    }
    $i++;
    $num++;
}

$i= $i-1;
$objPHPExcel->getActiveSheet()->getStyle("A4:M$i")->getAlignment()->setHorizontal('left');
$border_style= array('borders' => array('allborders' => array('style' => 
    PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('argb' => '000000'),)));
$objPHPExcel->getActiveSheet()->getStyle("A2:M$i")->applyFromArray($border_style);

$objPHPExcel->getActiveSheet()->setTitle($this->seccion->descripcion);
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-Excel');
header('Content-Disposition: attachment;filename="lista de estudiantes.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;