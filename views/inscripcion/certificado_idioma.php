<?php

include 'views/plantilla/plantilla_certificado.php';
     
   $pdf = new PDF('L','mm','Letter','A4');
	//$pdf->AliasNbPages();
    $pdf->AddPage();

    $pdf->Ln(5);

    $pdf->Image('src/reporte/Chino_Mandarin.png','','','280','217','PNG');
    
     //IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)

     ///////////////////////////FECHA EN LETRAS//////////////////////////////////////////7

    $meses=array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

     $pdf->SetFont('Arial','I',18);
     $pdf->SetXY(132,190);
     $pdf->WriteHTML(utf8_decode(''.date('d',strtotime($this->idiomas->fecha)).' de '.$meses[date('m',strtotime($this->idiomas->fecha))-1].' del '.(date('Y',strtotime($this->idiomas->fecha))).'' ));
    
     $pdf->SetFont('Arial','',19);
     $pdf->SetXY(205,130);
     $pdf->WriteHTML(utf8_decode(''.substr($this->idiomas->nivel, 5).''));

     $pdf->SetFont('Arial','',18);
     $pdf->SetXY(138,159);
     $pdf->WriteHTML(utf8_decode(''.strtoupper($this->idiomas->estatus_escrito).''));

     $pdf->SetFont('Arial','I',20);
     $pdf->SetXY(90,116);
     $pdf->WriteHTML(utf8_decode(''.strtoupper($this->idiomas->primer_nombre).' '.strtoupper($this->idiomas->primer_apellido).' '.strtoupper($this->idiomas->segundo_nombre).' '.strtoupper($this->idiomas->segundo_apellido).''));

     $pdf->SetFont('Arial','I',18);
     $pdf->SetXY(220,139);
     $pdf->WriteHTML(utf8_decode(''.strtoupper($this->idiomas->periodo).''));

     

//////////////////////////////PASAR AÑO A LETRAS///////////////////////////////////////////////77
function basico($numero) {
$valor = array ('uno','dos','tres','cuatro','cinco','seis','siete','ocho',
'nueve','diez','once','doce','trece','catorce','quince','dieciseis','diecisiete','dieciocho','diecinueve','veinte','veintiuno','veintidos','veintitres', 'veinticuatro','veinticinco',
'veintiséis','veintisiete','veintiocho','veintinueve');
return $valor[$numero - 1];
}

function decenas($n) {
$decenas = array (30=>'treinta',40=>'cuarenta',50=>'cincuenta',60=>'sesenta',
70=>'setenta',80=>'ochenta',90=>'noventa');
if( $n <= 29) return basico($n);
$x = $n % 10;
if ( $x == 0 ) {
return $decenas[$n];
} else return $decenas[$n - $x].' y '. basico($x);
}

function centenas($n) {
$cientos = array (100 =>'cien',200 =>'doscientos',300=>'trecientos',
400=>'cuatrocientos', 500=>'quinientos',600=>'seiscientos',
700=>'setecientos',800=>'ochocientos', 900 =>'novecientos');
if( $n >= 100) {
if ( $n % 100 == 0 ) {
return $cientos[$n];
} else {
$u = (int) substr($n,0,1);
$d = (int) substr($n,1,2);
return (($u == 1)?'ciento':$cientos[$u*100]).' '.decenas($d);
}
} else return decenas($n);
}

function miles($n) {
if($n > 999) {
if( $n == 1000) {return 'mil';}
else {
$l = strlen($n);
$c = (int)substr($n,0,$l-3);
$x = (int)substr($n,-3);
if($c == 1) {$cadena = 'mil '.centenas($x);}
else if($x != 0) {$cadena = centenas($c).' mil '.centenas($x);}
else $cadena = centenas($c). ' mil';
return $cadena;
}
} else return centenas($n);
}

function millones($n) {
if($n == 1000000) {return 'un millón';}
else {
$l = strlen($n);
$c = (int)substr($n,0,$l-6);
$x = (int)substr($n,-6);
if($c == 1) {
$cadena = ' millón ';
} else {
$cadena = ' millones ';
}
return miles($c).$cadena.(($x > 0)?miles($x):'');
}
}
function convertir($n) {
switch (true) {
case ( $n >= 1 && $n <= 29) : return basico($n); break;
case ( $n >= 30 && $n < 100) : return decenas($n); break;
case ( $n >= 100 && $n < 1000) : return centenas($n); break;
case ($n >= 1000 && $n <= 999999): return miles($n); break;
case ($n >= 1000000): return millones($n);
}
}

    //  $pdf->SetDrawColor(0,57,127);
      
           $pdf->Ln(15);

  
                $pdf->Ln();
                //Condición ternaria que cambia el valor de $letra
                ($letra == 'D') ? $letra = 'FD' : $letra = 'D';
                //Aumenta la siguiente posición de Y (recordar que X es fijo)
                //Se suma 7 porque cada celda tiene esa altura
                $ejeY = $ejeY + 7;
    

         	$pdf->Output();

?>