<?php

include  'views/plantilla/plantilla.php';
     
$pdf = new PDF('P','mm','Letter');
//$pdf->AliasNbPages();
$pdf->AddPage();

$pdf->Ln(5);

$pdf->SetFont('Arial','b',13);
$pdf->SetXY(80,60);
$pdf->Cell(120,10,utf8_decode("CONSTANCIA DE ESTUDIO"),0,1,'L');

 //  $pdf->Ln(3);
//Print 2 MultiCells   justificado


$pdf->SetFont('Arial','',11);
$pdf->SetXY(44,75);

$pdf->WriteHTML(utf8_decode('<p ALIGN="justify">     Por medio de la presente, hace constar que el ciudadano (a): <b>'.strtoupper($this->estudio->primer_nombre." ".$this->estudio->segundo_nombre." ".$this->estudio->primer_apellido).'</b>,
 titular de la cédula de identidad <b> No.'.strtoupper($this->estudio->tipo_documento).' - '.$this->estudio->identificacion.'</b>, participa en el curso de <b>'.$this->estudio->oferta.'</b>, en el turno de la <b>'.$this->estudio->turno.'</b>, en el  
<b>'.$this->estudio->nivel.'</b>, en horario de <b>'.$this->estudio->hora_inicio.' - '.$this->estudio->hora_fin.'</b> , para el periodo académico <b>'.$this->estudio->periodo.'</b> en el Instituto Confucio en la Universidad Bolivariana de Venezuela</p>'));

$pdf->SetXY(23,105);



$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

$pdf->Image('src/reporte/unnamed.png','82','132','38','36','PNG');
//IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)

$pdf->Image('src/reporte/sello.png','110','145','28','16','PNG');
//IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
  
$pdf->WriteHTML(utf8_decode('<br><p ALIGN="justify">  Constancia que se expide a petición de la parte interesada, a los '.date('d',strtotime($this->estudio->fecha)).' días del mes de '.$meses[date('n',strtotime($this->estudio->fecha))-1].' de '.(date('Y',strtotime($this->estudio->fecha))).'.</p>'));
    

$pdf->SetXY(94,140);
$pdf->WriteHTML(utf8_decode('<center>Atentamente,</center>'));

$pdf->SetXY(72,157);
$pdf->WriteHTML(utf8_decode('<center>________________________________</center>'));

$pdf->SetFont('Arial','I',9);
$pdf->SetXY(83,170);
$pdf->WriteHTML(utf8_decode('Luisa Josefina Lopez Moreno'));
$pdf->SetXY(76,175);
$pdf->WriteHTML(utf8_decode('Directora(E) del Instituto Confucio UBV'));
$pdf->SetXY(66,180);
$pdf->WriteHTML(utf8_decode('(Según Resolución N° CU-12-17-2020 del 31/12/2020)'));


//////////////////////////////PASAR AÑO A LETRAS///////////////////////////////////////////////77
function basico($numero) {
$valor = array ('uno','dos','tres','cuatro','cinco','seis','siete','ocho',
'nueve','diez','once','doce','trece','catorce','quince','dieciseis','diecisiete','dieciocho','diecinueve','veinte','veintiuno','veintidos','veintitres', 'veinticuatro','veinticinco',
'veintiséis','veintisiete','veintiocho','veintinueve');
return $valor[$numero - 1];
}

function decenas($n) {
$decenas = array (30=>'treinta',40=>'cuarenta',50=>'cincuenta',60=>'sesenta',
70=>'setenta',80=>'ochenta',90=>'noventa');
if( $n <= 29) return basico($n);
$x = $n % 10;
if ( $x == 0 ) {
return $decenas[$n];
} else return $decenas[$n - $x].' y '. basico($x);
}

function centenas($n) {
$cientos = array (100 =>'cien',200 =>'doscientos',300=>'trecientos',
400=>'cuatrocientos', 500=>'quinientos',600=>'seiscientos',
700=>'setecientos',800=>'ochocientos', 900 =>'novecientos');
if( $n >= 100) {
if ( $n % 100 == 0 ) {
return $cientos[$n];
} else {
$u = (int) substr($n,0,1);
$d = (int) substr($n,1,2);
return (($u == 1)?'ciento':$cientos[$u*100]).' '.decenas($d);
}
} else return decenas($n);
}

function miles($n) {
if($n > 999) {
if( $n == 1000) {return 'mil';}
else {
$l = strlen($n);
$c = (int)substr($n,0,$l-3);
$x = (int)substr($n,-3);
if($c == 1) {$cadena = 'mil '.centenas($x);}
else if($x != 0) {$cadena = centenas($c).' mil '.centenas($x);}
else $cadena = centenas($c). ' mil';
return $cadena;
}
} else return centenas($n);
}

function millones($n) {
if($n == 1000000) {return 'un millón';}
else {
$l = strlen($n);
$c = (int)substr($n,0,$l-6);
$x = (int)substr($n,-6);
if($c == 1) {
$cadena = ' millón ';
} else {
$cadena = ' millones ';
}
return miles($c).$cadena.(($x > 0)?miles($x):'');
}
}
function convertir($n) {
switch (true) {
case ( $n >= 1 && $n <= 29) : return basico($n); break;
case ( $n >= 30 && $n < 100) : return decenas($n); break;
case ( $n >= 100 && $n < 1000) : return centenas($n); break;
case ($n >= 1000 && $n <= 999999): return miles($n); break;
case ($n >= 1000000): return millones($n);
}
}




//////////////////////////////

//  $pdf->SetDrawColor(0,57,127);
  
       $pdf->Ln(15);


            $pdf->Ln();
            //Condición ternaria que cambia el valor de $letra
            ($letra == 'D') ? $letra = 'FD' : $letra = 'D';
            //Aumenta la siguiente posición de Y (recordar que X es fijo)
            //Se suma 7 porque cada celda tiene esa altura
            $ejeY = $ejeY + 7;


         $pdf->Output();
?>