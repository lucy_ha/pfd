<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Confucio-Ver Calificación</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico' />
</head>

<body>

<?php require 'views/header.php'; ?>  

       <!-- Main Content -->
       <div class="main-content">
        <section class="section">
          <div class="section-body">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Ver Calificación</h4>
                  </div>
                  <div class="card-body">
                  <?php echo $this->mensaje;?>  
                    <div class="table-responsive">
                      <table class="table table-striped" id="table-1">
                        <thead>
                        <tr>
                            <th class="text-center">
                              #
                            </th>
                            <th>Curso</th>
                            <th>Nivel</th>
                            <th>Nota</th>
                            <th>Estatus</th>
                            <th>Acción</th>
                          </tr>
                        </thead>
                        <tbody>
                       
                         <?php if($this->prosecucion->categoria==('Idioma')){?> 
                          <tr>
                          <input type="hidden" name="id_nota_actividad_idioma" value="<?php echo $this->prosecucion->id_nota_actividad_idioma;?>">
                          <td><?php echo $this->prosecucion->id_nota_actividad_idioma?></td>
                          <td><?php echo $this->prosecucion->seccion; ?></td>
                          <td><?php echo $this->prosecucion->nivel; ?></td>
                          <td><?php echo $this->prosecucion->nota; ?></td>
                          <td><?php echo $this->prosecucion->estatus_escrito; ?></td>
                          <?php if($this->prosecucion->estatus_escrito==('Aprobado')){?> 
                          <td>
                          <a href="<?php echo constant ('URL') . "inscripcion/certificado_idioma/" . $this->prosecucion->id_persona;?>" class="btn btn-icon btn-danger" data-toggle="tooltip" data-placement="bottom" title="Certificado"><i class="fas fa-file-pdf"></i></a>
                          <a href="<?php echo constant ('URL') . "inscripcion/oferta_lista/" . $this->prosecucion->id_persona;?>" class="btn btn-success" onclick="return confirm('¿Desea inscribirse nuevamente?');" ><i class="fa fa-check"></i>&nbsp; ¿Inscribirse Nuevamente? &nbsp;</button></a>
                        </td>
                          <?php }else {?>
                            <td>
                            <a href="<?php echo constant ('URL') . "inscripcion/oferta_lista/" . $this->prosecucion->id_persona;?>" class="btn btn-success" onclick="return confirm('¿Desea inscribirse nuevamente?');" ><i class="fa fa-check"></i>&nbsp; ¿Inscribirse Nuevamente? &nbsp;</button></a>
                          </td>
                          </tr>
                            <?php }?>
                          <?php  }?>
                         <?php if($this->prosecucion_1->categoria==('Otro')){?>
                          <tr>
                          <td><?php echo $this->prosecucion_1->id_nota_actividad?></td>
                          <td><?php echo $this->prosecucion_1->seccion; ?></td>
                          <td><?php echo $this->prosecucion_1->nivel; ?></td>
                          <td><?php echo $this->prosecucion_1->nota; ?></td>
                          <td><?php echo $this->prosecucion_1->estatus_escrito; ?></td>
                          <td>
                          <a href="<?php echo constant ('URL') . "inscripcion/certificado_actividad/" . $this->prosecucion_1->id_persona;?>" class="btn btn-icon btn-danger" data-toggle="tooltip" data-placement="bottom" title="Certificado"><i class="fas fa-file-pdf"></i></a>  
                          <a href="<?php echo constant ('URL') . "inscripcion/eliminar/" . $this->prosecucion_1->id_nota_actividad;?>" class="btn btn-outline-info">¿Curso Terminado?</a>
                          </td>
                          </tr>
                        <?php }?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
 <!--End Main Content-->
                 
                 </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div>
      <?php require 'views/footer.php'; ?>
  <!-- General JS Scripts -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <!-- JS Libraies -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-ui/jquery-ui.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/datatables.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno//assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>
</body>

</html>