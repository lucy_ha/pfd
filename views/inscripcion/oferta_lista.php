<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Confucio-Aspirante</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant('URL'); ?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant('URL'); ?>src/principal_uno/assets/bundles/datatables/datatables.min.css">
  <link rel="stylesheet" href="<?php echo constant('URL'); ?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant('URL'); ?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant('URL'); ?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant('URL'); ?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant('URL'); ?>src/principal_uno/assets/img/favi.ico' />

</head>

<body>
  <div class="loader"></div>
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4>Cursos Ofertados</h4>
                <div class="card-header-action">
                    <a href="<?php echo constant ('URL'). "inscripcion/reingreso/" . $this->reingreso->id_persona;?>" class="btn btn-warning">Volver</a>
                  </div>
              </div>
              <div class="card-body">
                <div class="table-responsive">

                <form action="<?php echo constant('URL'); ?>inscripcion/update" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="id_persona" value="<?php echo $this->reingreso->id_persona ?>">
                <input type="hidden" name="id_inscripcion" value="<?php echo $this->reingreso->id_inscripcion ?>">
                 
 
 
 
                        <table class="table table-striped" id="table-2">
                          <thead>
                            <tr>
                              <th class="text-center pt-3">
                              </th>
                              <th>Curso</th>
                              <th>Nivel</th>
                              <th>Horario</th>
                              <th>Turno</th>
                              <th>Modalidad</th>
                              <th>Periodo</th>
                            </tr>
                          </thead>
                          <tbody>
                          <?php $fecha_actual = date('Y-m-d');
                          foreach ($this->cursos as $row) {
                          $curso = new Confucio();
                           $curso = $row;
                          if ($fecha_actual >= $curso->inicio_periodo && $fecha_actual <= $curso->fin_periodo) { ?>
                              <tr>
                                <td class="text-center pt-2">
                                  <div class="custom-checkbox custom-control">
                                    <input type="checkbox" name="id_oferta_academica" value="<?php echo $curso->id_oferta_academica; ?>" onclick="boton.disabled = !this.checked">
                                  </div>
                                </td>
                                
                      <td><?php echo $curso->seccion; ?></td>
                      <td><?php echo $curso->nivel; ?></td>
                      <td><?php echo $curso->hora_inicio . "-" . $curso->hora_fin; ?></td>
                      <td><?php echo $curso->turno; ?></td>
                      <td><?php echo $curso->modalidad; ?></td>
                      <td><?php echo $curso->periodo; ?></td>
                      </tr>
                      <?php } else{?>
                    <div class="text-center">
                      <h5> La Inscripción para el siguiente periodo actualmente no se encuentra activa. <br>
                        Debe esperar a que inicie nuevamente el proceso de Inscripción Convocada por el Instituto Confucio.</h5>
                    </div>
                <?php }
                 }  ?>
                    </tbody>
                    </table>
                    <div class="text-center">
                    <button type="submit" value="submit" name="boton" class="btn btn-success" onclick="return confirm('¿Desea inscribirse nuevamente?');" disabled><i class="fa fa-check"></i>&nbsp; Validar Reingreso &nbsp;</button>
                  </div>

                      </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>


  <!-- General JS Scripts -->
  <script src="<?php echo constant('URL'); ?>src/principal_uno/assets/js/app.min.js"></script>
  <!-- JS Libraies -->
  <script src="<?php echo constant('URL'); ?>src/principal_uno/assets/bundles/datatables/datatables.min.js"></script>
  <script src="<?php echo constant('URL'); ?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo constant('URL'); ?>src/principal_uno/assets/bundles/jquery-ui/jquery-ui.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant('URL'); ?>src/principal_uno/assets/js/page/datatables.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant('URL'); ?>src/principal_uno//assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant('URL'); ?>src/principal_uno/assets/js/custom.js"></script>

</body>

</html>