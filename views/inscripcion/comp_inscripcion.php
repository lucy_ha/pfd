<?php

include  'views/plantilla/plantilla.php';
     
$pdf = new PDF('P','mm','Letter');
//$pdf->AliasNbPages();
$pdf->AddPage();

$pdf->Ln(5);

$pdf->SetFont('Arial','',8);
$pdf->SetXY(177,5);
$pdf->Cell(120,10,utf8_decode(('Fecha '.date("d/m/Y", strtotime($this->estudio->fecha)).'')),0,1,'L');

$pdf->SetFont('Arial','b',11);
$pdf->SetXY(75,60);
$pdf->Cell(120,10,utf8_decode("COMPROBANTE DE INSCRIPCIÓN"),0,1,'L');
$pdf->Ln(10);

$pdf->SetFont('Arial','b',9);
$pdf->Cell(110,12,"                              Nombres:  ".strtoupper($this->estudio->primer_nombre)." ".strtoupper($this->estudio->segundo_nombre));
$pdf->Cell(100,12,"Apellidos: ".strtoupper($this->estudio->primer_apellido)." ".strtoupper($this->estudio->segundo_apellido));

$pdf->Ln(7);
$pdf->Cell(110,12,"                              Cedula de Identidad:".strtoupper($this->estudio->identificacion));
$pdf->Cell(90,12,"Telefono:".strtoupper($this->estudio->telefono));


$pdf->Ln(7);
$pdf->Cell(110,12,"                              Correo:".strtoupper($this->estudio->correo));
$pdf->Cell(90,12,"Periodo:".strtoupper($this->estudio->periodo));


$pdf->SetXY(79,116);
$pdf->Cell(120,10,utf8_decode("CURSO INSCRITO DEL PERIODO ".strtoupper($this->estudio->periodo)),0,1,'L');

$pdf->SetXY(27,130);
$pdf->SetMargins(20,38);

$pdf->SetFillColor(232,232,232);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(50,6,'Curso',1,0,'C',1);
$pdf->Cell(30,6,'Nivel',1,0,'C',1);
$pdf->Cell(40,6,utf8_decode('Turno'),1,0,'C',1);
$pdf->Cell(40,6,utf8_decode('Horario'),1,0,'C',1);

$pdf->Ln(5);
$pdf->SetFont('Arial','B',7);
$pdf->Ln(1);

  $pdf->SetXY(27,136);
  $pdf->Cell(50,6,utf8_decode(' '.strtoupper($this->estudio->oferta).''),1,0,'C');
  $pdf->Cell(30,6,utf8_decode($this->estudio->nivel),1,0,'C');
  $pdf->Cell(40,6,utf8_decode($this->estudio->turno),1,0,'C');
  $pdf->Cell(40,6,utf8_decode(''.date("g:i a", strtotime($this->estudio->hora_inicio)).' - '.date("g:i a", strtotime($this->estudio->hora_fin)).'' ),1,0,'C');



$pdf->Ln(7);




///////////////////////////FECHA EN LETRAS//////////////////////////////////////////7
//$diassemana = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");


$pdf->Image('src/reporte/unnamed.png','82','170','38','36','PNG');
//IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)

$pdf->Image('src/reporte/sello.png','110','180','28','16','PNG');
//IMAGE (RUTA,X,Y,ANCHO,ALTO,EXTEN)
  

$pdf->SetXY(80,194);
$pdf->WriteHTML(utf8_decode('<center>________________________________</center>'));

$pdf->SetFont('Arial','I',9);
$pdf->SetXY(83,200);
$pdf->WriteHTML(utf8_decode('Luisa Josefina Lopez Moreno'));
$pdf->SetXY(76,206);
$pdf->WriteHTML(utf8_decode('Directora(E) del Instituto Confucio UBV'));
$pdf->SetXY(66,213);
$pdf->WriteHTML(utf8_decode('(Según Resolución N° CU-12-17-2020 del 31/12/2020)'));



//////////////////////////////PASAR AÑO A LETRAS///////////////////////////////////////////////77
function basico($numero) {
$valor = array ('uno','dos','tres','cuatro','cinco','seis','siete','ocho',
'nueve','diez','once','doce','trece','catorce','quince','dieciseis','diecisiete','dieciocho','diecinueve','veinte','veintiuno','veintidos','veintitres', 'veinticuatro','veinticinco',
'veintiséis','veintisiete','veintiocho','veintinueve');
return $valor[$numero - 1];
}

function decenas($n) {
$decenas = array (30=>'treinta',40=>'cuarenta',50=>'cincuenta',60=>'sesenta',
70=>'setenta',80=>'ochenta',90=>'noventa');
if( $n <= 29) return basico($n);
$x = $n % 10;
if ( $x == 0 ) {
return $decenas[$n];
} else return $decenas[$n - $x].' y '. basico($x);
}

function centenas($n) {
$cientos = array (100 =>'cien',200 =>'doscientos',300=>'trecientos',
400=>'cuatrocientos', 500=>'quinientos',600=>'seiscientos',
700=>'setecientos',800=>'ochocientos', 900 =>'novecientos');
if( $n >= 100) {
if ( $n % 100 == 0 ) {
return $cientos[$n];
} else {
$u = (int) substr($n,0,1);
$d = (int) substr($n,1,2);
return (($u == 1)?'ciento':$cientos[$u*100]).' '.decenas($d);
}
} else return decenas($n);
}

function miles($n) {
if($n > 999) {
if( $n == 1000) {return 'mil';}
else {
$l = strlen($n);
$c = (int)substr($n,0,$l-3);
$x = (int)substr($n,-3);
if($c == 1) {$cadena = 'mil '.centenas($x);}
else if($x != 0) {$cadena = centenas($c).' mil '.centenas($x);}
else $cadena = centenas($c). ' mil';
return $cadena;
}
} else return centenas($n);
}

function millones($n) {
if($n == 1000000) {return 'un millón';}
else {
$l = strlen($n);
$c = (int)substr($n,0,$l-6);
$x = (int)substr($n,-6);
if($c == 1) {
$cadena = ' millón ';
} else {
$cadena = ' millones ';
}
return miles($c).$cadena.(($x > 0)?miles($x):'');
}
}
function convertir($n) {
switch (true) {
case ( $n >= 1 && $n <= 29) : return basico($n); break;
case ( $n >= 30 && $n < 100) : return decenas($n); break;
case ( $n >= 100 && $n < 1000) : return centenas($n); break;
case ($n >= 1000 && $n <= 999999): return miles($n); break;
case ($n >= 1000000): return millones($n);
}
}




//////////////////////////////

//  $pdf->SetDrawColor(0,57,127);
  
       $pdf->Ln(15);


            $pdf->Ln();
            //Condición ternaria que cambia el valor de $letra
            ($letra == 'D') ? $letra = 'FD' : $letra = 'D';
            //Aumenta la siguiente posición de Y (recordar que X es fijo)
            //Se suma 7 porque cada celda tiene esa altura
            $ejeY = $ejeY + 7;


         $pdf->Output();

?>