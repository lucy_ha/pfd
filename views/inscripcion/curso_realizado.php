<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Confucio-Cursos Realizados</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico' />
</head>

<body>

<?php require 'views/header.php'; ?>  

       <!-- Main Content -->
       <div class="main-content">
        <section class="section">
          <div class="section-body">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Cursos Realizados</h4>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped" id="table-1">
                        <thead>
                        <tr>
                            <th>Curso</th>
                            <th>Nivel</th>
                            <th>Periodo</th>
                            <th>Nota</th>
                            <th>Nota Escrita</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php 
                      foreach($this->realizados as $row){
                        $realizado=new Confucio();
                        $realizado=$row;?> 
                       <tr>
                          <td><?php echo $realizado->seccion; ?></td>
                          <td><?php echo $realizado->nivel; ?></td>
                          <td><?php echo $realizado->periodo; ?></td>
                          <td><?php echo $realizado->nota; ?></td>
                          <?php if ($realizado->estatus_escrito == "Aprobado") { ?>
                              <td><div style= "color: green;">Aprobado</div></td>
                          <?php } ?>
                          <?php if ($realizado->estatus_escrito == "Reprobado") { ?>
                              <td><div style= "color: red;">Reprobado</div></td>
                          <?php } ?>
                          <!--<td><a href="<?php echo constant ('URL') . "inscripcion/certificado_idioma/" . $realizado->id_persona;?>" class="btn btn-icon btn-danger" data-toggle="tooltip" data-placement="bottom" title="Certificado"><i class="fas fa-file-pdf"></i></a>-->
                          </td>
                      </tr>
                          <?php }?>

                          <?php 
                      foreach($this->otros as $row){
                        $otro=new Confucio();
                        $otro=$row;?> 
                       <tr>
                          <td><?php echo $otro->seccion; ?></td>
                          <td><?php echo $otro->nivel; ?></td>
                          <td><?php echo $otro->periodo; ?></td>
                          <td><?php echo $otro->nota; ?></td>
                          <?php if ($otro->estatus_escrito == "Aprobado") { ?>
                              <td><div style= "color: green;">Aprobado</div></td>
                          <?php } ?>
                          <?php if ($otro->estatus_escrito == "Reprobado") { ?>
                              <td><div style= "color: red;">Reprobado</div></td>
                          <?php } ?>
                         <!-- <td><a href="<?php echo constant ('URL') . "inscripcion/certificado_actividad/" . $otro->id_persona;?>" class="btn btn-icon btn-danger" data-toggle="tooltip" data-placement="bottom" title="Certificado"><i class="fas fa-file-pdf"></i></a>--->
                          </td>
                      </tr>
                          <?php }?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
 <!--End Main Content-->
                 
                 </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div>
      <?php require 'views/footer.php'; ?>
  <!-- General JS Scripts -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <!-- JS Libraies -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-ui/jquery-ui.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/datatables.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno//assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>
</body>

</html>
