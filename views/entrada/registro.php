<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>PFD-Portal</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/css/select2.min.css">
  <!-- Template CSS --> <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/summernote/summernote-bs4.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/200.png' />
</head>

<body>

<?php require 'views/header.php'; ?>  
      <!-- Main Content -->
      <div class="main-content">
       
       <form action="<?php echo constant ('URL');?>entrada/agg" enctype="multipart/form-data"  class="needs-validation" method="POST">
       <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Registro de Noticia</h4>
                  </div>
                  <div class="card-body">
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Titulo</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" name="titulo" class="form-control">
                      </div>
                    </div>

                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Imagen</label>
                      <div class="col-sm-4 col-md-4">
                        <div id="image-preview" class="image-preview">
                          <label for="image-upload" id="image-label">Elija una imagen</label>
                          <input type="file" name="imagen" id="image-upload" accept="image/.jpeg,.jpg,.png" />
                        </div>
                      </div>
                    </div>
                    <!--<div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Imagen</label>
                      <div class="col-sm-12 col-md-7">
                      <input id="acceptTerms1"  type="file" name="url"  class="custom-file-input" accept="image/.jpeg,.jpg,.png" >
                      <label class="custom-file-label" for="customFile">Elija una imagen de entrada</label>
                      </div>
                    </div>-->
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Contenido</label>
                      <div class="col-sm-12 col-md-7">
                        <textarea class="summernote-simple" name="contenido"></textarea>
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                      <div class="col-sm-12 col-md-7">
                        <button class="btn btn-primary">Publicar</button>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
        </form>
      </div>

      <?php require 'views/footer.php'; ?>


  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <!-- JS Libraies -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/summernote/summernote-bs4.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-selectric/jquery.selectric.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/upload-preview/assets/js/jquery.uploadPreview.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-validation/dist/jquery.validate.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/create-post.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>    
         

<script>

$(document).ready(function(){
var extensionesValidas = ".jpg, .jpeg, .png";
var pesoPermitido = 1000000;
// Cuando cambie #fichero
$("#image-upload").change(function () {
    $('#image-upload').text('');
    if(vE(this)) {
        if(vP(this)) {   
        }
    }  
});

// Validacion de extensiones permitidas
function vE(datos) {
    var ruta = datos.value;
    var extension = ruta.substring(ruta.lastIndexOf('.') + 1).toLowerCase();
    var extensionValida = extensionesValidas.indexOf(extension);
    if(extensionValida < 0) {
        
           alert('La extensión no es válida Su Archivo tiene de extensión: .'+extension, 'Ha ocurrido un Error');

           document.getElementById("image-upload").value = "";
    
           return false;     
        } else {
              return true;
    }
}
// Validacion de peso del fichero en kbs
function vP(datos) {
    if (datos.files && datos.files[0]) {
        var pesoFichero = datos.files[0].size/1000000;
        if(pesoFichero > pesoPermitido) {

           alert('El peso maximo permitido del Archivo es: ' + pesoPermitido + ' KBs Su Archivo tiene: '+ pesoFichero +' KBs', 'Ha ocurrido un Error');

           document.getElementById("image-upload").value = "";
     
            return false;
        } else {
            return true;
        }
    }
}
});

</script>


</body>
</html>
