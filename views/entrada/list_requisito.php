<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>PFD-Portal</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/pretty-checkbox/pretty-checkbox.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico' />
</head>

<body>

<?php require 'views/header.php'; ?>  
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-body">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Ventana de Requisitos del Portal Web</h4>
                  </div>
                 
                  <div class="card-body">
                  <?php echo $this->mensaje;?>  
                    <div class="table-responsive">
                      <table class="table table-striped" id="table-1">
                        <thead>
                          <tr>
                            <th class="text-center">
                              #
                            </th>
                            <th>Monto Comunidad General</th>
                            <th>Monto Exonerados</th>
                            <th>Fecha de Inicio de Clases</th>
                            <th>Fecha de Inicio de Inscripcion</th>
                            <th>Fecha de Culminación de Inscripcion</th>
                            <th>Acciones</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php 
                      foreach($this->datos as $row){
                      $dato=new Confucio();
                      $dato=$row;?> 
                       <tr>
                          <td><?php echo $dato->id_requisito; ?></td>
                          <td><?php echo $dato->monto_uno; ?></td>
                          <td><?php echo $dato->monto_dos; ?></td>
                          <td><?php echo $dato->inicio_clase; ?></td>
                          <td><?php echo $dato->inscripcion_inicio; ?></td>
                          <td><?php echo $dato->inscripcion_fin; ?></td>
                          <td>
                          <a href="<?php echo constant ('URL') . "entrada/update_requisito/" . $dato->id_requisito;?>" class="btn btn-icon btn-danger" data-toggle="tooltip" data-placement="bottom" title="Editar Datos de Requisitos"><i class="fas fa-edit"></i></a>
                          </td>
                      </tr>
                  <?php }?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
 <!--End Main Content-->
                 
                 </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div>
      <?php require 'views/footer.php'; ?>
  <!-- General JS Scripts -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <!-- JS Libraies -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-ui/jquery-ui.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/datatables.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno//assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>

</body>

</html>

