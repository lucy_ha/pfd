<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>PFD-Portal</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/pretty-checkbox/pretty-checkbox.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/200.png' />
</head>

<body>

<?php require 'views/header.php'; ?>  
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-body">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Listado de Noticias</h4>
                    <div class="card-header-action">
                    <a href="<?php echo constant ('URL')?>entrada/registrar" class="btn btn-info"><i class="fas fa-plus"></i> Añadir Nueva Noticia</a>
                  </div>
                  </div>
                 
                  <div class="card-body">
                  <?php echo $this->mensaje;?>  
                    <div class="table-responsive">
                      <table class="table table-striped" id="table-1">
                        <thead>
                          <tr>
                            <th class="text-center">
                              #
                            </th>
                            <th>Imagen</th>
                            <th>Entrada</th>
                            <th>Informaciòn</th>
                            <th>Acciones</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php 
                      foreach($this->entradas as $row){
                      $entrada=new Confucio();
                      $entrada=$row;?> 
                       <tr>
                       <td><?php echo $entrada->id_portal; ?></td>
                          <td style="width:10%;"><img style="width:100%;" src="<?php echo constant ('URL') . "src/img/"."".$entrada->captura; ?>" alt="" ></td>
                          <td><?php echo $entrada->titulo; ?></td>
                          <td><?php echo $entrada->contenido; ?></td>
                        
                          <td>
                          <?php if($_SESSION['Consultar']==true){?>
                          <a href="<?php echo constant ('URL') . "entrada/delete/" . $entrada->id_portal;?>" class="btn btn-icon btn-danger" data-toggle="tooltip" data-placement="bottom" title="Eliminar Noticia"><i class="fas fa-trash"></i></a>
                          <?php }?>
                          <?php if($_SESSION['Editar']==true){?>
                          <a href="<?php echo constant ('URL') . "entrada/update/" . $entrada->id_portal;?>" class="btn btn-icon btn-success" data-toggle="tooltip" data-placement="bottom" title="Modificar Noticia"><i class="far fa-edit"></i></a>
                          <?php }?>
                          </td>
                      </tr>
                  <?php }?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
 <!--End Main Content-->
                 
                 </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div>
      <?php require 'views/footer.php'; ?>
  <!-- General JS Scripts -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <!-- JS Libraies -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/datatables.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-ui/jquery-ui.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/datatables.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno//assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>

</body>

</html>

