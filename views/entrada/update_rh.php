<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>PFD-Portal</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/select2/dist/css/select2.min.css">
  <!-- Template CSS --> <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/summernote/summernote-bs4.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/200.png' />
</head>

<body>

<?php require 'views/header.php'; ?>  
      <!-- Main Content -->
      <div class="main-content">
       
       <form action="<?php echo constant ('URL');?>entrada/FormHistoria" class="needs-validation" method="POST">
       <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Modificar Reseña Historica</h4>
                    <div class="card-header-action">
                  <a href="<?php echo constant ('URL')?>entrada/lista_historia" class="btn btn-warning">Volver</a>
                </div>
                  </div>
                  <div class="card-body">
                  <input name="id_historia" type="hidden" value="<?php echo $this->historia->id_historia;?>">
                  <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Reseña Historica</label>
                      <div class="col-sm-12 col-md-7">
                        <textarea class="summernote-simple" name="descripcion_historia"><?php echo $this->historia->descripcion_historia; ?></textarea>
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Misión</label>
                      <div class="col-sm-12 col-md-7">
                        <textarea class="summernote-simple" name="descripcion_mision"><?php echo $this->historia->descripcion_mision; ?></textarea>
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Visión</label>
                      <div class="col-sm-12 col-md-7">
                        <textarea class="summernote-simple" name="descripcion_vision"><?php echo $this->historia->descripcion_vision; ?></textarea>
                      </div>
                    </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                      <div class="col-sm-12 col-md-7">
                        <button class="btn btn-primary">Publicar</button>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
        </form>
      </div>

      <?php require 'views/footer.php'; ?>


  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <!-- JS Libraies -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/summernote/summernote-bs4.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-selectric/jquery.selectric.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/upload-preview/assets/js/jquery.uploadPreview.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-validation/dist/jquery.validate.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/create-post.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>    
         
</body>
</html>