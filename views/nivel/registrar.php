<!DOCTYPE html>
<html lang="en">

<!-- form-wizard.html  21 Nov 2019 03:55:16 GMT -->
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Confucio-Nivel</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/app.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo constant ('URL');?>src/principal_uno/assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo constant ('URL');?>src/principal_uno/assets/img/favi.ico'/>
</head>

<body>

  <?php require 'views/header.php'; ?>  

  <!-- Main Content -->
  <div class="main-content">
    <section class="section">
      <div class="section-body">
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
              <div class="card-header">
                <h4>Registrar Nivel</h4>
                <div class="card-header-action">
                  <a href="<?php echo constant ('URL')?>nivel" class="btn btn-warning">Volver</a>
                </div>
              </div>
              <div class="card-body">
                <form action="<?php echo constant('URL'); ?>nivel/RegisterNivel" id="wizard_with_validation"  novalidate="" method="POST">
                  <h3>Datos del Nivel</h3>
                  <fieldset>
                    <div class="row">
                      <div class="col-lg-5">
                        <div class="form-group form-float">
                          <div class="form-line">
                            <label class="form-label">Nombre del nivel<span style="color: red;">&nbsp;*</span></label>
                            <input type="text" class="form-control required" name="descripcion" >
                          </div>
                        </div>
                      </div>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <?php require 'views/footer.php'; ?>

  <!-- General JS Scripts -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/app.min.js"></script>
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-validation/dist/jquery.validate.min.js"></script>
  <!-- JS Libraies -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/bundles/jquery-steps/jquery.steps.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/page/form-wizard.js"></script>
  <!-- Template JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo constant ('URL');?>src/principal_uno/assets/js/custom.js"></script>
</body>


<!-- form-wizard.html  21 Nov 2019 03:55:20 GMT -->
</html>