<?php 

include_once 'sesiones/session_admin.php';

class Notas_historial extends Controller
{

  function __construct()
  {
   parent::__construct();
   $this->view->mensaje = "";
 }
////Vista-Administrador
function render(){ 
  $secciones=$this->model->getSeccionesHistorial();
  $this->view->secciones=$secciones;
  $this->view->render('notas_historial/index');
}

////Vista-docente
function historial($param=null){ 

  $id_persona=$_SESSION['id_persona'];
  $this->view->id_persona=$id_persona;

  $secciones=$this->model->getDocenteHistorial($id_persona);
  $this->view->secciones=$secciones;

  $this->view->render('notas_historial/index_notas');
}
/////////
function lista_historial($param=null){ 
  
  $id_seccion=$param[0];
  $this->view->id_seccion=$id_seccion;
  
  $estudiantes=$this->model->NotasHistorial_Otro($id_seccion);
  $this->view->estudiantes=$estudiantes;
  
  $this->view->render('notas_historial/notas_otro');
}

function lista_historial_idioma($param=null){ 
  
  $id_seccion=$param[0];
  $this->view->id_seccion=$id_seccion;

  $id_persona=$_SESSION['id_persona'];
  $this->view->id_persona=$id_persona;
  
  $estudiantes=$this->model->NotasHistorial_Idioma($id_seccion);
  $this->view->estudiantes=$estudiantes;
  
  $this->view->render('notas_historial/notas_idioma');
}

function lista_egresados(){ 
  $egresados = $this->view->datos = $this->model->getEgresados();
  $this->view->egresados = $egresados;
  $this->view->render('notas_historial/lista_egresados');
}





} 
?>