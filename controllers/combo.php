<?php
    //COMBOS DEPENDIENTES 
    include_once 'models/confucio.php';
    class Combo extends Controller {
        function __construct(){
            parent::__construct();

        }

                //CONBO DEPENDIENTE PAIS 
                function getByPais(){
                    $paises=$this->model->getCatalogoEstatus('pais');
                    
                    echo '<option value="">Seleccione</option>';
                    foreach($paises as $row){
                        $pais=new Confucio();
                        $pais=$row;
                        echo '<option value="' . $pais->id. '">' . $pais->descripcion. '</option>' . "\n";
                    }
                }

                function getByPaisEstado(){
                    $id_pais=$_POST['pais'];
                    $estados=$this->model->getByPaisEstado($id_pais);
                    echo '<option value="">Seleccione</option>';
                    foreach($estados as $row){
                        $estado=new Confucio();
                        $estado=$row;
                        echo '<option value="' . $estado->id_estado. '">' . $estado->descripcion. '</option>' . "\n";
                    }
                }

                function getByEstadoMunicipio(){
                    $id_estado=$_POST['estado_1'];
                    $municipios=$this->model->getByEstadoMunicipio($id_estado);
                    echo '<option value="">Seleccione</option>';
                    foreach($municipios as $row){
                        $municipio=new Confucio();
                        $municipio=$row;
                        echo '<option value="' . $municipio->id_municipio. '">' . $municipio->descripcion. '</option>' . "\n";
                    }
                }


                function getByMunicipioParroquia(){
                    $id_municipio=$_POST['parroquia'];
                    $parroquias=$this->model->getByMunicipioParroquia($id_municipio);
                    echo '<option value="">Seleccione</option>';
                    foreach($parroquias as $row){
                        $parroquia=new Confucio();
                        $parroquia=$row;
                        echo '<option value="' . $parroquia->id_parroquia. '">' . $parroquia->descripcion. '</option>' . "\n";
                    }
                }
                //END COMBO DEPENDIENTE PAIS

            

    }

?>