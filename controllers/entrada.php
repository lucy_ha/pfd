<?php

include_once 'sesiones/session_admin.php'; 

class Entrada extends Controller{

function __construct(){
    parent::__construct();
}

function render(){

    $entradas=$this->model->getentrada();
    $this->view->entradas=$entradas;

     $this->view->render('entrada/index');
}


function Registrar(){

$this->view->render('entrada/registro');  

}

function Update($param=null){

  $id_portal=$param[0];
  $this->view->id_portal=$id_portal;

$noticia=$this->model->getbyID($id_portal);
$this->view->noticia=$noticia;

 $this->view->render('entrada/update');  
      
}


function lista(){

  $datos=$this->model->get();
  $this->view->datos=$datos;

  $this->view->render('entrada/list_requisito'); 

  }

  function lista_historia(){

    $history=$this->model->getHistoria();
    $this->view->history=$history;
  
    $this->view->render('entrada/list_historia'); 
  
    }

    function lista_redes(){

      $redes=$this->model->getRedes();
      $this->view->redes=$redes;
    
      $this->view->render('entrada/list_redes'); 
    
      }
    


function Update_Requisito($param=null){

  $id_requisito=$param[0];
  $this->view->id_requisito=$id_requisito;

  $requisito=$this->model->getByIDRequisito($id_requisito);
  $this->view->requisito=$requisito;

  $this->view->render('entrada/update_requisito');  
  }



  function Update_Redes($param=null){

    $id_redes=$param[0];
    $this->view->id_redes=$id_redes;
  
    $redes=$this->model->getByIDRedes($id_redes);
    $this->view->redes=$redes;
  
    $this->view->render('entrada/update_rs');  
    }



    function Update_Historia($param=null){

      $id_historia=$param[0];
      $this->view->id_historia=$id_historia;
    
      $historia=$this->model->getByIDHistoria($id_historia);
      $this->view->historia=$historia;
    
      $this->view->render('entrada/update_rh');  
      }





function agg(){
    $titulo = $_POST['titulo'];
    $contenido = $_POST['contenido'];

    //para la imagen
    $time = time();
    $imagen=date("Y-m-d (H:i:s)", $time).$_FILES['imagen']['name'];
    $ruta1= $_FILES['imagen']['tmp_name'];
    $destino1 = "src/img/". $imagen;
    $fecha=date('Y-m-d'); 

    if($this->model->insert([
        'titulo' => $titulo,
        'contenido' => $contenido, 
        'imagen'=>$imagen, 
        'destino1'=>$destino1,
         'ruta1'=>$ruta1
      ])){
        $mensaje='<div class="alert alert-success alert-dismissible show fade">
        <div class="alert-body">
        <button class="close" data-dismiss="alert">
        <span>&times;</span>
        </button>
        Entrada registrada correctamente.
        </div>
        </div>';
        $this->view->mensaje=$mensaje;
        $this->render();
        exit();
    }else{
      $mensaje='<div class="alert alert-danger alert-dismissible show fade">
      <div class="alert-body">
      <button class="close" data-dismiss="alert">
      <span>&times;</span>
      </button>
      Hubo un error al registrar la entrada
      </div>
      </div>';
      $this->view->mensaje=$mensaje;
      $this->render();
      exit();
    }

  
    }


function FormUpdate(){

  $titulo=$_POST['titulo'];
  $contenido=$_POST['contenido'];
  $id_portal=$_POST['id_portal'];


    //para la imagen
    $time = time();
    $file1=date("Y-m-d (H:i:s)", $time).$_FILES['file1']['name'];
    $ruta1= $_FILES['file1']['tmp_name'];
    $destino1 = "src/img/". $file1;
    

  if($this->model->update(['titulo'=>$titulo, 'contenido'=>$contenido, 'id_portal'=>$id_portal, 'file1'=>$file1, 'destino1'=>$destino1,'ruta1'=>$ruta1])){
      $mensaje='<div class="alert alert-success alert-dismissible show fade">
      <div class="alert-body">
      <button class="close" data-dismiss="alert">
      <span>&times;</span>
      </button>
      Noticia Modificada Correctamente
      </div>
      </div>';
      $this->view->mensaje=$mensaje;
      $this->render();
      exit();
  }else{
    $mensaje='<div class="alert alert-danger alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Hubo un error al registrar la entrada
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();
  }
  
}


function delete($param = null){
    $id_portal = $param[0];
  
    if($this->model->delete($id_portal)){
      $mensaje='<div class="alert alert-success alert-dismissible show fade">
      <div class="alert-body">
      <button class="close" data-dismiss="alert">
      <span>&times;</span>
      </button>
    La entrada fue eliminada...
      </div>
      </div>';
      $this->view->mensaje=$mensaje;
      $this->render();
      exit();
      
    }else{
      $mensaje='<div class="alert alert-danger alert-dismissible show fade">
      <div class="alert-body">
      <button class="close" data-dismiss="alert">
      <span>&times;</span>
      </button>
      Hubo un error al eliminar la entrada
      </div>
      </div>';
      $this->view->mensaje=$mensaje;
      $this->render();
      exit();
    }
  
  }



  function FormRequisito(){
    $monto_uno = $_POST['monto_uno'];
    $monto_dos = $_POST['monto_dos'];
    $inicio_clase = $_POST['inicio_clase'];
    $inscripcion_inicio = $_POST['inscripcion_inicio'];
    $inscripcion_fin = $_POST['inscripcion_fin'];
    $id_requisito = $_POST['id_requisito'];

    if($this->model->UpdateRequisito([
        'monto_uno' => $monto_uno,
        'monto_dos' => $monto_dos, 
        'inicio_clase'=>$inicio_clase, 
        'inscripcion_inicio'=>$inscripcion_inicio,
        'inscripcion_fin'=>$inscripcion_fin,
        'id_requisito'=>$id_requisito
      ])){
        $mensaje='<div class="alert alert-success alert-dismissible show fade">
        <div class="alert-body">
        <button class="close" data-dismiss="alert">
        <span>&times;</span>
        </button>
        Datos para requisitos actualizada correctamente.
        </div>
        </div>';
        $this->view->mensaje=$mensaje;
        $this->lista();
        exit();
    }else{
      $mensaje='<div class="alert alert-danger alert-dismissible show fade">
      <div class="alert-body">
      <button class="close" data-dismiss="alert">
      <span>&times;</span>
      </button>
      Hubo un error al registrar los Datos para Requisitos.
      </div>
      </div>';
      $this->view->mensaje=$mensaje;
      $this->lista();
      exit();
    }

  
    }


    function FormHistoria(){
      $descripcion_historia = $_POST['descripcion_historia'];
      $descripcion_mision = $_POST['descripcion_mision'];
      $descripcion_vision = $_POST['descripcion_vision'];
      $id_historia = $_POST['id_historia'];
  
      if($this->model->UpdateHistoria([
          'descripcion_historia' => $descripcion_historia,
          'descripcion_mision' => $descripcion_mision, 
          'descripcion_vision'=>$descripcion_vision, 
          'id_historia'=>$id_historia
        ])){
          $mensaje='<div class="alert alert-success alert-dismissible show fade">
          <div class="alert-body">
          <button class="close" data-dismiss="alert">
          <span>&times;</span>
          </button>
          La Reseña Historica fue actualizada correctamente.
          </div>
          </div>';
          $this->view->mensaje=$mensaje;
          $this->lista_historia();
          exit();
      }else{
        $mensaje='<div class="alert alert-danger alert-dismissible show fade">
        <div class="alert-body">
        <button class="close" data-dismiss="alert">
        <span>&times;</span>
        </button>
        Hubo un error al actualizar los Datos.
        </div>
        </div>';
        $this->view->mensaje=$mensaje;
        $this->lista_historia();
        exit();
      }
  
    
      }


      function FormRedes(){
        $id_redes = $_POST['id_redes'];
        $link_facebook = $_POST['link_facebook'];
        $link_twitter = $_POST['link_twitter'];
        $link_instagram = $_POST['link_instagram'];
    
        if($this->model->UpdateRedes([
            'id_redes' => $id_redes,
            'link_facebook' => $link_facebook, 
            'link_twitter'=>$link_twitter, 
            'link_instagram'=>$link_instagram
          ])){
            $mensaje='<div class="alert alert-success alert-dismissible show fade">
            <div class="alert-body">
            <button class="close" data-dismiss="alert">
            <span>&times;</span>
            </button>
            Las redes sociales fueron actualizada correctamente.
            </div>
            </div>';
            $this->view->mensaje=$mensaje;
            $this->lista_redes();
            exit();
        }else{
          $mensaje='<div class="alert alert-danger alert-dismissible show fade">
          <div class="alert-body">
          <button class="close" data-dismiss="alert">
          <span>&times;</span>
          </button>
          Hubo un error al actualizar los Datos.
          </div>
          </div>';
          $this->view->mensaje=$mensaje;
          $this->lista_redes();
          exit();
        }
    
      
        }
    
  

}


?>