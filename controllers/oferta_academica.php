<?php 

include_once 'sesiones/session_admin.php';

class Oferta_academica extends Controller
{
  
  function __construct()
  {
   parent::__construct();
   $this->view->mensaje = "";
 }

///Vista de ofertas académicas inactivas
 function render(){

  $ofertas=$this->model->getOfertas();
  $this->view->ofertas=$ofertas;

  $this->view->render('oferta_academica/index');
}
///Vista de ofertas académicas activas
function ofertas_activas(){

  $ofertas=$this->model->getOfertasActivas();
  $this->view->ofertas=$ofertas;

  $this->view->render('oferta_academica/ofertas_activas');
}
///////////////////////////////////////////OFERTA-ACADEMICA///////////////////////////////////////////////////
/////////////ACTIVAR-OFERTA/////////////////
function activar($param=null){ 

  $id_oferta_academica=$param[0];
  $this->view->id_oferta_academica=$id_oferta_academica;
  
  $oferta=$this->model->getDetalles_Seccion($id_oferta_academica);
  $this->view->oferta=$oferta;

  $datos_h=$this->model->getbyID_dias($id_oferta_academica);
  $this->view->datos_h=$datos_h;

  $j=1;
  foreach($datos_h as $row){
   $dias_h=new Confucio();
   $dias_h=$row;
   $dia_h = $this->model->getDiabyID($id_oferta_academica, $dias_h->id_dia);

   $dia[$j]=[
     'id_dia'.$j=>$dia_h->id_dia,
     'dia'.$j=>$dia_h->dia
   ];

   $this->view->dia[$j]=$dia[$j];
   $j++;

 }
 $this->view->render('oferta_academica/activar_oferta');
}
/////
function ActivateOferta(){
  $id_oferta_academica = $_POST['id_oferta_academica'];
  $fecha_inicio = $_POST['fecha_inicio'];
  $fecha_fin = $_POST['fecha_fin'];
  
  if($this->model->activar([
    'id_oferta_academica' => $id_oferta_academica,
    'fecha_inicio' => $fecha_inicio, 
    'fecha_fin' => $fecha_fin
  ])){

    $mensaje='<div class="alert alert-success alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    La oferta académica fue activada.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->ofertas_activas();
    exit();

  }else{
    $mensaje='<div class="alert alert-danger alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Hubo un error al activar la oferta académica.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->ofertas_activas();
    exit();
  }
}

///////////////ELIMINAR-OFERTA/////////////////
function eliminar($param = null){
  $id_oferta_academica = $param[0];

  if($this->model->delete($id_oferta_academica)){
    $mensaje='<div class="alert alert-success alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    La oferta académica fue eliminada exitosamente.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();

  }else{
    $mensaje='<div class="alert alert-danger alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Hubo un error al eliminar la oferta académica.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();
  }
}
////////////////////////////////////////////////OFERTAS-ACTIVAS///////////////////////////////////////////////////
/////EDITAR-FECHA//////////
function editar($param=null){ 

  $id_activar_oferta=$param[0];
  $this->view->id_activar_oferta=$id_activar_oferta;

  $id_oferta_academica=$param[0];
  $this->view->id_oferta_academica=$id_oferta_academica;
  
  $detalles=$this->model->getDetalles_Seccion($id_oferta_academica);
  $this->view->detalles=$detalles;

  $datos_h=$this->model->getbyID_dias($id_oferta_academica);
  $this->view->datos_h=$datos_h;

  $j=1;
  foreach($datos_h as $row){
   $dias_h=new Confucio();
   $dias_h=$row;
   $dia_h = $this->model->getDiabyID($id_oferta_academica, $dias_h->id_dia);

   $dia[$j]=[
     'id_dia'.$j=>$dia_h->id_dia,
     'dia'.$j=>$dia_h->dia
   ];

   $this->view->dia[$j]=$dia[$j];
   $j++;

 }

  $oferta=$this->model->getActivar_Oferta($id_activar_oferta);
  $this->view->oferta=$oferta;

  $this->view->render('oferta_academica/editar');
}
////
function UpdateDate(){

  $id_activar_oferta = $_POST['id_activar_oferta'];
  $fecha_inicio = $_POST['fecha_inicio'];
  $fecha_fin = $_POST['fecha_fin'];

  if($this->model->update([
    'id_activar_oferta'=>$id_activar_oferta, 
    'fecha_inicio'=>$fecha_inicio,
    'fecha_fin'=>$fecha_fin
  ])){

    $mensaje='<div class="alert alert-success alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Los datos fueron actualizados exitosamente.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->ofertas_activas();
    exit();

  }else{
    $mensaje='<div class="alert alert-danger alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Hubo un error al actualizar los datos.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->ofertas_activas();
    exit();
  }
}

/////////////DESACTIVAR-OFERTA////////////////
function desactivar($param = null){
  $id_oferta_academica = $param[0];

  if($this->model->desactivar($id_oferta_academica)){
    $mensaje='<div class="alert alert-success alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    La oferta académica fue desactivada.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();

  }else{
    $mensaje='<div class="alert alert-danger alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    No se pudo desactivar la oferta académica.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();
  }
}

} 
?>