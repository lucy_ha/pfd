<?php

class Login extends Controller{

    function __construct()
    {
        parent::__construct();
    }


    function render(){ 
        session_start();
        if(isset($_SESSION['id_persona'])){
            header("Location: ".constant('URL')."home");
        }else{
            $this->view->render('login/index');
        }
    }

    function viewHome(){
        header("Refresh: 0; URL= ".constant('URL')."home");
    }

    function iniciar(){

        $usuario=$_POST['usuario'];
        $clave=$_POST['clave'];


      /////////////// CAPTCHA /////////////////////////////////  
        session_start();
        $catpcha=$_POST['captcha'];

        $check=false;
        if(isset($_SESSION['captcha'])){

            if($catpcha == $_SESSION['captcha']){
                $check = true;

            }else if($check==false){

                $mensaje= '<div class="alert alert-warning alert-dismissible show fade">
                <div class="alert-body">
                  <button class="close" data-dismiss="alert">
                    <span>&times;</span>
                  </button>
                  Error al Validar el captcha
                </div>
              </div>';

                $this->view->mensaje=$mensaje;
                $this->render();
                exit();

            }
            unset($_SESSION['captcha']);
        }
///////////////////////////// END CAPTCHA ///////////////////////

if($this->model->getStatus([
    'usuario'=> $usuario, 
    'clave' => $clave,
    'estatus' => 'Inactivo'
    ])){
    
        $mensaje='<div class="alert alert-danger alert-dismissible show fade">
        <div class="alert-body">
          <button class="close" data-dismiss="alert">
            <span>&times;</span>
          </button>
          Este Usuario se encuentra Inactivo. Debe Ingresar con un Usuario Activo en el Sistema.
        </div>
      </div>';

    $this->view->mensaje=$mensaje;
    $this->render();
    exit();
    
} 

if($this->model->getLogin(['usuario'=>$usuario, 'clave'=>$clave])){

    $this->viewHome();

}else{

    $mensaje='<div class="alert alert-danger alert-dismissible show fade">
    <div class="alert-body">
      <button class="close" data-dismiss="alert">
        <span>&times;</span>
      </button>
      Correo o Clave Incorrecta,<br> Por favor Vuelve a Intentarlo.
    </div>
  </div>';

}

$this->view->mensaje=$mensaje;
$this->render();

    }


    function logout(){
        session_start();

        // Destruir todas las variables de sesión.
        $_SESSION = array(
                        $_SESSION["id_persona"],
                        $_SESSION['primer_nombre'], 
                        $_SESSION['primer_apellido'], 
                        $_SESSION['perfil'],
                        $_SESSION['usuario'],
                        $_SESSION['id_perfil']
                        );
        //var_dump($_SESSION);
        // Si se desea destruir la sesión completamente, borre también la cookie de sesión.
        // Nota: ¡Esto destruirá la sesión, y no la información de la sesión!
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

        // Finalmente, destruir la sesión.
        session_destroy();
        session_unset();
        
        
        if(!headers_sent()) {
            echo '<script>
        alert("¡Hasta Pronto!");
        location.href="'.constant('URL').'main";
        </script>';
        }
    }


    function validacionCaptcha(){
        session_start();
        echo $_SESSION['captcha'];
    }

}
    ?>