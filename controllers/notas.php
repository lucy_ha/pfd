<?php 

include_once 'sesiones/session_admin.php';

class Notas extends Controller
{

  function __construct()
  {
   parent::__construct();
   $this->view->mensaje = "";
 }
///Vista Docente
 function lista_secciones($param=null){ 

  $id_persona=$_SESSION['id_persona'];
  $this->view->id_persona=$id_persona;
  
  $secciones=$this->model->getSecciones_Docente($id_persona);
  $this->view->secciones=$secciones;

  $fechas=$this->model->getFechas();
  $this->view->fechas=$fechas;
  
  $this->view->render('notas/index');
}
///Vista Administrador/Analista
function secciones(){ 

  $id_persona=$_SESSION['id_persona'];
  $this->view->id_persona=$id_persona;

  $secciones=$this->model->getSecciones();
  $this->view->secciones=$secciones;

  $roles=$this->model->getbyID_roles($id_persona);
  $this->view->roles=$roles;

  $this->view->render('notas/notas');
}

function lista_estudiantes($param=null){ 
  
  $id_seccion=$param[0];
  $this->view->id_seccion=$id_seccion;
  
  $estudiantes=$this->model->getEstudiantes($id_seccion);
  $this->view->estudiantes=$estudiantes;
  
  $this->view->render('notas/carga_notas');
}

function lista_estudiantes_idioma($param=null){ 
  
  $id_seccion=$param[0];
  $this->view->id_seccion=$id_seccion;
  
  $estudiantes=$this->model->getEstudiantes($id_seccion);
  $this->view->estudiantes=$estudiantes;

  $prosecucion=$this->model->getProsecucion($id_seccion);
  $this->view->prosecucion=$prosecucion;
  
  $this->view->render('notas/carga_notas_idioma');
}

////CARGA-DE-NOTAS
function cargar_notas(){

  $id_inscripcion=$_POST['id_inscripcion'];
  $id_persona=$_POST['id_persona'];
  $id_nivel=$_POST['id_nivel'];
  $id_seccion=$_POST['id_seccion'];
  $nota=$_POST['nota'];
  
  if($this->model->getExisteNota([
    'id_inscripcion'=> $id_inscripcion
  ])){
    
    $mensaje='<div class="alert alert-warning alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Ya se han registrado las notas de los estudiantes de esta sección.
    </div>
    </div>';

    $this->view->mensaje=$mensaje;
    $this->lista_secciones();
    exit();

  }else{
    
    if($this->model->insert([
      'id_inscripcion'=>$id_inscripcion, 
      'id_persona'=>$id_persona,
      'id_nivel' => $id_nivel,
      'id_seccion' => $id_seccion,
      'nota'=>$nota
    ])){

      $mensaje='<div class="alert alert-success alert-dismissible show fade">
      <div class="alert-body">
      <button class="close" data-dismiss="alert">
      <span>&times;</span>
      </button>
      Las notas de los estudiantes fueron cargadas exitosamente.
      </div>
      </div>';
      $this->view->mensaje=$mensaje;
      $this->lista_secciones();
      exit();

    }else{
      $mensaje='<div class="alert alert-danger alert-dismissible show fade">
      <div class="alert-body">
      <button class="close" data-dismiss="alert">
      <span>&times;</span>
      </button>
      Hubo un error al cargar las notas de los estudiantes.
      </div>
      </div>';
      $this->view->mensaje=$mensaje;
      $this->lista_secciones();
      exit();
    }
  }
} 

function cargar_notas_idioma(){

  $id_inscripcion=$_POST['id_inscripcion'];
  $id_persona=$_POST['id_persona'];
  $id_nivel=$_POST['id_nivel'];
  $id_seccion=$_POST['id_seccion'];
  $nota=$_POST['nota'];

  if($this->model->getExisteNotaIdioma([
    'id_inscripcion'=> $id_inscripcion
  ])){
    
    $mensaje='<div class="alert alert-warning alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Ya se han registrado las notas de los estudiantes de esta sección.
    </div>
    </div>';
    
    $this->view->mensaje=$mensaje;
    $this->lista_secciones();
    exit();
    
  }else{
    
    if($this->model->insertPro([
      'id_inscripcion'=>$id_inscripcion, 
      'id_persona'=>$id_persona,
      'id_nivel' => $id_nivel,
      'id_seccion' => $id_seccion,
      'nota'=>$nota
    ])){
      
      $mensaje= '<div class="alert alert-success alert-dismissible show fade">
      <div class="alert-body">
      <button class="close" data-dismiss="alert">
      <span>&times;</span>
      </button>
      Las notas de los estudiantes fueron cargadas exitosamente.
      </div>
      </div>';
      $this->view->mensaje=$mensaje;
      $this->lista_secciones();
      exit();
      
    }else{
      $mensaje='<div class="alert alert-danger alert-dismissible show fade">
      <div class="alert-body">
      <button class="close" data-dismiss="alert">
      <span>&times;</span>
      </button>
      Hubo un error al cargar las notas de los estudiantes.
      </div>
      </div>';
      $this->view->mensaje=$mensaje;
      $this->lista_secciones();
      exit();
    }
  }
}

////EDITAR-NOTAS
function lista_notas($param=null){ 
  
  $id_seccion=$param[0];
  $this->view->id_seccion=$id_seccion;

  $notas=$this->model->getNotas($id_seccion);
  $this->view->notas=$notas;

  $this->view->render('notas/editar_notas');
}

function editar_notas(){
  $id_inscripcion = $_POST['id_inscripcion'];
  $nota = $_POST['nota'];
  
  if($this->model->update([
    'id_inscripcion' => $id_inscripcion, 
    'nota' => $nota
  ])){

    $mensaje='<div class="alert alert-success alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Las notas de los estudiantes fueron editas exitosamente.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->secciones();
    exit();

  }else{
    $mensaje='<div class="alert alert-danger alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Hubo un error al editar las notas de los estudiantes.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->secciones();
    exit();
  }
}

////EDITAR-NOTAS-IDIOMA
function lista_notas_idioma($param=null){ 
  
  $id_seccion=$param[0];
  $this->view->id_seccion=$id_seccion;

  $notas=$this->model->getNotasIdioma($id_seccion);
  $this->view->notas=$notas;

  $this->view->render('notas/editar_notas_idioma');
}

function editar_notas_idioma(){
  $id_inscripcion = $_POST['id_inscripcion'];
  $nota = $_POST['nota'];
  
  if($this->model->updateIdioma([
    'id_inscripcion' => $id_inscripcion, 
    'nota' => $nota
  ])){

    $mensaje='<div class="alert alert-success alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Las notas de los estudiantes fueron editas exitosamente.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->secciones();
    exit();

  }else{
    $mensaje='<div class="alert alert-danger alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Hubo un error al editar las notas de loa estudiantes.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->secciones();
    exit();
  }
}




} 
?>