<?php 

include_once 'sesiones/session_admin.php';

class Seccion extends Controller {

  function __construct()
  {
   parent::__construct();
   $this->view->mensaje = "";
 }

 function render(){ 
   $secciones = $this->model->get();
   $this->view->secciones = $secciones;
   $this->view->render('seccion/index');
 }

////////////////////////////////////////REGISTRAR////////////////////////////////////////////////
 function registrar(){ 

  $docentes=$this->model->getDocentes();
  $this->view->docentes=$docentes;

  $periodos=$this->model->getCatalogo('periodo');
  $this->view->periodos=$periodos;

  $actividades=$this->model->getCatalogo('actividad');
  $this->view->actividades=$actividades;

  $niveles=$this->model->getCatalogo('nivel');
  $this->view->niveles=$niveles;

  $modalidades=$this->model->getCatalogo('modalidad');
  $this->view->modalidades=$modalidades;

  $turnos=$this->model->getCatalogo('turno');
  $this->view->turnos=$turnos;

  $dias=$this->model->getCatalogo('dia');
  $this->view->dias=$dias;

  $this->view->render('seccion/registrar');
}
///
function RegisterSeccion(){
  $descripcion = $_POST['descripcion'];
  $cupos = $_POST['cupos'];
  $id_docente = $_POST['id_docente'];
  $id_periodo = $_POST['id_periodo'];
  $id_actividad = $_POST['id_actividad'];
  $id_nivel = $_POST['id_nivel'];
  $id_modalidad = $_POST['id_modalidad'];
  $id_turno = $_POST['id_turno'];
  $dia = $_POST['dia'];
  $hora_inicio = $_POST['hora_inicio'];
  $hora_fin = $_POST['hora_fin'];
  
  if($this->model->insert([
    'descripcion' => $descripcion, 
    'cupos' => $cupos,
    'id_docente' => $id_docente,
    'id_periodo'=>$id_periodo,
    'id_actividad' => $id_actividad, 
    'id_nivel' => $id_nivel, 
    'id_modalidad' => $id_modalidad,
    'id_turno' => $id_turno, 
    'dia' => $dia, 
    'hora_inicio' => $hora_inicio, 
    'hora_fin' => $hora_fin
  ])){

    $mensaje='<div class="alert alert-success alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    La sección fue registrada exitosamente.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();

  }else{
    $mensaje='<div class="alert alert-danger alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Hubo un error al registrar la sección.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();
  }
}

////////////////////////////////////////EDITAR//////////////////////////////////////////////
function editar($param=null){

  $id_seccion=$param[0];
  $this->view->id_seccion=$id_seccion;

  $seccion=$this->model->getbyID($id_seccion);
  $this->view->seccion=$seccion;

  $docentes=$this->model->getDocentes();
  $this->view->docentes=$docentes;

  $actividades=$this->model->getCatalogo('actividad');
  $this->view->actividades=$actividades;

  $periodos=$this->model->getCatalogo('periodo');
  $this->view->periodos=$periodos;

  $niveles=$this->model->getCatalogo('nivel');
  $this->view->niveles=$niveles;

  $modalidades=$this->model->getCatalogo('modalidad');
  $this->view->modalidades=$modalidades;

  $turnos=$this->model->getCatalogo('turno');
  $this->view->turnos=$turnos;

  $dias=$this->model->getCatalogo('dia');
  $this->view->dias=$dias;

  $datos_h=$this->model->getbyID_dias($id_seccion);
  $this->view->datos_h=$datos_h;

  $j=1;
  foreach($datos_h as $row){
   $dias_h=new Confucio();
   $dias_h=$row;
   $dia_h = $this->model->getDiabyID($id_seccion, $dias_h->id_dia);

   $dia[$j]=[
     'id_dia'.$j=>$dia_h->id_dia,
     'dia'.$j=>$dia_h->dia
   ];

   $this->view->dia[$j]=$dia[$j];
   $j++;

 }

 $this->view->render('seccion/editar');
}
//////
function UpdateSeccion(){

  $descripcion = $_POST['descripcion'];
  $cupos = $_POST['cupos'];
  $id_docente = $_POST['id_docente'];
  $id_periodo = $_POST['id_periodo'];
  $id_actividad = $_POST['id_actividad'];
  $id_nivel = $_POST['id_nivel'];
  $id_modalidad = $_POST['id_modalidad'];
  $id_turno = $_POST['id_turno'];
  $dia = $_POST['dia'];
  $hora_inicio = $_POST['hora_inicio'];
  $hora_fin = $_POST['hora_fin'];
  $id_horario  = $_POST['id_horario'];
  $id_seccion = $_POST['id_seccion'];
  
  if($this->model->update([
    'descripcion'=>$descripcion, 
    'cupos' => $cupos,
    'id_docente'=>$id_docente,
    'id_periodo'=>$id_periodo,
    'id_actividad'=>$id_actividad, 
    'id_nivel'=>$id_nivel,
    'id_modalidad'=>$id_modalidad, 
    'id_turno'=>$id_turno, 
    'dia'=>$dia,
    'hora_inicio'=>$hora_inicio, 
    'hora_fin'=>$hora_fin,
    'id_horario'=>$id_horario,
    'id_seccion'=>$id_seccion
  ])){

    $mensaje='<div class="alert alert-success alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Los datos de la sección fuerron actualizados exitosamente.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();

  }else{
    $mensaje='<div class="alert alert-danger alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Hubo un error al editar la sección.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();
  }
}

////////////////////////////////////////ELIMINAR////////////////////////////////////////////
function eliminar($param=null){
  $id_seccion = $param[0];

  if($this->model->delete($id_seccion)){
    $mensaje='<div class="alert alert-success alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    La sección fue eliminada exitosamente.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();

  }else{
    $mensaje='<div class="alert alert-danger alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Hubo un error al eliminar la sección. <br>
    Elimine primero la oferta académica asociada a la sección.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();
  }
}

////////////////////////////////////////////DETALLES-SECCIÓN//////////////////////////////////////////////
function detalles_seccion($param=null){ 

  $id_seccion=$param[0];
  $this->view->id_seccion=$id_seccion;

  $seccion=$this->model->getDetalles_Seccion($id_seccion);
  $this->view->seccion=$seccion;

  $datos_h=$this->model->getbyID_dias($id_seccion);
  $this->view->datos_h=$datos_h;

  $j=1;
  foreach($datos_h as $row){
   $dias_h=new Confucio();
   $dias_h=$row;
   $dia_h = $this->model->getDiabyID($id_seccion, $dias_h->id_dia);

   $dia[$j]=[
     'id_dia'.$j=>$dia_h->id_dia,
     'dia'.$j=>$dia_h->dia
   ];

   $this->view->dia[$j]=$dia[$j];
   $j++;

 }
 $this->view->render('seccion/horario_seccion');
}

function lista_estudiantes($param=null){ 

  $id_seccion=$param[0];
  $this->view->id_seccion=$id_seccion;

  $seccion=$this->model->getDetalles_Seccion($id_seccion);
  $this->view->seccion=$seccion;

  $estudiantes=$this->model->getEstudiantes($id_seccion);
  $this->view->estudiantes=$estudiantes;

  $this->view->render('seccion/lista_estudiantes');
}

function reporte_estudiantes($param=null){ 

  $id_seccion=$param[0];
  $this->view->id_seccion=$id_seccion;

  $seccion=$this->model->getDetalles_Seccion($id_seccion);
  $this->view->seccion=$seccion;

  $datos_h=$this->model->getbyID_dias($id_seccion);
  $this->view->datos_h=$datos_h;

  $j=1;
  foreach($datos_h as $row){
   $dias_h=new Confucio();
   $dias_h=$row;
   $dia_h = $this->model->getDiabyID($id_seccion, $dias_h->id_dia);

   $dia[$j]=[
     'id_dia'.$j=>$dia_h->id_dia,
     'dia'.$j=>$dia_h->dia
   ];

   $this->view->dia[$j]=$dia[$j];
   $j++;

 }

 $estudiantes=$this->model->getEstudiantes($id_seccion);
 $this->view->estudiantes=$estudiantes;

 $this->view->render('seccion/reportes_estudiantes');
}

////////////////////// DATOS PARA REGISTRAR ASPIRANTES EN SECCION /////////////////////

function RegistrarAspirante($param=null){

  $id_oferta_academica=$param[0];
  $this->view->id_oferta_academica=$id_oferta_academica;

  $tipos=$this->model->getCatalogo('tipo_documento_identidad');
  $this->view->tipos=$tipos;

  $etnia=$this->model->getCatalogo('etnia');
  $this->view->etnia=$etnia;

  $paises=$this->model->getCatalogo('pais');
  $this->view->paises=$paises;

  $estados=$this->model->getCatalogo('estado');
  $this->view->estados=$estados;

  $tipo_dis=$this->model->getCatalogo('tipo_discapacidad');
  $this->view->tipo_dis=$tipo_dis;

  $nivel_aca=$this->model->getCatalogo('nivel_academico');
  $this->view->nivel_aca=$nivel_aca;

  $civiles=$this->model->getEstadoCivil();
  $this->view->civiles=$civiles;

  $cargos=$this->model->getCatalogo('cargo');
  $this->view->cargos=$cargos;

  $profesiones=$this->model->getCatalogo('profesion');
  $this->view->profesiones=$profesiones;

  $oferta=$this->model->getIdOferta($id_oferta_academica);
  $this->view->oferta=$oferta;

  $this->view->render('seccion/registrar_aspirante');
}



function FormAspirante(){

//datos personales
$primer_nombre=$_POST['primer_nombre'];
$segundo_nombre=$_POST['segundo_nombre'];
$primer_apellido=$_POST['primer_apellido'];
$segundo_apellido=$_POST['segundo_apellido'];
$identificacion=$_POST['identificacion'];
$id_civil=$_POST['id_civil'];

$telefono=$_POST['telefono'];
$correo=$_POST['correo'];
$genero=$_POST['genero'];
$id_tipo_documento_identidad=$_POST['id_tipo_documento_identidad'];

//datos de nacimiento
$fecha_nacimiento=$_POST['fecha_nacimiento'];
$pais=$_POST['pais'];

//datos de direccion_persona
$direccion=$_POST['direccion'];
$telefono_habitacion=$_POST['telefono_habitacion'];
$estado=$_POST['estado'];
$etnia=$_POST['etnia'];

//datos laborales
$empleo=$_POST['empleo'];
$telefono_institucion=$_POST['telefono_institucion'];
$correo_institucion=$_POST['correo_institucion'];   
$direccion_institucion=$_POST['direccion_institucion'];
$inst_publica=$_POST['inst_publica'];
$inst_privada=$_POST['inst_privada'];
$organismo=$_POST['organismo'];
$inst_inv=$_POST['inst_inv'];
$tipo_institucion=$_POST['tipo_institucion'];
$trabajador_ubv=$_POST['trabajador_ubv'];
$id_cargo=$_POST['id_cargo']; 
$state=$_POST['state'];
$id_profesion=$_POST['id_profesion'];    

//datos interes
$discapacidad=$_POST['discapacidad'];
$grupo_sanguineo=$_POST['grupo_sanguineo'];
$enfermedad=$_POST['enfermedad'];   
$descripcion_enfermedad=$_POST['descripcion_enfermedad'];
$idioma=$_POST['idioma']; //id del idioma
$descripcion=$_POST['descripcion']; //dato booleano
$id_tipo_discapacidad=$_POST['id_tipo_discapacidad'];  
$tratamiento=$_POST['tratamiento'];

//datos oferta
$id_oferta_academica=$_POST['id_oferta_academica'];

//datos experiencia academica
$id_nivel_academico=$_POST['id_nivel_academico'];
$institucion_nivel=$_POST['institucion_nivel'];
$estudiante_ubv=$_POST['estudiante_ubv'];
$mision_sucre=$_POST['mision_sucre'];


$fecha=date('Y-m-d'); 

$resultado= $fecha-$fecha_nacimiento;


if($this->model->Existente($identificacion)){

  ?>

  <script>

  alert('Este aspirante ya se encuentra inscrito en el sistema.');
    location.href='<?php echo constant ('URL')."seccion/validar/".$id_oferta_academica?>';
  
    </script>
    
<?php  

  $this->view->mensaje=$mensaje;
  $this->render();
} 

else if($resultado < 15) {

  $mensaje='<div class="alert alert-danger alert-dismissible show fade">
  <div class="alert-body">
    <button class="close" data-dismiss="alert">
      <span>&times;</span>
    </button>
    Usted no es mayor de 15 años.
  </div>
  </div>';
  

  $this->view->mensaje=$mensaje;
  $this->render();
}else{

if($this->model->InsertAspirante([ 
    'primer_nombre'=>$primer_nombre,
    'segundo_nombre'=>$segundo_nombre,
    'primer_apellido'=>$primer_apellido,
    'segundo_apellido'=>$segundo_apellido,
    'identificacion'=>$identificacion,
    'telefono'=>$telefono,
    'correo'=>$correo,
    'genero'=>$genero,
    'id_tipo_documento_identidad'=>$id_tipo_documento_identidad,
    'fecha_nacimiento'=>$fecha_nacimiento,
    'pais'=>$pais,
    'direccion'=>$direccion,
    'telefono_habitacion'=>$telefono_habitacion,
    'estado'=>$estado,
    'etnia'=>$etnia,
    'empleo'=>$empleo,
    'telefono_institucion'=>$telefono_institucion,
    'correo_institucion'=>$correo_institucion,
    'direccion_institucion'=>$direccion_institucion,
    'inst_publica'=>$inst_publica,
    'inst_privada'=>$inst_privada,
    'organismo'=>$organismo,
    'inst_inv'=>$inst_inv,
    'tipo_institucion'=>$tipo_institucion,
    'trabajador_ubv'=>$trabajador_ubv,
    'id_cargo'=>$id_cargo,
    'state'=>$state,
    'discapacidad'=>$discapacidad,
    'grupo_sanguineo'=>$grupo_sanguineo,
    'enfermedad'=>$enfermedad,
    'descripcion_enfermedad'=>$descripcion_enfermedad,
    'tratamiento'=>$tratamiento,
    'estudiante_ubv'=>$estudiante_ubv,
    'mision_sucre'=>$mision_sucre,
    'idioma'=>$idioma,
    'descripcion'=>$descripcion,
    'id_tipo_discapacidad'=>$id_tipo_discapacidad,
    'id_nivel_academico'=>$id_nivel_academico,
    'id_civil'=>$id_civil,
    'id_profesion'=>$id_profesion,
    'institucion_nivel'=>$institucion_nivel,
    'id_oferta_academica'=>$id_oferta_academica
])){ ?>

  <script>

  alert('Ha realizado exitosamente el Pre-ingreso del Aspirante en el Instituto Confucio.');
    location.href='<?php echo constant ('URL')."pre_inscripcion/solicitud"?>';
  
    </script>
    
<?php  
}else{

$mensaje= '<div class="alert alert-warning alert-dismissible show fade">
<div class="alert-body">
<button class="close" data-dismiss="alert">
<span>&times;</span>
</button>
Hubo un error al Registrar los Datos Solicitados Del Aspirante.
</div>
</div>';

}

$this->view->mensaje=$mensaje;
$this->render();
}


}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

function Validar($param=null){

  $id_oferta_academica=$param[0];
  $this->view->id_oferta_academica=$id_oferta_academica;

  $categoria=$this->model->getCV($id_oferta_academica);
  $this->view->categoria=$categoria;
      
  $this->view->render('seccion/registrado');
}


function ValidarCategoria(){

  $identificacion=$_POST['identificacion'];
  $categoria=$_POST['categoria'];
  $id_oferta_academica=$_POST['id_oferta_academica'];
/////////////// CAPTCHA /////////////////////////////////  
  session_start();
  $catpcha=$_POST['captcha'];

  $check=false;
  if(isset($_SESSION['captcha'])){

      if($catpcha == $_SESSION['captcha']){
          $check = true;

      }else if($check==false){

          /*$mensaje= '<div class="alert alert-warning alert-dismissible show fade">
          <div class="alert-body">
            <button class="close" data-dismiss="alert">
              <span>&times;</span>
            </button>
            Error al Validar el captcha
          </div>
        </div>';

          $this->view->mensaje=$mensaje;*/
          ?>

          <script>
        
          alert('Error al Validar el captcha');
            location.href='<?php echo constant ('URL')."seccion/validar/".$id_oferta_academica?>';
            </script>
            
        <?php  
          exit();
      }
      unset($_SESSION['captcha']);
  }
///////////////////////////// END CAPTCHA ///////////////////////

if($this->model->getCategoria([
'identificacion'=> $identificacion, 
'categoria' => $categoria,
'id_oferta_academica'=>$id_oferta_academica
])){

 /* $mensaje='<div class="alert alert-danger alert-dismissible show fade">
  <div class="alert-body">
    <button class="close" data-dismiss="alert">
      <span>&times;</span>
    </button>
    Usted ya se encuentra Pre-inscrito en una Oferta Académica.
  </div>
</div>';

$this->view->mensaje=$mensaje;*/
?>

<script>

alert('Se ha registrado exitosamente la Oferta Académica elegida.');
location.href='<?php echo constant ('URL')."seccion"?>';
</script>

<?php  
exit();

} else{

  ?>

  <script>
  
  alert('A ocurrido un error al realizar esta Pre-inscripcion.');
  location.href='<?php echo constant ('URL')."seccion"?>';
  </script>
  
  <?php  

}

}


}
?>