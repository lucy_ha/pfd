<?php

include_once 'sesiones/session_admin.php'; 

class Perfil extends Controller{

    function __construct(){
        parent::__construct();
    }


    function render($param=null){

        $id_persona=$param[0];
        $this->view->id_persona=$id_persona;
    
        $datos=$this->model->getbyID_user($id_persona);
        $this->view->datos=$datos;

        $datos_u=$this->model->getbyID_roles($id_persona);
        $this->view->datos_u=$datos_u;
    
        $j=1;
        foreach($datos_u as $row){
            $roles_u=new Confucio();
            $roles_u=$row;
            $rol_u = $this->model->getRolbyID($id_persona, $roles_u->id_rol);
    
            $rol[$j]=[
                'id_usuario'.$j=>$rol_u->id_usuario,
                'id_rol'.$j=>$rol_u->id_rol,
                'rol'.$j=>$rol_u->rol
            ];
    
            $this->view->rol[$j]=$rol[$j];
            $j++;
    
        }

    $this->view->render('perfil/index');

    }


    //visualizar datos registrados del usuario///
function Update($param=null){

    $id_persona=$param[0];
    $this->view->id_persona=$id_persona;

    $datos=$this->model->getbyID_user($id_persona);
    $this->view->datos=$datos;

    $datos_u=$this->model->getbyID_roles($id_persona);
    $this->view->datos_u=$datos_u;

    $j=1;
    foreach($datos_u as $row){
        $roles_u=new Confucio();
        $roles_u=$row;
        $rol_u = $this->model->getRolbyID($id_persona, $roles_u->id_rol);

        $rol[$j]=[
            'id_usuario'.$j=>$rol_u->id_usuario,
            'id_rol'.$j=>$rol_u->id_rol,
            'rol'.$j=>$rol_u->rol
        ];

        $this->view->rol[$j]=$rol[$j];
        $j++;

    }

    $perfiles=$this->model->getPerfil();
    $this->view->perfiles=$perfiles;

    $roles=$this->model->getCatalogo('rol');
    $this->view->roles=$roles;

    $this->view->render('usuarios/update');
}


//visualizar datos personales registrados del usuario///
function Update_Date($param=null){

    $id_persona=$param[0];
    $this->view->id_persona=$id_persona;

    $datos=$this->model->getbyID_user($id_persona);
    $this->view->datos=$datos;

    $tipos=$this->model->getCatalogo('tipo_documento_identidad');
    $this->view->tipos=$tipos;

    $this->view->render('usuarios/update_date');
}


function FormUpdate(){

    $usuario=$_POST['usuario'];
    $perfil=$_POST['perfil'];
    $estatus=$_POST['estatus'];
    $clave=$_POST['clave'];
    $rol=$_POST['rol'];
    $id_persona=$_POST['id_persona'];

    if($this->model->update([
        'usuario'=>$usuario,
        'perfil'=>$perfil,
        'rol'=>$rol,
        'estatus'=>$estatus,
        'clave'=>$clave,
        'id_persona'=>$id_persona
        ]))
    {
        $mensaje='<div class="alert alert-success alert-dismissible show fade">
        <div class="alert-body">
          <button class="close" data-dismiss="alert">
            <span>&times;</span>
          </button>
          La configuración de Usuario fue hecho correctamente.
        </div>
      </div>';
      $this->view->mensaje=$mensaje;
      $this->render();
      exit();
          
    }else{
        $mensaje='<div class="alert alert-danger alert-dismissible show fade">
        <div class="alert-body">
          <button class="close" data-dismiss="alert">
            <span>&times;</span>
          </button>
          Hubo un error al Configurar el Usuario.
        </div>
      </div>';
      $this->view->mensaje=$mensaje;
      $this->render();
      exit();
    }

    //$this->view->mensaje=$mensaje;
    $this->render();

}


function FormUpdate_Date(){

$identificacion=$_POST['identificacion'];
$primer_nombre=$_POST['primer_nombre'];
$segundo_nombre=$_POST['segundo_nombre'];
$primer_apellido=$_POST['primer_apellido'];
$segundo_apellido=$_POST['segundo_apellido'];
$identificacion=$_POST['identificacion'];
$telefono=$_POST['telefono'];
$id_tipo_documento_identidad=$_POST['id_tipo_documento_identidad'];
$genero=$_POST['genero'];
$correo=$_POST['correo'];
$id_persona=$_POST['id_persona'];


if($this->model->update_date(['identificacion'=>$identificacion, 'primer_nombre'=>$primer_nombre, 'segundo_nombre'=>$segundo_nombre, 'primer_apellido'=>$primer_apellido, 'segundo_apellido'=>$segundo_apellido, 'telefono'=>$telefono, 'id_tipo_documento_identidad'=>$id_tipo_documento_identidad, 'genero'=>$genero, 'correo'=>$correo,'id_persona'=>$id_persona])){

$mensaje='<div class="alert alert-success alert-dismissible show fade">
<div class="alert-body">
  <button class="close" data-dismiss="alert">
    <span>&times;</span>
  </button>
 La Actualización de Datos del Usuario hecho correctamente.
</div>
</div>';
$this->view->mensaje=$mensaje;
$this->render();
exit();

}else{

$mensaje='<div class="alert alert-danger alert-dismissible show fade">
<div class="alert-body">
  <button class="close" data-dismiss="alert">
    <span>&times;</span>
  </button>
  Hubo un error al Actualizar los Datos del usuario.
</div>
</div>';

$this->view->mensaje=$mensaje;
$this->render();
exit();
}


//$this->view->mensaje=$mensaje;
$this->render();

}


}

?>