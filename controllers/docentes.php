<?php 

include_once 'sesiones/session_admin.php';

class Docentes extends Controller
{

  function __construct()
  {
   parent::__construct();
   $this->view->mensaje = "";
 }

 function render(){ 
  $docentes = $this->view->datos = $this->model->get();
  $this->view->docentes = $docentes;
  $this->view->render('docentes/index');
}

////////////////////////////////////////////REGISTRAR///////////////////////////////////////////////
function registrar(){ 


  $tipos=$this->model->getCatalogo('tipo_documento_identidad');
  $this->view->tipos=$tipos;

  $this->view->render('docentes/registrar');
}
///
function RegisterDocente(){

  $primer_nombre=$_POST['primer_nombre'];
  $segundo_nombre=$_POST['segundo_nombre'];
  $primer_apellido=$_POST['primer_apellido'];
  $segundo_apellido=$_POST['segundo_apellido'];
  $id_tipo_documento_identidad=$_POST['id_tipo_documento_identidad'];
  $identificacion=$_POST['identificacion'];
  $telefono=$_POST['telefono'];
  $correo=$_POST['correo'];
  $genero=$_POST['genero'];
  $identificacion_visa=$_POST['identificacion_visa'];
  $idioma=$_POST['idioma'];
  $descripcion=$_POST['descripcion']; 

  if($this->model->insert([ 
    'primer_nombre'=>$primer_nombre,
    'segundo_nombre'=>$segundo_nombre,
    'primer_apellido'=>$primer_apellido,
    'segundo_apellido'=>$segundo_apellido,
    'id_tipo_documento_identidad'=>$id_tipo_documento_identidad,
    'identificacion'=>$identificacion,
    'telefono'=>$telefono,
    'correo'=>$correo,
    'genero'=>$genero,
    'identificacion_visa'=>$identificacion_visa,
    'idioma'=>$idioma,
    'descripcion'=>$descripcion
  ])){

    $mensaje='<div class="alert alert-success alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    El docente fue registrado exitosamente.
    </div>
    </div>
    <div class="alert alert-info alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    El docente registrado puede acceder al Sistema con las siguientes credenciales:<br>
    Usuario:<b> icubv'.$identificacion.', </b> Clave: <b>icubv$'.$identificacion.'</b>
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();

  }else{
    $mensaje='<div class="alert alert-danger alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Hubo un error al registrar el docente.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();
  }
}
////////////////////////////////////////EDITAR////////////////////////////////////////////
function editar($param=null){

  $id_docente=$param[0];
  $this->view->id_docente=$id_docente;

  $docente=$this->model->getbyID($id_docente);
  $this->view->docente=$docente;

  $tipos=$this->model->getCatalogo('tipo_documento_identidad');
  $this->view->tipos=$tipos;

  $this->view->render('docentes/editar');
}
///
function UpdateDocente(){

  $id_persona=$_POST['id_persona'];
  $primer_nombre=$_POST['primer_nombre'];
  $segundo_nombre=$_POST['segundo_nombre'];
  $primer_apellido=$_POST['primer_apellido'];
  $segundo_apellido=$_POST['segundo_apellido'];
  $id_tipo_documento_identidad=$_POST['id_tipo_documento_identidad'];
  $identificacion=$_POST['identificacion'];
  $telefono=$_POST['telefono'];
  $correo=$_POST['correo'];
  $genero=$_POST['genero'];
  $identificacion_visa=$_POST['identificacion_visa'];
  $idioma=$_POST['idioma'];
  $idiomas_descripcion=$_POST['idiomas_descripcion']; 

  if($this->model->update([
    'id_persona'=>$id_persona,
    'primer_nombre'=>$primer_nombre,
    'segundo_nombre'=>$segundo_nombre,
    'primer_apellido'=>$primer_apellido,
    'segundo_apellido'=>$segundo_apellido,
    'id_tipo_documento_identidad'=>$id_tipo_documento_identidad,
    'identificacion'=>$identificacion,
    'telefono'=>$telefono,
    'correo'=>$correo,
    'genero'=>$genero,
    'identificacion_visa'=>$identificacion_visa,
    'idioma'=>$idioma,
    'idiomas_descripcion'=>$idiomas_descripcion
  ])){

    $mensaje='<div class="alert alert-success alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Los datos del docente fueron actualizados exitosamente.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();

  }else{
    $mensaje='<div class="alert alert-danger alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Hubo un error al actulizar los datos del docente.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();
  }
}
////////////////////////////////////////////LISTAS///////////////////////////////////////////////
function lista_secciones($param=null){ 

  $id_persona=$param[0];
  $this->view->id_persona=$id_persona;

  $secciones=$this->model->getSecciones_Docente($id_persona);
  $this->view->secciones=$secciones;

  $this->view->render('docentes/lista_secciones');
}


} 
?>