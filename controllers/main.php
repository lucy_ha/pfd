<?php

class Main extends Controller{

    function __construct() {
        parent::__construct();
    }

    function render($param=null){

        $ofertas = $this->model->get();
        $this->view->ofertas = $ofertas;

        $historia = $this->model->getHistoria();
        $this->view->historia = $historia;

        $redes = $this->model->getRedes();
        $this->view->redes = $redes;
       //para mostrar las noticas en el home 
        $noticia=$this->model->getnoticia();
        $this->view->noticia=$noticia;

        $certificados=$this->model->getCertificados();
        $this->view->certificados=$certificados;

        $certificadosDos=$this->model->getCertificadosDos();
        $this->view->certificadosDos=$certificadosDos;

        $certificadosTres=$this->model->getCertificadosTres();
        $this->view->certificadosTres=$certificadosTres;

        $certificadosCuatro=$this->model->getCertificadosCuatro();
        $this->view->certificadosCuatro=$certificadosCuatro;

        $this->view->render('main/index');
    }

    function detalles($param=null){ 

        $id_seccion=$param[0];
        $this->view->id_seccion=$id_seccion;

        $seccion=$this->model->getDetalles($id_seccion);
        $this->view->seccion=$seccion;

        $datos_h=$this->model->getbyID_dias($id_seccion);
        $this->view->datos_h=$datos_h;

        $j=1;
        foreach($datos_h as $row){
         $dias_h=new Confucio();
         $dias_h=$row;
         $dia_h = $this->model->getDiabyID($id_seccion, $dias_h->id_dia);

         $dia[$j]=[
           'id_dia'.$j=>$dia_h->id_dia,
           'dia'.$j=>$dia_h->dia
       ];

       $this->view->dia[$j]=$dia[$j];
       $j++;

   }
   $this->view->render('main/detalles');
}



   function certificados($param=null){  

    $id_tipo_certificado=$param[0];
    $this->view->id_tipo_certificado=$id_tipo_certificado;

    $tipo_certificados = $this->model->getTipoCertificado($id_tipo_certificado);
    $this->view->tipo_certificados = $tipo_certificados;

    $this->view->render('main/certificados');
   }

   function certificados_dos($param=null){  

    $id_tipo_certificado=$param[0];
    $this->view->id_tipo_certificado=$id_tipo_certificado;

    $tipo_certificados = $this->model->getTipoCertificado($id_tipo_certificado);
    $this->view->tipo_certificados = $tipo_certificados;

    $this->view->render('main/certificados_dos');
   }

   function certificados_tres($param=null){  

    $id_tipo_certificado=$param[0];
    $this->view->id_tipo_certificado=$id_tipo_certificado;

    $tipo_certificados = $this->model->getTipoCertificado($id_tipo_certificado);
    $this->view->tipo_certificados = $tipo_certificados;

    $this->view->render('main/certificados_tres');
   }

   function certificados_cuatro($param=null){  

    $id_tipo_certificado=$param[0];
    $this->view->id_tipo_certificado=$id_tipo_certificado;

    $tipo_certificados = $this->model->getTipoCertificado($id_tipo_certificado);
    $this->view->tipo_certificados = $tipo_certificados;

    $this->view->render('main/certificados_cuatro');
   }



  function validar(){

    $identificacion=$_POST['identificacion'];
    $id_tipo_certificado=$_POST['id_tipo_certificado'];

  /////////////// CAPTCHA /////////////////////////////////  
    session_start();
    $catpcha=$_POST['captcha'];

    $check=false;
    if(isset($_SESSION['captcha'])){

        if($catpcha == $_SESSION['captcha']){
            $check = true;

        }else if($check==false){

            $mensaje= '  <div class="mb-3">
            <div class="sent-message text-center">¡Error al Validar el Captcha!</div>
          </div>';

            $this->view->mensaje=$mensaje;
            $this->render();
            exit();

        }
        unset($_SESSION['captcha']);
    }
///////////////////////////// END CAPTCHA ///////////////////////

  if($this->model->getLeeCertificado(['identificacion'=>$identificacion, 'id_tipo_certificado'=>$id_tipo_certificado])){

  ?>

   <script>

   alert('¡Verificación Exitosa!. Imprima o Descargue su Certificado.');
   location.href='<?php echo constant ('URL')."main/certificado_fd/".$identificacion."/".$id_tipo_certificado;?>';
 
   </script>


   <?php
   exit();

  }else{
  ?>

  <script>

   alert('Usted no es participante de este Curso.');
   location.href='<?php echo constant ('URL')."main";?>';
 
   </script>


   <?php
   exit();

  }
  $this->certificados();

}




function validar_dos(){

  $identificacion=$_POST['identificacion'];
  $id_tipo_certificado=$_POST['id_tipo_certificado'];

/////////////// CAPTCHA /////////////////////////////////  
  session_start();
  $catpcha=$_POST['captcha'];

  $check=false;
  if(isset($_SESSION['captcha'])){

      if($catpcha == $_SESSION['captcha']){
          $check = true;

      }else if($check==false){

          $mensaje= '  <div class="mb-3">
          <div class="sent-message text-center">¡Error al Validar el Captcha!</div>
        </div>';

          $this->view->mensaje=$mensaje;
          $this->render();
          exit();

      }
      unset($_SESSION['captcha']);
  }
///////////////////////////// END CAPTCHA ///////////////////////

if($this->model->getLeeCertificado(['identificacion'=>$identificacion, 'id_tipo_certificado'=>$id_tipo_certificado])){

?>

 <script>

 alert('¡Verificación Exitosa!. Imprima o Descargue su Certificado.');
 location.href='<?php echo constant ('URL')."main/certificado_fdd/".$identificacion."/".$id_tipo_certificado;?>';

 </script>


 <?php
 exit();

}else{
?>

<script>

 alert('Usted no es participante de este Curso.');
 location.href='<?php echo constant ('URL')."main";?>';

 </script>


 <?php
 exit();

}
$this->certificados();

}



function validar_tres(){

  $identificacion=$_POST['identificacion'];
  $id_tipo_certificado=$_POST['id_tipo_certificado'];

/////////////// CAPTCHA /////////////////////////////////  
  session_start();
  $catpcha=$_POST['captcha'];

  $check=false;
  if(isset($_SESSION['captcha'])){

      if($catpcha == $_SESSION['captcha']){
          $check = true;

      }else if($check==false){

          $mensaje= '  <div class="mb-3">
          <div class="sent-message text-center">¡Error al Validar el Captcha!</div>
        </div>';

          $this->view->mensaje=$mensaje;
          $this->render();
          exit();

      }
      unset($_SESSION['captcha']);
  }
///////////////////////////// END CAPTCHA ///////////////////////

if($this->model->getLeeCertificado(['identificacion'=>$identificacion, 'id_tipo_certificado'=>$id_tipo_certificado])){

?>

 <script>

 alert('¡Verificación Exitosa!. Imprima o Descargue su Certificado.');
 location.href='<?php echo constant ('URL')."main/certificados_fdt/".$identificacion."/".$id_tipo_certificado;?>';

 </script>


 <?php
 exit();

}else{
?>

<script>

 alert('Usted no es participante de este Curso.');
 location.href='<?php echo constant ('URL')."main";?>';

 </script>


 <?php
 exit();

}
$this->certificados();

}


function validar_cuatro(){

  $identificacion=$_POST['identificacion'];
  $id_tipo_certificado=$_POST['id_tipo_certificado'];

/////////////// CAPTCHA /////////////////////////////////  
  session_start();
  $catpcha=$_POST['captcha'];

  $check=false;
  if(isset($_SESSION['captcha'])){

      if($catpcha == $_SESSION['captcha']){
          $check = true;

      }else if($check==false){

          $mensaje= '  <div class="mb-3">
          <div class="sent-message text-center">¡Error al Validar el Captcha!</div>
        </div>';

          $this->view->mensaje=$mensaje;
          $this->render();
          exit();

      }
      unset($_SESSION['captcha']);
  }
///////////////////////////// END CAPTCHA ///////////////////////

if($this->model->getLeeCertificado(['identificacion'=>$identificacion, 'id_tipo_certificado'=>$id_tipo_certificado])){

?>

 <script>

 alert('¡Verificación Exitosa!. Imprima o Descargue su Certificado.');
 location.href='<?php echo constant ('URL')."main/certificado_fdc/".$identificacion."/".$id_tipo_certificado;?>';

 </script>


 <?php
 exit();

}else{
?>

<script>

 alert('Usted no es participante de este Curso.');
 location.href='<?php echo constant ('URL')."main";?>';

 </script>


 <?php
 exit();

}
$this->certificados();

}



   public function certificado_fd($param=null){

      $identificacion=$param[0];
      $this->view->identificacion=$identificacion;
      
      $tipo=1;
      $uno=$this->model->getDatosCertificados($identificacion, $tipo);
      $this->view->uno=$uno;

      $tipo=2;
      $dos=$this->model->getDatosCertificados($identificacion, $tipo);
      $this->view->dos=$dos;

      $tipo=3;
      $tres=$this->model->getDatosCertificados($identificacion, $tipo);
      $this->view->tres=$tres;

      $tipo=4;
      $cuatro=$this->model->getDatosCertificados($identificacion, $tipo);
      $this->view->cuatro=$cuatro;


      $tipo=5;
      $cinco=$this->model->getDatosCertificados($identificacion, $tipo);
      $this->view->cinco=$cinco;

      $tipo=6;
      $seis=$this->model->getDatosCertificados($identificacion, $tipo);
      $this->view->seis=$seis;

      $tipo=7;
      $siete=$this->model->getDatosCertificados($identificacion, $tipo);
      $this->view->siete=$siete;

      $tipo=8;
      $ocho=$this->model->getDatosCertificados($identificacion, $tipo);
      $this->view->ocho=$ocho;

      $tipo=9;
      $nueve=$this->model->getDatosCertificados($identificacion, $tipo);
      $this->view->nueve=$nueve;

      $tipo=10;
      $diez=$this->model->getDatosCertificados($identificacion, $tipo);
      $this->view->diez=$diez;

     

      $this->view->render('main/certificado_fd');  
  } 

  public function certificado_fdd($param=null){

    $identificacion=$param[0];
    $this->view->identificacion=$identificacion;

    
    $tipo=11;
    $once=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->once=$once;
  
    $tipo=12;
    $doce=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->doce=$doce;
  
    $tipo=13;
    $trece=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->trece=$trece;
  
    $tipo=14;
    $catorce=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->catorce=$catorce;
  
    $tipo=15;
    $quince=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->quince=$quince;
  
    $tipo=16;
    $dieciseis=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->dieciseis=$dieciseis;
  
    $tipo=17;
    $diecisiete=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->diecisiete=$diecisiete;
  
    $tipo=18;
    $dieciocho=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->dieciocho=$dieciocho;
  
    $tipo=19;
    $diecinueve=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->diecinueve=$diecinueve;
  
    $tipo=20;
    $veinte=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->veinte=$veinte;

   
    $this->view->render('main/certificado_fdd');  
} 


public function certificados_fdt($param=null){

  $identificacion=$param[0];
  $this->view->identificacion=$identificacion;

  $tipo=21;
  $v1=$this->model->getDatosCertificados($identificacion, $tipo);
  $this->view->v1=$v1;

  $tipo=22;
  $v2=$this->model->getDatosCertificados($identificacion, $tipo);
  $this->view->v2=$v2;

  $tipo=23;
  $v3=$this->model->getDatosCertificados($identificacion, $tipo);
  $this->view->v3=$v3;

  $tipo=24;
  $v4=$this->model->getDatosCertificados($identificacion, $tipo);
  $this->view->v4=$v4;

  $tipo=25;
  $v5=$this->model->getDatosCertificados($identificacion, $tipo);
  $this->view->v5=$v5;

  $tipo=26;
  $v6=$this->model->getDatosCertificados($identificacion, $tipo);
  $this->view->v6=$v6;

  $tipo=27;
  $v7=$this->model->getDatosCertificados($identificacion, $tipo);
  $this->view->v7=$v7;

  $tipo=28;
  $v8=$this->model->getDatosCertificados($identificacion, $tipo);
  $this->view->v8=$v8;

  $tipo=29;
  $v9=$this->model->getDatosCertificados($identificacion, $tipo);
  $this->view->v9=$v9;

  $tipo=30;
  $treinta=$this->model->getDatosCertificados($identificacion, $tipo);
  $this->view->treinta=$treinta;

  $this->view->render('main/certificados_fdt');  
} 


public function certificado_fdc($param=null){

  $identificacion=$param[0];
  $this->view->identificacion=$identificacion;

  $tipo=31;
  $t1=$this->model->getDatosCertificados($identificacion, $tipo);
  $this->view->t1=$t1;

  $tipo=32;
  $t2=$this->model->getDatosCertificados($identificacion, $tipo);
  $this->view->t2=$t2;

  $tipo=33;
  $t3=$this->model->getDatosCertificados($identificacion, $tipo);
  $this->view->t3=$t3;

  $tipo=34;
  $t4=$this->model->getDatosCertificados($identificacion, $tipo);
  $this->view->t4=$t4;

  $tipo=35;
  $t5=$this->model->getDatosCertificados($identificacion, $tipo);
  $this->view->t5=$t5;

  $tipo=36;
  $t6=$this->model->getDatosCertificados($identificacion, $tipo);
  $this->view->t6=$t6;

  $tipo=37;
  $t7=$this->model->getDatosCertificados($identificacion, $tipo);
  $this->view->t7=$t7;

  $tipo=38;
  $t8=$this->model->getDatosCertificados($identificacion, $tipo);
  $this->view->t8=$t8;

  $tipo=39;
  $t9=$this->model->getDatosCertificados($identificacion, $tipo);
  $this->view->t9=$t9;

  $tipo=40;
  $cuarenta=$this->model->getDatosCertificados($identificacion, $tipo);
  $this->view->cuarenta=$cuarenta;

  $tipo=41;
  $c1=$this->model->getDatosCertificados($identificacion, $tipo);
  $this->view->c1=$c1;


  $this->view->render('main/certificado_fdc');  
} 

  

  
  public function culminacion($param=null){

    $identificacion=$param[0];
    $this->view->identificacion=$identificacion;


    $tipo=1;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;

    $tipo=2;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;

    $tipo=3;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;

    $tipo=4;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;


    $tipo=5;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;

    $tipo=6;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;

    $tipo=7;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;

    $tipo=8;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;

    $tipo=9;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;

    $tipo=10;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;

    $tipo=11;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;
  
    $tipo=12;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;
  
    $tipo=13;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;
  
    $tipo=14;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;
  
    $tipo=15;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;
  
    $tipo=16;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;
  
    $tipo=17;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;
  
    $tipo=18;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;
  
    $tipo=19;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;
  
    $tipo=20;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;


    $tipo=21;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;
  
    $tipo=22;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;
  
    $tipo=23;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;
  
    $tipo=24;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;;
  
    $tipo=25;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;
  
    $tipo=26;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;
  
    $tipo=27;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;
  
    $tipo=28;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;
  
    $tipo=29;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;
  
    $tipo=30;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;

    $tipo=31;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;
  
    $tipo=32;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;
  
    $tipo=33;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;
  
    $tipo=34;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;
  
    $tipo=35;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;
  
    $tipo=36;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;
  
    $tipo=37;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;
  
    $tipo=38;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;
  
    $tipo=39;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;
  
    $tipo=40;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;
  
    $tipo=41;
    $culminacion=$this->model->getDatosCertificados($identificacion, $tipo);
    $this->view->culminacion=$culminacion;


    $this->view->render('main/culminacion');  
  } 


}
?>