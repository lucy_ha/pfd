<?php

include_once 'sesiones/session_admin.php'; 

class Inscripcion extends Controller{


function __construct(){

parent::__construct();
}

function verInscripcion($param=null){

    $id_persona=$param[0];
    $this->view->id_persona=$id_persona;

    $estudiante=$this->model->getbyEstudiante($id_persona);
    $this->view->estudiante=$estudiante;
        
    $this->view->render('inscripcion/verInscripcion');

}


function Reingreso($param=null){

    $id_persona=$param[0];
    $this->view->id_persona=$id_persona;

    $prosecucion=$this->model->getCursoIdioma($id_persona);
    $this->view->prosecucion=$prosecucion;

    $prosecucion_1=$this->model->getCurso($id_persona);
    $this->view->prosecucion_1=$prosecucion_1;
        
    $this->view->render('inscripcion/reingreso');

}


function Oferta_lista($param=null){

  $id_persona=$param[0];
  $this->view->id_persona=$id_persona;

  $reingreso=$this->model->getReingreso($id_persona);
  $this->view->reingreso=$reingreso;

  $cursos=$this->model->getOfertas();
  $this->view->cursos=$cursos;
      
$this->view->render('inscripcion/oferta_lista');
}


function Curso_Realizado($param=null){

  $id_persona=$param[0];
  $this->view->id_persona=$id_persona;

  $realizados=$this->model->getCursosRealizados($id_persona);
  $this->view->realizados=$realizados;

  $otros=$this->model->getCursosRealizadosOtro($id_persona);
  $this->view->otros=$otros;

$this->view->render('inscripcion/curso_realizado');
}



function eliminar($param=null){
  $id_nota_actividad = $param[0];

  if($this->model->delete($id_nota_actividad)){      ?>
  
    <script>

    alert('Validación guardada existosamente. Gracias por participar en nuestro Instituto Confucio');
      location.href='<?php echo constant ('URL')."inscripcion/curso_realizado/"?>';
    
      </script>
      
<?php }else{
    $mensaje='<div class="alert alert-danger alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Hubo un error al actualizar su culminación al Curso.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->Reingreso();
    exit();
  }
}




function update(){

    $id_oferta_academica=$_POST['id_oferta_academica'];
    $id_persona=$_POST['id_persona'];
    $id_inscripcion=$_POST['id_inscripcion'];
 
   //var_dump($id_inscripcion);
   //var_dump($id_nota_actividad_idioma);
   
    if($this->model->update(['id_oferta_academica'=>$id_oferta_academica, 'id_persona'=>$id_persona, 'id_inscripcion'=>$id_inscripcion])){
      ?>
  
      <script>

      alert('Reingreso realizado existosamente. Debe completar nuevamente su inscripción');
        location.href='<?php echo constant ('URL')."pre_inscripcion/cuenta"?>';
      
        </script>
        
  <?php }else{
    
    $mensaje='<div class="alert alert-danger alert-dismissible show fade">
    <div class="alert-body">
      <button class="close" data-dismiss="alert">
        <span>&times;</span>
      </button>
      Hubo un error al realizar su inscripcion nuevamente.
    </div>
    </div>';
    
    $this->view->mensaje=$mensaje;
    $this->Reingreso();
    exit();
    }
    
}

function Comp_Inscripcion($param){
  $id_persona=$param[0];
  $this->view->id_persona=$id_persona;

  $estudio=$this->model->getConstanciadeEstudio($id_persona);
  $this->view->estudio=$estudio;

$this->view->render('inscripcion/comp_inscripcion');

}

function Cons_Estudio($param){

  $id_persona=$param[0];
  $this->view->id_persona=$id_persona;

  $estudio=$this->model->getConstanciadeEstudio($id_persona);
  $this->view->estudio=$estudio;

  $this->view->render('inscripcion/cons_estudio');
  
  }

  function Certificado_Idioma($param){

    $id_persona=$param[0];
    $this->view->id_persona=$id_persona;
  
    $idiomas=$this->model->getCertificadoIdioma($id_persona);
    $this->view->idiomas=$idiomas;
  
    $this->view->render('inscripcion/certificado_idioma');
    
    }

    function Certificado_Actividad($param){

      $id_persona=$param[0];
      $this->view->id_persona=$id_persona;
    
      $actividad=$this->model->getCertificadoActividad($id_persona);
      $this->view->actividad=$actividad;
  
      $this->view->render('inscripcion/certificado_actividad');
      
      }

}

?>