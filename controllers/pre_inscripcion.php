<?php


class Pre_inscripcion extends Controller{

    function __construct()
    {
        parent::__construct();
    }

    function render($param=null){

        $id_oferta_academica=$param[0];
        $this->view->id_oferta_academica=$id_oferta_academica;
    
        $tipos=$this->model->getCatalogo('tipo_documento_identidad');
        $this->view->tipos=$tipos;

        $etnia=$this->model->getCatalogo('etnia');
        $this->view->etnia=$etnia;

        $paises=$this->model->getCatalogo('pais');
        $this->view->paises=$paises;

        $estados=$this->model->getCatalogo('estado');
        $this->view->estados=$estados;

        $tipo_dis=$this->model->getCatalogo('tipo_discapacidad');
        $this->view->tipo_dis=$tipo_dis;

        $nivel_aca=$this->model->getCatalogo('nivel_academico');
        $this->view->nivel_aca=$nivel_aca;

        $civiles=$this->model->getEstadoCivil();
        $this->view->civiles=$civiles;

        $cargos=$this->model->getCatalogo('cargo');
        $this->view->cargos=$cargos;

        $profesiones=$this->model->getCatalogo('profesion');
        $this->view->profesiones=$profesiones;

        $cursos=$this->model->getOfertas();
        $this->view->cursos=$cursos;

        $oferta=$this->model->getIdOferta($id_oferta_academica);
        $this->view->oferta=$oferta;


        $this->view->render('pre_inscripcion/index');
    }


    function Cuenta(){
      $this->view->render('pre_inscripcion/cuenta');
  }


    function FormAspirante(){

      //datos personales
      $primer_nombre=$_POST['primer_nombre'];
      $segundo_nombre=$_POST['segundo_nombre'];
      $primer_apellido=$_POST['primer_apellido'];
      $segundo_apellido=$_POST['segundo_apellido'];
      $identificacion=$_POST['identificacion'];
      $id_civil=$_POST['id_civil'];
  
      $telefono=$_POST['telefono'];
      $correo=$_POST['correo'];
      $genero=$_POST['genero'];
      $id_tipo_documento_identidad=$_POST['id_tipo_documento_identidad'];

      //datos de nacimiento
      $fecha_nacimiento=$_POST['fecha_nacimiento'];
      $pais=$_POST['pais'];
    
      //datos de direccion_persona
      $direccion=$_POST['direccion'];
      $telefono_habitacion=$_POST['telefono_habitacion'];
      $estado=$_POST['estado'];
      $etnia=$_POST['etnia'];

      //datos laborales
      $empleo=$_POST['empleo'];
      $telefono_institucion=$_POST['telefono_institucion'];
      $correo_institucion=$_POST['correo_institucion'];   
      $direccion_institucion=$_POST['direccion_institucion'];
      $inst_publica=$_POST['inst_publica'];
      $inst_privada=$_POST['inst_privada'];
      $organismo=$_POST['organismo'];
      $inst_inv=$_POST['inst_inv'];
      $tipo_institucion=$_POST['tipo_institucion'];
      $trabajador_ubv=$_POST['trabajador_ubv'];
      $id_cargo=$_POST['id_cargo']; 
      $state=$_POST['state'];
      $id_profesion=$_POST['id_profesion'];    
      
      //datos interes
      $discapacidad=$_POST['discapacidad'];
      $grupo_sanguineo=$_POST['grupo_sanguineo'];
      $enfermedad=$_POST['enfermedad'];   
      $descripcion_enfermedad=$_POST['descripcion_enfermedad'];
      $idioma=$_POST['idioma']; //id del idioma
      $descripcion=$_POST['descripcion']; //dato booleano
      $id_tipo_discapacidad=$_POST['id_tipo_discapacidad'];  
      $tratamiento=$_POST['tratamiento'];

      //datos oferta
      $id_oferta_academica=$_POST['id_oferta_academica'];

      //datos experiencia academica
      $id_nivel_academico=$_POST['id_nivel_academico'];
      $institucion_nivel=$_POST['institucion_nivel'];
      $estudiante_ubv=$_POST['estudiante_ubv'];
      $mision_sucre=$_POST['mision_sucre'];

            /////////////// CAPTCHA /////////////////////////////////  
            session_start();
            $catpcha=$_POST['captcha'];
    
            $check=false;
            if(isset($_SESSION['captcha'])){
    
                if($catpcha == $_SESSION['captcha']){
                    $check = true;
    
                }else if($check==false){
    
                    $mensaje= '<div class="alert alert-warning alert-dismissible show fade">
                    <div class="alert-body">
                      <button class="close" data-dismiss="alert">
                        <span>&times;</span>
                      </button>
                      Error al Validar el captcha
                    </div>
                  </div>';
    
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
    
                }
                unset($_SESSION['captcha']);
            }
    ///////////////////////////// END CAPTCHA ///////////////////////
    


      $fecha=date('Y-m-d'); 

      $resultado= $fecha-$fecha_nacimiento;


      if($this->model->Existente($identificacion)){

        $mensaje='<div class="alert alert-danger alert-dismissible show fade">
        <div class="alert-body">
          <button class="close" data-dismiss="alert">
            <span>&times;</span>
          </button>
          Usted ya se encuentra pre-inscrito en el sistema.
        </div>
        </div>';
        
    
        $this->view->mensaje=$mensaje;
        $this->render();
      } 
      
     else if($resultado < 15) {

        $mensaje='<div class="alert alert-danger alert-dismissible show fade">
        <div class="alert-body">
          <button class="close" data-dismiss="alert">
            <span>&times;</span>
          </button>
          Usted no es mayor de 15 años.
        </div>
        </div>';
        
    
        $this->view->mensaje=$mensaje;
        $this->render();
    }else{

    if($this->model->insert([ 
          'primer_nombre'=>$primer_nombre,
          'segundo_nombre'=>$segundo_nombre,
          'primer_apellido'=>$primer_apellido,
          'segundo_apellido'=>$segundo_apellido,
          'identificacion'=>$identificacion,
          'telefono'=>$telefono,
          'correo'=>$correo,
          'genero'=>$genero,
          'id_tipo_documento_identidad'=>$id_tipo_documento_identidad,
          'fecha_nacimiento'=>$fecha_nacimiento,
          'pais'=>$pais,
          'direccion'=>$direccion,
          'telefono_habitacion'=>$telefono_habitacion,
          'estado'=>$estado,
          'etnia'=>$etnia,
          'empleo'=>$empleo,
          'telefono_institucion'=>$telefono_institucion,
          'correo_institucion'=>$correo_institucion,
          'direccion_institucion'=>$direccion_institucion,
          'inst_publica'=>$inst_publica,
          'inst_privada'=>$inst_privada,
          'organismo'=>$organismo,
          'inst_inv'=>$inst_inv,
          'tipo_institucion'=>$tipo_institucion,
          'trabajador_ubv'=>$trabajador_ubv,
          'id_cargo'=>$id_cargo,
          'state'=>$state,
          'discapacidad'=>$discapacidad,
          'grupo_sanguineo'=>$grupo_sanguineo,
          'enfermedad'=>$enfermedad,
          'descripcion_enfermedad'=>$descripcion_enfermedad,
          'tratamiento'=>$tratamiento,
          'estudiante_ubv'=>$estudiante_ubv,
          'mision_sucre'=>$mision_sucre,
          'idioma'=>$idioma,
          'descripcion'=>$descripcion,
          'id_tipo_discapacidad'=>$id_tipo_discapacidad,
          'id_nivel_academico'=>$id_nivel_academico,
          'id_civil'=>$id_civil,
          'id_profesion'=>$id_profesion,
          'institucion_nivel'=>$institucion_nivel,
          'id_oferta_academica'=>$id_oferta_academica
      ])){ ?>
  
        <script>
 
        alert('Se ha realizado exitosamente su Pre-ingreso al Instituto Confucio.');
          location.href='<?php echo constant ('URL')."pre_inscripcion/cuenta"?>';
        
          </script>
          
    <?php  
}else{
  
  $mensaje= '<div class="alert alert-warning alert-dismissible show fade">
  <div class="alert-body">
    <button class="close" data-dismiss="alert">
      <span>&times;</span>
    </button>
    Hubo un error al Registrar los Datos Solicitados.
  </div>
  </div>';
  
      }
  
      $this->view->mensaje=$mensaje;
      $this->render();
    }


  }



    function Validar($param=null){

      $id_oferta_academica=$param[0];
      $this->view->id_oferta_academica=$id_oferta_academica;

      $categoria=$this->model->getCV($id_oferta_academica);
      $this->view->categoria=$categoria;
          
      $this->view->render('pre_inscripcion/registrado');
  }


    function ValidarCategoria(){

      $identificacion=$_POST['identificacion'];
      $categoria=$_POST['categoria'];
      $id_oferta_academica=$_POST['id_oferta_academica'];
    /////////////// CAPTCHA /////////////////////////////////  
      session_start();
      $catpcha=$_POST['captcha'];

      $check=false;
      if(isset($_SESSION['captcha'])){

          if($catpcha == $_SESSION['captcha']){
              $check = true;

          }else if($check==false){

              /*$mensaje= '<div class="alert alert-warning alert-dismissible show fade">
              <div class="alert-body">
                <button class="close" data-dismiss="alert">
                  <span>&times;</span>
                </button>
                Error al Validar el captcha
              </div>
            </div>';

              $this->view->mensaje=$mensaje;*/
              ?>
  
              <script>
            
              alert('Error al Validar el captcha');
                location.href='<?php echo constant ('URL')."pre_inscripcion/validar/".$id_oferta_academica?>';
                </script>
                
            <?php  
              exit();
          }
          unset($_SESSION['captcha']);
      }
///////////////////////////// END CAPTCHA ///////////////////////

if($this->model->getCategoria([
  'identificacion'=> $identificacion, 
  'categoria' => $categoria,
  'id_oferta_academica'=>$id_oferta_academica
  ])){
  
     /* $mensaje='<div class="alert alert-danger alert-dismissible show fade">
      <div class="alert-body">
        <button class="close" data-dismiss="alert">
          <span>&times;</span>
        </button>
        Usted ya se encuentra Pre-inscrito en una Oferta Académica.
      </div>
    </div>';

  $this->view->mensaje=$mensaje;*/
  ?>
  
  <script>

  alert(' Usted ya se encuentra Pre-inscrito en una Oferta Académica.');
    location.href='<?php echo constant ('URL')."pre_inscripcion/validar/".$id_oferta_academica?>';
    </script>
    
<?php  
  exit();
  
} else{

  ?>
  
  <script>

  alert('Se ha registrado exitosamente la Oferta Académica elegida.');
    location.href='<?php echo constant ('URL')."main"?>';
  
    </script>
    
<?php  

}

}


    

  function validacionCaptcha(){
    session_start();
    echo $_SESSION['captcha'];
}


    function Solicitud(){
      
      include_once 'sesiones/session_admin.php'; 

      $solicitudes=$this->model->get();
      $this->view->solicitudes=$solicitudes;
    
      $this->view->render('pre_inscripcion/solicitud');
  }



  function Registrar_Deposito($param=null){

    include_once 'sesiones/session_admin.php';

    $id_aspirante=$param[0];
    $this->view->id_aspirante=$id_aspirante;
    
    $aspirante=$this->model->getIdDeposito($id_aspirante);
    $this->view->aspirante=$aspirante;

$this->view->render('pre_inscripcion/registrar_deposito');  

}


  function FormRegistrarDeposito(){

    $numero_vouche=$_POST['numero_vouche'];
    $exonerado=$_POST['exonerado'];
    $modalidad_exo=$_POST['modalidad_exo'];
    $monto=$_POST['monto'];
    $fecha_deposito=$_POST['fecha_deposito'];
    $observacion=$_POST['observacion'];
    $numero_vouche_50=$_POST['numero_vouche_50'];
    $monto_50=$_POST['monto_50'];
    $fecha_deposito_50=$_POST['fecha_deposito_50'];
    $observacion_50=$_POST['observacion_50'];
    $id_aspirante=$_POST['id_aspirante'];
  
    if($this->model->insertDeposito([ 
        'numero_vouche'=>$numero_vouche,
        'monto'=>$monto,
        'modalidad_exo'=>$modalidad_exo,
        'exonerado'=>$exonerado,
        'observacion'=>$observacion,
        'fecha_deposito'=>$fecha_deposito,
        'numero_vouche_50'=>$numero_vouche_50,
        'monto_50'=>$monto_50,
        'observacion_50'=>$observacion_50,
        'fecha_deposito_50'=>$fecha_deposito_50,
        'id_aspirante'=>$id_aspirante
    ])){?>
  
      <script>

      alert('¡Registro realizado exitosamente!');
        location.href='<?php echo constant ('URL')."pre_inscripcion/VerPreIngreso/".$id_aspirante?>';
      
        </script>
        
  <?php  }else{

$mensaje= '<div class="alert alert-danger alert-dismissible show fade">
<div class="alert-body">
  <button class="close" data-dismiss="alert">
    <span>&times;</span>
  </button>
  Hubo un error al Registrar los datos del Deposito bancario.
</div>
</div>';

    }

    $this->view->mensaje=$mensaje;
    $this->Registrar_Deposito();


}


  function VerPreIngreso($param = null){

    include_once 'sesiones/session_admin.php'; 

    $id_aspirante=$param[0];
    $this->view->id_aspirante=$id_aspirante;

    $aspirante=$this->model->getbyAspirante($id_aspirante);
    $this->view->aspirante=$aspirante;
        
    $this->view->render('pre_inscripcion/verPreingreso');

}

function VerPreIngresoDetalle($param = null){

  include_once 'sesiones/session_admin.php'; 

  $id_aspirante=$param[0];
  $this->view->id_aspirante=$id_aspirante;

  $aspirante=$this->model->getbyAspiranteDetalle($id_aspirante);
  $this->view->aspirante=$aspirante;
      
  $this->view->render('pre_inscripcion/DetallePreingreso');

}


function VerInscripcionDetalle($param = null){

  include_once 'sesiones/session_admin.php'; 

  $id_inscripcion=$param[0];
  $this->view->id_inscripcion=$id_inscripcion;

  $estudiante=$this->model->getbyEstudianteDetalle($id_inscripcion);
  $this->view->estudiante=$estudiante;
      
  $this->view->render('pre_inscripcion/VerInscripcionDetalle');

}



function Lista_Inscritos(){

  include_once 'sesiones/session_admin.php'; 

  $inscripciones=$this->model->getInscripcion();
    $this->view->inscripciones=$inscripciones;

$this->view->render('pre_inscripcion/lista_inscritos');
}


function ValidaIns(){

  include_once 'sesiones/session_admin.php'; 


  $estatus=$_POST['estatus'];
  $id_aspirante=$_POST['id_aspirante'];
  $id_persona=$_POST['id_persona'];
  $identificacion=$_POST['identificacion'];
  $id_oferta_academica=$_POST['id_oferta_academica'];
  $id_deposito=$_POST['id_deposito'];

  if($this->model->Existente($id_aspirante)){
    $mensaje='<div class="alert alert-warning alert-dismissible show fade">
    <div class="alert-body">
      <button class="close" data-dismiss="alert">
        <span>&times;</span>
      </button>
     El Aspirante ya fue Inscrito. Ahora se encuentra como Estudiante.
    </div>
  </div>';
  $this->view->mensaje=$mensaje;
  $this->Lista_Inscritos();

}else{

  /*if ($this->model->ValidarCupos([
    'id_oferta_academica'=>$id_oferta_academica
  ])) {
    $mensaje='<div class="alert alert-warning alert-dismissible show fade">
      <div class="alert-body">
        <button class="close" data-dismiss="alert">
          <span>&times;</span>
        </button>
       Ya no hay cupos disponibles en la sección: '.$id_seccion.'.
      </div>
    </div>';
  }else{*/


  if($this->model->insertInscripcion([ 
    'estatus'=>$estatus,
    'id_aspirante'=>$id_aspirante,
    'id_persona'=>$id_persona,
    'identificacion'=>$identificacion,
    'id_oferta_academica'=>$id_oferta_academica,
    'id_deposito'=>$id_deposito
])){


$mensaje= '<div class="alert alert-success alert-dismissible show fade">
<div class="alert-body">
  <button class="close" data-dismiss="alert">
    <span>&times;</span>
  </button>
  Inscripción del Aspirante '.$identificacion.' Fue validada.
</div>
</div>
<div class="alert alert-info alert-dismissible show fade">
<div class="alert-body">
  <button class="close" data-dismiss="alert">
    <span>&times;</span>
  </button>
 El Aspirante Inscrito puede acceder como Usuario-Estudiante al Sistema con las siguientes credenciales:<br>
 Usuario:<b> icubv'.$identificacion.' </b>, Clave: <b>icubv$'.$identificacion.'</b>
</div>
</div>';

}

  }

$this->view->mensaje=$mensaje;
$this->Lista_Inscritos();
}


function eliminar($param=null){
     
  $id_persona=$param[0];

  if($this->model->delete($id_persona)){
   
      $mensaje='<div class="alert alert-success alert-dismissible show fade">
      <div class="alert-body">
        <button class="close" data-dismiss="alert">
          <span>&times;</span>
        </button>
       La solicitud del Aspirante fue removido correctamente.
      </div>
      </div>';
    
  }else{

      $mensaje='<div class="alert alert-danger alert-dismissible show fade">
      <div class="alert-body">
        <button class="close" data-dismiss="alert">
          <span>&times;</span>
        </button>
        Hubo un error al Eliminar La solicitud del Aspirante.
      </div>
      </div>';
  
  }

$this->view->mensaje=$mensaje;
$this->Solicitud();

}

function editarSolicitud($param = null){

  include_once 'sesiones/session_admin.php'; 

  $id_aspirante=$param[0];
  $this->view->id_aspirante=$id_aspirante;

  $aspirante=$this->model->getbyAspiranteDetalle($id_aspirante);
  $this->view->aspirante=$aspirante;

  $secciones=$this->model->getOfertasDisponibles();
  $this->view->secciones=$secciones;

  $this->view->render('pre_inscripcion/editar_solicitud');

}

function updateSolicitud(){

  $id_oferta_academica = $_POST['id_oferta_academica'];
  $id_seccion = $_POST['id_seccion'];
  $id_aspirante = $_POST['id_aspirante'];
  $identificacion = $_POST['identificacion'];
  $oferta = $_POST['oferta'];
  
  if($this->model->updateSolicitud([
    'id_seccion'=>$id_seccion,
    'id_aspirante'=>$id_aspirante,
    'id_oferta_academica'=>$id_oferta_academica
  ])){

    $mensaje='<div class="alert alert-success alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Se ha modificado exitosamente la solicitud de pre-inscripción de: '.$identificacion.'.
    </div>
    </div>
    <div class="alert alert-info alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Se ha liberaro un cupo en la sección: '.$oferta.'. 
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->Solicitud();
    exit();

  }else{
    $mensaje='<div class="alert alert-danger alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Hubo un error al editar la solicitud de pre-inscripción.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->Solicitud();
    exit();
  }
}

function editarInscripcion($param = null){

  include_once 'sesiones/session_admin.php'; 

  $id_inscripcion=$param[0];
  $this->view->id_inscripcion=$id_inscripcion;

  $estudiante=$this->model->getbyEstudianteDetalle($id_inscripcion);
  $this->view->estudiante=$estudiante;

  $secciones=$this->model->getOfertasDisponibles();
  $this->view->secciones=$secciones;

  $this->view->render('pre_inscripcion/editar_inscripcion');

}

function updateInsc(){

  $id_oferta_academica = $_POST['id_oferta_academica'];
  $id_inscripcion = $_POST['id_inscripcion'];
  $id_seccion = $_POST['id_seccion'];
  $id_aspirante = $_POST['id_aspirante'];
  $identificacion = $_POST['identificacion'];
  $oferta = $_POST['oferta'];
  
  if($this->model->updateInscrip([
    'id_inscripcion'=>$id_inscripcion,
    'id_seccion'=>$id_seccion,
    'id_aspirante'=>$id_aspirante,
    'id_oferta_academica'=>$id_oferta_academica
  ])){

    $mensaje='<div class="alert alert-success alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Se ha modificado exitosamente los datos de la inscripción del estudiante: '.$identificacion.'.
    </div>
    </div>
    <div class="alert alert-info alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Se ha liberaro un cupo en la sección: '.$oferta.'. 
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->Lista_Inscritos();
    exit();

  }else{
    $mensaje='<div class="alert alert-danger alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Hubo un error al editar la inscripción.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->Lista_Inscritos();
    exit();
  }
}

function editarEstudiante($param = null){

  include_once 'sesiones/session_admin.php'; 

  $id_inscripcion=$param[0];
  $this->view->id_inscripcion=$id_inscripcion;

  $estudiante=$this->model->getbyIDEstudiante($id_inscripcion);
  $this->view->estudiante=$estudiante;

  $tipos=$this->model->getCatalogo('tipo_documento_identidad');
  $this->view->tipos=$tipos;

  $etnia=$this->model->getCatalogo('etnia');
  $this->view->etnia=$etnia;

  $paises=$this->model->getCatalogo('pais');
  $this->view->paises=$paises;

  $estados=$this->model->getCatalogo('estado');
  $this->view->estados=$estados;

  $tipo_dis=$this->model->getCatalogo('tipo_discapacidad');
  $this->view->tipo_dis=$tipo_dis;

  $nivel_aca=$this->model->getCatalogo('nivel_academico');
  $this->view->nivel_aca=$nivel_aca;

  $civiles=$this->model->getEstadoCivil();
  $this->view->civiles=$civiles;

  $cargos=$this->model->getCatalogo('cargo');
  $this->view->cargos=$cargos;

  $profesiones=$this->model->getCatalogo('profesion');
  $this->view->profesiones=$profesiones;

      
  $this->view->render('pre_inscripcion/editarDatosEstudiante');

}



function editarAspirante($param = null){

  include_once 'sesiones/session_admin.php'; 

  $id_aspirante=$param[0];
  $this->view->id_aspirante=$id_aspirante;

  $aspirante=$this->model->getbyIDAspirante($id_aspirante);
  $this->view->aspirante=$aspirante;

  $tipos=$this->model->getCatalogo('tipo_documento_identidad');
  $this->view->tipos=$tipos;

  $etnia=$this->model->getCatalogo('etnia');
  $this->view->etnia=$etnia;

  $paises=$this->model->getCatalogo('pais');
  $this->view->paises=$paises;

  $estados=$this->model->getCatalogo('estado');
  $this->view->estados=$estados;

  $tipo_dis=$this->model->getCatalogo('tipo_discapacidad');
  $this->view->tipo_dis=$tipo_dis;

  $nivel_aca=$this->model->getCatalogo('nivel_academico');
  $this->view->nivel_aca=$nivel_aca;

  $civiles=$this->model->getEstadoCivil();
  $this->view->civiles=$civiles;

  $cargos=$this->model->getCatalogo('cargo');
  $this->view->cargos=$cargos;

  $profesiones=$this->model->getCatalogo('profesion');
  $this->view->profesiones=$profesiones;

      
  $this->view->render('pre_inscripcion/editarDatosAspirante');

}


function FormEstudianteUpdate(){

  $primer_nombre=$_POST['primer_nombre'];
  $segundo_nombre=$_POST['segundo_nombre'];
  $primer_apellido=$_POST['primer_apellido'];
  $segundo_apellido=$_POST['segundo_apellido'];
  $telefono=$_POST['telefono'];
  $correo=$_POST['correo'];
  $genero=$_POST['genero'];
  $fecha_nacimiento=$_POST['fecha_nacimiento'];
  $direccion=$_POST['direccion'];
  $telefono_habitacion=$_POST['telefono_habitacion'];
  $empleo=$_POST['empleo'];
  $telefono_institucion=$_POST['telefono_institucion'];
  $correo_institucion=$_POST['correo_institucion'];   
  $direccion_institucion=$_POST['direccion_institucion'];
  $inst_publica=$_POST['inst_publica'];
  $inst_privada=$_POST['inst_privada'];
  $organismo=$_POST['organismo'];
  $inst_inv=$_POST['inst_inv'];
  $tipo_institucion=$_POST['tipo_institucion'];
  $trabajador_ubv=$_POST['trabajador_ubv'];
  $discapacidad=$_POST['discapacidad'];
  $grupo_sanguineo=$_POST['grupo_sanguineo'];
  $enfermedad=$_POST['enfermedad'];   
  $descripcion_enfermedad=$_POST['descripcion_enfermedad'];
  $idioma=$_POST['idioma']; 
  $descripcion=$_POST['descripcion'];
  $tratamiento=$_POST['tratamiento'];
  $institucion_nivel=$_POST['institucion_nivel'];
  $estudiante_ubv=$_POST['estudiante_ubv'];
  $mision_sucre=$_POST['mision_sucre'];

$etnia=$_POST['etnia'];  
$pais=$_POST['pais'];
$estado=$_POST['estado'];
$id_cargo=$_POST['id_cargo']; 
$state=$_POST['state'];
$id_profesion=$_POST['id_profesion']; 
$id_nivel_academico=$_POST['id_nivel_academico'];
$id_civil=$_POST['id_civil'];
$id_persona=$_POST['id_persona'];
$id_nacimiento=$_POST['id_nacimiento'];
$id_direccion=$_POST['id_direccion'];
$id_dato_laboral=$_POST['id_dato_laboral'];
$id_datos_interes=$_POST['id_datos_interes'];
$id_aspirante=$_POST['id_aspirante'];
$id_tipo_documento_identidad=$_POST['id_tipo_documento_identidad'];
$id_tipo_discapacidad=$_POST['id_tipo_discapacidad'];  

if($this->model->Update([ 
      'primer_nombre'=>$primer_nombre,
      'segundo_nombre'=>$segundo_nombre,
      'primer_apellido'=>$primer_apellido,
      'segundo_apellido'=>$segundo_apellido,
      'telefono'=>$telefono,
      'correo'=>$correo,
      'genero'=>$genero,
      'id_tipo_documento_identidad'=>$id_tipo_documento_identidad,
      'fecha_nacimiento'=>$fecha_nacimiento,
      'pais'=>$pais,
      'direccion'=>$direccion,
      'telefono_habitacion'=>$telefono_habitacion,
      'estado'=>$estado,
      'etnia'=>$etnia,
      'empleo'=>$empleo,
      'telefono_institucion'=>$telefono_institucion,
      'correo_institucion'=>$correo_institucion,
      'direccion_institucion'=>$direccion_institucion,
      'inst_publica'=>$inst_publica,
      'inst_privada'=>$inst_privada,
      'organismo'=>$organismo,
      'inst_inv'=>$inst_inv,
      'tipo_institucion'=>$tipo_institucion,
      'trabajador_ubv'=>$trabajador_ubv,
      'id_cargo'=>$id_cargo,
      'state'=>$state,
      'discapacidad'=>$discapacidad,
      'grupo_sanguineo'=>$grupo_sanguineo,
      'enfermedad'=>$enfermedad,
      'descripcion_enfermedad'=>$descripcion_enfermedad,
      'tratamiento'=>$tratamiento,
      'estudiante_ubv'=>$estudiante_ubv,
      'mision_sucre'=>$mision_sucre,
      'idioma'=>$idioma,
      'descripcion'=>$descripcion,
      'id_tipo_discapacidad'=>$id_tipo_discapacidad,
      'id_nivel_academico'=>$id_nivel_academico,
      'id_civil'=>$id_civil,
      'id_profesion'=>$id_profesion,
      'institucion_nivel'=>$institucion_nivel,
      'id_persona'=>$id_persona,
      'id_nacimiento'=>$id_nacimiento,
      'id_direccion'=>$id_direccion,
      'id_dato_laboral'=>$id_dato_laboral,
      'id_datos_interes'=>$id_datos_interes,
      'id_aspirante'=>$id_aspirante
  ])){ 

    $mensaje='<div class="alert alert-success alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Se ha modificado exitosamente los datos registrados del Aspirante '.$identificacion.'.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->Lista_Inscritos();
    exit();

}else{

$mensaje= '<div class="alert alert-warning alert-dismissible show fade">
<div class="alert-body">
<button class="close" data-dismiss="alert">
  <span>&times;</span>
</button>
Hubo un error al editar los Datos Solicitados.
</div>
</div>';

  }

  $this->view->mensaje=$mensaje;
  $this->Lista_Inscritos();
}



function FormAspiranteUpdate(){

  $primer_nombre=$_POST['primer_nombre'];
  $segundo_nombre=$_POST['segundo_nombre'];
  $primer_apellido=$_POST['primer_apellido'];
  $segundo_apellido=$_POST['segundo_apellido'];
  $telefono=$_POST['telefono'];
  $correo=$_POST['correo'];
  $genero=$_POST['genero'];
  $fecha_nacimiento=$_POST['fecha_nacimiento'];
  $direccion=$_POST['direccion'];
  $telefono_habitacion=$_POST['telefono_habitacion'];
  $empleo=$_POST['empleo'];
  $telefono_institucion=$_POST['telefono_institucion'];
  $correo_institucion=$_POST['correo_institucion'];   
  $direccion_institucion=$_POST['direccion_institucion'];
  $inst_publica=$_POST['inst_publica'];
  $inst_privada=$_POST['inst_privada'];
  $organismo=$_POST['organismo'];
  $inst_inv=$_POST['inst_inv'];
  $tipo_institucion=$_POST['tipo_institucion'];
  $trabajador_ubv=$_POST['trabajador_ubv'];
  $discapacidad=$_POST['discapacidad'];
  $grupo_sanguineo=$_POST['grupo_sanguineo'];
  $enfermedad=$_POST['enfermedad'];   
  $descripcion_enfermedad=$_POST['descripcion_enfermedad'];
  $idioma=$_POST['idioma']; 
  $descripcion=$_POST['descripcion'];
  $tratamiento=$_POST['tratamiento'];
  $institucion_nivel=$_POST['institucion_nivel'];
  $estudiante_ubv=$_POST['estudiante_ubv'];
  $mision_sucre=$_POST['mision_sucre'];

$etnia=$_POST['etnia'];  
$pais=$_POST['pais'];
$estado=$_POST['estado'];
$id_cargo=$_POST['id_cargo']; 
$state=$_POST['state'];
$id_profesion=$_POST['id_profesion']; 
$id_nivel_academico=$_POST['id_nivel_academico'];
$id_civil=$_POST['id_civil'];
$id_persona=$_POST['id_persona'];
$id_nacimiento=$_POST['id_nacimiento'];
$id_direccion=$_POST['id_direccion'];
$id_dato_laboral=$_POST['id_dato_laboral'];
$id_datos_interes=$_POST['id_datos_interes'];
$id_aspirante=$_POST['id_aspirante'];
$id_tipo_documento_identidad=$_POST['id_tipo_documento_identidad'];
$id_tipo_discapacidad=$_POST['id_tipo_discapacidad'];  

if($this->model->Update([ 
      'primer_nombre'=>$primer_nombre,
      'segundo_nombre'=>$segundo_nombre,
      'primer_apellido'=>$primer_apellido,
      'segundo_apellido'=>$segundo_apellido,
      'telefono'=>$telefono,
      'correo'=>$correo,
      'genero'=>$genero,
      'id_tipo_documento_identidad'=>$id_tipo_documento_identidad,
      'fecha_nacimiento'=>$fecha_nacimiento,
      'pais'=>$pais,
      'direccion'=>$direccion,
      'telefono_habitacion'=>$telefono_habitacion,
      'estado'=>$estado,
      'etnia'=>$etnia,
      'empleo'=>$empleo,
      'telefono_institucion'=>$telefono_institucion,
      'correo_institucion'=>$correo_institucion,
      'direccion_institucion'=>$direccion_institucion,
      'inst_publica'=>$inst_publica,
      'inst_privada'=>$inst_privada,
      'organismo'=>$organismo,
      'inst_inv'=>$inst_inv,
      'tipo_institucion'=>$tipo_institucion,
      'trabajador_ubv'=>$trabajador_ubv,
      'id_cargo'=>$id_cargo,
      'state'=>$state,
      'discapacidad'=>$discapacidad,
      'grupo_sanguineo'=>$grupo_sanguineo,
      'enfermedad'=>$enfermedad,
      'descripcion_enfermedad'=>$descripcion_enfermedad,
      'tratamiento'=>$tratamiento,
      'estudiante_ubv'=>$estudiante_ubv,
      'mision_sucre'=>$mision_sucre,
      'idioma'=>$idioma,
      'descripcion'=>$descripcion,
      'id_tipo_discapacidad'=>$id_tipo_discapacidad,
      'id_nivel_academico'=>$id_nivel_academico,
      'id_civil'=>$id_civil,
      'id_profesion'=>$id_profesion,
      'institucion_nivel'=>$institucion_nivel,
      'id_persona'=>$id_persona,
      'id_nacimiento'=>$id_nacimiento,
      'id_direccion'=>$id_direccion,
      'id_dato_laboral'=>$id_dato_laboral,
      'id_datos_interes'=>$id_datos_interes,
      'id_aspirante'=>$id_aspirante
  ])){ 

    $mensaje='<div class="alert alert-success alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Se ha modificado exitosamente los datos registrados del Aspirante '.$identificacion.'.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->Solicitud();
    exit();

}else{

$mensaje= '<div class="alert alert-warning alert-dismissible show fade">
<div class="alert-body">
<button class="close" data-dismiss="alert">
  <span>&times;</span>
</button>
Hubo un error al editar los Datos Solicitados.
</div>
</div>';

  }

  $this->view->mensaje=$mensaje;
  $this->Solicitud();
}



//////// REPORTE/////////////


function Comp_Inscripcion($param){
  $id_inscripcion=$param[0];
  $this->view->id_inscripcion=$id_inscripcion;

  $estudio=$this->model->getConstanciadeEstudio($id_inscripcion);
  $this->view->estudio=$estudio;

$this->view->render('pre_inscripcion/comp_inscripcion');

}

function Cons_Estudio($param){

  $id_inscripcion=$param[0];
  $this->view->id_inscripcion=$id_inscripcion;

  $estudio=$this->model->getConstanciadeEstudio($id_inscripcion);
  $this->view->estudio=$estudio;

  $this->view->render('pre_inscripcion/cons_estudio');
  
  }

//////////////////////////////

/////// COMBO DEPENDIENTES /////////////////////

function getPaisEstado(){
  $id_pais=$_POST['pais'];
  $estados= $this->model->getPaisbyEstado($id_pais);
  foreach($estados as $row){
    $estado=new Confucio();
    $estado=$row;
    echo '<option value="'.$estado->id_estado.'">'.$estado->descripcion.'</option>';
  }
}

function getEstadoMunicipio(){
  $id_estado=$_POST['estado'];
  $municipios= $this->model->getEstadobyMunicipio($id_estado);
  foreach($municipios as $row){
    $municipio=new Confucio();
    $municipio=$row;
    echo '<option value="'.$municipio->id_municipio.'">'.$municipio->descripcion.'</option>';
  }
}

function getMunicipioParroquia(){
  $id_municipio=$_POST['municipio'];
  $parroquias= $this->model->getMunicipiobyParroquia($id_municipio);
  foreach($parroquias as $row){
    $parroquia=new Confucio();
    $parroquia=$row;
    echo '<option value="'.$parroquia->id_parroquia.'">'.$parroquia->descripcion.'</option>';
  }
}


}

?>