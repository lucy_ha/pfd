<?php 

include_once 'sesiones/session_admin.php';

class Periodo extends Controller
{

  function __construct()
  {
   parent::__construct();
   $this->view->mensaje = "";
 }

 function render(){ 
  $periodos = $this->view->datos = $this->model->get();
  $this->view->periodos = $periodos;
  $this->view->render('periodo/index');
}

////////////////////////////////////////////REGISTRAR///////////////////////////////////////////////
function registrar(){ 
  $this->view->render('periodo/registrar');
}
///
function RegisterPeriodo(){
  $descripcion = $_POST['descripcion'];
  $inicio_periodo = $_POST['inicio_periodo'];
  $fin_periodo = $_POST['fin_periodo'];
  $inicio_carga_notas = $_POST['inicio_carga_notas'];
  $fin_carga_notas = $_POST['fin_carga_notas'];
  $estatus = $_POST['estatus'];

  if($this->model->insert([
   'descripcion' => $descripcion, 
   'inicio_periodo' => $inicio_periodo,
   'fin_periodo' => $fin_periodo,
   'inicio_carga_notas' => $inicio_carga_notas,
   'fin_carga_notas' => $fin_carga_notas,
   'estatus' => $estatus
 ])){

    $mensaje='<div class="alert alert-success alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    El periodo académico fue registrado exitosamente.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();

  }else{
    $mensaje='<div class="alert alert-danger alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Hubo un error al registrar el periodo académico.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();
  }
}

////////////////////////////////////////EDITAR////////////////////////////////////////////
function editar($param=null){

  $id_periodo=$param[0];
  $this->view->id_periodo=$id_periodo;

  $periodo=$this->model->getbyID($id_periodo);
  $this->view->periodo=$periodo;

  $this->view->render('periodo/editar');
}
///
function UpdatePeriodo(){

  $id_periodo = $_POST['id_periodo'];
  $descripcion = $_POST['descripcion'];
  $inicio_periodo = $_POST['inicio_periodo'];
  $fin_periodo = $_POST['fin_periodo'];
  $inicio_carga_notas = $_POST['inicio_carga_notas'];
  $fin_carga_notas = $_POST['fin_carga_notas'];
  $estatus = $_POST['estatus'];

  if($this->model->update([
    'id_periodo'=>$id_periodo,
    'descripcion'=>$descripcion,
    'inicio_periodo' => $inicio_periodo, 
    'fin_periodo'=>$fin_periodo,
    'inicio_carga_notas' => $inicio_carga_notas,
    'fin_carga_notas' => $fin_carga_notas,
    'estatus' => $estatus
  ])){

    $mensaje='<div class="alert alert-success alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    El periodo académico fue actualizado exitosamente.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();

  }else{
    $mensaje='<div class="alert alert-danger alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Hubo un error al actualizar el periodo académico.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();
  }
}


} 
?>