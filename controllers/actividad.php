<?php 

include_once 'sesiones/session_admin.php';

class Actividad extends Controller
{

  function __construct()
  {
   parent::__construct();
   $this->view->mensaje = "";
 }

//Llamar a la vista principal de actividad
 function render(){ 
  $actividades = $this->view->datos = $this->model->get();
  $this->view->actividades = $actividades;
  $this->view->render('actividad/index');
}

////////////////////////////////////////////REGISTRAR///////////////////////////////////////////////
function registrar(){ 
  $this->view->render('actividad/registrar');
}
///
function RegisterActividad(){
  $descripcion = $_POST['descripcion'];
  $categoria = $_POST['categoria'];

  if($this->model->insert(['descripcion' => $descripcion, 'categoria' => $categoria])){

    $mensaje='<div class="alert alert-success alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    La actividad fue registrada exitosamente.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();

  }else{
    $mensaje='<div class="alert alert-danger alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Hubo un error al registrar la actividad.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();
  }
}

////////////////////////////////////////EDITAR////////////////////////////////////////////
function editar($param=null){

  $id_actividad=$param[0];
  $this->view->id_actividad=$id_actividad;

  $actividad=$this->model->getbyID($id_actividad);
  $this->view->actividad=$actividad;

  $this->view->render('actividad/editar');
}
///
function UpdateActividad(){

  $descripcion  = $_POST['descripcion'];
  $categoria = $_POST['categoria'];
  $id_actividad = $_POST['id_actividad'];
  
  if($this->model->update(['descripcion'=>$descripcion, 'categoria' => $categoria, 'id_actividad'=>$id_actividad])){
    $mensaje='<div class="alert alert-success alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    La actividad fue actualizada exitosamente.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();

  }else{
    $mensaje='<div class="alert alert-danger alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Hubo un error al actualizar la actividad.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();
  }
}

////////////////////////////////////////ELIMINAR////////////////////////////////////////////
function eliminar($param = null){
  $id_actividad = $param[0];

  if($this->model->delete($id_actividad)){
    $mensaje='<div class="alert alert-success alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    La actividad fue eliminada exitosamente.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();

  }else{
    $mensaje='<div class="alert alert-danger alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Hubo un error al eliminar la actividad.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();
  }
}



} 
?>