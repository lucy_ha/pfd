<?php

include_once 'sesiones/session_admin.php'; 

class Home extends Controller{

    function __construct()
    {
        parent::__construct();
    }


    function render($id_persona=null){

        $id_persona=$_SESSION['id_persona'];
        $this->view->id_persona=$id_persona;

        $caspirante=$this->model->getAspiranteCant();
        $this->view->caspirante=$caspirante;

        $cinscritos=$this->model->getCant('inscripcion');
        $this->view->cinscritos=$cinscritos;

        $coferta=$this->model->getCursos();
        $this->view->coferta=$coferta;

        $cseccion=$this->model->getSeccionesA($id_persona);
        $this->view->cseccion=$cseccion;

        $cestudiante=$this->model->getEstudianteAsig($id_persona);
        $this->view->cestudiante=$cestudiante;

        $cdocent=$this->model->getCantDocent();
        $this->view->cdocent=$cdocent;

    $this->view->render('home/index');       
    
    }

    
}
    ?>