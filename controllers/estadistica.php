<?php 

include_once 'sesiones/session_admin.php';

class Estadistica extends Controller
{

  function __construct()
  {
   parent::__construct();
   
 }

 function render(){ 

  $estadis=$this->model->lista_estadistica();
  $this->view->estadis=$estadis;
  $encabezado="Cuadro de Resultados";
  $this->view->encabezado=$encabezado;
  $this->view->render('estadistica/index');
}


function consulta_estadistica(){

  $estadistica=$_POST["estadistica"];

  $estadis=$this->model->lista_estadistica();
  $this->view->estadis=$estadis;

  $consul=$this->model->consultas($estadistica);
  $this->view->consul=$consul;
  
  $this->view->render('estadistica/index');

}

function reportes_estadistica($param=null){

  $estadistica=$param[0];
  $this->view->estadistica=$estadistica;

  $estadis=$this->model->lista_estadistica();
  $this->view->estadis=$estadis;

  $consul=$this->model->consultas($estadistica);
  $this->view->consul=$consul;
  
  $this->view->render('estadistica/datos_estadisticos');

}







} 
?>