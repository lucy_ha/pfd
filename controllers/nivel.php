<?php 

include_once 'sesiones/session_admin.php';

class Nivel extends Controller
{

  function __construct(){
   parent::__construct();
   $this->view->mensaje = "";
 }

 function render(){ 
  $niveles = $this->view->datos = $this->model->get();
  $this->view->niveles = $niveles;
  $this->view->render('nivel/index');
}

////////////////////////////////////////////REGISTRAR///////////////////////////////////////////////
function registrar(){ 
  $this->view->render('nivel/registrar');
}
///
function RegisterNivel(){
  $descripcion = $_POST['descripcion'];

  if($this->model->insert(['descripcion' => $descripcion])){

    $mensaje='<div class="alert alert-success alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    El nivel fue registrado exitosamente.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();

  }else{
    $mensaje='<div class="alert alert-danger alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Hubo un error al registrar el nivel.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();
  }
}

////////////////////////////////////////EDITAR////////////////////////////////////////////
function editar($param=null){

  $id_nivel=$param[0];
  $this->view->id_nivel=$id_nivel;

  $nivel=$this->model->getbyID($id_nivel);
  $this->view->nivel=$nivel;

  $this->view->render('nivel/editar');
}
///
function UpdateNivel(){

  $descripcion=$_POST['descripcion'];
  $id_nivel=$_POST['id_nivel'];
  
  if($this->model->update(['descripcion'=>$descripcion, 'id_nivel'=>$id_nivel])){

    $mensaje='<div class="alert alert-success alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    El nivel fue actualizado exitosamente.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();

  }else{
    $mensaje='<div class="alert alert-danger alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Hubo un error al editar el nivel.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();
  }
}

////////////////////////////////////////ELIMINAR////////////////////////////////////////////
function eliminar($param = null){
  $id_nivel = $param[0];

  if($this->model->delete($id_nivel)){
    $mensaje='<div class="alert alert-success alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    El nivel fue eliminado exitosamente.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();

  }else{
    $mensaje='<div class="alert alert-danger alert-dismissible show fade">
    <div class="alert-body">
    <button class="close" data-dismiss="alert">
    <span>&times;</span>
    </button>
    Hubo un error al eliminar el nivel.
    </div>
    </div>';
    $this->view->mensaje=$mensaje;
    $this->render();
    exit();
  }
}


} 
?>